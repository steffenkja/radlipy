# cython: profile=True
# cython: linetrace=True
 #  --#directive profile=true
 #  --#directive linetrace=true
# cython:true
# #c#ython: boundscheck=False

# -*- coding: utf-8 -*-
"""
Created on Tue Feb  3 10:31:48 2015

@author: steffenkj

This module contains various Cython modules, constructed to eliminate speed
bottlenecks in my Python programs. 

"""
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
from __future__ import division
STUFF = "Hi"
"Using deprecated NumPy API, disable it by " "#defining NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION"
import warnings
cdef extern from *:
    pass
cimport cython
#111v_test=71
import itertools
import math
from radmc3dPy.natconst import *
from cython.parallel import prange
#Get html report
import Cython.Compiler.Options
Cython.Compiler.Options.annotate = True
import time
import os
# Be aware that I suppress deprecated library warnings from numpy.

# import numpy as np
# the_path = numpy.get_include()

# keep "the_path" to configure  the two environment variables:

# CPATH=the_path
# CFLAGS=-Ithe_path

#cdef extern from "/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python/numpy/core/include/numpy/npy_no_deprecated_api.h": pass
#test it
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings('ignore')
cimport numpy as np
import numpy as np
from collections import defaultdict

np.import_array()
#include "numpy_common.pxi"
#np.import_umath
import math
DTYPE = np.float64
ctypedef np.float64_t DTYPE_t
#ctypedef np.int64_t DTYPE_int
from libc.math cimport sin as m_sin
from libc.math cimport cos as m_cos
from libc.math cimport sqrt as m_sqrt
from libc.math cimport asin as m_asin
from libc.math cimport acos as m_acos
from libc.math cimport atan as m_atan
from libc.math cimport atan2 as m_atan2
from libc.math cimport abs as absc
#from libc.math cimport M_PI as PI
from cgs_cnst import AU, PI
warnings.filterwarnings('always')
np.import_array()
warnings.filterwarnings("always", category=DeprecationWarning)
import scipy.spatial as sci_spat
from si_cnst import AU as AU_SI
from si_cnst import M_SUN as M_SUN_SI
#void import_umath()
#cdef extern from *:
#    pass

#cdef extern from *:
#    bint FALSE "0"
#    void import_array()
#    void 
#import_umath()

#if FALSE:
#    import_array()
#    import_umath()
#v_test=8    

@cython.binding(True)
cdef class DustDensity:
    """This class assigns the dust density for a wide variety of model options.
    
    ARGS:    
    -----    
        cav_enable: Bool. Insert a cavity into the model if True.

    ATTRIBUTES:
    -----------
        nx       - nr of radial grid points.
        ny       - nr of meridioanl grid points.
        nz       - nr of azimuthal grid points.
        
        n_stream - nr of streamlines to be discretely evaluated in the case of
                   a rotating collapse.
        n_stream_r - nr of radial points evaluated in a single streamline in a 
                   rotating collapse.
        ref_nr: The total number of added cells to the grid, due to octree
                refinement. I.e. the total cell number is now 
                nx*ny*nz + ref_nr.

        sh_wall_1 - the first radial shell evaluation point (for use in LIME 
            nearest neighbour search)
        sh_wall_2 - the second radial shell evaluation point.
        sh_wall_3 - the third radial shell evaluation point.
        sh_wall_4 - the fourth radial shell evaluation point.
        octree_len - length of the flattened coordinate array after the octree 
            has been inserted. This is the length og the ENTIRE array, not just the 
            refined part.
        n_dust     - number of dust species. [integer]
        evap_rad  - Evaporation radius of water away from the stellar 
                source(s) [cm]. Manually set, alternatively engage recurs_subli, to
                iteratively determine the cells where water is evaporated.
        cav_rad   - Radius of the central cavity [cm].

        inn_rad - Innermost radial grid edge [cm]
        out_rad - Outer radius of the model radial grid [cm].  
        source_nr - Number of stellar sources [integer]. 
        prho - Power of the radial density distribution (in the case of a
                static envelope [float].
        rho0 - Dust density at inn_rad [g/cm^3, float].
        r_c - Centrifugal radius of the infalling, rotating envelope [cm,float].
        m_dot - Accretion rate of envelope material onto the central 
            star(s) [g/s,float].
        m_star -  Mass of central star.
        bare: If 'True', then use bare grain opacities as well
            as ice-grain opacities. If 'False', then use only ice-grains, 
            everywhere in model.
        rot_collapse: If 'True', then use a rotating collapse density
            strtucture. If 'False', then use a static envelope.
        cav_enable: If 'True', use an inner cavity of constant density.
            If 'False', use a either a static envelope or rotating infall
            all the way into inn_rad.
    METHODS:
    -------- 
    """
        
    cdef:

        int nx
        int ny 
        int nz

        int n_stream      
        int n_stream_r      
        
        int ref_nr
        double sh_wall_1
        double sh_wall_2
        double sh_wall_3
        double sh_wall_4
        double M_SUN
        int octree_len
        int n_dust
        double evap_rad
        double cav_rad
        double inn_rad
        double out_rad
        double oct_rad_1,oct_rad_2
        double inn_disk_ref_1_rad,inn_disk_ref_2_rad
        int source_nr
        int ref_ctr
        
        double r_disk            
        double theta_disk
        double phi_disk
        
        double prho
        double rho0
        double r_c
        double m_dot
        double m_star
        double x_c
        double y_c
        double z_c
        double dist_to_star_1
        double dist_to_star_2
        
        double azi_lim_1
        double azi_lim_2
        
        bint disk_rot_coll
        bint recurs_subli
        bint bare
        bint rot_collapse
        bint cav_enable
        bint octree
        bint inside_a_disk

    #Initiate the class and define the required args.    
    def __init__(self, double prho, 
                 double rho0, double r_c, double m_dot, double m_star, 
                 double evap_rad, double cav_rad, double inn_rad, 
                 double out_rad, int nx, int ny, int nz,
                 int n_stream, int n_stream_r, int ref_nr,
                 int n_dust, int source_nr, 
                 double inn_disk_ref_1_rad, double inn_disk_ref_2_rad,
                 double oct_rad_1, double oct_rad_2, bint disk_rot_coll,
                 bint recurs_subli, bint bare, bint rot_collapse, 
                 bint cav_enable, double x_c, double y_c, double z_c,
                 **kwargs):
      
        self.nx = nx
        self.ny = ny 
        self.nz = nz
        self.x_c = x_c
        self.y_c = y_c
        self.z_c = z_c

        self.n_stream = n_stream      
        self.n_stream_r = n_stream_r      
        
        self.ref_nr = ref_nr
        self.sh_wall_1 = 300.*AU
        self.sh_wall_2 = 700.*AU
        self.sh_wall_3 = 3000.*AU
        self.sh_wall_4 = 9000.*AU
        self.M_SUN = 1.9891*10.0**33.0 #[g]
        self.ref_ctr = 0 # Goes from 0 to length of octree list (-1 due to zero indexing). 

        self.octree_len = ref_nr
        self.n_dust = n_dust
        self.evap_rad = evap_rad
        self.cav_rad = cav_rad
        self.inn_rad = inn_rad
        self.out_rad = out_rad
        self.source_nr = source_nr
        self.prho = prho
        self.rho0 = rho0
        self.r_c = r_c
        self.m_dot = m_dot
        self.m_star = m_star
        
        self.inn_disk_ref_1_rad = inn_disk_ref_1_rad
        self.inn_disk_ref_2_rad = inn_disk_ref_2_rad
        self.oct_rad_1 = oct_rad_1
        self.oct_rad_2 = oct_rad_2
        self.disk_rot_coll = disk_rot_coll
        self.recurs_subli = recurs_subli
        self.bare = bare
        self.rot_collapse = rot_collapse
        self.cav_enable = cav_enable
        self.inside_a_disk = False
        
    def dust_setup(self,np.ndarray[DTYPE_t, ndim=1] 
        g_ix,np.ndarray[DTYPE_t, ndim=1] g_iy,np.ndarray[DTYPE_t, ndim=1] g_iz,
        np.ndarray[DTYPE_t, ndim=1] gx, 
        np.ndarray[DTYPE_t, ndim=1] gy, np.ndarray[DTYPE_t, ndim=1] gz,  
        np.ndarray[DTYPE_t, ndim=2] p_stars, dict disk_dic, dict tor_dic, 
        set subli_list, octree_indices=[], oct_inst=None, **kwargs):

        """Function to create a dust density distribution in a spherical coordinate
        system.
        
        Handles a combination of different geometric components. A rotating or
        static envelope with or without an inner cavity of constant volume density,
        together with several or a single stellar source at different positions. 
        The opacity will either follow a evaporation temperature consistent scheme
        of ice- and bare-grain opacities or simply ice-grain opacities everywhere.
        Created in Cython due to the heavy use of brute-force approaches entailing 
        visits to millions, even billions of grid cells,
        depending on the setup.
        
        Args:           
            gx: Numpy array of radial grid points. 
            gy: Numpy array of meridional grid points. 
            gz: Numpy array of azimuthal grid points. 
            g_ix: Numpy array of radial wall points. 
            g_iy: Numpy array of meridional wall points. 
            g_iz: Numpy array of azimuthal wall points. 
            p_stars:  2D array of the star positions in cartesian coordinates 
                [star_nr[cm,cm,cm]]           
            disk_dic = dictionary of protoplanetary disk values.
            tor_dic = dictionary of torus (dust arm) values.
            subli_list = set containing the indices of cells above the dust
                sublimation temperature.
            octree_indices: List of binary numbers for the base grid. If 1, the 
                cell is refined into an octree. 0 is then no refinement.
            oct_inst: Class instance containing octree grid information as class 
                attributes.
        Returns:
            rho: Volume density [g/cm^3] in a four-dim array. First three are the 
                spherical coordinates (r,phi,theta) while the last is the dust 
                species nr.
            rho_oct: If the grid is octree refined, the dust density is returned
                as a flat list instead.
            theta_bounds: Numpy array of the meriodional streamline grid node
                positions [n_stream,n_stream_r].
            stream_r: One-dim Numpy array of radial streamline grid node positions,
                logarithmically spaced.   
            vel: Array of the velocity components.

        Raises:
            Nothing.
        Log:    
            30/3/2015: At first I simply decided the streamline bounds/nodes for 
            whatever r and theta value that was actually in the model grid,
            but this turned out to be unwise, as the meridional grid may 
            completely miss the greater radial distances of the chosen 
            streamlines. This would mean that you cannot choose the correct 
            streamline at greater distances, i.e. no density can be assigned.
            
            Instead, I will calculate a fine array of streamline (r,theta) 
            coordinates, or nodes if you will. For a given RADMC-3D grid node, 
            I will then loop through the streamline nodes positions and choose the
            closest. Eventually, some form of interpolation might be included. 
            This is very CPU costly, so optimize this scheme at some point.
        Important arrays & values:
            theta_bounds and stream_r are explained above.
            theta_0: The meridional starting positions of the streamlines. Runs 
                from [0,pi].
            theta_center_pos: The theta_0 of the streamline closest to the 
                current cell.            
            theta_cell: The theta_bounds of the streamline closest to the current
                cell.
            
        To-do:
            22/4-2015: 
                Implement plausible accretion-rates (currently too low).
        """
   
        cdef:
            int r_ctr_1, r_ctr_2, r_ctr_3, r_ctr_4 
            int base_ctr = 0
            
            double PI = 3.14159265359
            double PI_HALF = 3.14159265359/2.0
            double M_SUN = 1.9891*10.0**33.0  # [g]
            double R_SUN = 6.955*10.0**10.0  # Solar radius [cm]
            double G = 6.67259*10.0**(-8.0)  # [cm^3 g-1 s-2]
            double x_sph_c, y_sph_c, z_sph_c     
#            double start
#            double elapsed
 #           double dx,dy,dz
            double star_1_azi = 0.0
            double star_2_azi = 0.0
            
            list x_sph_c_arr, y_sph_c_arr, z_sph_c_arr
            list node_pos_dum, vel_list_dum
                     
            dict par
            
            set disk_1_idcs = set()
            set disk_2_idcs = set()
            set tor_idcs = set()
            
            Py_ssize_t ix, iy, iz
            Py_ssize_t x, y
            Py_ssize_t ir, i_theta_0

            # Using lists in cython is tricky, as memory errors can easily
            # happen. Instead, numpy arrays are used.
            np.ndarray[DTYPE_t, ndim=1] theta_0 = np.zeros([self.n_stream], 
                       dtype=np.float64)
            np.ndarray[DTYPE_t, ndim=1] stream_r = np.zeros([self.n_stream_r], 
                       dtype=np.float64)
            np.ndarray[DTYPE_t, ndim=2] theta_bounds = np.zeros([self.n_stream,
                       self.n_stream_r], dtype=np.float64)
            np.ndarray[DTYPE_t, ndim=1] theta_0_d_1 = np.zeros([self.n_stream], 
                      dtype=np.float64)
            np.ndarray[DTYPE_t, ndim=1] stream_r_d_1 = np.zeros([self.n_stream_r], 
                      dtype=np.float64)
            np.ndarray[DTYPE_t, ndim=2] theta_bounds_d_1 = np.zeros([self.n_stream,
                      self.n_stream_r], dtype=np.float64)
            np.ndarray[DTYPE_t, ndim=1] theta_0_d_2 = np.zeros([self.n_stream], 
                      dtype=np.float64)
            np.ndarray[DTYPE_t, ndim=1] stream_r_d_2 = np.zeros([self.n_stream_r], 
                      dtype=np.float64) 
            np.ndarray[DTYPE_t, ndim=2] theta_bounds_d_2 = np.zeros([self.n_stream,
                      self.n_stream_r], dtype=np.float64)   
            np.ndarray[DTYPE_t, ndim=4] rho = np.zeros([self.nx, self.ny, 
                      self.nz, self.n_dust], dtype=np.float64)
            
            # In the case of octree refinement, we need to write the dust density 
            # directly to a 1D array, as writing out 
            # octree coordinates into a 3D array is not possible.

            np.ndarray[DTYPE_t,ndim=2] rho_oct = np.zeros(
                                    [self.ref_nr, self.n_dust], dtype=np.float64) 
            np.ndarray[DTYPE_t,ndim=2] vel = np.zeros((self.ref_nr, 3), 
                    dtype=np.float64)  
  
        #Find the increase in cellnumber due to octree refinement. 
        self.octree = False
        if octree_indices != []:
            self.octree = True     

        print 'ref_nr (nr of octree cells in the grid) is %i and nx*ny*nz is %i' % (self.ref_nr, self.nx*self.ny*self.nz)      

        #Define functions to avoid interpreter overhead.
        sin = m_sin
        cos = m_cos
        sqrt = m_sqrt
        asin = m_asin
        acos = m_acos
        isnan = np.isnan

        par = {
               'rho':rho, 'rho_oct':rho_oct, 
               'p_stars':p_stars,'subli_list':subli_list,
               'disk_1_idcs':disk_1_idcs, 'disk_2_idcs':disk_2_idcs,
               'tor_idcs': tor_idcs
               }
       
        if disk_dic['disk']:
            print 'Initializing disks..'
        par.update(**disk_dic)
        
        if tor_dic['torus']:
            print 'Initializing torus..'
            star_1_azi = azi_from_cart(p_stars[0, 0], p_stars[0, 1])
            star_2_azi = azi_from_cart(p_stars[1, 0], p_stars[1, 1])
            self.azi_lim_1, self.azi_lim_2 = np.sort([star_1_azi, star_2_azi])
            par['tree'] = mk_dust_arc_tree(sm_min = tor_dic['sm_min'])
        if not tor_dic['torus']:
            par['tree'] = 'dummy' # Key need to be initialized for 
                                  # _assign_dens_and_vel()

        par.update(**tor_dic)

        # ==================== Handle a rotating cloud collapse ===============

        vel_dic = defaultdict(list)     
        if self.rot_collapse:
            print 'Creating a global rotating collapse..'
            
            # Get nodes for global collapse
            stream_r, theta_bounds, theta_0 = self._rot_col_nodes(
                r_in=self.inn_rad, r_out=self.out_rad, r_c = self.r_c)

        if self.disk_rot_coll:
            print 'Creating a rotating collapse around each star..'
            
            # Get nodes for collapse around star 1

            stream_r_d_1, theta_bounds_d_1, theta_0_d_1 = self._rot_col_nodes(
                r_in=disk_dic['disk_dic_1']['r_in'], 
                r_out=disk_dic['disk_dic_1']['r_out'], 
                r_c=disk_dic['disk_dic_1']['r_c'])
            
            disk_dic['disk_dic_1']['stream_r'] = stream_r_d_1
            disk_dic['disk_dic_1']['theta_bounds'] = theta_bounds_d_1
            disk_dic['disk_dic_1']['theta_0'] = theta_0_d_1
            
            # Get nodes for collapse around star 2
            stream_r_d_2, theta_bounds_d_2, theta_0_d_2 = self._rot_col_nodes( 
                r_in=disk_dic['disk_dic_2']['r_in'], 
                r_out=disk_dic['disk_dic_2']['r_out'], 
                r_c=disk_dic['disk_dic_2']['r_c'])
            disk_dic['disk_dic_2']['stream_r'] = stream_r_d_2
            disk_dic['disk_dic_2']['theta_bounds'] = theta_bounds_d_2
            disk_dic['disk_dic_2']['theta_0'] = theta_0_d_2         

        elif not self.disk_rot_coll:
            # Insert dummy values
            disk_dic['disk_dic_1']['stream_r'] = np.zeros(2)
            disk_dic['disk_dic_1']['theta_bounds'] = np.zeros((2,2))
            disk_dic['disk_dic_1']['theta_0'] = np.zeros(2)
            disk_dic['disk_dic_2']['stream_r'] = np.zeros(2)
            disk_dic['disk_dic_2']['theta_bounds'] = np.zeros((2,2))
            disk_dic['disk_dic_2']['theta_0'] = np.zeros(2)
        # Read the rotating collapse parameters into our par dictionary.
        # If there is no rotating collapse, then make dummy key that will 
        # not be used (but required as an argument)

        par.update({'theta_bounds':theta_bounds,'stream_r':stream_r,
                    'theta_0':theta_0})   
       
    #========================= ASSIGN DUST DENSITY ============================
        
        for iz in xrange(self.nz):
          for iy in xrange(self.ny):
            
    #---------Reset dics and counters for the next meridional cone search------
            
            # Radial shell counter in the vel vector arrays. Reset after each 
            # radial search in a meridional cone is finished.   
            r_ctr_1 = 0 
            r_ctr_2 = 0 
            r_ctr_3 = 0 
            r_ctr_4 = 0
            #The meridional cones of the model.        
            #The semicones of grid cell nodes.
            #Semicone due to radial shell splitting.        
            vel_lev2_0 = defaultdict(list)
            vel_lev2_1 = defaultdict(list)  
            vel_lev2_2 = defaultdict(list)  
            vel_lev2_3 = defaultdict(list)      
    
            vel_tempo_dic ={'vel_lev2_0':vel_lev2_0,'vel_lev2_1':vel_lev2_1,
                         'vel_lev2_2':vel_lev2_2,'vel_lev2_3':vel_lev2_3,
                         'r_ctr_1':r_ctr_1,'r_ctr_2':r_ctr_2,'r_ctr_3':r_ctr_3,
                         'r_ctr_4':r_ctr_4
                         }
                        
            for ix in xrange(self.nx):               
                if self.octree:
                    #If the grid cell is refined.
                    if octree_indices[base_ctr] == 1:
                        #Assign the oct-tree refinement node positions.
                        reg_unref, levels, disk_1_ref, disk_2_ref = find_octree_reg(x_sph_c=gx[ix],
                            y_sph_c=gy[iy], z_sph_c=gz[iz], oct_inst=oct_inst,#source_nr=self.source_nr,
                            p_stars=p_stars, tor_dic=tor_dic, tree=par['tree'])
 
                        x_sph_c_arr,y_sph_c_arr, z_sph_c_arr, node_pos_dum, vel_list_dum = octree_nodes(g_ix[ix],
                        g_ix[ix+1], g_iy[iy], g_iy[iy+1], g_iz[iz], g_iz[iz+1],
                        levels,[],[],[],[],[])

                        for z_sph_c, y_sph_c, x_sph_c in (
                            zip(z_sph_c_arr, y_sph_c_arr, x_sph_c_arr)):                                                        
                                        
                            par.update({'x_sph_c':x_sph_c,'y_sph_c':y_sph_c,
                            'z_sph_c':z_sph_c,'rho_oct':rho_oct,'ix':ix,
                            'iy':iy,'iz':iz,
                            'vel_tempo_dic':vel_tempo_dic, 'vel':vel, 
                            'disk_dic':disk_dic,'tor_dic':tor_dic})

                            rho_oct, vel, vel_tempo_dic, subli_list, disk_1_idcs, disk_2_idcs, tor_idcs = self._assign_dens_and_vel(par, **par)                                 
                            par.update({'subli_list':subli_list,
                            'disk_1_idcs':disk_1_idcs,
                            'disk_2_idcs':disk_2_idcs,
                            'tor_dic':tor_dic, 'tor_idcs': tor_idcs})
    
                            
                    #If the grid cell is not refined we use the base grid.                
                    elif octree_indices[base_ctr] != 1:
                        par.update({'x_sph_c':gx[ix], 'y_sph_c':gy[iy],
                                    'z_sph_c':gz[iz], 'rho_oct':rho_oct,
                                    'ix':ix,'iy':iy,'iz':iz,
                                    'vel_tempo_dic':vel_tempo_dic, 'vel':vel, 
                                    'disk_dic':disk_dic, 'tor_dic':tor_dic})

                        rho_oct, vel, vel_tempo_dic, subli_list, disk_1_idcs, disk_2_idcs, tor_idcs = self._assign_dens_and_vel(par, **par)     
                        par.update({'subli_list':subli_list,
                        'disk_1_idcs':disk_1_idcs, 'disk_2_idcs':disk_2_idcs,
                        'tor_dic':tor_dic, 'tor_idcs': tor_idcs})                                
                          
                if not self.octree:

                    par.update({'x_sph_c':gx[ix], 'y_sph_c':gy[iy],
                                'z_sph_c':gz[iz], 'rho_oct':rho_oct, 'ix':ix,
                                'iy':iy,'iz':iz, 'vel_tempo_dic':vel_tempo_dic,
                                'vel':vel, 'disk_dic':disk_dic,
                                'tor_dic':tor_dic})

                    rho, vel, vel_tempo_dic, subli_list, disk_1_idcs, disk_2_idcs, tor_idcs = self._assign_dens_and_vel(par, **par)
                    par.update({'subli_list':subli_list,
                    'disk_1_idcs': disk_1_idcs, 'disk_2_idcs': disk_2_idcs,
                    'tor_dic': tor_dic, 'tor_idcs': tor_idcs})                                
                base_ctr += 1
                
            #After loop is finished in radial coordinate, construct the meridional subarray.
            if iz == 0:    
                vel_dic[iy] = [vel_tempo_dic['vel_lev2_0'],
                               vel_tempo_dic['vel_lev2_1'],
                               vel_tempo_dic['vel_lev2_2'],
                               vel_tempo_dic['vel_lev2_3']]

        #Return the density distribution, plus information on the streamline nodes.
        if not self.octree:   
            return rho, theta_bounds, stream_r, vel, vel_dic, disk_1_idcs, disk_2_idcs, tor_idcs
        elif self.octree:
            return rho_oct, theta_bounds, stream_r, vel, vel_dic, disk_1_idcs, disk_2_idcs, tor_idcs
    
    def _assign_dens_and_vel(self, dict par,double x_sph_c, double y_sph_c, 
                             double z_sph_c, 
                             np.ndarray[DTYPE_t, ndim=2] rho_oct, 
                             Py_ssize_t ix, Py_ssize_t iy, 
                             Py_ssize_t iz,
                             dict vel_tempo_dic, dict disk_dic, 
                             dict tor_dic, 
                             np.ndarray[DTYPE_t, ndim=2] p_stars,
                             np.ndarray[DTYPE_t, ndim=2] vel, rho, 
                             bint disk, set subli_list, set disk_1_idcs,
                             set disk_2_idcs, tree, set tor_idcs,
                             **kwargs):
        """
        This function assigns the density and velocity values after deciding
        which model type we are in and where in the model our grid node is.
        
        """
        cdef:
            double veli_r = 0.0
            double veli_theta = 0.0 
            double veli_phi = 0.0
#            double vel_x, vel_y, vel_z
            list vel_vect, tilt_list, vels
            dict assign_dust_args
            
        self.x_c, self.y_c, self.z_c = transf_sph_cart(x_sph_c,y_sph_c,z_sph_c)
    
        if self.source_nr == 2:
            self.dist_to_star_1 = m_sqrt((p_stars[0,0] - self.x_c)**2. 
                + (p_stars[0,1] - self.y_c)**2. + (p_stars[0,2] -self.z_c)**2.)
            self.dist_to_star_2 = m_sqrt((p_stars[1,0] - self.x_c)**2. 
                + (p_stars[1,1] - self.y_c)**2. + (p_stars[1,2] -self.z_c)**2.)        
        else:
            self.dist_to_star_2 = 1e80 #Dummy large value so the real star distance is
            #always the minimum.
            self.dist_to_star_1 = x_sph_c
    
        par.update({'x_sph_c':x_sph_c}) 
        
        # Reset inside assessment, for the new round of coordinate analysis.
        tor_dic['ins_tor'] = False
        self.inside_a_disk = False
        
        if (tor_dic['torus'] and x_sph_c <= self.cav_rad):
            tor_dic['ins_tor'], tor_dic['r_tor'], tor_dic['dens'] = find_torus(
            r_sphr=x_sph_c, theta=y_sph_c, phi=z_sph_c, 
            tor_rad=tor_dic['tor_rad'], tor_incl=tor_dic['tor_incl'],
            max_dens=tor_dic['max_dens'], tree=tree, p_stars=p_stars,
            tor_p=tor_dic['tor_p'],
            fixed_dens_across_arc=tor_dic['fixed_dens_across_arc'])

        par.update({'tor_dic':tor_dic})

        if disk_dic['disk'] and self.dist_to_star_1 <= disk_dic['disk_dic_1']['r_out']:

            disk_dic['disk_dic_1']['r_disk'], disk_dic['disk_dic_1']['theta_disk'] = self._tilt_disk(p_stars[0, :],
                         x_sph_c,y_sph_c,
                         z_sph_c,disk_dic['disk_dic_1']['incl'],
                         disk_dic['disk_dic_1']['r_fill'],
                         disk_dic['disk_dic_1']['azith_turn'])
            self.inside_a_disk = True
            
            #Save disk index as we are now inside a disk.
            disk_1_idcs.add(self.ref_ctr)

        elif disk_dic['disk'] and self.dist_to_star_2 <= disk_dic['disk_dic_2']['r_out']:
             
            disk_dic['disk_dic_2']['r_disk'], disk_dic['disk_dic_2']['theta_disk'] = self._tilt_disk(p_stars[1, :],
                         x_sph_c, y_sph_c,
                         z_sph_c, disk_dic['disk_dic_2']['incl'],
                         disk_dic['disk_dic_2']['r_fill'],
                         disk_dic['disk_dic_2']['azith_turn'])
            self.inside_a_disk = True

            #Save disk index as we are now inside a disk.
            disk_2_idcs.add(self.ref_ctr)
                        
        par.update({'disk_dic':disk_dic})   
        
        if tor_dic['ins_tor'] and not self.inside_a_disk:

            #Save index as we are now inside the filament but inside a disk.
            tor_idcs.add(self.ref_ctr)

        if self.octree:                
            rho_oct, vel, subli_list = self._assign_dust_dens_to_arr(
                    rho=par['rho'],
                    rho_oct=par['rho_oct'],
                    ix=par['ix'], iy=par['iy'], iz=par['iz'],
                    p_stars=par['p_stars'], vel=par['vel'],
                    x_sph_c=par['x_sph_c'], disk_dic_1=par['disk_dic_1'],
                    disk_dic_2=par['disk_dic_2'], tor_dic = par['tor_dic'],
                    subli_list=par['subli_list'],
                    theta_bounds=par['theta_bounds'],
                    theta_0=par['theta_0'],
                    stream_r=par['stream_r'],
                    disk=par['disk'])

        elif not self.octree:
            rho, vel, subli_list = self._assign_dust_dens_to_arr(rho=par['rho'], 
            rho_oct=par['rho_oct'],
            ix=par['ix'], iy=par['iy'], iz=par['iz'],
            p_stars=par['p_stars'], vel=par['vel'],
            x_sph_c=par['x_sph_c'], disk_dic_1=par['disk_dic_1'],
            disk_dic_2=par['disk_dic_2'], tor_dic=par['tor_dic'],
            subli_list=par['subli_list'], 
            theta_bounds=par['theta_bounds'], theta_0=par['theta_0'],
            stream_r=par['stream_r'], disk=par['disk'])

        self.ref_ctr += 1

        if iz == 0:
    
            vel_vect = [x_sph_c, y_sph_c, veli_r, veli_theta, veli_phi]
           
            if 0.0 <= x_sph_c < self.sh_wall_1:
                vel_tempo_dic['vel_lev2_0'][vel_tempo_dic['r_ctr_1']] = vel_vect
                vel_tempo_dic['r_ctr_1'] += 1
            elif self.sh_wall_1 <= x_sph_c < self.sh_wall_2:
                vel_tempo_dic['vel_lev2_1'][vel_tempo_dic['r_ctr_2']] = vel_vect
                vel_tempo_dic['r_ctr_2'] += 1
            elif self.sh_wall_2 <= x_sph_c < self.sh_wall_3:
                vel_tempo_dic['vel_lev2_2'][vel_tempo_dic['r_ctr_3']] = vel_vect
                vel_tempo_dic['r_ctr_3'] += 1
            elif self.sh_wall_3 <= x_sph_c <= self.sh_wall_4:
                vel_tempo_dic['vel_lev2_3'][vel_tempo_dic['r_ctr_4']] = vel_vect
                vel_tempo_dic['r_ctr_4'] += 1

        if self.octree:                     
            return rho_oct, vel, vel_tempo_dic, subli_list, disk_1_idcs, disk_2_idcs, tor_idcs
        elif not self.octree:
            return rho, vel, vel_tempo_dic, subli_list, disk_1_idcs, disk_2_idcs, tor_idcs
    
    cpdef tuple _rot_col_nodes(self, double r_in, double r_out, double r_c):

        """ This function computes the reference nodes of the rotating collapse
            scenario to be used later in a nearest-neighbour search to 
            determine the grid dust density and velocity vectors."""

        cdef:
            double PI_HALF = 3.14159265359/2.0
            np.ndarray[DTYPE_t, ndim=1] theta_0 = np.zeros([self.n_stream], 
            dtype=np.float64)
            np.ndarray[DTYPE_t, ndim=1] stream_r = np.zeros([self.n_stream_r], 
            dtype=np.float64) #New addition
            np.ndarray[DTYPE_t, ndim=2] theta_bounds = np.zeros([self.n_stream,
            self.n_stream_r], dtype=np.float64)        
            np.ndarray[DTYPE_t, ndim=1] stream_rad = np.zeros(self.n_stream_r+1)
            Py_ssize_t ix, y

        theta_0 = np.arange(0.,1.,1./self.n_stream)
        theta_0[:] = theta_0[:]*PI_HALF
        theta_0[0] = 0.001 #Slight offset to avoid zero-division in 
        #trig. functions
        
        #The radial streamline grid wall positions are made.
        stream_rad = np.zeros(self.n_stream_r+1)

        stream_rad[:] = r_in*(r_out/r_in)**(np.arange(
            self.n_stream_r+1)/(self.n_stream_r))

        #The grid node positions is computed as the center of the wall 
        #positions. 
     
        stream_r[:] = np.sqrt(stream_rad[0:self.n_stream_r]*
            stream_rad[1:self.n_stream_r+1])

        #From the radial streamlines node positions, the meridional streamline 
        #nodes are made. Derived from eq. 4.40 in Hartmann, 'Accretion 
        #processes..'.

        #These are simple loops which fills up the arrays. Parallel computing
        #can be done.
        for y in xrange(len(theta_0)):         
            for ix in xrange(len(stream_r)):

                theta_bounds[y,ix] = m_acos(m_cos(theta_0[y])-(r_c/stream_r[ix])*
                (m_sin(theta_0[y])**2.0)*m_cos(theta_0[y]))
            
                #Streamlines should not cross into the other midplane, as the
                #material should suffer collisions and remain roughly at the 
                #midplane to form a disk. Slight offset is made to avoid zero-
                #division in trig. functions.
                if theta_bounds[y,ix] > PI_HALF:
                    theta_bounds[y,ix] = PI_HALF-0.000001
        
        return stream_r, theta_bounds, theta_0

    cdef tuple _tilt_disk(self, np.ndarray[DTYPE_t, ndim=1] p_stars, 
                  double r_sphr, double theta, double phi, double incl, 
                  double r_fill, double azith_turn):
            """This function performs 3D rotation in order to 
               create turned and tilted disks in the 3D model, i.e. 
               rotation around the z- and x- axis.
                      
               Be aware that the wikipedia entry on 3D rotation 
               matrices (https://en.wikipedia.org/wiki/Rotation_matrix#Basic_rotations)
               used clockwise rotation, while I use 
               counter-clockwise rotation as shown 
               in http://mathworld.wolfram.com/RotationMatrix.html
                      
               Therefore the signs alternate in the two versions.
               
            """
                      
            cdef:
                double AU  = 1.49597871*10.0**13.0 #[cm]
                double theta_transf
                double phi_native
                double x_c_zrot, y_c_xrot, z_c_xrot
                double y_c_zrot
                double x_c, y_c, z_c
#                double theta_upp, theta_low   

            #Get meridional coordinate, relative to the disk midplane axis
            #around star
    
            x_c, y_c, z_c = transf_sph_cart(r_sphr, theta, phi)
            
            x_c = x_c - p_stars[0]
            y_c = y_c - p_stars[1]        
            z_c = z_c - p_stars[2]
    
            #Transform coordinate system by rotating around axes.
            #First z-axis rotation (i.e. turn in azimuth).
            x_c_zrot = x_c*m_cos(azith_turn) + y_c*m_sin(azith_turn)
            y_c_zrot = -x_c*m_sin(azith_turn) + y_c*m_cos(azith_turn) 
    
            y_c_xrot = y_c_zrot*m_cos(incl) + z_c*m_sin(incl)
            z_c_xrot = -y_c_zrot*m_sin(incl) + z_c*m_cos(incl)
    
            r_disk = m_sqrt((x_c_zrot)**2.+
                (y_c_xrot)**2.+(z_c_xrot)**2.)
       
            theta_transf = m_acos(z_c_xrot/r_disk)
           
            phi_native = azi_from_cart(x_c_zrot, y_c_xrot)

            self.r_disk = r_disk            
            self.theta_disk = theta_transf
            self.phi_disk = phi_native            
    
            return r_disk, theta_transf
   
    cpdef tuple _disk_dens(self, dict disk_dic,
                           np.ndarray[DTYPE_t, ndim=2] theta_bounds,
                           np.ndarray[DTYPE_t, ndim=1] stream_r, 
                           np.ndarray[DTYPE_t, ndim=1] theta_0, 
                           bint test_class_disk = False):
        """
        This function returns a dust density for a disk structure. 
        The options are a standard T Tauri disk, a rotating collapse 
        (resulting in a pseudodisk) or a very puffed 'standard' disk model.
        
        Be aware that good resolution of the disk is needed otherwise artefacts
        appear (e.g. a large inner hole) due to poor meridional resolution. 
        Args:
            disk_dic: dictionary containg all the relevant parameters of the 
                      disk structure.
            theta_bounds: 
            stream_r:
            theta_0:
        Returns:
            disk_rho: Dust density [g cm^-3]
        
        """
        #Coordinate conversion.

        cdef:
            double z_disk = 0.0
            double H_d = 0.0
            double disk_rho = -99.0
            double midplane_r = 0.0
#            double AU = 1.49597871*10.0**13.0 #[cm]
            list vels = [0,0,0,0,0,0] # set to zero if disk rot col.         

        if not self.disk_rot_coll or test_class_disk:
            z_disk = disk_dic['r_disk']*m_cos(disk_dic['theta_disk'])
            midplane_r = disk_dic['r_disk']*m_sin(disk_dic['theta_disk'])
            disk_dic['surf_dens'] = (disk_dic['rho_0']*
                (midplane_r/disk_dic['r_0'])**disk_dic['p']) 
           # print 'midplane_r', midplane_r/AU 

            H_d = disk_dic['H_0']*(midplane_r/(disk_dic['r_0']))**(1.+disk_dic['flr'])  # Was divided by 10 AU before
            disk_rho = (disk_dic['surf_dens']/(H_d*m_sqrt(2.*PI)))*np.exp(
                -z_disk**2./(2.*H_d**2.)) 
          #  print 'disk_rho', disk_rho

        elif self.disk_rot_coll:
            disk_rho, vels = self._find_streamline(
                 theta_bounds, stream_r, theta_0, disk_dic['m_star'], 
                 disk_dic['m_dot'], disk_dic['r_c'], disk_dic['incl'],
                 disk_dic['azith_turn'])

        if disk_dic['r_in'] >= disk_dic['r_disk']:
            disk_rho = 1.0e-25

        assert disk_rho >= 0.0, 'disk_rho not assigned, disk domain not found.'

        if disk_rho < 1e-100:
            disk_rho = 1e-100 # Arbitrary low number, to avoid interference with the numpy zero search.
             
        assert(disk_rho > 1e-320, 'Very low dust density found, interferes with numpy zero search.')
        return disk_rho, vels
    
    cpdef tuple _assign_dust_dens_to_arr(self,
        np.ndarray[DTYPE_t, ndim=4] rho, 
        np.ndarray[DTYPE_t, ndim=2] rho_oct, 
        Py_ssize_t ix, Py_ssize_t iy, Py_ssize_t iz, 
        np.ndarray[DTYPE_t, ndim=2] p_stars,
        np.ndarray[DTYPE_t, ndim=2] vel,
        double x_sph_c, 
        dict disk_dic_1, dict disk_dic_2, dict tor_dic,
        set subli_list, np.ndarray[DTYPE_t, ndim=2] theta_bounds,
        theta_0=None, stream_r=None, bint disk = False):
            
        """This function finds the density at the current position and returns
        the modified dust density array.
        Args:
            ref_ctr: The octree cell index of the cell being investigated.      
            See getDustDensity_cyth for the other args.
        Returns:
            The dust density array with the added dust density value of the current
            cell. Also returns the 3D velocity vectors as three double values. If
            not a rotating collapse, this is three dummy doubles of value 0.
        Raises:
            None.
            """
        
        cdef:
            int condit_counter = 0
            double AU  = 1.49597871*10.0**13.0 #[cm] 
            double rho_cell = -99.0 # negative if rho_cell is not returned.
            double rho_border = -99.0
            double vel_r, vel_theta, vel_phi, vel_x, vel_y, vel_z
            list vels = [0., 0., 0., 0., 0., 0.]
            Py_ssize_t subl_idx, ref_ctr
        
        ref_ctr = self.ref_ctr
        vel_r, vel_theta, vel_phi, vel_x, vel_y, vel_z = (0.,0.,0.,0.,0.,0.)
        #First, find the dust density at the current grid position.
        if self.rot_collapse:
            rho_cell, vels = self._find_streamline(
                 theta_bounds, stream_r, theta_0, self.m_star, self.m_dot, 
                 self.r_c)      
            condit_counter += 1    
                
        elif self.cav_enable and x_sph_c <= self.cav_rad:
            rho_cell = self.rho0 * (self.cav_rad/AU)**self.prho  
            condit_counter += 1 
        elif not self.cav_enable or x_sph_c > self.cav_rad:
            rho_cell = self.rho0 * (x_sph_c/AU)**self.prho
            condit_counter += 1
        #Assign torus density first so it will be overwritten by the disk dens.
        if (tor_dic['torus'] and tor_dic['ins_tor']):
            rho_cell = tor_dic['dens']
        if disk: 
            rho_border = self.rho0 * (self.cav_rad/AU)**self.prho    

            if self.dist_to_star_1 <= disk_dic_1['r_out']:        
                rho_cell, vels = self._disk_dens(disk_dic_1,
                           disk_dic_1['theta_bounds'], disk_dic_1['stream_r'], 
                           disk_dic_1['theta_0'])
                
            elif self.dist_to_star_2 <= disk_dic_2['r_out']:
                rho_cell, vels = self._disk_dens(disk_dic_2, 
                           disk_dic_2['theta_bounds'], disk_dic_2['stream_r'],
                           disk_dic_2['theta_0'])
                
        assert condit_counter < 2, ('Two or more conditions were satisfied - there' 
        'can be only one!')
    
        # Assign the dust opacities dependent on the freeze-out radius and grid 
        # refinement scheme.
        
        # Unpack velocities
        if self.octree:
            vel_r, vel_theta, vel_phi, vel_x, vel_y, vel_z = vels[:]
            vel[ref_ctr] = (vel_x, vel_y, vel_z)

        if self.octree and self.bare and self.recurs_subli:
            if ref_ctr in subli_list:
                rho_oct[ref_ctr, 1] = rho_cell
                rho_oct[ref_ctr, 0] = 1e-150                         
                subli_list.remove(ref_ctr)
                
            else:
                rho_oct[ref_ctr, 0] = rho_cell
                rho_oct[ref_ctr, 1] = 1e-150            
                
            return rho_oct, vel, subli_list
    
        elif not self.octree and self.bare and not self.recurs_subli:
            if (self.source_nr == 2 and self.dist_to_star_1 > self.evap_rad and self.dist_to_star_2 > self.evap_rad or
            self.source_nr == 1 and self.dist_to_star_1 > self.evap_rad):                    
                rho[ix,iy,iz, 0] = rho_cell
                rho[ix,iy,iz, 1] = 1e-150
            else:                         
                rho[ix,iy,iz, 1] = rho_cell
                rho[ix,iy,iz, 0] = 1e-150
            return rho, vel, subli_list 
            
        elif not self.octree and not self.bare:                   
            rho[ix,iy,iz,0] = rho_cell
            return rho, vel, subli_list 
            
        elif self.octree and self.bare and not self.recurs_subli:
            if (self.source_nr == 2 and self.dist_to_star_1 > self.evap_rad and self.dist_to_star_2 > self.evap_rad or
            self.source_nr == 1 and self.dist_to_star_1 > self.evap_rad):                      
                rho_oct[ref_ctr, 0] = rho_cell
                rho_oct[ref_ctr, 1] = 1e-150
            else:                         
                rho_oct[ref_ctr, 1] = rho_cell
                rho_oct[ref_ctr, 0] = 1e-150
            return rho_oct, vel, subli_list        
            
        elif self.octree and not self.bare:                   
            rho_oct[ref_ctr] = rho_cell     
            return rho_oct, vel, subli_list
        
        assert rho_cell >= 0.0, 'Rho_cell was not returned. No dust scenario found.'
    
    cpdef tuple _find_streamline(self,
        np.ndarray[DTYPE_t, ndim=2] theta_bounds, 
        np.ndarray[DTYPE_t, ndim=1] stream_r, 
        np.ndarray[DTYPE_t, ndim=1] theta_0, double m_star, double m_dot, 
        double r_c, double incl = 0.0, double azith_turn = 0.0):
        """
        This functions finds the nearest streamline to the input cell, 
        in a rotating collapse model.
        Args:
             m_star = Mass of central star [g]
             m_dot = Mass accretion rate onto central star [g/s]
             theta_bounds
             stream_r
             theta_0
        Returns:
            Four doubles: The dust density [g] of the current cell, based on the
            nearest streamline along with the 3 velocity vector components of 
            the cell [cm/s].     
        """
       
        cdef: 
            double rho_cell = 0.0
            double PI = 3.14159265359
            double PI_HALF = 3.14159265359/2.0
            double AU  = 1.49597871*10.0**13.0 #[cm]
            double M_SUN = 1.9891*10.0**33.0 #[g]
            double R_SUN = 6.955*10.0**10.0 #Solar radius [cm]
            double G = 6.67259*10.0**(-8.0) #	[cm^3 g-1 s-2]
            double x_sph_c = -99.0
            double y_sph_c = -99.0
            double radmc3d_grid_theta = 10.0  # Assigned value to be used in assert
            double min_dist = 10.0**60.0 
#            double node_dist
            double r_cell = -99.0 # Neg if unassigned.
            double theta_center_pos = 200.0 # Random high number, so assertion will catch unassigned theta_center_pos
            double vel_r, vel_theta, vel_phi
            double vel_x, vel_y, vel_z
#            double x_cs, z_cs
            double thet_node_dist, r_node_dist
            double phi_s = -99.0
            double theta_s  #, factor
            Py_ssize_t ir, i_r_cell
            Py_ssize_t i_theta_0
            list vels
        
        assert m_dot > 0.0, ('Zero or negative accretion rate. Please check '
                             'your input accretion rate value.')
        
        if not self.inside_a_disk:
            x_sph_c = m_sqrt(self.x_c**2.+self.y_c**2.+self.z_c**2.)
            #Convert from cartesian to spherical.

            y_sph_c = m_acos(self.z_c/x_sph_c)
            if self.y_c>= 0:
                phi_s = math.atan2(self.y_c, self.x_c) 
            elif self.y_c < 0:
                phi_s = math.atan2(self.y_c, self.x_c)+2.*PI #Ensure range is [0,2 pi]. 
                 
        elif self.inside_a_disk:

            assert r_c > 0.0, 'r_c_disk was not assigned.'
            # In case of a rotating collapse pseudo-disk
            x_sph_c = self.r_disk
            y_sph_c = self.theta_disk
            phi_s = self.phi_disk
        
        assert x_sph_c >= 0.0 and y_sph_c >= 0.0, 'x_sph_c or y_sph_c not assigned, cannot determine if disk or non-disk domain.'
        
        # Mirror-symmetry around the midplane is used. 
        if y_sph_c <= PI_HALF:
            radmc3d_grid_theta = y_sph_c
        elif y_sph_c > PI_HALF:
            radmc3d_grid_theta = PI_HALF - (y_sph_c - PI_HALF)
            
        #TODO Question: Should the dust density follow the streamline, as if the radial 
        #distance was cav_rad or should it go with a constant density inside, in 
        #the case of a rotating collapse?
          
        if x_sph_c <= self.cav_rad and self.cav_enable and not self.inside_a_disk:
            x_sph_c = self.cav_rad
            radmc3d_grid_theta = PI/3.*1.07#Set to low (not the disk, but a 
            #streamline from above) constant density inside the cavity.  
            min_dist = 10.0**60.0

        assert radmc3d_grid_theta >= 0 and radmc3d_grid_theta <= PI/2.0, (
        'Error in coordinate transformation of meridional coordinate, ' 
        'theta is now %f while input was %f' % (radmc3d_grid_theta, y_sph_c))                     
        
        # Now the coordinates are set, and we can search for the nearest streamline
    
        for ir in xrange(len(stream_r)):
            r_node_dist = abs(x_sph_c-stream_r[ir])
            if r_node_dist < min_dist:
                min_dist = r_node_dist
                r_cell = stream_r[ir]
                i_r_cell = ir
        
        min_dist = 10.0**60.0
    
        for i_theta_0 in xrange(len(theta_0)):
            thet_node_dist = abs(radmc3d_grid_theta - theta_bounds[i_theta_0, i_r_cell]) 
            
            #If the new streamline node is closer than the 
            #previously chosen node, we assign it instead.
            
            if thet_node_dist < min_dist:
                min_dist = thet_node_dist
                theta_center_pos = theta_0[i_theta_0] # Starting angle of the streamline
      
                # The current meridional position of the streamline with the given radius.
                theta_cell = theta_bounds[i_theta_0, i_r_cell] 
        
        assert theta_center_pos <= PI_HALF, ("Streamline evaluated "
                                             "at wrong side of the midplane.")
        assert theta_cell <= PI_HALF, ("Streamline evaluated "
                                       "at wrong side of the midplane.")
        
        # Derived in hand by rewriting the equation from Hartmanns book on 
        # accretion processes.
        # rho_cell = self.m_dot/(4.0*PI*m_sqrt(
#         G*self.m_star*(r_cell)**3.0))*(
#         2.0-r_c/r_cell*m_sin(theta_center_pos)**2.0)**(
#         -0.5)*(
#         1-r_c/r_cell*m_sin(theta_center_pos)**2.0+
#         2.0*m_cos(theta_center_pos)**2.0/
#         ((r_cell)/r_c))**(-1.0)
        assert r_cell >= 0.0, 'r_cell not assigned, cannot find rho_cell'

        rho_cell = m_dot/(4.0*PI*m_sqrt(
                G*m_star*(r_cell)**3.0))*(
                1.0+m_cos(theta_cell)/m_cos(theta_center_pos))**(-0.5)*(
                m_cos(theta_cell)/m_cos(theta_center_pos)+
                2.*(m_cos(theta_center_pos))**2./(r_cell/r_c))**(-1.)

        if rho_cell < 0.0:
            print 'theta_center_pos is', theta_center_pos
            print 'theta_cell is', theta_cell
            print 'negative rho_cell ', rho_cell

            print 'r_cell', r_cell
            print 'r_c', r_c
            print 'm_star', m_star
            print 'm_dot', m_dot
            
        assert rho_cell >= 0., 'cell rho has a negative value'
              
        #Find velocity vector of a particle in the rotating collapse.
        #See eq. 4.31, 4.33 and 4.35 (page 70) of "Accretion Processes" book.
        
        #Division by 100 for conv to SI units (cm to m). Remember that both 
        #vel_theta and vel_phi are normalized to r. See "Accretion Processes"       
        vel_r = (-m_sqrt(G*m_star/r_cell)*m_sqrt(1+m_cos(theta_cell)/
            m_cos(theta_center_pos)))/100.
    
        vel_theta = (m_sqrt(G*m_star/r_cell)*(m_cos(theta_center_pos)-
            m_cos(theta_cell))*m_sqrt((m_cos(theta_center_pos)+m_cos(theta_cell))/
            (m_cos(theta_center_pos)*(m_sin(theta_cell))**2.)))/100.
            
        vel_phi = (m_sqrt(G*m_star/r_cell)*m_sqrt(1- m_cos(theta_cell)/
            m_cos(theta_center_pos))*m_sin(theta_center_pos)/m_sin(theta_cell))/100.
          
        #Convert spherical velocity vectors to cartesian.
        #From http://www.astrosurf.com/jephem/library/li110spherCart_en.htm
        #and "Accretion processes page 70"
        #Bear in mind that vel_phi and vel_theta include some of the terms 
        #from astrosurf. eq.  

        #The used eq. used another meridional coordinate system.
        theta_s = radmc3d_grid_theta #Only evaluate upper midplane
        
        #Since the sph-cart conversion eq. has a different meridional range and direction,
        #I need to convert the meriodional coordinates and vel vectors before I calculate the cart vels.

        assert phi_s >= 0.0 and phi_s <= 2*PI, 'phi_s has wrong range.'
        assert theta_s >= 0.0 and theta_s <= PI/2., 'theta_s has wrong range.'
 
        if rho_cell < 10.**(-40):
            print 'unexpectedly low density result'
            print 'rho_cell is ', rho_cell
            print 'theta_center_pos is', theta_center_pos
            print 'theta_cell is', theta_cell
        assert rho_cell > 10.**(-40), 'Shit'
        #Based on my own derivations with standard spherical coordinate system.
        vel_x = (vel_r*m_sin(theta_s)*m_cos(phi_s) + m_cos(theta_s)*m_cos(phi_s)*vel_theta
            - m_sin(phi_s)*vel_phi)
        vel_y = (vel_r*m_sin(theta_s)*m_sin(phi_s) + m_cos(theta_s)*m_sin(phi_s)*vel_theta +
            m_cos(phi_s)*vel_phi)
        vel_z = vel_r*m_cos(theta_s) - m_sin(theta_s)*vel_theta
        
        #TODO: Why is this correct? Are the coordinate systems not different?
        #Based on the coordinate system of      
        #http://www.astrosurf.com/jephem/library/li110spherCart_en.htm
        if self.inside_a_disk:
            if y_sph_c > PI_HALF:
                vel_z = -vel_z                
        elif not self.inside_a_disk:
            if self.z_c < 0.0:
                vel_z = -vel_z
        self.inside_a_disk = False #  Assume next point is outside disk, to 
        # allow the inside disk search to function properly
        #TODO set somewhere else, this is vulnerable to error.
        #vels = [vel_r, vel_theta, vel_phi, vel_x, vel_y, vel_z] 
        
        # Convert velocity vectors to global cartesian vectors.
        vel_x_global, vel_y_global, vel_z_global = rot_vel_for_lime(
                         vel_x_disk=vel_x, vel_y_disk=vel_y, 
                         vel_z_disk=vel_z, incl=incl, 
                         azith_turn=azith_turn)        
        
        vels = [vel_r, vel_theta, vel_phi, vel_x_global, vel_y_global,
                vel_z_global] 

        return rho_cell, vels

cdef class DustAnalysis:
    """This class analyzes the dust density of the input model.

    ATTRIBUTES:
    -----------
        n_dust       - number of dust species
        surf_dens    - the surface density at the chosen column.

    METHODS:
    -------- 
        get_sigma_dust
    
    """
    cdef:
        int n_dust
        double surf_dens_1
        double surf_dens_2
        
    def __init__(self,int n_dust):
        self.n_dust = n_dust
        
    cpdef get_sigma_dust(self, np.ndarray[DTYPE_t, ndim=2] rho_dust, 
                         np.ndarray[DTYPE_t, ndim=2] node_pos, 
                         np.ndarray[DTYPE_t, ndim=1] vol, 
                         np.ndarray[DTYPE_t, ndim=2] p_stars,
                         octree, octree_indices, invest_disks=True,
                         tuple target_pos = (0.0, 0.0), rec_scale=150*AU):
        """
        Function to calculate dust surface density.
        Args:
            rho_dust: 2D numpy array. 0 index is flattened list of (x,y,z) 
                dust densities, while 1 index is the dust species index.
            node_pos: 2D numpy array of octree node positions. Tuples.
            octree: Boolean. Informs whether or not octree refinement has been
                performed. Currently must be be set to True.
            octree_indices: flat list of 0's and 1's, denoting all cells. If 1, 
            then the base grid cell in question has been refined.
            p_stars: Positions of the stars in cartesian coords.
            rec_scale: The absolute vertical distance to midplane, within which
                the dust will be integrated - reflects the fact that some 
                interferometers do not include largescale structures,
                i.e. some emission will be resolved out.
        Returns:
            surf_dens: The surface density [g/cm^2] of the square beam.
        """
        cdef:    
            np.ndarray[DTYPE_t, ndim=1] x_bounds_1,x_bounds_2
            np.ndarray[DTYPE_t, ndim=1] y_bounds_1,y_bounds_2
            Py_ssize_t j
            int ctr = 0
            int nd_caught_1 = 0
            int nd_caught_2 = 0
            
            double m_col_1 = 0.0
            double m_col_2 = 0.0
            
            double surf_1 = -1.
            double surf_2 = -1.
            double x_c,y_c,z_c
            list x_list = []
            list y_list = []
            list r_list = []
            list theta_list = []
            list azi_list = []
            bint square = False
            bint circle = True
            double beam_r = 30.*AU
            double dist_to_pt = 0.

        assert octree, 'Octree not present. get_sigma_dust requires octree.'
        #Step 1. Define a box-column boundary.
        x_bounds_1 = np.array([p_stars[0,0]-30.*AU,p_stars[0,0]+30.*AU])
        y_bounds_1 = np.array([p_stars[0,1]-30.*AU,p_stars[0,1]+30.*AU])

        x_bounds_2 = np.array([p_stars[1,0]-30.*AU,p_stars[1,0]+30.*AU])
        y_bounds_2 = np.array([p_stars[1,1]-30.*AU,p_stars[1,1]+30.*AU])

        assert x_bounds_1[1]>x_bounds_1[0], 'x_bounds order must go from low to high'
        assert y_bounds_1[1]>y_bounds_1[0], 'y_bounds order must go from low to high'

        assert x_bounds_2[1]>x_bounds_2[0], 'x_bounds order must go from low to high'
        assert y_bounds_2[1]>y_bounds_2[0], 'y_bounds order must go from low to high'

        #Step 2. Loop through all cells and determine whether or not they are inside
        #   or outside the box-column.
            
        for pos in node_pos:
            r_list.append(pos[0])
            theta_list.append(pos[1])
            azi_list.append(pos[2])

            x_c, y_c, z_c = transf_sph_cart(pos[0], pos[1], pos[2])

            if invest_disks:
                st_1_dist = np.sqrt((x_c - p_stars[0,0])**2. + 
                                    (y_c - p_stars[0,1])**2.)
                st_2_dist = np.sqrt((x_c - p_stars[1,0])**2. +
                                    (y_c - p_stars[1,1])**2.)
            else:
                dist_to_pt = np.sqrt((x_c - target_pos[0])**2. +
                                      (y_c - target_pos[1])**2.)

            x_list.append(x_c)
            y_list.append(y_c)
            
            if square:
                if (x_bounds_1[0] <= x_c <= x_bounds_1[1] and 
                y_bounds_1[0] <= y_c <= y_bounds_1[1]):
                    assert y_c < y_bounds_1[1], 'y_c=%.3e out of bounds' %(y_c/AU)
                        
                    nd_caught_1 += 1
                    for j in xrange(self.n_dust):
                        m_col_1 += rho_dust[ctr,j]*vol[ctr]
                        
                #Look at the next disk
                elif (x_bounds_2[0] <= x_c <= x_bounds_2[1] and 
                y_bounds_2[0] <= y_c <= y_bounds_2[1]):
                    assert y_c < y_bounds_2[1], 'y_c=%.3e out of bounds' %(y_c/AU)
                        
                    nd_caught_2 += 1
                    for j in xrange(self.n_dust):
                        m_col_2 += rho_dust[ctr,j]*vol[ctr] 
            elif circle:
                if invest_disks:
                    if st_1_dist<beam_r and abs(z_c) <= rec_scale:                        
                        nd_caught_1 += 1
                        for j in xrange(self.n_dust):
                            m_col_1 += rho_dust[ctr,j]*vol[ctr]
                            
                    #Look at the next disk
                    elif st_2_dist<=beam_r and abs(z_c) <= rec_scale:
                            
                        nd_caught_2 += 1
                        for j in xrange(self.n_dust):
                            m_col_2 += rho_dust[ctr,j]*vol[ctr]                 
                else:
                    if dist_to_pt < beam_r and abs(z_c) <= rec_scale:                        
                        nd_caught_1 += 1
                        for j in xrange(self.n_dust):
                            m_col_1 += rho_dust[ctr,j]*vol[ctr]
            ctr += 1
        if invest_disks:
            if square:
                surf_1 = (x_bounds_1[1]-x_bounds_1[0])*(y_bounds_1[1]-y_bounds_1[0])
                surf_2 = (x_bounds_2[1]-x_bounds_2[0])*(y_bounds_2[1]-y_bounds_2[0])
            elif circle:
                surf_1 = PI*beam_r**2.
                surf_2 = PI*beam_r**2.
            assert surf_1 > 0. and surf_2 > 0, 'surf vals not initialized'
        else:
            surf_1 = PI*beam_r**2.
        #Step 3 ??
        #Step 4. Profit.
        if invest_disks:
            self.surf_dens_1 = m_col_1/surf_1 #[g/cm^2]
            self.surf_dens_2 = m_col_2/surf_2 #[g/cm^2]
            
            assert self.surf_dens_1 >= 0.0
            assert self.surf_dens_2 >= 0.0

            return self.surf_dens_1, self.surf_dens_2
        else:
            assert self.surf_dens_1 >= 0.0
            self.surf_dens_1 = m_col_1/surf_1 #[g/cm^2]
            return self.surf_dens_1

cpdef find_torus(double r_sphr, double theta, double phi, double tor_rad,
        double tor_incl, double max_dens, tree,
        np.ndarray[DTYPE_t, ndim=2] p_stars,
        double azi_lim_1 = PI/4., double azi_lim_2=5.*PI/4.,
        double disk_1_rad=150.*AU, double disk_2_rad=50.*AU,
        double tor_p=-0.25, bint for_testing=False,
        bint fixed_dens_across_arc=True):

        """
        This function checks if the input coordinates are within a ring torus,
        defined as starting and ending between the two stars. Assumes that the
        stars lie in the x-y plane. 
        
        Note that azi_lim_1 to azi_lim_2 is the aximuthal range wherein the 
        dust arc is defined. Usual counterclockwise direction (observed from 
        above x-y plane) is used, so azi_lim_1 is the azimuthal position from
        source B and azi_lim_2 is the azimuthal position of source A, i.e.,
        alphabetically counterintuitive. 
        
        Args:
            p_stars: Star positions in cartesian coords.
            r_sphr: R coord, spherical coords.
            theta: Meridional coord, spherical coords.
            phi: Azimuthal coord, spherical coords.
            tor_rad: radius of the torus ring itself, i.e. its fatness. 
            tor_incl: The torus incl. The coord system
                will be transformed into this with a x-axis rotation.
        Returns:
            Tuple of the boolean (inside or not) as well as the radius position
            to be used in _tor_dens().
        """
        cdef:
            double dist = -1.
            double tor_dens = -1.
            double x_c, y_c, z_c
            double phi_native
            double x_c_zrot, y_c_zrot
            double y_c_xrot, z_c_xrot
            bint inside_torus = False

            double bubble_rad_1 = tor_rad  # radius of the dust bubble around source A
            double bubble_rad_2 = tor_rad  # radius of the dust bubble around source B
    
            double x_eval_start = -700.*AU # x_A - max(bubble_rad_1, tor_rad)
            double x_eval_end = 700.*AU # x_B + max(bubble_rad_2, tor_rad)
            double y_min = -450.*AU # y_A - max(bubble_rad_1, bubble_rad_2)
            double y_max = 450.*AU # tor_rad + sm_maj
            double z_min, z_max

#            np.ndarray[DTYPE_t, ndim=2] p_stars = np.ndarray((2, 3), dtype=np.float64)

        # Grid point distance to the stars.
        
   #     p_stars[0, :] = [x for x in [-212.132 * AU, -212.132 * AU, 0.]]
   #     p_stars[1, :] = [x for x in [212.132 * AU, 212.132 * AU, 0.]]
        if for_testing:
            tor_dens = 0.0
        x_c, y_c, z_c = transf_sph_cart(r_sphr,theta,phi)
        dist_star_1 = np.sqrt((p_stars[0,0] - x_c)**2. + 
                              (p_stars[0,1] - y_c)**2. + 
                              (p_stars[0,2] - z_c)**2.)
        dist_star_2 = np.sqrt((p_stars[1,0] - x_c)**2. + 
                              (p_stars[1,1] - y_c)**2. + 
                              (p_stars[1,2] - z_c)**2.)
        
        # -------- Convert to dust arm reference coordinate system ------------
        
        # Perform an z-axis rotation, to set source A and B at PI and 0 
        # radians, respectively.
        # Convert coordinates to the star-torus frame (z-axis rotation)
        # The x-axis now starts at the source B position
        x_c_zrot = x_c*m_cos(azi_lim_1) + y_c*m_sin(azi_lim_1)
        y_c_zrot = -x_c*m_sin(azi_lim_1) + y_c*m_cos(azi_lim_1)
        
        # Now, tilt the torus-starframe (i.e. tilt x-y plane) with x-axis 
        # rotation. Nullify this step by setting tor_incl = 0.0
        y_c_xrot = y_c_zrot*m_cos(tor_incl) + z_c*m_sin(tor_incl)
        z_c_xrot = -y_c_zrot*m_sin(tor_incl) + z_c*m_cos(tor_incl)
        
        # Get grid node azimuth position in the new reference frame.
        phi_native = azi_from_cart(x_c_zrot,y_c_xrot)
        # Create vectors serving as 'bones' around which we add material.
        # These positions are calculated in the local reference plane where
        # source A is at (-300, 0) and source B at (300,0).

        # Azimuthal positions of the bending points in the xy-plane.

        z_min = - max(bubble_rad_1, bubble_rad_2, tor_rad)
        z_max = max(bubble_rad_1, bubble_rad_2, tor_rad)

        if (x_eval_start <= x_c_zrot <= x_eval_end and y_min <= y_c_xrot 
            <= y_max and z_min <= z_c_xrot <= z_max):
            if dist_star_1 > disk_1_rad and dist_star_2 > disk_2_rad:
                
                dist, loc = tree.query((x_c_zrot, y_c_xrot, z_c_xrot), k=1, 
                                       eps=0.05, p=2, n_jobs=1)
                    
                if 0.0 <= phi_native <= np.pi:
                    inside_torus = dist <= tor_rad
                if dist_star_1 <= bubble_rad_1 or dist_star_2 <= bubble_rad_2:
                    inside_torus = True
                if inside_torus:
                    assert dist >= 0.0, 'negative radius'
                    if fixed_dens_across_arc:
                        tor_dens = max_dens*(dist/AU)**(tor_p)
                    else:
                        azi_corr = phi_native
                        max_dens = max_dens/10.
                        if azi_corr < 0.2*np.pi or azi_corr > 1.5*np.pi:
                            azi_corr = 0.2*np.pi
                        tor_dens = max_dens*(azi_corr/0.5*np.pi)*(dist/AU)**(tor_p)
        if for_testing:
            return tor_dens
        else:
            return inside_torus, dist, tor_dens

@cython.profile(True)
cpdef mk_dust_arc_tree(double sm_maj = 300.0*AU, double sm_min = 150.*AU):
    """
    Define dust arc and construct kd tree
    """
    cdef:      
        double bubble_rad_1 = 130.0*AU  # radius of the dust bubble around source A
        double bubble_rad_2 = 130.0*AU  # radius of the dust bubble around source B

        int n_reg_1 = 600
        int n_reg_2 = 600
        double phi_B_A_end, phi_B_extra_ang, phi_B_2PI
        np.ndarray[DTYPE_t, ndim=1] reg_ellip_1 
        np.ndarray[DTYPE_t, ndim=1] reg_ellip_2     
        np.ndarray[DTYPE_t, ndim=2] reg_1 
        np.ndarray[DTYPE_t, ndim=2] reg_2
        np.ndarray[DTYPE_t, ndim=2] tree_nodes

    # region B to A
    phi_B_A_end = np.pi + np.tan(bubble_rad_1/sm_maj)
    phi_B_extra_ang = np.pi/4. #np.tan(bubble_rad_2/sm_maj) 
    phi_B_2PI = 2.*np.pi - phi_B_extra_ang
    reg_ellip_1 = np.arange(0.0, phi_B_A_end, 
                      phi_B_A_end/n_reg_1)
    
    reg_ellip_2 = np.arange(phi_B_2PI, 2.*np.pi, 
                      phi_B_extra_ang/n_reg_2)

    reg_1 = np.ndarray((len(reg_ellip_1), 3), dtype=np.float64)
    reg_2 = np.ndarray((len(reg_ellip_2), 3), dtype=np.float64)
    tree_nodes = np.ndarray((len(reg_ellip_1)+len(reg_ellip_2), 3), dtype=np.float64)

    reg_1[:] = [(sm_maj*np.cos(x),sm_min*np.sin(x), 0.0) for x in reg_ellip_1]
    reg_2[:] = [(sm_maj*np.cos(x),sm_min*np.sin(x), 0.0) for x in reg_ellip_2]

    tree_nodes = np.concatenate([reg_1, reg_2], axis=0)

    # Use KD tree to find distance to nearest neighbor.
    print 'Nodes ready, now making kd-tree..'

    tree = sci_spat.cKDTree(tree_nodes, copy_data=False, 
                            balanced_tree=False)
    print 'kd-tree made, returning..'
    return tree

cpdef double mk_line(double x, double x_0, double x_1, double y_0, double y_1):
    """ 
    Make linear function and return function value
    
    Returns:
        y = function value
    """
    x_line = x - x_0  # x position in the local line reference system
    y = x_line*(y_1-y_0)/(x_1-x_0) + y_0
    
    return y

@cython.profile(True)
cpdef construct_octree(list octree_key, int levels):
    """This recursive function returns the flat list of octree keys, for 
    a single cell refined n times.
    Args:
        octree_key = numpy 1d array of octree indices. If refined does not exist at the
        time of use of construct_octree(), pass an empty list instead.
        levels = int, the level of refinement desired for the cell.
    Returns:
        octree_key = the modified list octree keys, now including those of the 
        refined cell.
    """
    cdef:
        int divide
        Py_ssize_t subcell

    # Insert criterion for whether cell should be sub-divided.
    if levels == 0:
        divide = 0
    elif levels > 0:
        divide = 1
    else:
        divide = -99
    assert divide >= 0, 'Octree position must be at base level or in upper level.'
    
    octree_key.append(divide)
           
    for subcell in xrange(8):            
        # If the cell is sub-divided, recursively divide it further
        if levels>0:
            octree_key = construct_octree(octree_key,levels-1)
    
    return octree_key  

cpdef tuple octree_nodes(double xw_0, double xw_1, double yw_0, double yw_1, 
                 double zw_0, double zw_1, int levels, list x_arr=[], 
                 list y_arr=[], list z_arr=[], list node_pos=[], 
                 list vol_list=[]):
    """
    This function returns three arrays of a single refined cell new node 
    positions, list of lists of node positions and a single list of the subcell
    volumes.
    Args:
        xw_0 = cell wall start (x-dim)
        xw_1 = cell wall end (x-dim)
        yw_0 = cell wall start (y-dim)
        yw_1 = cell wall end (y-dim)
        zw_0 = cell wall start (z-dim)
        zw_1 = cell wall end (z-dim)
        levels = refinement level of the cell.
    Returns:
        x_arr = octree ordered xnode positions.
        y_arr = octree ordered ynode positions.
        z_arr = octree ordered znode positions.
        These three arrays can be used in a simple for loop with z,y,x order,
        and the nodes will be picked in correct radmc3d octree order.
    """
    cdef:
        double dx, dy, dz
        int i,j,k,#zi,yi,xi
        list xw_arr,yw_arr,zw_arr
        list x_n_arr, y_n_arr, z_n_arr
        tuple xw,yw,zw
        
    #The new grid centers are set, depending on the refinement 
    #level. 
    dx = (xw_1-xw_0)/2. #SHOULD THIS NOT BE 4, even refined once? 
    dy = (yw_1-yw_0)/2.
    dz = (zw_1-zw_0)/2.

    #Get node positions
    if levels == 0: 
        x_arr.append(xw_0+dx) 
        y_arr.append(yw_0+dy) 
        z_arr.append(zw_0+dz)
        node_pos.append([xw_0+dx,yw_0+dy,zw_0+dz])

        diff_r3 = xw_1**3. - xw_0**3.
        diff_cost = m_cos(yw_0) - m_cos(yw_1)
        diff_phi = zw_1 - zw_0
        vol_list.append(1./3.*diff_r3*diff_cost*diff_phi)

    if levels > 0:
        #Get wall positions
        for i in xrange(2):
            if i == 0:
                xw_arr = [(xw_0,xw_0+dx)] 
                yw_arr = [(yw_0,yw_0+dy)] 
                zw_arr = [(zw_0,zw_0+dz)]
            elif i==1:    
                xw_arr.append((xw_0+dx,xw_1))
                yw_arr.append((yw_0+dy,yw_1))
                zw_arr.append((zw_0+dz,zw_1))
        #Recursively get a single node positions. If at level 1, then 8 node
        #positions are returned.                
        for zw, yw, xw in itertools.product(zw_arr, yw_arr, xw_arr):
            x_arr, y_arr, z_arr, node_pos, vol_list = octree_nodes(xw[0],
                    xw[1], yw[0], yw[1], zw[0], zw[1],
                    levels-1, x_arr, y_arr, z_arr, node_pos, vol_list)

    return x_arr, y_arr, z_arr, node_pos, vol_list

cpdef find_octree_reg(double x_sph_c, double y_sph_c, double z_sph_c, oct_inst,
                    np.ndarray[DTYPE_t, ndim=2] p_stars, dict tor_dic, tree):

    """
    This function investigates which octree refinement region we are inside.
    Currently there are 5 different regions.
    Inside the disk arm torus.
    Inside disk 1 or 2 or inside their respective inner regions.
    In none of the above, then no refinement will be performed.
    Returns:
        ins_disk
    """
        
    cdef:
        double dist_to_star_1 = -99.0 # Negative value to catch source_nr error.
        double dist_to_star_2 = -99.0
        #Unpack oct_inst
        int tor_lev = oct_inst.tor_lev
        int disk_1_lev = oct_inst.disk_1_lev
        int disk_2_lev = oct_inst.disk_2_lev
        int inn_disk_1_lev = oct_inst.inn_disk_1_lev
        int inn_disk_2_lev = oct_inst.inn_disk_2_lev
        int no_disk_level = oct_inst.no_disk_level
        int levels = -99  # Set to negative value if no refinement is being performed.
        int source_nr = oct_inst.source_nr
        double azi_lim_1 = oct_inst.azi_lim_1
        double azi_lim_2 = oct_inst.azi_lim_2
        double oct_rad_1 = oct_inst.oct_rad_1  
        double oct_rad_2 = oct_inst.oct_rad_2
        double inn_disk_ref_1_rad = oct_inst.inn_disk_ref_1_rad
        double inn_disk_ref_2_rad = oct_inst.inn_disk_ref_2_rad
        double x_c, y_c, z_c
        bint ins_torus = False
        bint ins_disk_1 = False
        bint ins_disk_2 = False
        bint reg_unref = False 
        bint disk_1_inn_ref = False
        bint disk_2_inn_ref = False
        bint disks = oct_inst.disks
    x_c, y_c, z_c = transf_sph_cart(x_sph_c,y_sph_c,z_sph_c)    
   
    if source_nr == 2:
        dist_to_star_1 = m_sqrt((p_stars[0,0] - x_c)**2. 
            + (p_stars[0,1] - y_c)**2. + (p_stars[0,2] -z_c)**2.)
        dist_to_star_2 = m_sqrt((p_stars[1,0] - x_c)**2. 
            + (p_stars[1,1] - y_c)**2. + (p_stars[1,2] -z_c)**2.)
    elif source_nr == 1:
        dist_to_star_1 = m_sqrt(x_c**2.+y_c**2.+z_c**2.)
        dist_to_star_2 = 1e80

    assert dist_to_star_1 >= 0.0 and dist_to_star_2 >= 0.0, 'Star number must be 1 or 2.'
    
    if (tor_dic['torus'] and x_sph_c <= 600.*AU
       and tor_lev >= 1):
          
        ins_torus, tor_dic['r_tor'], tor_dic['dens'] = find_torus(
        r_sphr=x_sph_c, theta=y_sph_c, phi=z_sph_c, tor_rad=tor_dic['tor_rad'], 
        tor_incl=tor_dic['tor_incl'], max_dens=tor_dic['max_dens'], tree=tree,
        p_stars=p_stars, tor_p=tor_dic['tor_p'],
        fixed_dens_across_arc=tor_dic['fixed_dens_across_arc'])

        if ins_torus:
            levels = tor_lev
    if dist_to_star_1 <= oct_rad_1:
        if disks:
            ins_disk_1 = True
            ins_torus = False
            levels = disk_1_lev
    
            if dist_to_star_1 <= inn_disk_ref_1_rad:
                levels = inn_disk_1_lev
                disk_1_inn_ref = True
        elif not disks:
            levels = no_disk_level
    elif source_nr == 2 and dist_to_star_2 <= oct_rad_2:
        if disks:
            ins_disk_2 = True
            ins_torus = False
            levels = disk_2_lev
            if dist_to_star_2 <= inn_disk_ref_2_rad:
                levels = inn_disk_2_lev
                disk_2_inn_ref = True
        elif not disks:
            levels = no_disk_level      
        
    if (source_nr==2 and dist_to_star_1 > oct_rad_1 
        and dist_to_star_2 > oct_rad_2 and not ins_torus or
        source_nr==1 and dist_to_star_1 > oct_rad_1 and not ins_torus):
        
        reg_unref = True

    return reg_unref, levels, disk_1_inn_ref, disk_2_inn_ref

cpdef insert_octree(np.ndarray[DTYPE_t, ndim=1] gx,
                  np.ndarray[DTYPE_t, ndim=1] gy,
                  np.ndarray[DTYPE_t, ndim=1] gz,
                  np.ndarray[DTYPE_t, ndim=1] gxi,
                  np.ndarray[DTYPE_t, ndim=1] gyi,
                  np.ndarray[DTYPE_t, ndim=1] gzi, int nx, int ny, int nz,
                  oct_inst, tree, int source_nr):
                      
    """
    This function performs oct-tree refinement into an existing grid.
    Be aware that the length of octree_key is not the total nr of cells in the
    refined grid. octree_key contains a 1 for each level of refinement
    i.e. the length of octree_key will be larger than the total refined grid cell
    number.
    Note the subtle difference between the two outputs.
    
    Returns:
        indices = 1D numpy array of binary numbers for the base grid. 
           0 denotes no refinement, while 1 denotes that the cell is refined.
           
        octree_key = 1D numpy array of the full octree as printed in 
           amr_grid.inp.         
    """
    
    cdef: 
        double AU = 1.49597871*10.0**13.0 #[cm]
        Py_ssize_t ix, iy, iz
        Py_ssize_t ir, i_theta_0
        double x_c, y_c, z_c
#        double dist_to_star_1, dist_to_star_2
        list x_list, y_list, z_list, oct_coords
        int octree_detector = 0
        list octree_key = []
        list indices = [] #One long list of indices, which represents the global grid.
        list r_out = []
        list theta_out = []
        list phi_out = []
        int ndata = nx*ny*nz #Total cell number in global grid
        int levels
        bint reg_unref
        bint disk_1_inn_refined = False
        bint disk_2_inn_refined = False

    print 'Performing octree refinement..'

    # -------------------- Initialise arrays ----------------------------------

    for iz in xrange(nz):
        for iy in xrange(ny):
            for ix in xrange(nx):
                 
                reg_unref,levels, disk_1_inn_ref, disk_2_inn_ref = find_octree_reg(x_sph_c=gx[ix],
                    y_sph_c=gy[iy], z_sph_c=gz[iz], oct_inst=oct_inst,
                    p_stars=oct_inst.p_stars, tor_dic=oct_inst.tor_dic,tree=tree)
                if disk_1_inn_ref:
                    disk_1_inn_refined = True
                if disk_2_inn_ref:
                    disk_2_inn_refined = True
                if reg_unref:
                    indices.append(0) #This translates as no refinement in the final octree.
                    octree_key.append(0)
                    assert levels < 0,'levels error, should be negative'
                    
                    #Save node positions into arrays
                    r_out.append(gx[ix])
                    theta_out.append(gy[iy])
                    phi_out.append(gz[iz])
                    
                elif not reg_unref:
                    octree_detector = 1 #Flag that octree refinement has been done.
                    indices.append(1)
                    assert levels >= 0, 'levels error'
                    octree_key = construct_octree(octree_key,levels)

                    #Save node positions into arrays
                    x_list, y_list, z_list, node_pos, vol_list = octree_nodes(
                            gxi[ix],
                            gxi[ix + 1], gyi[iy], gyi[iy + 1], gzi[iz],
                            gzi[iz + 1], levels, [], [], [], [], [])
                    r_out.extend(x_list)
                    theta_out.extend(y_list)
                    phi_out.extend(z_list)
                    
                else:
                    raise RuntimeError("No regions were found. Are you somehow outside the model sphere?")
    if oct_inst.disks:
        assert disk_1_inn_refined, 'Disk 1 inner region not refined.'
        ' Please increase grid resolution.'
        if source_nr > 1:
            assert disk_2_inn_refined, 'Disk 2 inner region not refined.'
            ' Please increase grid resolution.'
    oct_coords = [r_out, theta_out, phi_out]

    #Now the base grid has been traversed and the octree constructed (as a 
    #long list of binary numbers to be read by radmc3d) we run a few tests.)
          
    # Assert that we've been through all data, unless we have no objects to 
    # refine
    if oct_inst.disks or oct_inst.tor_dic['torus']:
        assert octree_detector == 1,('No octree refinement has been performed. '
        'Perhaps your radial base-grid is too coarse to pick up the oct_rad_1' 
        'of %.1f AU' %(oct_inst.oct_rad_1/AU))
    if len(indices) != ndata:
        raise ValueError('Length of indices and ndata are not equal. It appears that we '
        'have not traversed entire base grid?')

    return indices, octree_key, oct_coords
                       
def find_evap_rad_oct(np.ndarray[DTYPE_t, ndim=1] vol,
                      np.ndarray[DTYPE_t, ndim=2] rhodust,
                      np.ndarray[DTYPE_t, ndim=2] dust_temp, int temp, 
                      Py_ssize_t i_dust, int n_dust, int source_nr,
                      recurs_subli=False,
                      total_mass = False,**kwargs):

    """This function finds the mass fraction of dust above
    the input temperature.
    Args:
          vol = 1-dim array of cell volumes.
          rhodust = 2-dim array of dust density. Last dim is dust species nr. [g cm^(-3)]
          dust_temp = 2-dim array of dust temperature. Last dim is dust species nr. [K]
          n_dust = nr of dust species.
          temp = dust sublimation temperature [K]
          recurs_subli = If set, retrieves the set of cell indices where the 
          dust temperature is above 90 K.
          
    Returns: 
          mas = The total dust mass above temp. [g]
          rad = The radius of the assumed spherical region of dust cells above
          temp. [cm]
          vol_tot = total volume of cells above temp.
          recurs_subli = If set, this function only returns recurs_subli, which
              is a set of all the cell indices with dust temperature above
              the arg temp.
            
          """
    from numpy import isnan      
    cdef: 
        Py_ssize_t i
        double mas, vol_tot
        double cell_mass = 0.0
        double rad = 0.0
        double res = 0.0
        list vol_all, mass
        set subli_list = set()
        
    mass = []
    vol_all = []
    above = True
    below = False

    for i in xrange(len(vol)):
        if above and not total_mass and not recurs_subli:
            if dust_temp[i,i_dust]>= temp:                                     
                cell_mass = vol[i] * rhodust[i, i_dust]           
                vol_all.append(vol[i])                      
                mass.append(cell_mass)

        if above and total_mass or above and recurs_subli:
            for j in xrange(n_dust):
                if dust_temp[i,j]>= temp and rhodust[i,j]>10.**(-48):                                     
                    cell_mass = vol[i] * rhodust[i,j]           
                    vol_all.append(vol[i])
                    mass.append(cell_mass)   
                    if recurs_subli:
                        if i not in subli_list:
                            subli_list.add(i)

        elif below:
            if dust_temp[i,i_dust]< temp:                                     
                res = vol[i] * rhodust[i, i_dust]           
                vol_all.append(vol[i])                      
                mass.append(res)     

    vol_tot = np.sum(vol_all)
    assert source_nr == 1 or source_nr == 2, 'Unexpected protostar nr.'
    if source_nr == 1:    
        rad = (3./(4.*PI)*(vol_tot))**(1./3.)
    #If two stars I divide the volume up between the stars.
    if source_nr == 2:
        rad = (3./(4.*PI)*(vol_tot/2.))**(1./3.)

    mas = np.sum(mass)

    assert isnan(mas) == False, 'Result is NaN.' 
    if not recurs_subli:            
        return mas, rad, vol_tot
    elif recurs_subli:            
        return subli_list
        
        
cpdef find_evap_rad(np.ndarray[DTYPE_t, ndim=3] vol,
                   np.ndarray[DTYPE_t, ndim=4] rhodust,
                   np.ndarray[DTYPE_t, ndim=4] dust_temp, int temp, 
                   Py_ssize_t i_dust, int source_nr,total_mass=False,n_dust=2,
                   recurs_subli=False):
    """This is a specific function made to find the mass fraction of dust above
    90 K. This Cython implementation speeds up the code by a factor 1000(!).
    Note that you need to assign c-types to the variables and constants in 
    order to obtain the desired speed-up.
    Args:
          vol = 3-dim array of cell volumes.
          rhodust = 4-dim array of dust density. Last dim is dust species nr.
          dust_temp = 4-dim array of dust temperature. Last dim is singular, 
                    shape(dust_temp) = (x,y,z,1). I do not currently understand 
                    why this is necessary (see 
                    radmc3dPy.analyze.radmc3dData.scalarfieldReader()).
          n_dust = dust species nr.
          temp = cutoff dust temperature
    Returns: 
          mas = The total dust mass above temp. [g] 
            
          """
    from numpy import isnan      
          
    cdef Py_ssize_t i,j,k
    cdef int nx,nz,ny
    cdef double mas

    above = True
    below = False
    
    nx = len(dust_temp[:,0,0,:])
    ny = len(dust_temp[0,:,0,:])
    nz = len(dust_temp[0,0,:,:])
    mass = []
    vol_all = []
    for i in xrange(nx):
        for j in xrange(ny):
            for k in xrange(nz):
                try:
                    if above:
                        if dust_temp[i,j,k,i_dust]>= temp:                                     
                            res = vol[i,j,k] * rhodust[i,j,k,i_dust]           
                            vol_all.append(vol[i,j,k])                      
                            mass.append(res)             
                    elif below:
                        if dust_temp[i,j,k,i_dust]< temp:                                     
                            res = vol[i,j,k] * rhodust[i,j,k,i_dust]           
                            vol_all.append(vol[i,j,k])                      
                            mass.append(res)
                except:
                    print 'Error: No conditions set in find find_evap_rad()'

    vol_tot = np.sum(vol_all)
    assert source_nr == 1 or source_nr == 2, 'Unexpected protostar nr.'
    if source_nr == 1:    
        rad = (3./(4.*PI)*(vol_tot))**(1./3.)
    #If two stars I divide the volume up between the stars.
    if source_nr == 2:
        rad = (3./(4.*PI)*(vol_tot/2.))**(1./3.)
 
    mas = np.sum(mass)
    assert isnan(mas) == False, 'Result is NaN.' 
                
    return mas, rad, vol_tot

cpdef azi_from_cart(double x_c, double y_c):
    cdef:
        double phi_1
    
    phi_1 = m_atan2(y_c,x_c) #Input is np.arctan(y,x)
       
    if phi_1 < 0.0:
        phi_1 = 2.*PI+phi_1
    
    return phi_1

cpdef transf_cart_sph(double x_c, double y_c, double z_c):
    """Converts cartesian coordinates to spherical coordinates.
       Args:
           x_c: x coordinate
           y_c: y coordinate
           z_c: z coordinate
       Returns: radius, theta (meridional angle), phi (azimuthal angle)
    """
    cdef:
        double rad
        double theta
        double phi
        
    rad = m_sqrt(x_c**2.+y_c**2.+z_c**2.)
    theta = m_acos(z_c/rad)
    phi = azi_from_cart(x_c,y_c)
    
    return rad, theta, phi
    
def NN_ellip(double x_p, double y_p, np.ndarray[DTYPE_t, ndim=2] data):
    cdef:
        np.ndarray[DTYPE_t, ndim=1] dists = np.zeros(len(data), dtype=np.float64)
        double x, y
        double dist
        double r_max = 20.0
        double r_thet
        double a = 300.
        double b = 150.
        double phi = 0.5
        double phi_2 = 0.707
    for x in xrange(10):
        r_thet = a**2.*(m_cos(phi_2)-m_cos(phi))**2.+b**2.*(m_sin(phi_2)-m_sin(phi))**2.    
    dists[:] = [m_sqrt((x-x_p)**2.+(y-y_p)**2.) for x,y in data] 
    dist = min(dists)
    
    return dist
    #e = np.where(dists == dist)
    #loc = e[0][0]
    
cpdef transf_sph_cart(double x_sph_c, double y_sph_c, double z_sph_c):
    """Converts spherical coordinates to cartesian coordinates.
        Args:
            x_sph_c: spherical radial coordinate. 
            y_sph_c: spherical meridional coordinate. 
            z_sph_c: spherical azimuthal coordinate. 

        Returns: x_c, y_c, z_c (cartesian coordinates in units of the length unit
            x_sph_c had). 
    """    
    cdef:
        double x_c
        double y_c
        double z_c
        
    x_c = x_sph_c*m_sin(y_sph_c+1e-50) * m_cos(z_sph_c)
    y_c = x_sph_c*m_sin(y_sph_c+1e-50) * m_sin(z_sph_c) 
    z_c = x_sph_c*m_cos(y_sph_c+1e-50)
    
    return x_c, y_c, z_c
    
cpdef tuple rot_vel_for_lime(double vel_x_disk, double vel_y_disk, 
                             double vel_z_disk, double incl, 
                             double azith_turn):
    """
    This function converts the sperical velocity vectors of the local star 
    reference frame into the cartesian velocity vectors of the global model.
    This will be passed to lime in an header array, where the velocity vectors 
    are extracted through a nearest-neighbour search to the lime nodes.
    Args:
    
    """
    cdef:
        double M_PI = 3.14159265359
        double vel_y_xback, vel_z_xback
        double vel_x_rot_fin, vel_y_rot_fin, vel_z_rot_fin    
    
    # First correct y and z vel component back from the tilted plane 
    # (reverse x-rotation).
    
    vel_y_xback = (vel_y_disk * m_cos(2. * M_PI - incl) + 
                   vel_z_disk * m_sin(2. * M_PI - incl))
    vel_z_xback = (-vel_y_disk * m_sin(2. * M_PI - incl) + 
                   vel_z_disk * m_cos(2. * M_PI - incl))

    # Then use the de-tilted y vel-component to find the vel x-component
    vel_x_rot_fin = (vel_x_disk * m_cos(2. * M_PI - azith_turn) + 
                     vel_y_xback * m_sin(2. * M_PI - azith_turn))
    vel_y_rot_fin = (-vel_x_disk * m_sin(2. * M_PI - azith_turn) + 
                     vel_y_xback * m_cos(2. * M_PI - azith_turn))
    vel_z_rot_fin = vel_z_xback
    
    return vel_x_rot_fin, vel_y_rot_fin, vel_z_rot_fin


def z_x_axis_rot(double x_c, double y_c, double z_c, double incl,
                 double azith_turn,
                 ref_pos=[0.0, 0.0, 0.0], return_cart=True):
    """
    This function performs rotation around the z-axis and the rotation 
    around the x-axis.
    Transform coordinate system by rotating around axes.
    First z-axis rotation (i.e. turn in azimuth).
    Coordinate transformation can result in array value spacings being
    stretched or compressed.
    Args: x_c, y_c, z_c (Cartesian coordinates of the input point)
          ref_pos = reference position, usually that of a star.
    """
    cdef:
        double x_c_zrot, y_c_zrot, y_c_xrot, z_c_xrot
   # print 'Input x_c is {}'.format(x_c)
    x_c = x_c - ref_pos[0]
    y_c = y_c - ref_pos[1]     
    z_c = z_c - ref_pos[2]
   # print 'Second change of x_c is {}'.format(x_c)

    #Transform coordinate system by rotating around axes.
    #First z-axis rotation (i.e. turn in azimuth).
    x_c_zrot = x_c*m_cos(azith_turn) + y_c*m_sin(azith_turn)
    y_c_zrot = -x_c*m_sin(azith_turn) + y_c*m_cos(azith_turn) 
    
    #Then perform rotation around the z-axis.
    y_c_xrot = y_c_zrot*m_cos(incl) + z_c*m_sin(incl)
    z_c_xrot = -y_c_zrot*m_sin(incl) + z_c*m_cos(incl)

    grid_radius = m_sqrt((x_c_zrot)**2. + (y_c_xrot)**2. + (z_c_xrot)**2.)
    
    midplane_r = np.sqrt(x_c_zrot**2.0 + y_c_xrot**2.0)
    theta_disk = m_acos(z_c_xrot/grid_radius)
    phi_disk = azi_from_cart(x_c_zrot, y_c_xrot)
   # print 'Third change of x_c is {}'.format(x_c_zrot)

    if return_cart:
        return x_c_zrot, y_c_xrot, z_c_xrot
    else:
        return grid_radius, theta_disk, phi_disk, midplane_r

def test_uninitialized_bug():

    cdef:
        double unini_val
        int int_val
    expected = False
    
    if expected:
        print 'Assigning val'
        unini_val = 5.0
        int_val = 5

    else:
        unini_val = 4.0
        int_val = 4
        print 'Not assigning val'

    print 'unini_val is', unini_val
    print 'int_val is', int_val


cpdef run_chi2_function_on_imfit_res(np.ndarray[DTYPE_t, ndim=1] stellar_mass_list,
                                     np.ndarray[DTYPE_t, ndim=1] disk_radius_list, 
                                     np.ndarray[DTYPE_t, ndim=1] beta_list, 
                                     np.ndarray[DTYPE_t, ndim=1] imfit_chan_vel,
                                     np.ndarray[DTYPE_t, ndim=1] peak_offset_pix,
                                     np.ndarray[DTYPE_t, ndim=1] peak_offset_err_pix,
                                     bint only_neg_vel_regime_for_chi2,
                                     double rest_vel, molecule, list CS_peak_pos,
                                     double dist_pc, cube,
                                     bint exclude_disk_edge=False):
    cdef:
        double chi2_kep, chi2_infall, stellar_mass, disk_radius, beta
        double chi2_kep_large_scale_CS, chi2_infall_large_scale_CS
        np.ndarray[DTYPE_t, ndim=1] rad_ran
        np.ndarray[DTYPE_t, ndim=1] vel_dom_pos
        np.ndarray[DTYPE_t, ndim=1] vel_dom_neg
        np.ndarray[DTYPE_t, ndim=1] model_radii_pos_pix
        np.ndarray[DTYPE_t, ndim=1] model_radii_neg_pix
        np.ndarray[DTYPE_t, ndim=1] model_radii_pos_AU
        np.ndarray[DTYPE_t, ndim=1] model_radii_neg_AU

        list model_radii_neg_AU_list = []
        list model_radii_pos_AU_list = []
        list model_radii_pos_pix_list = []
        list model_radii_neg_pix_list = []
        list large_scale_CS_model_radii_AU_list
        list large_scale_CS_model_arcsec_list
        list large_scale_CS_obs_arcsec
        list chi2_kep_list = []
        list chi2_infall_list = []
        list chi2_kep_large_scale_CS_list = []
        list chi2_infall_large_scale_CS_list = []
        list peak_offset_for_chi2fit_temp = []
        list peak_offset_err_for_chi2fit_temp = []

        np.ndarray[DTYPE_t, ndim=1] model_kepler_offset_pix
        np.ndarray[DTYPE_t, ndim=1] model_infall_offset_pix
        np.ndarray[DTYPE_t, ndim=1] peak_offset_for_chi2fit
        np.ndarray[DTYPE_t, ndim=1] peak_offset_err_for_chi2fit
        Py_ssize_t ctr = 0

    if only_neg_vel_regime_for_chi2:
        imfit_chan_vel_for_chi2_fit = [x for x in imfit_chan_vel if x < 0.0]
    else:
        imfit_chan_vel_for_chi2_fit = imfit_chan_vel
    n_data = len(imfit_chan_vel_for_chi2_fit)

    if only_neg_vel_regime_for_chi2:
        for x, y, z in zip(imfit_chan_vel, peak_offset_pix,
                           peak_offset_err_pix):
            if x < 0.0:
                peak_offset_for_chi2fit_temp.append(y)
                peak_offset_err_for_chi2fit_temp.append(z)

        peak_offset_for_chi2fit = np.array(peak_offset_for_chi2fit_temp)
        peak_offset_err_for_chi2fit = np.array(peak_offset_err_for_chi2fit_temp)

    else:
        peak_offset_for_chi2fit = peak_offset_pix
        peak_offset_err_for_chi2fit = peak_offset_err_pix

    if molecule == 'CS':
        large_scale_CS_analysis = True
    else:
        large_scale_CS_analysis = False

    for stellar_mass, disk_radius, beta in itertools.product(
                                                    stellar_mass_list,
                                                    disk_radius_list,
                                                    beta_list):
        model_radii_pos_pix_list = []
        model_radii_neg_pix_list = []
        model_radii_pos_AU_list = []
        model_radii_neg_AU_list = []

        # =============================================================
        #      Calculate chi square values for each velocity model
        # =============================================================

        # Define three seperate domains.
        # Positive/negative velocities and disk-edge velocities.
        # Combine into a single array
        # Define disk edge domain

        rad_ran = np.arange(1.0, disk_radius/AU_SI, 0.1)*AU_SI  # Radius points in AU
        print('disk_radius is {}'.format(disk_radius/AU_SI))
        PV_vel_profile = vel_profile(stellar_mass, disk_radius)

        if exclude_disk_edge:
            vel_dom_pos = np.array([x for x in imfit_chan_vel_for_chi2_fit if x > 0.0])
            vel_dom_neg = np.array([x for x in imfit_chan_vel_for_chi2_fit if x < 0.0])
        else:
            vel_pos = extract_vel_model_set(stellar_mass, disk_radius,
                                            stellar_mass, disk_radius,
                                            cube, rad_ran, rad_ran, beta)
    
            edge_vel_dom = np.array([x for x in imfit_chan_vel_for_chi2_fit if min(vel_pos['edge_kep']['vel_neg']) <= x <= max(vel_pos['edge_kep']['vel_pos'])])
            vel_dom_pos = np.array([x for x in imfit_chan_vel_for_chi2_fit if max(vel_pos['edge_kep']['vel_pos']) < x <= max(imfit_chan_vel_for_chi2_fit)])
            vel_dom_neg = np.array([x for x in imfit_chan_vel_for_chi2_fit if min(vel_pos['edge_kep']['vel_neg']) > x >= min(imfit_chan_vel_for_chi2_fit)])
            
        print('Rest_vel used in chi2 function is {}'.format(rest_vel))

        for dist_func in [PV_vel_profile._find_kepler_vel_radius,
                          PV_vel_profile._find_infall_ang_mom_cons_radius,
                          PV_vel_profile._find_proj_kepler_radius]:

            # Find the model offset positions, for each channel velocity,
            # in order to compare with the imfit results in the chi^2
            # analysis.

            if dist_func == PV_vel_profile._find_kepler_vel_radius:
                # Kepler or infall velocity profile.
                model_radii_pos_AU = np.array([dist_func(x) for x in vel_dom_pos])
                model_radii_neg_AU = np.array([dist_func(x) for x in vel_dom_neg])
            elif dist_func == PV_vel_profile._find_infall_ang_mom_cons_radius:
                model_radii_pos_AU = np.array([dist_func(x, beta) for x in vel_dom_pos])
                model_radii_neg_AU = np.array([dist_func(x, beta) for x in vel_dom_neg])
            elif dist_func == PV_vel_profile._find_proj_kepler_radius and not exclude_disk_edge:
                # Disk edge velocity profile.
                model_radii_pos_AU = np.array([dist_func(x) for x in
                                      edge_vel_dom if x >= 0.0])
                model_radii_neg_AU = np.array([dist_func(x) for x in
                                      edge_vel_dom if x < 0.0])

            model_radii_pos_pix = np.array([x/(AU_SI*cube.AU_pr_px) for x in
                                   model_radii_pos_AU])
            model_radii_neg_pix = np.array([x/(AU_SI*cube.AU_pr_px) for x in
                                   model_radii_neg_AU])

            model_radii_pos_AU_list.append(model_radii_pos_AU)
            model_radii_neg_AU_list.append(model_radii_neg_AU)

            model_radii_pos_pix_list.append(model_radii_pos_pix)
            model_radii_neg_pix_list.append(model_radii_neg_pix)

        # Assemble offset pixel values
        # Index 2 is the disk_edge_vels
        if only_neg_vel_regime_for_chi2:
            if exclude_disk_edge:
                model_kepler_offset_pix = np.array(model_radii_neg_pix_list[0])
                model_infall_offset_pix = np.array(model_radii_neg_pix_list[1])               
            else:
                model_kepler_offset_pix = np.concatenate((model_radii_neg_pix_list[0],
                                           model_radii_neg_pix_list[2]))
                model_infall_offset_pix = np.concatenate((model_radii_neg_pix_list[1],
                                           model_radii_neg_pix_list[2]))

        else:
            if exclude_disk_edge:
                model_kepler_offset_pix = np.concatenate((model_radii_neg_pix_list[0],
                                           model_radii_pos_pix_list[0]))
                model_infall_offset_pix = np.concatenate((model_radii_neg_pix_list[1],
                                           model_radii_pos_pix_list[1]))
            
            else:
                model_kepler_offset_pix = np.concatenate((model_radii_neg_pix_list[0],
                                           model_radii_neg_pix_list[2],
                                           model_radii_pos_pix_list[2],
                                           model_radii_pos_pix_list[0]))
                model_infall_offset_pix = np.concatenate((model_radii_neg_pix_list[1],
                                           model_radii_neg_pix_list[2],
                                           model_radii_pos_pix_list[2],
                                           model_radii_pos_pix_list[1]))

        chi2_kep = chi_squared(peak_offset_for_chi2fit,
                               model_kepler_offset_pix,
                               peak_offset_err_for_chi2fit)
        chi2_infall = chi_squared(peak_offset_for_chi2fit,
                                  model_infall_offset_pix,
                                  peak_offset_err_for_chi2fit)

        # Calculate reduced chi^2
        print('n_data is {}'.format(n_data))
        if exclude_disk_edge:
            chi2_kep = chi2_kep/(n_data - 1) # mass is free par
            chi2_infall = chi2_infall/(n_data - 1) # beta is pars
        else:            
            chi2_kep = chi2_kep/(n_data - 2) # mass and disk radius are pars
            chi2_infall = chi2_infall/(n_data - 3) # mass, disk radius and beta are pars
        
        chi2_kep_list.append(chi2_kep)
        chi2_infall_list.append(chi2_infall)
        print('stellar mass is {}'.format(stellar_mass/M_SUN_SI))

        if large_scale_CS_analysis:

            # Analysis of large scale CS emission from 2004 JKJ paper.
            # Large-scale CS radius, taken from JKJ 2004
            # Calculate position based on velocity

            large_scale_CS_model_radii_AU_list = []
            for dist_func in [PV_vel_profile._find_kepler_vel_radius,
                              PV_vel_profile._find_infall_ang_mom_cons_radius]:
                if dist_func == PV_vel_profile._find_kepler_vel_radius:
                    large_scale_CS_radii_pos_AU = dist_func(CS_peak_pos[0][1])/AU_SI
                    large_scale_CS_radii_neg_AU = dist_func(CS_peak_pos[1][1])/AU_SI
                elif dist_func == PV_vel_profile._find_infall_ang_mom_cons_radius:
                    large_scale_CS_radii_pos_AU = dist_func(CS_peak_pos[0][1], beta)/AU_SI
                    large_scale_CS_radii_neg_AU = dist_func(CS_peak_pos[1][1], beta)/AU_SI                            

                large_scale_CS_model_radii_AU_list.append(large_scale_CS_radii_pos_AU)
                large_scale_CS_model_radii_AU_list.append(large_scale_CS_radii_neg_AU)

            large_scale_CS_model_arcsec_list = [x/(dist_pc) for x in
                                                large_scale_CS_model_radii_AU_list]

            # Assume position error of 10 percent.
            large_scale_CS_arcsec_err_list = [x*0.1 for x in
                                              large_scale_CS_model_arcsec_list]
            large_scale_CS_obs_arcsec = [CS_peak_pos[0][0],
                                         CS_peak_pos[1][0]]
            chi2_kep_large_scale_CS = chi_squared(large_scale_CS_obs_arcsec,
                                                  large_scale_CS_model_arcsec_list[0:2],
                                                  large_scale_CS_arcsec_err_list[0:2])

            chi2_infall_large_scale_CS = chi_squared(large_scale_CS_obs_arcsec,
                                                     large_scale_CS_model_arcsec_list[2:4],
                                                     large_scale_CS_arcsec_err_list[2:4])

            chi2_kep_large_scale_CS_list.append(chi2_kep_large_scale_CS)
            chi2_infall_large_scale_CS_list.append(chi2_infall_large_scale_CS)

# -----------------------------------------------------------------------------

    print("Now writing {} chi^2 results to file..".format(molecule))

    # Write chi^2 square values to file. Will overwrite file if it exists.
    ctr = 0
    with open('chi2_{}_res.dat'.format(molecule), 'w') as f:
        f.write('Mass [M_SUN], Disk radius [AU], v_LSR [M/S],'
                'red_chi_2_kepler, beta/AU, red_chi_2_infall \n')

        for stellar_mass, disk_radius, beta in itertools.product(
                                                    stellar_mass_list,
                                                    disk_radius_list,
                                                    beta_list):

            f.write('{}, {}, {}, {}, {}, {} \n'.format(stellar_mass/M_SUN_SI,
                                                   disk_radius/AU_SI,
                                                   rest_vel,
                                                   chi2_kep_list[ctr],
                                                   beta/AU_SI,
                                                   chi2_infall_list[ctr]))
            ctr += 1
        
    
    if molecule == 'CS' and large_scale_CS_analysis:
        ctr = 0
        with open('chi2_CS_large_scale_res.dat', 'w') as f:
            
            f.write('Mass [M_SUN], Disk radius [AU], v_LSR [M/S],'
                    'chi_2_kepler, beta/AU, chi_2_infall \n')
            
            for stellar_mass, disk_radius, beta in itertools.product(
                                                    stellar_mass_list,
                                                    disk_radius_list,
                                                    beta_list):
                f.write('{}, {}, {}, {}, {}, {} \n'.format(stellar_mass/M_SUN_SI,
                                                           disk_radius/AU_SI,
                                                           rest_vel,
                                                           chi2_kep_large_scale_CS_list[ctr],
                                                           beta/AU_SI,
                                                           chi2_infall_large_scale_CS_list[ctr]))
                ctr += 1


class vel_profile():
    """
    This class contains varies velocity profiles packaged as methods.
    """
    
# =============================================================================
#     cdef:
#         double self.mass
#         double self.disk_radius
# =============================================================================

    def __init__(self, double mass, double disk_radius):
        assert disk_radius > 10e-6*AU_SI, 'Please use larger disk'
        self.mass = mass
        self.disk_radius = disk_radius

    def _kepler(self, double dist):
        """
        Kepler orbital speed
        """
        cdef double vel
        assert dist > 10e-6*AU_SI, 'Please use larger distance'

        vel = np.sqrt(6.67e-11*self.mass/dist)

        return vel

    def _free_fall(self, double dist):
        """
        See formation of stars pdf, page 296
        """
        cdef double vel
        assert dist > 10e-6*AU_SI, 'Please use larger distance'

        vel = np.sqrt(2.0*6.67e-11*self.mass/dist)

        return vel

    def _infall_ang_mom_cons(self, double dist, double beta):
        """
        Velocity of infalling material in the equitorial plane, assuming no
        rotation and no pressure gradient.
        """
        cdef double vel
#        vel = np.sqrt(2*(start_dist - dist)*6.67e-11*self.mass/dist**2.0)
#        beta = 15*50*AU_SI
        #beta = 7.5e4*AU_SI
        assert beta > AU_SI, 'Please use larger beta constant'
        assert dist > 10e-6*AU_SI, 'Please use larger distance'

        vel = beta/dist

        return vel

    def _proj_kepler(self, double proj_dist):
        """
        Projected kepler orbital speed, see Lindberg et al. 2014. Traces the
        edge of the disk, along the projected distance from the disk center.
        """

        cdef double proj_vel
        assert proj_dist > 10e-6*AU_SI, 'Please use larger distance'

        proj_vel = np.sqrt(6.67e-11*self.mass)*self.disk_radius**(-3./2.)*proj_dist

        return proj_vel

    def _find_kepler_vel_radius(self, double vel):
        """
        Find radius based on kepler orbital speed
        """
        cdef double sign, dist
        if vel < 0.0:
            sign = -1.0
        else:
            sign = 1.0

        dist = sign*(6.67e-11*self.mass)/vel**2.0

        return dist

    def _find_free_fall_radius(self, double vel):
        """
        See formation of stars pdf, page 296
        """
        cdef double dist

        dist = 2.0*6.67e-11*self.mass/vel**2.0

        return dist

    def _find_infall_ang_mom_cons_radius(self, double vel, double beta):
        """
        Velocity of infalling material in the equitorial plane, assuming no
        rotation and no pressure gradient.
        """
        cdef double dist
        assert beta > AU_SI, 'Please use larger beta constant'

        dist = beta/vel

        return dist


    def _find_proj_kepler_radius(self, double proj_vel):
        """
        Projected kepler orbital speed, see Lindberg et al. 2014. Traces the
        edge of the disk, along the projected distance from the disk center.
        """
        cdef double proj_dist
        proj_dist = proj_vel/(np.sqrt(6.67e-11*self.mass)*self.disk_radius**(-3./2.))

        return proj_dist


cpdef extract_vel_model_set(m_star_kep, disk_rad_kep, m_star_infall,
                            disk_rad_infall, cube, rad_ran_kep, rad_ran_infall,
                            beta):
    """
    Extract set of velocity profiles.
    """

    # Extract velocity and offset info for imfit plots.
    PV_vel_profile_kep = vel_profile(m_star_kep, disk_rad_kep)
    PV_vel_profile_infall = vel_profile(m_star_infall, disk_rad_infall)

    vel_func = PV_vel_profile_kep._kepler
    (kepler_vels, kepler_vels_neg, kepler_offset_arcsec,
     kepler_offset_arcsec_neg, kepler_vels_inv,
     kepler_vels_neg_inv) = pos_to_vel_and_arcsec(vel_func,
                                                  rad_ran_kep,
                                                  cube.AU_pr_px,
                                                  cube.arcs_pr_px)

    vel_func = PV_vel_profile_infall._infall_ang_mom_cons
    (infall_vels, infall_vels_neg, infall_offset_arcsec,
     infall_offset_arcsec_neg, infall_vels_inv,
     infall_vels_neg_inv) = pos_to_vel_and_arcsec(vel_func,
                                                  rad_ran_infall,
                                                  cube.AU_pr_px,
                                                  cube.arcs_pr_px,
                                                  beta=beta)

    vel_func = PV_vel_profile_kep._proj_kepler
    (edge_vels_kep, edge_vels_neg_kep, edge_offset_arcsec_kep,
     edge_offset_arcsec_neg_kep, edge_vels_inv_kep,
     edge_vels_neg_inv_kep) = pos_to_vel_and_arcsec(vel_func,
                                                    rad_ran_kep,
                                                    cube.AU_pr_px,
                                                    cube.arcs_pr_px)

    # Retrieve projected kepler velocities, with infall velocity model
    # determined stellar mass and disk radius.

    vel_func = PV_vel_profile_infall._proj_kepler
    (edge_vels_infall, edge_vels_neg_infall, edge_offset_arcsec_infall,
     edge_offset_arcsec_neg_infall, edge_vels_inv_infall,
     edge_vels_neg_inv_infall) = pos_to_vel_and_arcsec(vel_func,
                                                       rad_ran_infall,
                                                       cube.AU_pr_px,
                                                       cube.arcs_pr_px)

    vel_pos = {'kep': {'vel_pos': kepler_vels,
                       'vel_neg': kepler_vels_neg,
                       'offset_arcsec': kepler_offset_arcsec,
                       'offset_arcsec_neg': kepler_offset_arcsec_neg,
                       'inv_vel_pos': kepler_vels_inv,
                       'inv_vel_neg': kepler_vels_neg_inv
                       },
               'infall': {'vel_pos': infall_vels,
                          'vel_neg': infall_vels_neg,
                          'offset_arcsec': infall_offset_arcsec,
                          'offset_arcsec_neg': infall_offset_arcsec_neg,
                          'inv_vel_pos': infall_vels_inv,
                          'inv_vel_neg': infall_vels_neg_inv
                          },
               'edge_infall': {'vel_pos': edge_vels_infall,
                               'vel_neg': edge_vels_neg_infall,
                               'offset_arcsec': edge_offset_arcsec_infall,
                               'offset_arcsec_neg': edge_offset_arcsec_neg_infall,
                               'inv_vel_pos': edge_vels_inv_infall,
                               'inv_vel_neg': edge_vels_neg_inv_infall
                               },
               'edge_kep': {'vel_pos': edge_vels_kep,
                            'vel_neg': edge_vels_neg_kep,
                            'offset_arcsec': edge_offset_arcsec_kep,
                            'offset_arcsec_neg': edge_offset_arcsec_neg_kep,
                            'inv_vel_pos': edge_vels_inv_kep,
                            'inv_vel_neg': edge_vels_neg_inv_kep
                            }
               }

    return vel_pos


cpdef pos_to_vel_and_arcsec(vel_func, rad_ran, AU_pr_px, arcs_pr_px, beta=1.0):
    """
    Calculate the peak emission position, in terms of pixels, in a given
    channel, based on the input velocity profile.

    Args:
        vel_func = velocity function [function object]
        rad_ran = radius values in AU [array]
        AU_pr_px = AU per pixel [float]
        arcs_pr_px = arcseconds per pixel [float]
    """

    # Velocities at given radii

    if vel_func.__name__ == '_infall_ang_mom_cons':
        vels = np.array([vel_func(x, beta) for x in rad_ran])
    else:
        vels = np.array([vel_func(x) for x in rad_ran])

    vels_neg = np.array([-x for x in vels])

    # Make Kepler vels for imageplot.
    # Find according channel number for the kepler vel, i.e. y_pix in the PV
    # diagram.

    offset_arcsec = np.array([x/(AU_pr_px*AU_SI)*arcs_pr_px for x in rad_ran])
    offset_arcsec_neg = np.array([-x for x in offset_arcsec])

    # INVERSE VELOCITY
    vels_inv = np.array([x**(-1) for x in vels])
    vels_neg_inv = np.array([x**(-1) for x in vels_neg])

    return (vels, vels_neg, offset_arcsec, offset_arcsec_neg, vels_inv,
            vels_neg_inv)
    

def chi_squared(obs, mod, errs):
    """
    Calculate the chi square value of the deviations of the model from
    observations.

    Requires three equal length arrays of the observations, model values and
    observation uncertainty.

    returns: chi squared (float).
    """

    cdef double chi_2
    vals = []
    assert len(obs) == len(mod) == len(errs), ('Unequal array lengths, '
        'len(obs)={}, len(mod)={}, len(errs)={}'.format(len(obs), len(mod),
        len(errs)))

    for i in xrange(len(obs)):
        val = ((obs[i] - mod[i])/errs[i])**2.0
        vals.append(val)
    chi_2 = sum(vals)
    return chi_2
    
