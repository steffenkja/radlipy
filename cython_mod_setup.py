# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 09:40:47 2015

@author: Steffen_KJ
"""

from distutils.core import setup
from Cython.Build import cythonize
import numpy

setup(
    ext_modules = cythonize("cython_mod.pyx"),
    include_dirs=[numpy.get_include()]

)
