#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 10 14:01:52 2017

@author: Steffen_KJ
"""
from __future__ import division
from scipy.integrate import dblquad, tplquad

import numpy as np
from cgs_cnst import AU, M_SUN, PC
from radmc3d_tools import (log_interp, freq_wav_conv, spectral_radiance_wav,
                           spectral_radiance_freq, disk_mass_opt_thin_em)
from cython_mod import transf_sph_cart
from sympy.solvers import solve
from sympy import Symbol


def integrand(y, x):
    'y must be the first argument, and x the second.'
    return y + x


derive_tot_mass = True
derive_sigma_0 = True
calc_av_wav = False
derive_mass_with_scipy = False
find_rad_half_mass = False


def rho_dust_wxy(z, y, x, sigma_0=0.6, total_rad=38.3*AU):
    'y must be the first argument, and x the second.'
    assert z >= 0.0, 'z must not be negative'
    r_0 = 10.0*AU
    H_0 = 1.5*AU
    z_disk = z

    midplane_r = np.sqrt(x**2.0 + y**2.0)
    surf_dens = sigma_0*(midplane_r/r_0)**(-0.75)
    H_d = H_0*(midplane_r/r_0)**(1.0 + 0.25)  # Was divided by 10 AU before

    rho_dust = surf_dens/(H_d*np.sqrt(2.*np.pi))*np.exp(-z_disk**2./(2.*H_d**2.))

    if np.sqrt(z**2.0 + y**2.0 + x**2.0) > total_rad:
        rho_dust = 1e-50

    return rho_dust


def rho_dust_wxy_halfrad_mass(z, y, x, sigma_0=0.075):
    'y must be the first argument, and x the second.'
    assert z >= 0.0, 'z must not be negative'
    r_0 = 10.0*AU
    H_0 = 1.5*AU
    z_disk = z

    midplane_r = np.sqrt(x**2.0 + y**2.0)
    surf_dens = sigma_0*(midplane_r/r_0)**(-0.75)
    H_d = H_0*(midplane_r/r_0)**(1.0 + 0.25)  # Was divided by 10 AU before

    rho_dust = surf_dens/(H_d*np.sqrt(2.*np.pi))*np.exp(-z_disk**2./(2.*H_d**2.))

    return rho_dust


def rho_dust_spher(phi, theta, rad):
    'y must be the first argument, and x the second.'

    x, y, z = transf_sph_cart(rad, theta, phi)
    sigma_0 = 0.6
    assert z >= 0.0, 'z must not be negative'
    assert np.allclose(rad, np.sqrt(x**2.0 + y**2.0 + z**2.0))
    r_0 = 10.0*AU
    H_0 = 1.5*AU
    z_disk = z
    midplane_r = np.sqrt(x**2.0 + y**2.0)
    surf_dens = sigma_0*(midplane_r/r_0)**(-0.75)
    H_d = H_0*(midplane_r/r_0)**(1.0 + 0.25)  # Was divided by 10 AU before

    rho_dust = surf_dens/(H_d*np.sqrt(2.*np.pi))*np.exp(-z_disk**2./(2.*H_d**2.))

    return rho_dust


if derive_sigma_0:

    ans, err = dblquad(integrand, -2, 3,
                       lambda x: -1,
                       lambda x: 3)
    print ans

    def rho_dust(z_disk, midplane_r, sigma_0=2.0):
        'y must be the first argument, and x the second.'
        x_0 = 10.0*AU
        H_0 = 1.5*AU

        surf_dens = sigma_0*(midplane_r/x_0)**(-0.75)
        H_d = H_0*(midplane_r/x_0)**(1.0 + 0.25)  # Was divided by 10 AU before

        rho_dust = surf_dens/(H_d*np.sqrt(2.*np.pi))*np.exp(-z_disk**2./(2.*H_d**2.))

        return rho_dust


    # ans, err = dblquad(rho_dust, 0.1*AU, 20*AU,
    #                   lambda x: 0.001*AU,
    #                   lambda x: 20*AU)

    # print ans/M_SUN

    # Calculate the optical depth through the midplane.

    opt_AU = rho_dust(0.0, midplane_r=1.0*AU, sigma_0=0.98)*4.56*1*AU
    opt_02AU = rho_dust(0.0, midplane_r=0.2*AU, sigma_0=0.98)*4.56*0.2*AU
    opt_015AU = rho_dust(0.0, midplane_r=0.15*AU, sigma_0=0.98)*4.56*0.15*AU
    opt_01AU = rho_dust(0.0, midplane_r=0.1*AU, sigma_0=0.98)*4.56*0.1*AU

    print 'Optical depth at 1 AU is {}'.format(opt_AU)
    print 'Optical depth at 0.2 AU is {}'.format(opt_02AU)
    print 'Optical depth at 0.15 AU is {}'.format(opt_015AU)
    print 'Optical depth at 0.1 AU is {}'.format(opt_01AU)

    # Calculate disk mass as sum of small slices through the disk geometry.

    tot_mass = 0.0
    disk_r = 38.3*AU
    dr = 0.01*AU
    dz = 0.01*AU

# =============================================================================
#     for r in np.arange(0.001*AU, disk_r, dr):
#         for z in np.arange(0.001*AU, disk_r, dz):
# 
#             rho = rho_dust(z, r, sigma_0=2.0)
#             mass = rho*np.pi*(r**2.0*dz - (r - dr)**2.0*dz)
# 
#             if np.sqrt(z**2.0 + r**2.0) > disk_r:
#                 mass = 0.0
#             tot_mass += mass
# 
#     # Factor 2 comes from adding the two disk sides together
#     print 'total mass (gas + dust) of disk using sigma_0=2.0 is', 100.0*2.0*tot_mass/M_SUN
# 
#     tot_mass = 0.0
#     for r in np.arange(0.001*AU, disk_r, dr):
#         for z in np.arange(0.001*AU, disk_r, dz):
# 
#             rho = rho_dust(z, r, sigma_0=0.25)
#             mass = rho*np.pi*(r**2.0*dz - (r - dr)**2.0*dz)
# 
#             if np.sqrt(z**2.0 + r**2.0) > disk_r:
#                 mass = 0.0
#             tot_mass += mass
# 
# 
#     print 'total mass (gas + dust) of disk using sigma_0=0.25 is', 100.0*2.0*tot_mass/M_SUN
# 
#     tot_mass = 0.0
#     for r in np.arange(0.001*AU, disk_r, dr):
#         for z in np.arange(0.001*AU, disk_r, dz):
# 
#             rho = rho_dust(z, r, sigma_0=0.4)
#             mass = rho*np.pi*(r**2.0*dz - (r - dr)**2.0*dz)
# 
#             if np.sqrt(z**2.0 + r**2.0) > disk_r:
#                 mass = 0.0
#             tot_mass += mass
# 
# 
#     print 'total mass (gas + dust) of disk using sigma_0=0.4 is', 100.0*2.0*tot_mass/M_SUN
# 
# =============================================================================
    tot_mass = 0.0
    for r in np.arange(0.1*AU, disk_r, dr):
        for z in np.arange(0.0001*AU, disk_r, dz):

            rho = rho_dust(z, r, sigma_0=0.092)
            mass = rho*np.pi*(r**2.0*dz - (r - dr)**2.0*dz)

            if np.sqrt(z**2.0 + r**2.0) > disk_r:
                mass = 0.0
            tot_mass += mass

    print 'total mass (gas + dust) of disk using sigma_0=0.092 is', 100.0*2.0*tot_mass/M_SUN

    tot_mass = 0.0
    for r in np.arange(0.1*AU, disk_r, dr):
        for z in np.arange(0.0001*AU, disk_r, dz):

            rho = rho_dust(z, r, sigma_0=0.07)
            mass = rho*np.pi*(r**2.0*dz - (r - dr)**2.0*dz)

            if np.sqrt(z**2.0 + r**2.0) > disk_r:
                mass = 0.0
            tot_mass += mass

    print 'total mass (gas + dust) of disk using sigma_0=0.07 is', 100.0*2.0*tot_mass/M_SUN




# =============================================================================
#     tot_mass = 0.0
#     for r in np.arange(0.4*AU, disk_r, dr):
#         for z in np.arange(0.4*AU, disk_r, dz):
# 
#             rho = rho_dust(z, r, sigma_0=0.6)
#             mass = rho*np.pi*(r**2.0*dz - (r - dr)**2.0*dz)
# 
#             if np.sqrt(z**2.0 + r**2.0) > disk_r:
#                 mass = 0.0
#             tot_mass += mass
# 
#     print ('total mass (gas + dust) of disk using sigma_0=0.6 with 0.4 AU '
#            'stepsizeis', 100.0*2.0*tot_mass/M_SUN)
# =============================================================================

    tot_mass = 0.0
    disk_r = 38.3
    dr = 0.01
    dz = 0.01
    rho = 2.0

    for r in np.arange(0.1, disk_r, dr):
        for z in np.arange(0.0001, disk_r, dz):

            mass = rho*np.pi*(r**2.0*dz - (r - dr)**2.0*dz)
            if np.sqrt(z**2.0 + r**2.0) > disk_r:
                mass = 0.0
            tot_mass += mass

    tot_mass = 2.0*tot_mass

    # Sanity check
    san_val = rho*4.0/3.0*np.pi*disk_r**3.0
    print 'Function value is {}, sanity check value is {}'.format(tot_mass,
                                                                  san_val)
    assert np.allclose(san_val, tot_mass, rtol=0.1)


if derive_mass_with_scipy:

    res, err = tplquad(rho_dust_wxy, 0.1*AU, 40.0*AU, lambda x: 0.1*AU,
                       lambda x: 40.0*AU, lambda x, y: 0.1*AU,
                       lambda x, y: 40.0*AU)

    print 'tplquad res is {} Msun'.format(8.0*res*100.0/M_SUN)
    print 'tplquad err is {} Msun'.format(8.0*err*100.0/M_SUN)

    res, err = tplquad(rho_dust_spher, 0.1*AU, 40.0*AU, lambda x: 0.0,
                       lambda x: np.pi/2.0, lambda x, y: 0.0,
                       lambda x, y: 2.0*np.pi)

    print 'tplquad res is {} Msun'.format(2.0*res*100.0/M_SUN)
    print 'tplquad err is {} Msun'.format(2.0*err*100.0/M_SUN)


if derive_tot_mass:
    # Average frequency of cubes

    av_freq = (354.50541e9 + 356.731e9 + 342.881e9 + 345.341e9)/4.0
    temp = 70.0  # K, read from plot after considering half mass radius at ~ 21 AU, with a 38.3 AU disk
    tot_flux_jy = 0.0662  # 66.2 mJy, derived from casaviewer of cycle 1 + 3 dust cont image.
    dist_PC = 200.0
    tot_mass = disk_mass_opt_thin_em(av_freq, temp, tot_flux_jy, dist_PC,
                                      ice_fract=0.85, bare_grain_fract=0.15,
                                      use_JKJ_deriv=False)

    tot_mass_JKJ = disk_mass_opt_thin_em(av_freq, temp, tot_flux_jy, dist_PC,
                                          ice_fract=0.85,
                                          bare_grain_fract=0.15,
                                          use_JKJ_deriv=True)

    print('tot_mass is {} solar masses'.format(tot_mass))
    print('tot_mass with JKJ derivation is {} solar masses'.format(tot_mass_JKJ))

    # Sanity check mass derivation at 870 microns
    freq = 3.446e11  # roughly 870 microns
    tot_flux_jy = 1.0
    temp = 30.0
    tot_mass = disk_mass_opt_thin_em(freq, temp, tot_flux_jy, dist_PC,
                                     ice_fract=1.0, bare_grain_fract=0.0,
                                     use_JKJ_deriv=False)

    tot_mass_JKJ = disk_mass_opt_thin_em(freq, temp, tot_flux_jy, dist_PC,
                                         ice_fract=1.0,
                                         bare_grain_fract=0.0,
                                         use_JKJ_deriv=True)

    print('Difference between derivations are {}'.format((tot_mass - tot_mass_JKJ)/tot_mass))
    assert np.allclose(tot_mass, tot_mass_JKJ, rtol=1e-2), ('tot_mass is {} '
                      'solar masses tot_mass with JKJ derivation is {} '
                      'solar masses'.format(tot_mass, tot_mass_JKJ))

    # Sanity check mass derivation at 800 microns
    freq = 3.750e11  # roughly 800 microns
    tot_mass = disk_mass_opt_thin_em(freq, temp, tot_flux_jy, dist_PC,
                                     ice_fract=1.0, bare_grain_fract=0.0,
                                     use_JKJ_deriv=False)

    tot_mass_JKJ = disk_mass_opt_thin_em(freq, temp, tot_flux_jy, dist_PC,
                                         ice_fract=1.0,
                                         bare_grain_fract=0.0,
                                         use_JKJ_deriv=True)

# Assert will fail, as JKJ apparently used 870 microns not 800 microns.
#    assert np.allclose(tot_mass, tot_mass_JKJ, rtol=1e-4), ('tot_mass is {} '
#                       'solar masses tot_mass with JKJ derivation is {} '
#                       'solar masses'.format(tot_mass, tot_mass_JKJ))

if find_rad_half_mass:

    an_half_mass = False
    tot_mass = 8.0*np.pi*(38.3**0.25 - 0.1**0.25)

    r = Symbol('r')

    full_mass_rad = solve(8.0*np.pi*(r**(0.25) - 0.1**0.25) - tot_mass, r)

    print('full_mass_rad is {}'.format(full_mass_rad))

    half_mass_rad = solve(8.0*np.pi*(r**(0.25) - 0.1**0.25) - tot_mass/2.0, r)

    print('half_mass_rad is {}'.format(half_mass_rad))

    # Test result

    res, err = tplquad(rho_dust_wxy_halfrad_mass, 0.1*AU, 38.3*AU, lambda x: 0.1*AU,
                       lambda x: 38.3*AU, lambda x, y: 0.0*AU,
                       lambda x, y: 38.3*AU)

    print 'tplquad res is {} Msun'.format(8.0*res*100.0/M_SUN)
    print 'tplquad err is {} Msun'.format(8.0*err*100.0/M_SUN)

    res, err = tplquad(rho_dust_wxy_halfrad_mass, 0.1*AU, 38.3*AU/1.8, lambda x: 0.1*AU,
                       lambda x: 38.3*AU/1.8, lambda x, y: 0.0*AU,
                       lambda x, y: 38.3*AU/1.8)

    print 'tplquad 1.8 res is {} Msun'.format(8.0*res*100.0/M_SUN)
    print 'tplquad 1.8 err is {} Msun'.format(8.0*err*100.0/M_SUN)

    res, err = tplquad(rho_dust_wxy_halfrad_mass, 0.1*AU, 38.3*AU/2.0, lambda x: 0.1*AU,
                       lambda x: 38.3*AU/2.0, lambda x, y: 0.0*AU,
                       lambda x, y: 38.3*AU/2.0)

    print 'tplquad 2 res is {} Msun'.format(8.0*res*100.0/M_SUN)
    print 'tplquad 2 err is {} Msun'.format(8.0*err*100.0/M_SUN)

    res, err = tplquad(rho_dust_wxy_halfrad_mass, 0.1*AU, 38.3*AU/3.0, lambda x: 0.1*AU,
                       lambda x: 38.3*AU/3.0, lambda x, y: 0.0*AU,
                       lambda x, y: 38.3*AU/3.0)

    print 'tplquad 3 res is {} Msun'.format(8.0*res*100.0/M_SUN)
    print 'tplquad 3 err is {} Msun'.format(8.0*err*100.0/M_SUN)

    if an_half_mass:
        res, err = tplquad(rho_dust_wxy_halfrad_mass, 0.1*AU, 5.40889333303838*AU, lambda x: 0.1*AU,
                           lambda x: 5.40889333303838*AU, lambda x, y: 0.0*AU,
                           lambda x, y: 200.3*AU)
    
        print 'tplquad res of halfmassrad is {} Msun'.format(8.0*res*100.0/M_SUN)
        print 'tplquad err of halfmassrad is {} Msun'.format(8.0*err*100.0/M_SUN)
