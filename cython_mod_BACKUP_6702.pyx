# cython: profile=True
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  3 10:31:48 2015

@author: steffenkj

This module contains various Cython modules, constructed to eliminate speed
bottlenecks in my Python programs. 

"""
from __future__ import division
#-Wno-unused-function
STUFF = "Hi"
"Using deprecated NumPy API, disable it by " "#defining NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION"
import warnings

cimport cython
#v_test=71

import itertools
#import gc
import time
import math
#from libc.stdlib cimport malloc, free
from radmc3dPy.natconst import *
#from scipy import spatial
#import pandas as pd
#Get html report
import Cython.Compiler.Options
Cython.Compiler.Options.annotate = True


#Be aware that I suppress deprecated library warnings from numpy.

#import numpy as np
#the_path = numpy.get_include()

#keep "the_path" to configure  the two environment variables:

#CPATH=the_path
#CFLAGS=-Ithe_path

#cdef extern from "/System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python/numpy/core/include/numpy/npy_no_deprecated_api.h": pass

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings('ignore')
cimport numpy as np
import numpy as np
from collections import defaultdict
from cgs_cnst import PI

np.import_array()
#include "numpy_common.pxi"
#np.import_umath
import math
DTYPE = np.float64
ctypedef np.float64_t DTYPE_t
#ctypedef np.int64_t DTYPE_int
from libc.math cimport sin as m_sin
from libc.math cimport cos as m_cos
from libc.math cimport sqrt as m_sqrt
from libc.math cimport asin as m_asin
from libc.math cimport acos as m_acos
from libc.math cimport atan as m_atan
from libc.math cimport atan2 as m_atan2
from libc.math cimport abs as absc

warnings.filterwarnings('always')
np.import_array()
warnings.filterwarnings("always", category=DeprecationWarning)

#void import_umath()
#cdef extern from *:
#    pass

#cdef extern from *:
#    bint FALSE "0"
#    void import_array()
#    void 
#import_umath()

#if FALSE:
#    import_array()
#    import_umath()
#v_test=8    
#@cython.always_allow_keywords(True)  
#def getDustDensity_cyth(int nx,int ny,int nz, np.ndarray[DTYPE_t, ndim=1] 
#    g_ix,np.ndarray[DTYPE_t, ndim=1] g_iy,np.ndarray[DTYPE_t, ndim=1] g_iz,
#    int ndust, int n_stream, int n_stream_r, 
#    double evap_rad, double cav_rad, double inn_rad, double out_rad, 
#    int source_nr, np.ndarray[DTYPE_t, ndim=1] gx, 
#    np.ndarray[DTYPE_t, ndim=1] gy, np.ndarray[DTYPE_t, ndim=1] gz,  
#    double prho, double rho0, double r_c, double m_dot, double m_star, 
#    np.ndarray[DTYPE_t, ndim=2] p_stars, dict disk_dic, bare=False, rot_collapse=False, 
#    cav_enable=False, octree_indices=False, ref_nr=None, **kwargs):
 
def getDustDensity_cyth(int nx,int ny,int nz, np.ndarray[DTYPE_t, ndim=1] 
    g_ix,np.ndarray[DTYPE_t, ndim=1] g_iy,np.ndarray[DTYPE_t, ndim=1] g_iz,
    int ndust, int n_stream, int n_stream_r, 
    double evap_rad, double cav_rad, double inn_rad, double out_rad, 
    int source_nr, np.ndarray[DTYPE_t, ndim=1] gx, 
    np.ndarray[DTYPE_t, ndim=1] gy, np.ndarray[DTYPE_t, ndim=1] gz,  
    double prho, double rho0, double r_c, double m_dot, double m_star, 
<<<<<<< HEAD
    np.ndarray[DTYPE_t, ndim=2] p_stars, dict disk_dic, bare=False, rot_collapse=False, 
    cav_enable=False, octree_indices=False, ref_nr=None,levels = None, **kwargs):
=======
    np.ndarray[DTYPE_t, ndim=2] p_stars, dict disk_dic, set subli_list,
    recurs_subli = False, 
    bare=False, rot_collapse=False, cav_enable=False, octree_indices=False, 
    ref_nr=None,levels = None, **kwargs):
>>>>>>> recurs_bare_allo
        
    """Function to create a dust density distribution in a spherical coordinate
    system.
    
    Handles a combination of different geometric components. A rotating or
    static envelope with or without an inner cavity of constant volume density,
    together with several or a single stellar source at different positions. 
    The opacity will either follow a evaporation temperature consistent scheme
    of ice- and bare-grain opacities or simply ice-grain opacities everywhere.
    Created in Cython due to the heavy use of brute-force approaches entailing 
    visits to millions, even billions of grid cells,
    depending on the setup.
    
    Args:           
        nx: number of radial grid cells [integer].
        ny: number of meridional grid cells [integer].
        nz: number of azimuthal grid cells [integer].
        ndust: number of dust species in the model [integer].
        n_stream: number of calculated streamlines in the case of a 
            rotating collapse [integer].
        n_stream_r: number of evaluated radial grid nodes in each streamline
            [integer].
        evap_rad: Evaporation radius of water away from the stellar 
            source(s). This is currently [10/4-2015] manually defined, but 
            the goal is to determine this distance recursively so various 
            luminosities can be used without inconsistency issues with this 
            freeze-out radius. [cm]
        cav_rad: Radius of the central cavity [AU].
        inn_rad: Inner radius of the model radial grid [cm].
        out_rad: Outer radius of the model radial grid [cm].  
        source_nr: Number of stellar sources [integer]. 
        gx: Numpy array of radial grid points. 
        gy: Numpy array of meridional grid points. 
        gz: Numpy array of azimuthal grid points. 
        prho: Power of the radial density distribution (in the case of a
            static envelope [float].
        rho0: Dust density at inn_rad [g/cm^3, float].
        r_c: Co-rotation radius of the infalling, rotating envelope [cm,float].
        m_dot: Accretion rate of material onto the central star(s) [g/s,float].
        m_star: Mass of central star.
        p_stars: 2D array of the star positions in cartesian coordinates 
            [star_nr[cm,cm,cm]]
        bare: If 'True', then use bare grain opacities as well
            as ice-grain opacities. If 'False', then use only ice-grains, 
            everywhere in model.
        rot_collapse: If 'True', then use a rotating collapse density
            strtucture. If 'False', then use a static envelope.
        cav_enable: If 'True', use an inner cavity of constant density.
            If 'False', use a either a static envelope or rotating infall
            all the way into inn_rad.           
        octree_indices: List of binary numbers for the base grid. If 1, the 
            cell is refined into an octree. 0 is then no refinement.
    Returns:
        rho: Volume density [g/cm^3] in a four-dim array. First three are the 
            spherical coordinates (r,phi,theta) while the last is the dust 
            species nr.
        theta_bounds: Numpy array of the meriodional streamline grid node
            positions [n_stream,n_stream_r].
        stream_r: One-dim Numpy array of radial streamline grid node positions,
            logarithmically spaced.   
        vel: Array of the velocity components in the case of a rotating 
            collapse.
        ref_nr: The total number of added cells to the grid, due to octree
            refinement. I.e. the total cell number is now nx*ny*nz + ref_nr.
    Raises:
        Nothing.
    Log:    
        30/3/2015: At first I simply decided the streamline bounds/nodes for 
        whatever r and theta value that was actually in the model grid,
        but this turned out to be unwise, as the meridional grid may 
        completely miss the greater radial distances of the chosen 
        streamlines. This would mean that you cannot choose the correct 
        streamline at greater distances, i.e. no density can be assigned.
        
        Instead, I will calculate a fine array of streamline (r,theta) 
        coordinates, or nodes if you will. For a given RADMC-3D grid node, 
        I will then loop through the streamline nodes positions and choose the
        closest. Eventually, some form of interpolation might be included. 
        This is very CPU costly, so optimize this scheme at some point.
    Important arrays & values:
        theta_bounds and stream_r are explained above.
        theta_0: The meridional starting positions of the streamlines. Runs 
            from [0,pi].
        theta_center_pos: The theta_0 of the streamline closest to the 
            current cell.            
        theta_cell: The theta_bounds of the streamline closest to the current
            cell.
        
    To-do:
        24/4-2015: Define m_star as the sum of stellar sources, in the case of 
            multiple stars?
        22/4-2015: 
            Implement plausible accretion-rates (currently too low).
            Fix apparent offset in the dust density near the midplane
            (when viewed in Paraview), skewed towards one side of the midplane.             
        3/7-2015:
            Are the octree node positions correct?
        
        Implement rotating collapse for only ice-grain case.
        
        13/8: Change vel to [n_cell,3] array instead of [n_cell*3] 1D array?           
    """

   
    cdef:
        int count
        int ref_ctr = 0 # Goes from 0 to length of octree list (-1 due to zeroindexing). 
        #Used to sort the rho_dust 1Dim list.
        int r_ctr_1,r_ctr_2,r_ctr_3,r_ctr_4 
        int base_ctr = 0
        double PI = 3.14159265359
        double PI_HALF = 3.14159265359/2.0
        double AU  = 1.49597871*10.0**13.0 #[cm]
        double M_SUN = 1.9891*10.0**33.0 #[g]
        double R_SUN = 6.955*10.0**10.0 #Solar radius [cm]
        double G = 6.67259*10.0**(-8.0) #	[cm^3 g-1 s-2]
        double min_dist
        double node_dist
        double x_stream
        double y_stream
        double z_stream
        double radmc3d_grid_theta
        double theta_center_pos          
        double dist_to_star_1
        double dist_to_star_2
        double rho_cav
        double phi_pos
        double x_c
        double y_c
        double z_c         
        double y_cs
        double r_cs
        double x_sphc, y_sphc, z_sphc 
        int octree_len
        double veli_r, veli_theta, veli_phi        
        double start
        double elapsed
        double sh_wall_1,sh_wall_2,sh_wall_3,sh_wall_4
        list x_sphc_tot_arr,y_sphc_tot_arr,z_sphc_tot_arr
        list x_sphc_arr,y_sphc_arr,z_sphc_arr
        
        double dx,dy,dz
                                
        list vel_stream        
        
        dict par
        set disk_1_idcs = set()
        set disk_2_idcs = set()
        
        Py_ssize_t ix
        Py_ssize_t iy
        Py_ssize_t iz
        Py_ssize_t x
        Py_ssize_t y
        Py_ssize_t i_theta_0
        Py_ssize_t ir
     
        #Using lists in cython is tricky, as memory errors can easily happen. 
        #Instead, numpy arrays are used.
        np.ndarray[DTYPE_t, ndim=1] theta_0 = np.zeros([n_stream], 
        dtype=np.float64)
        np.ndarray[DTYPE_t, ndim=1] stream_r = np.zeros([n_stream_r], 
        dtype=np.float64) #New addition
        np.ndarray[DTYPE_t, ndim=2] theta_bounds = np.zeros([n_stream,
        n_stream_r], dtype=np.float64)        
        np.ndarray[DTYPE_t, ndim=1] stream_rad = np.zeros(n_stream_r+1)
        np.ndarray[DTYPE_t, ndim=4] rho = np.zeros([nx, ny, 
            nz, ndust], dtype=np.float64)
        
        # In the case of octree refinement, we need to write the dust density 
        # directly to a 1D array, as writing out 
        # octree coordinates into a 3D array is not possible (e.g. consider a 
        # 15x10x5 grid where octree refinement in two separate cells have 
        # taken place. n_cell_true = 15*10*5+2*7= 764 versus 
        # n_cell_nd_array_wrong_estimate= 17*12*7= 1428).
        np.ndarray[DTYPE_t,ndim=2] rho_oct = np.zeros(
                                [nx*ny*nz+ref_nr, ndust], dtype=np.float64)
        np.ndarray[DTYPE_t,ndim=1] vel = np.zeros((nx*ny*nz+ref_nr)*3, 
            dtype=np.float64)  
                            
    start = time.time()  
    octree_len = nx*ny*nz+ref_nr #Length of the octree, incl. base-grid.
    
    #Find the increase in cellnumber due to oct-tree refinement. 
    octree = False
  
    if type(octree_indices) != bool:
        octree = True     
    else:
        octree_indices = np.zeros([nx*ny*nz])
    print 'ref_nr is %i and nx*ny*nz is %i' % (ref_nr, nx*ny*nz)      
    #Define functions to avoid interpreter overhead.
    sin = m_sin
    cos = m_cos
    sqrt = m_sqrt 
    asin = m_asin
    acos = m_acos
    isnan = np.isnan
   # assign_dust_dens_to_arr_dum = assign_dust_dens_to_arr
    
    phi_pos = 0.0  
    par = {'source_nr':source_nr,'evap_rad':evap_rad,'rho':rho,'rho0':rho0,
           'prho':prho,'rho_oct':rho_oct,'octree':octree,'bare':bare,
           'rot_collapse':rot_collapse,'m_dot':m_dot,'r_c':r_c,'m_star':m_star, 
           'cav_rad':cav_rad,'cav_enable':cav_enable, 'phi_pos':phi_pos,
<<<<<<< HEAD
           'p_stars':p_stars,'octree_len':octree_len}

    work_counter = 0  

 #   disk_dic = {'disk_dic_1':{'r_out': 5.*AU,'incl':0.0,#np.pi/2.*0.75,
 #   'azith_turn':0.0},#np.pi*1.07},
 #   #'disk_dic_2':{'r_out': 125.*AU,'incl':np.pi/2.*0.1,'azith_turn':3./2.*np.pi},
 #   'disk_dic_2':{'r_out': 5.*AU,'incl':0.0,'azith_turn':0.0}, 
 #   'disk': disk
   # }
=======
           'p_stars':p_stars,'octree_len':octree_len,'subli_list':subli_list,
           'disk_1_idcs':disk_1_idcs, 'disk_2_idcs':disk_2_idcs,
           'recurs_subli':recurs_subli}
>>>>>>> recurs_bare_allo
   
    if disk_dic['disk']:
        print 'Initializing disks..'
    par.update(**disk_dic)
#========================= Handle a rotating cloud collapse ===================
    vel_dic = defaultdict(list)     
    if rot_collapse:
        #Assign variable names to functions for speedup.
        find_streamline_dum = find_streamline
        print 'Creating a rotating collapse..'
        #The streamlines are azimuthally symmetric, so the streamline is 
        #only evaluated at a single azimuthal position.                   
        #Gather info on the computational costs.

        
        #Create the meridional coordinates at model outer radius, for the 
        #streamlines.

        theta_0 = np.arange(0.,1.,1./n_stream)
        theta_0[:] = theta_0[:]*PI_HALF
        theta_0[0] = 0.01 #Slight offset to avoid zero-division in 
        #trig. functions
        
        #The radial streamline grid wall positions are made.
        stream_rad = np.zeros(n_stream_r+1)

        stream_rad[:] = inn_rad*(out_rad/inn_rad)**(np.arange(
            n_stream_r+1)/(n_stream_r))
        
        #The grid node positions is computed as the center of the wall 
        #positions. 
     
        stream_r[:] = np.sqrt(stream_rad[0:n_stream_r]*
            stream_rad[1:n_stream_r+1])

        #From the radial streamlines node positions, the meridional streamline 
        #nodes are made. Derived from eq. 4.40 in Hartmann, 'Accretion 
        #processes..'.

        for y in xrange(len(theta_0)):         
            for ix in xrange(len(stream_r)):

                theta_bounds[y,ix] = acos(cos(theta_0[y])-(r_c/stream_r[ix])*
                (sin(theta_0[y])**2.0)*cos(theta_0[y]))
            
                #Streamlines should not cross into the other midplane, as the
                #material should suffer collisions and remain roughly at the 
                #midplane to form a disk. Slight offset is made to avoid zero-
                #division in trig. functions.
                if theta_bounds[y,ix] > PI_HALF:
                    theta_bounds[y,ix] = PI_HALF-0.000001

        #NaNs appear in theta_bounds if the radial points are too close in 
        #- arccos out-of-bounds errors. This is fine, since this means that the 
        #streamline is not defined at this radial distance, it has already hit 
        #the deck (the midplane). This is how I interpret the NaNs - the plots
        #seem very reasonable as well. 
  
        #Read the rotating collapse parameters into our par dictionary.
        par.update({'theta_bounds':theta_bounds,'stream_r':stream_r,
        'theta_0':theta_0,'phi_pos':phi_pos})                   
        
        #Super dictionary of the velocities.
        
        vel_dic = defaultdict(list) 

   
#========================= ASSIGN DUST DENSITY ================================
    
    #Radial shell wall values
    sh_wall_1 = 300.*AU
    sh_wall_2 = 700.*AU
    sh_wall_3 = 3000.*AU 
    sh_wall_4 = 9000.*AU
    par.update({'sh_wall_1':sh_wall_1,'sh_wall_2':sh_wall_2,
    'sh_wall_3':sh_wall_3,'sh_wall_4':sh_wall_4})
    for iz in xrange(nz):
      for iy in xrange(ny):
        
#---------Reset dics and counters for the next meridional cone search----------
        
        # Radial shell counter in the vel vector arrays. Reset after each 
        # radial search in a meridional cone is finished.   
        r_ctr_1 = 0 
        r_ctr_2 = 0 
        r_ctr_3 = 0 
        r_ctr_4 = 0
        #The meridional cones of the model.        
        #The semicones of grid cell nodes.
        #Semicone due to radial shell splitting.        
        vel_lev2_0 = defaultdict(list)
        vel_lev2_1 = defaultdict(list)  
        vel_lev2_2 = defaultdict(list)  
        vel_lev2_3 = defaultdict(list) 
       # print 'with %i gy[iy] is %.6f' % (iy,gy[iy])
 

        vel_tempo_dic ={'vel_lev2_0':vel_lev2_0,'vel_lev2_1':vel_lev2_1,
                     'vel_lev2_2':vel_lev2_2,'vel_lev2_3':vel_lev2_3,
                     'r_ctr_1':r_ctr_1,'r_ctr_2':r_ctr_2,'r_ctr_3':r_ctr_3,
                     'r_ctr_4':r_ctr_4
                     }
                    
        for ix in xrange(nx):               
            if octree:
                #If the grid cell is refined.
                if octree_indices[base_ctr] == 1:
                    #Assign the oct-tree refinement node positions.
                          
                    #The new grid centers are set, depending on the refinement 
                    #level. 
                    dx = (g_ix[ix+1]-g_ix[ix])/(2.**(levels+1)) #With e.g. 3 levels, this a dx 1/16 of the original r wall length.
                    dy = (g_iy[iy+1]-g_iy[iy])/(2.**(levels+1))      
                    dz = (g_iz[iz+1]-g_iz[iz])/(2.**(levels+1))      
                    
                    #Next, the refined node positions are given in three arrays.
                    for i in xrange(2**levels):
                        if i == 0:
                            x_sphc_tot_arr = [g_ix[ix]+dx] #First index
                            y_sphc_tot_arr = [g_iy[iy]+dy] #First index
                            z_sphc_tot_arr = [g_iz[iz]+dz] #First index
                        else:    
                            x_sphc_tot_arr.append(x_sphc_tot_arr[-1]+2.*dx)
                            y_sphc_tot_arr.append(y_sphc_tot_arr[-1]+2.*dy)
                            z_sphc_tot_arr.append(z_sphc_tot_arr[-1]+2.*dz)
                    
                    if levels == 1:
                            x_sphc_arr = x_sphc_tot_arr
                            y_sphc_arr = y_sphc_tot_arr
                            z_sphc_arr = z_sphc_tot_arr

                            for z_sphc,y_sphc,x_sphc in itertools.product(
                            z_sphc_arr, y_sphc_arr, x_sphc_arr):                                  
                                            
                                par.update({'x_sphc':x_sphc,'y_sphc':y_sphc,
                                'z_sphc':z_sphc,'rho_oct':rho_oct,'ix':ix,
                                'iy':iy,'iz':iz,
                                'ref_ctr':ref_ctr,
                                'vel_tempo_dic':vel_tempo_dic,'vel':vel, 
                                'disk_dic':disk_dic})
                                
<<<<<<< HEAD
                                rho_oct, vel, idx_ctr, work_counter, vel_tempo_dic = assign_dens_and_vel(par,**par)                                 

=======
                                rho_oct, vel, ref_ctr, vel_tempo_dic, subli_list,disk_1_idcs,disk_2_idcs = assign_dens_and_vel(par,**par)                                 
                                par.update({'subli_list':subli_list,
                                'disk_1_idcs':disk_1_idcs,
                                'disk_2_idcs':disk_2_idcs})
                                
>>>>>>> recurs_bare_allo
                    elif levels > 1:
                        for i,j,k in itertools.product(
                                 xrange(levels-1),xrange(levels-1),xrange(levels-1)):                   
                            
                        #Now that all the grid nodes are founds, we need to evaluate
                        #the octree refined subcells in the correct radmc3d order.
    
                            for zi,yi,xi in itertools.product(
                                xrange(2), xrange(2), xrange(2)):
            
                                x_sphc_arr = x_sphc_tot_arr[2*(xi+2*k):2*(xi+2*k)+2]
                                y_sphc_arr = y_sphc_tot_arr[2*(yi+2*j):2*(yi+2*j)+2]
                                z_sphc_arr = z_sphc_tot_arr[2*(zi+2*i):2*(zi+2*i)+2]
        
                                for z_sphc,y_sphc,x_sphc in itertools.product(
                                z_sphc_arr, y_sphc_arr, x_sphc_arr):                                  
                                    
                                    par.update({'x_sphc':x_sphc,'y_sphc':y_sphc,
                                    'z_sphc':z_sphc, 'rho_oct':rho_oct,'ix':ix,
                                    'iy':iy,'iz':iz,'ref_ctr':ref_ctr,
                                    'vel_tempo_dic':vel_tempo_dic,'vel':vel, 
                                    'disk_dic':disk_dic})
                                    
<<<<<<< HEAD
                                    rho_oct, vel, idx_ctr, work_counter, vel_tempo_dic = assign_dens_and_vel(par,**par)     
                                    
=======
                                    rho_oct, vel, ref_ctr, vel_tempo_dic, subli_list, disk_1_idcs, disk_2_idcs = assign_dens_and_vel(par,**par)     
                                    par.update({'subli_list':subli_list,
                                    'disk_1_idcs':disk_1_idcs,
                                    'disk_2_idcs':disk_2_idcs})           

>>>>>>> recurs_bare_allo
                        
                #If the grid cell is not refined we use the base grid.                
                elif octree_indices[base_ctr] != 1:
                    par.update({'x_sphc':gx[ix],'y_sphc':gy[iy],'z_sphc':gz[iz],
                                'rho_oct':rho_oct,'ix':ix,'iy':iy,'iz':iz,
                                'ref_ctr':ref_ctr,
                                'vel_tempo_dic':vel_tempo_dic,'vel':vel, 
                                'disk_dic':disk_dic})
                                    
<<<<<<< HEAD
                    rho_oct, vel, idx_ctr, work_counter, vel_tempo_dic = assign_dens_and_vel(par,**par)     
=======
                    rho_oct, vel, ref_ctr, vel_tempo_dic, subli_list, disk_1_idcs, disk_2_idcs = assign_dens_and_vel(par,**par)     
                    par.update({'subli_list':subli_list,
                    'disk_1_idcs':disk_1_idcs,'disk_2_idcs':disk_2_idcs})                                
>>>>>>> recurs_bare_allo
                      
            if not octree:             
                
                par.update({'x_sphc':gx[ix],'y_sphc':gy[iy],'z_sphc':gz[iz],
                                'rho_oct':rho_oct,'ix':ix,'iy':iy,'iz':iz,
                                'ref_ctr':ref_ctr,
                                'vel_tempo_dic':vel_tempo_dic,'vel':vel, 
                                'disk_dic':disk_dic})
                                
<<<<<<< HEAD
                rho, vel, idx_ctr, work_counter, vel_tempo_dic = assign_dens_and_vel(par,**par)
                                                
            oct_counter += 1
=======
                rho, vel, ref_ctr, vel_tempo_dic, subli_list, disk_1_idcs, disk_2_idcs = assign_dens_and_vel(par,**par)
                par.update({'subli_list':subli_list,
                'disk_1_idcs':disk_1_idcs,'disk_2_idcs':disk_2_idcs})                                
            base_ctr += 1
>>>>>>> recurs_bare_allo
            
        #After loop is finished in radial coordinate, construct the meridional subarray.
        if iz == 0:    
            vel_dic[iy] = [vel_tempo_dic['vel_lev2_0'],vel_tempo_dic['vel_lev2_1'],
                    vel_tempo_dic['vel_lev2_2'],vel_tempo_dic['vel_lev2_3']]               
        
    assert octree_len == ref_ctr, 'vel array is offset by %i' %(octree_len - ref_ctr)   
    elapsed = (time.time() - start)
    print ("Time elapsed in dust_density.cyth (rot_collapse for loop) = " 
    "%.6f seconds" % elapsed)     
    
    #Return the density distribution, plus information on the streamline nodes.
    if not octree:   
        return rho, theta_bounds, stream_r,vel,vel_dic
    if type(octree_indices) != bool:
        assert int(np.count_nonzero(rho_oct)/2.0) == int(len(rho_oct[:,0])),(
        'Some grid cells are not filled. Missing cells are %i out of %i (i.e. real grid size is half this, as we have two dust species.)' % (len(rho_oct[:,0])-np.count_nonzero(rho_oct)/2.0,len(rho_oct[:,0])))
        
        assert (nx*ny*nz + ref_nr) == int((len(rho_oct[:,0])+len(rho_oct[:,1]))/2.),(
            "Octree refined dust density array 'rho_oct' has unexpected length." 
            " Has %i should have %i." % (int((len(rho_oct[:,0])+len(rho_oct[:,1]))/2.), nx*ny*nz + ref_nr))       
        print 'ref_nr',ref_nr        
        return rho_oct, theta_bounds, stream_r,vel,vel_dic, disk_1_idcs, disk_2_idcs

def assign_dens_and_vel(dict par,double x_sphc, double y_sphc, double z_sphc,
                        np.ndarray[DTYPE_t, ndim=2] rho_oct, 
                        Py_ssize_t ix, Py_ssize_t iy, Py_ssize_t iz,
                        int ref_ctr, dict vel_tempo_dic,
                        dict disk_dic,np.ndarray[DTYPE_t, ndim=2] p_stars,
                        np.ndarray[DTYPE_t,ndim=1] vel, int octree_len,
                        double sh_wall_1,double sh_wall_2,double sh_wall_3,
<<<<<<< HEAD
                        double sh_wall_4,**kwargs):
=======
                        double sh_wall_4, int source_nr, set subli_list, 
                        set disk_1_idcs, set disk_2_idcs
                        **kwargs):
>>>>>>> recurs_bare_allo
    cdef:
        double x_c, y_c, z_c
        double veli_r,veli_theta, veli_phi
        list vel_vect
        double dist_to_star_1,dist_to_star_2
#        double x_sphc
        
    x_c, y_c, z_c = transf_sph_cart(x_sphc,y_sphc,z_sphc)

    #if not recurs_subli:
    if source_nr == 2:
        dist_to_star_1 = m_sqrt((p_stars[0,0] - x_c)**2. 
            + (p_stars[0,1] - y_c)**2. + (p_stars[0,2] -z_c)**2.)
        dist_to_star_2 = m_sqrt((p_stars[1,0] - x_c)**2. 
            + (p_stars[1,1] - y_c)**2. + (p_stars[1,2] -z_c)**2.)        
    else:
        dist_to_star_2 = 1e80 #Dummy large value so the real star distance is
        #always the minimum.
        dist_to_star_1 = x_sphc

    par.update({'dist_to_star_1':dist_to_star_1,'dist_to_star_2':dist_to_star_2,
                'x_sphc':x_sphc}) 
                         
    #Change dictionary values.    
    if (disk_dic['disk'] and dist_to_star_1 <= disk_dic['disk_dic_1']['r_out'] 
    or disk_dic['disk'] and dist_to_star_2 <= disk_dic['disk_dic_2']['r_out']):
        tilt_disk_args = {
        'p_stars': p_stars[0,:],
        'r_sphr': x_sphc,
        'theta': y_sphc,
        'phi': z_sphc,
        'incl': disk_dic['disk_dic_1']['incl'],
        'r_fill': disk_dic['disk_dic_1']['r_fill'],  
        'azith_turn': disk_dic['disk_dic_1']['azith_turn']
        }
 
        disk_dic['disk_dic_1']['ins_disk'],disk_dic['disk_dic_1']['r_disk'],disk_dic['disk_dic_1']['theta_disk'] = tilt_disk(**tilt_disk_args)
        
        tilt_disk_args.update({'incl':disk_dic['disk_dic_2']['incl'],
                               'r_fill':disk_dic['disk_dic_2']['r_fill'],
                               'azith_turn': disk_dic['disk_dic_2']['azith_turn'],
                               'p_stars':p_stars[1,:]})
                               
        disk_dic['disk_dic_2']['ins_disk'],disk_dic['disk_dic_2']['r_disk'],disk_dic['disk_dic_2']['theta_disk'] = tilt_disk(**tilt_disk_args)
        
        #Save disk indice if the current coord is inside a disk.
        if disk_dic['disk_dic_1']['ins_disk']:
            disk_1_idcs.add(ref_ctr)
        elif disk_dic['disk_dic_2']['ins_disk']:
            disk_2_idcs.add(ref_ctr)
                    
    par.update({'x_c':x_c,'y_c':y_c,'z_c':z_c,'disk_dic':disk_dic})   

<<<<<<< HEAD
    rho_oct, veli_r, veli_theta, veli_phi = assign_dust_dens_to_arr(**par)
    vel[idx_ctr],vel[idx_ctr+octree_len],vel[idx_ctr+ 2*octree_len] = veli_r, veli_theta, veli_phi
    idx_ctr += 1    
    work_counter += 1
=======

    rho_oct, veli_r, veli_theta, veli_phi, subli_list = assign_dust_dens_to_arr(**par)
        
    vel[ref_ctr],vel[ref_ctr+octree_len],vel[ref_ctr+ 2*octree_len] = veli_r, veli_theta, veli_phi
    ref_ctr += 1
>>>>>>> recurs_bare_allo

    if iz == 0:

        vel_vect = [x_sphc, y_sphc, veli_r, veli_theta, veli_phi]
       
        if 0.0 <= x_sphc < sh_wall_1:
            vel_tempo_dic['vel_lev2_0'][vel_tempo_dic['r_ctr_1']] = vel_vect
            vel_tempo_dic['r_ctr_1'] += 1
        elif sh_wall_1 <= x_sphc < sh_wall_2:
            vel_tempo_dic['vel_lev2_1'][vel_tempo_dic['r_ctr_2']] = vel_vect
            vel_tempo_dic['r_ctr_2'] += 1
        elif sh_wall_2 <= x_sphc < sh_wall_3:
            vel_tempo_dic['vel_lev2_2'][vel_tempo_dic['r_ctr_3']] = vel_vect
            vel_tempo_dic['r_ctr_3'] += 1
        elif sh_wall_3 <= x_sphc <= sh_wall_4:
            vel_tempo_dic['vel_lev2_3'][vel_tempo_dic['r_ctr_4']] = vel_vect
            vel_tempo_dic['r_ctr_4'] += 1
<<<<<<< HEAD
    
        #vel_tempo_dic.update({'vel_lev2_0':vel_lev2_0,'vel_lev2_1':vel_lev2_1,
        #                 'vel_lev2_2':vel_lev2_2,'vel_lev2_3':vel_lev2_3,
        #                 'r_ctr_1':r_ctr_1,'r_ctr_2':r_ctr_2,'r_ctr_3':r_ctr_3,
        #                 'r_ctr_4':r_ctr_4
        #                 })
                     
    return rho_oct, vel, idx_ctr, work_counter, vel_tempo_dic
=======
                         
    return rho_oct, vel, ref_ctr, vel_tempo_dic, subli_list, disk_1_idcs, disk_2_idcs
>>>>>>> recurs_bare_allo


def tilt_disk(np.ndarray[DTYPE_t, ndim=1] p_stars, 
              double r_sphr, double theta, double phi, double incl, 
              double r_fill, double azith_turn):
        cdef:
            double AU  = 1.49597871*10.0**13.0 #[cm]
            double azimuth_turn, theta_transf, theta_1
            double phi_native, phi_1
            double x_c, y_c, z_c
            double x_c_zrot, y_c_xrot, z_c_xrot
            double y_c_zrot
            double theta_upp, theta_low    
            
        #Get meridional coordinate, relative to the disk midplane axis
        #around star

        x_c, y_c, z_c = transf_sph_cart(r_sphr,theta,phi)

        x_c = x_c-p_stars[0]
        y_c = y_c-p_stars[1]        
        z_c = z_c-p_stars[2]
        
       # r_disk= sqrt((x_c)**2.+
       # (y_c)**2.+(z_c)**2.)
        
      #  theta_transf = m_acos(z_c/r_disk)

        phi_native = m_atan2(y_c,x_c) #Input is np.arctan(y,x)
           
        if phi_native < 0.0:
            phi_native = 2.*PI+phi_native
        elif phi_native >= 0.0:
            phi_native = phi_native
        phi_1 = phi_native

        #Transform coordinate system by rotating around axes.
        #First z-axis rotation (i.e. turn in azimuth).
        x_c_zrot = x_c*m_cos(azith_turn) + y_c*m_sin(azith_turn)
        y_c_zrot = -x_c*m_sin(azith_turn) + y_c*m_cos(azith_turn) 

        #phi_native = np.arctan2(y_c_zrot,x_c_zrot) #Input is np.arctan(y,x)
        phi_native = m_atan2(y_c_zrot,x_c_zrot) #Input is np.arctan(y,x)atan2  
        if phi_native < 0.0:
            phi_native = 2.*PI+phi_native
        elif phi_native >= 0.0:
            phi_native = phi_native

        y_c_xrot = y_c_zrot*m_cos(incl) + z_c*m_sin(incl)
        z_c_xrot = -y_c_zrot*m_sin(incl) + z_c*m_cos(incl)

        r_disk= m_sqrt((x_c_zrot)**2.+
            (y_c_xrot)**2.+(z_c_xrot)**2.)
   
        theta_transf = m_acos(z_c_xrot/r_disk)
      
        theta_upp = PI/2. + PI/12.
        theta_low = PI/2. - PI/12.

        phi_native = m_atan2(y_c_xrot,x_c_zrot) #Input is np.arctan(y,x)
           
        if phi_native < 0.0:
            phi_native = 2.*PI+phi_native
        elif phi_native >= 0.0:
            phi_native = phi_native
        
        if (theta_transf <= theta_upp and theta_transf >= theta_low
        or r_disk<=r_fill):# 12.0*AU and disk_dic['r_out']> 50.*AU): #Last condition set to avoid a hole in the middle, when there is poor resolution.            
            inside_disk = True
        else:
            inside_disk = False             

        return inside_disk, r_disk, theta_transf

def disk_dens(dict disk_dic, double rho_border,double r_disk):
    """
    This function returns a dust density for a disk inserted into the grid
    at a specific location. Since the density of this model is only radially
    dependent, any tilting or turning of the disk (in meridional and azithumal
    directions) need to be coordinated with the call of this function.
    Args:
        rho_border: Density at the border. Controls the density value scale.
        disk_dic: Dictionary containing disk parameters.
    Returns:
        Dust density at radial distance. 
    """
    #Coordinate conversion.
      
    cdef:
        double z_disk
        double H_d     
        double disk_rho
        double AU  = 1.49597871*10.0**13.0 #[cm]

    #2D is not advised unless good resolution of the disk is provided.
    #Otherwise artefacts appear.    
    two_D = True
    flr = 0.25      
    if two_D:
        z_disk = r_disk*m_cos(disk_dic['theta_disk'])        
        disk_dic['H_R_10'] = 0.90*AU #Disk scale height at 10 AU.
    
    #Radial density model.
    disk_dic['p'] = -0.70 #Powerlaw exponent of the dust density.
    disk_dic['surf_dens'] = (disk_dic['rho_0']*
        rho_border*(r_disk/(10.*AU))**disk_dic['p']) 
    
    if two_D:
        #2D density model. Standard disk, no flaring.     
        H_d = disk_dic['H_R_10']*(r_disk/(10.*AU))**(1.+flr) #Was divided by 10 AU before
        disk_rho = (disk_dic['surf_dens']/(H_d*m_sqrt(2.*PI)))*np.exp(
            -z_disk**2./(2.*H_d**2.)) 
    else:
        #Create only radial dependence.
        disk_rho = disk_dic['surf_dens']
    return disk_rho


def transf_sph_cart(double x_sphc, double y_sphc, double z_sphc):#, 
 #   conv_vel=False, **kwargs):
    """Converts spherical coordinates to cartesian coordinates.
        Args:
            x_sphc: spherical radial coordinate. 
            y_sphc: spherical meridional coordinate. 
            z_sphc: spherical azimuthal coordinate. 

        Returns: x, y, z (cartesian coordinates in units of the length unit
            x_sphc had). 
    """    
    cdef:
        double x_c
        double y_c
        double z_c
        
    x_c = x_sphc*m_sin(y_sphc+1e-50) * m_cos(z_sphc)
    y_c = x_sphc*m_sin(y_sphc+1e-50) * m_sin(z_sphc) 
    z_c = x_sphc*m_cos(y_sphc+1e-50)
       
        #Convert spherical velocity vectors to cartesian.
        #From http://www.astrosurf.com/jephem/library/li110spherCart_en.htm
        #and "Accretion processes page 70"        
    
    return x_c, y_c, z_c
  
def assign_dust_dens_to_arr(int source_nr, double x_c, double y_c, double z_c, 
    double phi_pos, double evap_rad, np.ndarray[DTYPE_t, ndim=4] rho, 
    double rho0, double prho, np.ndarray[DTYPE_t, ndim=2] rho_oct, 
<<<<<<< HEAD
    Py_ssize_t ix, Py_ssize_t iy, Py_ssize_t iz,int work_counter, 
    np.ndarray[DTYPE_t, ndim=2] p_stars, octree, bare,cav_enable, rot_collapse, 
    disk_dic_1, disk_dic_2, double cav_rad,double m_dot, double m_star, double r_c, theta_bounds= None,
    theta_0=None, stream_r=None, disk = False,**kwargs):
=======
    Py_ssize_t ix, Py_ssize_t iy, Py_ssize_t iz, Py_ssize_t ref_ctr, 
    np.ndarray[DTYPE_t, ndim=2] p_stars, octree, bare,cav_enable, rot_collapse,
    double dist_to_star_1, double dist_to_star_2, double x_sphc, 
    dict disk_dic_1, dict disk_dic_2, double cav_rad,double m_dot, double m_star, 
    set subli_list, double r_c, theta_bounds= None,
    theta_0=None, stream_r=None,recurs_subli =False, disk = False,**kwargs):
>>>>>>> recurs_bare_allo
    """This function finds the density at the current position and returns
    the modified dust density array.
    Args:
        ref_ctr: The octree cell index of the cell being investigated.
        phi_pos: The phi value used in the rot. coll. case.
        
        See getDustDensity_cyth for the other args.
    Returns:
        The dust density array with the added dust density value of the current
        cell. Also returns the 3D velocity vectors as three double values. If
        not a rotating collapse, this is three dummy doubles of value 0.
    Raises:
        None.
        """
    
    cdef:
        int condit_counter
        double AU  = 1.49597871*10.0**13.0 #[cm] 
        double rho_cell
        double veli_r
        double veli_theta
        double veli_phi
        Py_ssize_t subl_idx
        
    veli_r, veli_theta, veli_phi = 0.,0.,0. #Dummy values in case of no rot. col.
    condit_counter = 0

    #First, find the dust density at the current grid position.
    if rot_collapse:
        rho_cell, veli_r, veli_theta, veli_phi = find_streamline(source_nr,
            x_c,y_c,z_c,evap_rad, phi_pos, theta_bounds, stream_r,theta_0,
            cav_rad, cav_enable, m_dot, m_star, r_c)      
        condit_counter += 1    
            
    elif cav_enable and x_sphc/AU <= cav_rad:
        rho_cell = rho0 * (cav_rad)**prho  
        condit_counter += 1 
    elif not cav_enable or x_sphc/AU > cav_rad:
        rho_cell = rho0 * (x_sphc/AU)**prho
        condit_counter += 1
    if disk: 
        rho_border = rho0 * (cav_rad)**prho    

        if (dist_to_star_1 <= disk_dic_1['r_out'] and disk_dic_1['ins_disk']
        and dist_to_star_1 > disk_dic_1['r_in']):
            r_disk = disk_dic_1['r_disk']
            rho_cell = disk_dens(disk_dic_1,rho_border,r_disk)
            
        elif (dist_to_star_2 <= disk_dic_2['r_out'] and disk_dic_2['ins_disk']
        and dist_to_star_2 > disk_dic_2['r_in']):           
            r_disk = disk_dic_2['r_disk']

            rho_cell = disk_dens(disk_dic_2,rho_border,r_disk)
 
    assert condit_counter < 2, ('Two or more conditions were satisfied - there' 
    'can be only one!')

    #Assign the dust opacities dependent on the freeze-out radius and grid 
    #refinement scheme.

    if octree and bare and recurs_subli:
        if ref_ctr in subli_list:
            rho_oct[ref_ctr, 1] = rho_cell
            rho_oct[ref_ctr, 0] = 1e-50                         
            subli_list.remove(ref_ctr)
            
        else:
            rho_oct[ref_ctr, 0] = rho_cell
            rho_oct[ref_ctr, 1] = 1e-50            
            
        return rho_oct,veli_r, veli_theta, veli_phi, subli_list


    elif not octree and bare and not recurs_subli:
        if (source_nr == 2 and dist_to_star_1 > evap_rad and dist_to_star_2 > evap_rad or
        source_nr == 1 and dist_to_star_1 > evap_rad):                    
            rho[ix,iy,iz, 0] = rho_cell
            rho[ix,iy,iz, 1] = 1e-50
        else:                         
            rho[ix,iy,iz, 1] = rho_cell
            rho[ix,iy,iz, 0] = 1e-50
        return rho,veli_r, veli_theta, veli_phi 
        
    elif not octree and not bare:                   
        rho[ix,iy,iz,0] = rho_cell
        return rho,veli_r, veli_theta, veli_phi 
        
    elif octree and bare and not recurs_subli:
        if (source_nr == 2 and dist_to_star_1 > evap_rad and dist_to_star_2 > evap_rad or
        source_nr == 1 and dist_to_star_1 > evap_rad):                      
            rho_oct[ref_ctr, 0] = rho_cell
            rho_oct[ref_ctr, 1] = 1e-50
        else:                         
<<<<<<< HEAD
            rho_oct[work_counter, 1] = rho_cell
            rho_oct[work_counter, 0] = 1e-50
        return rho_oct,veli_r, veli_theta, veli_phi 
        
    if octree and not bare:                   
        rho_oct[work_counter] = rho_cell     
        return rho_oct,veli_r, veli_theta, veli_phi

=======
            rho_oct[ref_ctr, 1] = rho_cell
            rho_oct[ref_ctr, 0] = 1e-50
        return rho_oct,veli_r, veli_theta, veli_phi, subli_list        
        
    elif octree and not bare:                   
        rho_oct[ref_ctr] = rho_cell     
        return rho_oct,veli_r, veli_theta, veli_phi, subli_list
>>>>>>> recurs_bare_allo
    
def find_streamline(int source_nr, double x_c, double y_c, double z_c, 
    double evap_rad, double phi_pos, np.ndarray[DTYPE_t, ndim=2] theta_bounds, 
    np.ndarray[DTYPE_t, ndim=1] stream_r, np.ndarray[DTYPE_t, ndim=1] theta_0, 
    double cav_rad, cav_enable, double m_dot, double m_star, double r_c,
    lime=True):
    """
    This functions finds the nearest streamline to the input cell, 
    in a rotating collapse model.
    Args:
        See getDustDensity_cyth.
    Returns:
        Four doubles: The dust density [g] of the current cell, based on the
        nearest streamline along with the 3 velocity vector components of 
        the cell [cm/s].     
    """
   
    cdef: 
        double rho_cell
        double PI = 3.14159265359
        double PI_HALF = 3.14159265359/2.0
        double AU  = 1.49597871*10.0**13.0 #[cm]
        double M_SUN = 1.9891*10.0**33.0 #[g]
        double R_SUN = 6.955*10.0**10.0 #Solar radius [cm]
        double G = 6.67259*10.0**(-8.0) #	[cm^3 g-1 s-2]
        double x_sphc,y_sphc,radmc3d_grid_theta, z_sphc
        double min_dist
        double z_stream
        double x_stream
        double node_dist
        double r_cell
        double theta_center_pos
        double vel_r, vel_theta, vel_phi
        double vel_x, vel_y, vel_z
        double x_cs, z_cs
        double thet_node_dist,r_node_dist
        
        double phi_s,theta_s


        Py_ssize_t ir, i_r_cell
        Py_ssize_t i_theta_0
           
    x_sphc = m_sqrt(x_c**2.+y_c**2.+z_c**2.)
    min_dist=10.0**60.0
         
    #Convert from cartesian to spherical.
    y_sphc = m_acos(z_c/x_sphc)
    #The azimuthal coordinate is difficult, as the value depends on the quadrant.
    #z_sphc = m_atan(y_c/x_c)
  
    if y_c>= 0:
        z_sphc = math.atan2(y_c,x_c) #Should return m_atan(y_c/x_c) between -pi and +pi  
    elif y_c < 0:
        z_sphc = math.atan2(y_c,x_c)+2.*PI #Ensure range is [0,2 pi]. 
    #Mirror-symmetry around the midplane is used. 
    radmc3d_grid_theta = 10.0 
    if y_sphc <= PI_HALF:
        radmc3d_grid_theta = y_sphc
    elif y_sphc > PI_HALF:
        radmc3d_grid_theta = PI_HALF - (y_sphc - PI_HALF)
    assert radmc3d_grid_theta < 9.0,'error in y_sphc assignment'
    assert radmc3d_grid_theta >= 0 and radmc3d_grid_theta <= PI/2.0, (
    'Error in coordinate transformation of meridional coordinate, ' 
    'theta is now %f while input was %f' % (radmc3d_grid_theta,y_sphc))                     
    
    #New algorithm for search

    for ir in xrange(len(stream_r)):
        r_node_dist = abs(x_sphc-stream_r[ir])
        if r_node_dist < min_dist:
            min_dist = r_node_dist
            r_cell = stream_r[ir]
            i_r_cell = ir
    
    min_dist = 10.0**60.0

    for i_theta_0 in xrange(len(theta_0)):
        thet_node_dist = abs(radmc3d_grid_theta - theta_bounds[i_theta_0, i_r_cell]) 
        
        #If the new streamline node is closer than the 
        #previously chosen node, we assign it instead.
        
        if thet_node_dist < min_dist:
            min_dist = thet_node_dist
            theta_center_pos = theta_0[i_theta_0]
            theta_cell = theta_bounds[i_theta_0, i_r_cell]
  
    assert theta_center_pos < PI_HALF, ("Streamline evaluated "
    "at wrong side of the midplane.")
  
    rho_cell = m_dot/(4.0*PI*m_sqrt(
            G*m_star*(r_cell)**3.0))*(
            2.0-r_c/r_cell*m_sin(theta_center_pos)**2.0)**(
            -0.5)*(
            1-r_c/r_cell*m_sin(theta_center_pos)**2.0+
            2.0*m_cos(theta_center_pos)**2.0/
            ((r_cell)/r_c))**(-1.0)

          
    #Find velocity vector of a particle in the rotating collapse.
    #See eq. 4.31, 4.33 and 4.35 (page 70) of "Accretion Processes" book.
    
    #Division by 100 for conv to SI units (cm to m). Remember that both 
    #vel_theta and vel_phi are normalized to r. See "Accretion Processes"       
    vel_r = (-m_sqrt(G*m_star/r_cell)*m_sqrt(1+m_cos(theta_cell)/
        m_cos(theta_center_pos)))/100.

    vel_theta = (m_sqrt(G*m_star/r_cell)*(m_cos(theta_center_pos)-
        m_cos(theta_cell))*m_sqrt((m_cos(theta_center_pos)+m_cos(theta_cell))/
        (m_cos(theta_center_pos)*(m_sin(theta_cell))**2.)))/100.
        
    vel_phi = (m_sqrt(G*m_star/r_cell)*m_sqrt(1- m_cos(theta_cell)/
        m_cos(theta_center_pos))*m_sin(theta_center_pos)/m_sin(theta_cell))/100.
      
    #Convert spherical velocity vectors to cartesian.
    #From http://www.astrosurf.com/jephem/library/li110spherCart_en.htm
    #and "Accretion processes page 70"
    #Bear in mind that vel_phi and vel_theta include some of the terms from astrosurf. eq.  
    #vel_theta = -vel_theta #The used eq. used another meridional coordinate system.
    theta_s = radmc3d_grid_theta #y_sphc #Only evaluate upper midplane
    
    #Since the sph-cart conversion eq. has a different meridional range and direction,
    #i need to convert the meriodional coordinates and vel vectors before I calculate the cart vels.
    # The two corrections below gave non expected vels    
   # theta_s = PI/2.-theta_s #midplane towards pole is [0,pi/2] in conv. system.
   # vel_thetat = -vel_theta #Opposite unit vector direction
    
    phi_s = z_sphc
    assert phi_s >= 0.0 and phi_s <= 2*PI, 'phi_s has wrong range.'
    #The azimuthal range must be from [0, 2pi] as usual, but the meridional
    #is from [-pi/2,pi/2] which requires some conversion.
    #theta_s = abs(theta_s -PI/2.)
    assert theta_s >= 0.0 and theta_s <= PI/2., 'theta_s has wrong range.'
    
    #Based on my own derivations with standard spherical coordinate system.
    vel_x = (vel_r*m_sin(theta_s)*m_cos(phi_s) + m_cos(theta_s)*m_cos(phi_s)*vel_theta
        - m_sin(phi_s)*vel_phi)
    vel_y = (vel_r*m_sin(theta_s)*m_sin(phi_s) + m_cos(theta_s)*m_sin(phi_s)*vel_theta +
        m_cos(phi_s)*vel_phi)
    vel_z = vel_r*m_cos(theta_s) - m_sin(theta_s)*vel_theta
    
    #TODO: Why is this correct? Are the coordinate systems not different?
    #Based on the coordinate system of      
    #http://www.astrosurf.com/jephem/library/li110spherCart_en.htm

    #TODO vel values correct? Sanity check them. 
    
    
    #TODO Question: Should the dust density follow the streamline, as if the radial 
    #distance was cav_rad or should it go with a constant density inside, in 
    #the case of a rotating collapse?
    if x_sphc/AU <= cav_rad and cav_enable:
        x_sphc = cav_rad*AU
        radmc3d_grid_theta = PI/3.*1.07#Set to low (not the disk, but a 
        #streamline from above) constant density inside the cavity.  
        min_dist = 10.0**60.0
        
        for ir in xrange(len(stream_r)):
            r_node_dist = abs(x_sphc-stream_r[ir])
            if r_node_dist < min_dist:
                min_dist = r_node_dist
                r_cell = stream_r[ir]
                i_r_cell = ir
        
        min_dist = 10.0**60.0
    
        for i_theta_0 in xrange(len(theta_0)):
            thet_node_dist = abs(radmc3d_grid_theta - theta_bounds[i_theta_0, i_r_cell]) 
            
            #If the new streamline node is closer than the 
            #previously chosen node, we assign it instead.
            
            if thet_node_dist < min_dist:
                min_dist = thet_node_dist
                theta_center_pos = theta_0[i_theta_0]
                theta_cell = theta_bounds[i_theta_0, i_r_cell]
      
        assert theta_center_pos < PI_HALF, ("Streamline evaluated "
        "at wrong side of the midplane.")
      
        rho_cell = m_dot/(4.0*PI*m_sqrt(
                G*m_star*(r_cell)**3.0))*(
                2.0-r_c/r_cell*m_sin(theta_center_pos)**2.0)**(
                -0.5)*(
                1-r_c/r_cell*m_sin(theta_center_pos)**2.0+
                2.0*m_cos(theta_center_pos)**2.0/
                ((r_cell)/r_c))**(-1.0)

    
    if z_c <0.0:
        vel_z = -vel_z

    assert lime, 'lime not engaged'    

    return rho_cell, vel_r, vel_theta, vel_phi 

                    
def insert_octree(np.ndarray[DTYPE_t, ndim=1] gx, 
                  np.ndarray[DTYPE_t, ndim=1] gy, 
                  np.ndarray[DTYPE_t, ndim=1] gz, int nx, int ny, int nz, 
                  double oct_rad, int levels,
                  np.ndarray[DTYPE_t, ndim=2] p_stars, int source_nr):
    """This function inserts oct-tree refinement into an existing grid.
    Note the subtle difference between the two outputs.
    Be aware that the length of octree_key is not the total nr of cells in the
    refined grid. octree_key contains 1's everytime we go a level down, i.e.
    the length of octree_key will be larger than the total refined grid cell
    number.
    
    Output:
           indices: 1D numpy array of binary numbers for the base grid. 
           0 denotes no refinement, while 1 denotes that the cell is refined.
           
           octree_key: 1D numpy array of the full octree as printed in 
           amr_grid.inp.
           
           """
    
    #11/5-2015. Currently implementing the octree_sort() looping routine. 
    #the goal is to have the octree written out (series of 0s and 1s for 
    #amr_grid.inp) in the interesting regions.    
    

    cdef: 
        double AU  = 1.49597871*10.0**13.0 #[cm]
        Py_ssize_t ix
        Py_ssize_t iy
        Py_ssize_t iz
        Py_ssize_t ir
        Py_ssize_t i_theta_0
        int ndata

        
    #Define functions to avoid interpreter overhead.
      # ------- Set Global Variables
    #global indices, pos1, pos2, #noctree, octree_key
    cos = m_cos
    sin = m_sin
    sqrt = m_sqrt 
    asin = m_asin
    acos = m_acos
    isnan = np.isnan
    
    print 'Performing octree refinement..'

    # ------- Initialise arrays

    ndata = nx*ny*nz #Total cell number in global grid
    indices  = np.zeros(ndata,dtype=np.int64) #np.int64 #One long list of indices, which 
    #represents the global grid.
    #indarr   = np.arange(len(gx)) #Appears to only be used in amr_sort, 
    #which is only relevant for reading ramses files.
    pos1     = 0L #Indices index nr. Start at 0, eventually go through all cells in base grid.
    pos2     = 0L #octree_key index nr., i.e. will go through all cells in the complete refinement.
                  #Final length is unknown at start.
    
    noctree = round(10000) #round to get an exact int?
    octree_key  = np.zeros(noctree,dtype=np.int64)#np.int64
  #  octree_key  = np.zeros(noctree,dtype=np.int8)
    assert noctree == len(octree_key), ('unexpected octree array length. noctree is %i'
  ', while len(octree) is %i' % (noctree, len(octree_key)))
    index = 1
    octree_detector = 0
    
    #with octree, we have to write out in the order x,y,z (usually I write out z,y,x).

    for iz in xrange(nz):    
        for iy in xrange(ny):
            for ix in xrange(nx):
                assert noctree == len(octree_key), ('unexpected octree array length. noctree is %i'
                ', while len(octree_key) is %i' % (noctree, len(octree_key)))      
                
                x_c, y_c, z_c = transf_sph_cart(gx[ix],gy[iy],gz[iz])
               # x_c = sin(gy[iy]+1e-50) * cos(gz[iz]) * gx[ix]
               # y_c = sin(gy[iy]+1e-50) * sin(gz[iz]) * gx[ix]
               # z_c = cos(gy[iy]+1e-50) * gx[ix]
                
                if source_nr == 1:
                    dist_to_star_1 = m_sqrt(x_c**2.+y_c**2.+z_c**2.)
                    dist_to_star_2 = 1e80
                if source_nr == 2:
                    dist_to_star_1 = m_sqrt((p_stars[0,0] - x_c)**2. 
                        + (p_stars[0,1] - y_c)**2. + (p_stars[0,2] -z_c)**2.)
                    dist_to_star_2 = m_sqrt((p_stars[1,0] - x_c)**2. 
                        + (p_stars[1,1] - y_c)**2. + (p_stars[1,2] -z_c)**2.)
  
                if (source_nr==2 and dist_to_star_1 > oct_rad and dist_to_star_2 > oct_rad or
                source_nr==1 and dist_to_star_1 > oct_rad):
                    indices[pos1] = 0       #0 Means no refinement for the base grid.
                    octree_key[pos2] = 0    #Naturally, this translates as no refinement in the final octree.
                    #We update the index numbers to the next cell in both base grid and octree.
                    pos1 += 1  
                    pos2 += 1
                    
                    #If the octree grid array has reached the limit of allocated length,
                    #add array length.
                    if pos2 == noctree: 
                        octree_key, noctree = _allocate_more(10000,octree_key, noctree)
                        assert noctree == len(octree_key), ('unexpected octree array length. noctree is %i'
                        ' while len(octree_key) is %i' % (noctree, len(octree_key))) 
                elif (source_nr==2 and (dist_to_star_1 <= oct_rad or dist_to_star_2 <= oct_rad)
                or source_nr==1 and dist_to_star_1 <= oct_rad):
                    octree_detector = 1 #Flag that octree refinement has been done.
                    #Refine one level.
                    if levels == 1:
                        indices[pos1] = 1
                        pos1 += 1
                        octree_key,pos1,pos2,noctree = ref_branch_with_leaves(octree_key,pos1,pos2,noctree)
                        assert noctree == len(octree_key), ('unexpected octree_key array length. noctree is %i'
                        ', while len(noctree) is %i' % (noctree, len(octree_key))) 
                    elif levels >1:
                        indices,octree_key,pos1,pos2,noctree = ref_branch_with_branches(indices,octree_key,pos1,pos2,levels,noctree)

                          #  index = 1 #? Not sure how index should be used, check
                                      # code.
                          #  void = _amrsort(r[index,:],dx[index],indarr[index])
                else:
                    raise ValueError("Something happened in the octree levels. Stellar distance to star 1 is %f" % (dist_to_star_1/AU))# Non-Recursive)# %i" % count)
    
    #After the base grid has been traversed and the octree constructed (as a 
    #long list of binary numbers to be read by radmc3d) we need to remove 
    #unused space in octree_key, as the array size has to be exact.
    
    octree_key = octree_key[0:pos2] # Include only the array length that has
        #been traversed.
          
          # ------- Assert that we've been through all data
    if octree_detector != 1:
        print ('No octree refinement has been performed. Perhaps your '
        'radial base-grid is too coarse to pick up the oct_rad of %.1f AU' 
        %(oct_rad/AU))
    if pos1 != ndata:
        raise ValueError('pos1 and ndata are not equal. It appears that we '
        'have not traversed entire base grid?')
            
    print 'Length of octree_key is %i' % len(octree_key)
       
    return indices, octree_key
    
def ref_branch_with_branches(indices,octree_key,
                            int pos1, int pos2, int levels, int noctree):
    """This helper function refines a single cell as many times as indicated
    by the input, 'levels'.
    
    In the description the levels are givens as level 0 (lowest level, dungeon 
    level). """

    #First, pass information to base grid array that this cell is being refined.
    indices[pos1] = 1                   
    pos1 += 1

    #First declare that the base grid is being refined.
    octree_key[pos2] = 1
    pos2 += 1
    if pos2 == noctree: 
        octree_key, noctree = _allocate_more(10000,octree_key, noctree) 
    
    #Then move in to refine the grid levels.
    if levels == 2:            
        for x in xrange(8):        
            #level 1    
            octree_key,pos1,pos2,noctree = ref_branch_with_leaves(octree_key,pos1,pos2,noctree)
            #level 0. A 1 is declared in ref_branch_with_leaves.                    

    elif levels == 3:
        for x in xrange(8):
            #level 2.
            octree_key[pos2] = 1
            pos2 += 1
            if pos2 == noctree: 
                octree_key, noctree = _allocate_more(10000,octree_key, noctree)    
                
            for x in xrange(8):        
                #level 1    
                octree_key,pos1,pos2,noctree = ref_branch_with_leaves(octree_key,pos1,pos2,noctree)
                #level 0. A 1 is declared in ref_branch_with_leaves.                    
    elif levels == 4:
        for x in xrange(8):
            #level 3.
            octree_key[pos2] = 1
            pos2 += 1
            if pos2 == noctree: 
                octree_key, noctree = _allocate_more(10000,octree_key, noctree)    

            for x in xrange(8):
                #level 2.
                octree_key[pos2] = 1
                pos2 += 1
                if pos2 == noctree: 
                    octree_key, noctree = _allocate_more(10000,octree_key, noctree)    
                    
                for x in xrange(8):        
                    #level 1    
                    octree_key,pos1,pos2,noctree = ref_branch_with_leaves(octree_key,pos1,pos2,noctree)
                    #level 0. A 1 is declared in ref_branch_with_leaves.                    
                  
    return indices,octree_key,pos1,pos2,noctree
    
def ref_branch_with_leaves(octree_key,
                           int pos1, int pos2, int noctree):
    """This helper functions refines a single cell."""                 

    octree_key[pos2] = 1
    pos2 += 1
    if pos2 == noctree: 
        octree_key, noctree = _allocate_more(10000,octree_key, noctree)
        assert noctree == len(octree_key), ('unexpected octree_key array length. noctree is %i' #TODO this assert appears redundant as _allocate_more has its own assert statement.
        ', while len(octree_key) is %i' % (noctree, len(octree_key))) 
    for x in xrange(8):
        octree_key[pos2] = 0
        pos2 += 1
        if pos2 == noctree: 
            octree_key, noctree = _allocate_more(10000,octree_key, noctree)
            assert noctree == len(octree_key), ('unexpected octree_key array length. noctree is %i'
            'while len(octree_key) is %i' % (noctree, len(octree_key))) 
            
    return octree_key,pos1,pos2,noctree      
                      
def _allocate_more(nmore,octree_key, noctree):
  """
  Helper Function.
  Allocates more nmore array length for octree_key (octree grid), while also
  updating noctree to the new value.  
  
  Credit: S. Frimann.
  """
  # ------- Set Global Variables
  
  noctree += round(nmore)
  octree_key   = np.append(octree_key,np.zeros(nmore,dtype=np.int64))
  assert noctree == len(octree_key), ('unexpected octree_key array length. noctree is %i'
  ', while len(octree_key) is %i' % (noctree, len(octree_key)))
  
  return octree_key, noctree


           
def coords(double gx, double gy, double gz):
    cdef:
        double x_c
        double y_c
        double z_c
    sin = m_sin
    cos = m_cos        
    x_c = sin(gz+1e-50) * cos(gy) * gx
    y_c = sin(gz+1e-50) * sin(gy) * gx
    z_c = cos(gz+1e-50) * gx
    return x_c, y_c, z_c

@cython.boundscheck(False)
@cython.wraparound(False)  
def distss(double x, double y, double z):
    
    cdef double res    
    res = m_sqrt(x**2. +y**2.+z**2.)
    
    return res

def octree_order(double x_0, double x_1, double y_0, double y_1, double z_0,
                 double z_1,int levels, vol=False, **kwargs):
    """This function supplies the travel order of the octree.
    Assumes that the cell is octree refined.
    
    Args:
        x_0 = lower radial cell wall of the base cell
        x_1 = upper radial cell wall of the base cell
        y_0 = lower meridional cell wall of the base cell
        y_1 = upper meridional cell wall of the base cell
        z_0 = lower azimuth cell wall of the base cell
        z_1 = upper azimuth cell wall of the base cell        
    """    
    cdef:
        list x_tot_arr,y_tot_arr,z_tot_arr
        list x_cent,y_cent,z_cent
        double dx,dy,dz
        double x_ce,y_ce,z_ce
        int xi,yi,zi
        int i,j,k
        list x_list = []
        list y_list = []
        list z_list = []

        
    #Assign the oct-tree refinement node positions.
          
    #The new grid centers are set, depending on the refinement 
    #level. 
    #With e.g. 3 levels, this a dx 1/16 of the original r wall length.      
    dx = (x_1-x_0)/(2.**(levels+1)) 
    dy = (y_1-y_0)/(2.**(levels+1))      
    dz = (z_1-z_0)/(2.**(levels+1))      
    if vol:
        vol_list = []
    #Next, the refined center node positions are given in three arrays.
    for i in xrange(2**levels):
        if i == 0:
            #First index
            x_tot_arr = [x_0+dx] 
            y_tot_arr = [y_0+dy] 
            z_tot_arr = [z_0+dz]
            #Find cell wall
            x_wall_arr = [x_0] 
            y_wall_arr = [y_0] 
            z_wall_arr = [z_0]            
        else:
            #Incrementally find next cell node position
            x_tot_arr.append(x_tot_arr[-1]+2.*dx) 
            y_tot_arr.append(y_tot_arr[-1]+2.*dy)
            z_tot_arr.append(z_tot_arr[-1]+2.*dz)
            #Find cell wall
            x_wall_arr.append(x_0+2.*dx) 
            y_wall_arr.append(y_0+2.*dy) 
            z_wall_arr.append(z_0+2.*dz)
            
    if levels == 1:
            x_cent = x_tot_arr
            y_cent = y_tot_arr
            z_cent = z_tot_arr
            
            x_wal = x_wall_arr
            y_wal = y_wall_arr
            z_wal = z_wall_arr
                            
            for z_ce,y_ce,x_ce in itertools.product(
            z_cent, y_cent, x_cent):
                x_list.append(x_ce)                                  
                y_list.append(y_ce)                                  
                z_list.append(z_ce)
            if vol:
                for z_wi, y_wi, x_wi in itertools.product(
                xrange(len(z_wal)-1), xrange(len(y_wal)-1), xrange(len(x_wal)-1)):
                    diff_r3   = x_wal[x_wi+1]**3. - x_wal[x_wi]**3.
                    diff_cost = m_cos(y_wal[y_wi]) - m_cos(y_wal[y_wi+1])
                    diff_phi  = z_wal[z_wi+1] - z_wal[z_wi]
                    vol_list.append(1./3.*diff_r3*diff_cost*diff_phi)
                
                
    elif levels > 1:
        for i,j,k in itertools.product(
                 xrange(levels-1),xrange(levels-1),xrange(levels-1)):                   
            
        #Now that all the grid nodes are founds, we need to evaluate
        #the octree refined subcells in the correct radmc3d order.
    
            for zi,yi,xi in itertools.product(
                xrange(2), xrange(2), xrange(2)):
    
                x_cent = x_tot_arr[2*(xi+2*k):2*(xi+2*k)+2]
                y_cent = y_tot_arr[2*(yi+2*j):2*(yi+2*j)+2]
                z_cent = z_tot_arr[2*(zi+2*i):2*(zi+2*i)+2]
                
                x_wal = x_wall_arr[2*(xi+2*k):2*(xi+2*k)+2]
                y_wal = y_wall_arr[2*(xi+2*k):2*(xi+2*k)+2]
                z_wal = z_wall_arr[2*(xi+2*k):2*(xi+2*k)+2]
                #Todo test that they sample the wall correctly
                for z_ce,y_ce,x_ce in itertools.product(
                    z_cent, y_cent, x_cent):                                    
                    
                    x_list.append(x_ce)                                  
                    y_list.append(y_ce)                                  
                    z_list.append(z_ce)
                    if vol:
                        #Volume determination. Find walls by going dx,dy,dz 
                        #forward and backwards from the cell center.
                        diff_r3   = (x_ce+dx)**3. - (x_ce-dx)**3.
                        diff_cost = m_cos(y_ce-dy) - m_cos(y_ce+dy)
                        diff_phi  = (z_ce+dz) - (z_ce-dz)
                        vol_list.append(1./3.*diff_r3*diff_cost*diff_phi)
    if not vol:
        return x_list,y_list,z_list        
    elif vol:
        return vol_list

def _amrsort(r,dx,indarr):
  """
  Helper function for amrsort. Deals with recursive calls (Courtesy of S. Frimann).
  """
  # ------- Set Global Variables
  global indices, pos1, pos2, noctree, octree

  # ------- min/max in all directions
  amin = np.argmin(r,axis=0)
  amax = np.argmax(r,axis=0)
  
  minx, maxx = r[amin[0],0]-dx[amin[0]]/2, r[amax[0],0]+dx[amax[0]]/2
  miny, maxy = r[amin[1],1]-dx[amin[1]]/2, r[amax[1],1]+dx[amax[1]]/2
  minz, maxz = r[amin[2],2]-dx[amin[2]]/2, r[amax[2],2]+dx[amax[2]]/2

  grid_size = (maxx-minx)/2
  # ------- Bin edges
  binx = np.linspace(minx,maxx,num=3)
  biny = np.linspace(miny,maxy,num=3)
  binz = np.linspace(minz,maxz,num=3)
    
  # ------- Indices that fall into the bins
  ix = np.searchsorted(binx,r[:,0],side='right')
  iy = np.searchsorted(biny,r[:,1],side='right')
  iz = np.searchsorted(binz,r[:,2],side='right')
  
  # ------- Looping
  zz = np.array([1, 1, 1, 1, 2, 2, 2, 2])
  yy = np.array([1, 1, 2, 2, 1, 1, 2, 2])
  xx = np.array([1, 2, 1, 2, 1, 2, 1, 2])
  nndata = len(dx)
  for iix,iiy,iiz in zip(xx,yy,zz):
    index = (ix == iix) & (iy == iiy) & (iz == iiz)
    count = np.count_nonzero(index)
    if count == 1:
      indices[pos1] = indarr[index]
      octree[pos2] = 0
      pos1 += 1; pos2 += 1
      if pos2 == noctree: _allocate_more(10000)
    elif count > 7:
      octree[pos2] = 1
      pos2 += 1
      if pos2 == noctree: _allocate_more(10000)
      void = _amrsort(r[index,:],dx[index],indarr[index])
    else:
      raise ValueError("Something happened in the octree levels. Recursive %i" % count)

  return 0

def find_evap_rad_oct(np.ndarray[DTYPE_t, ndim=1] vol,
                   np.ndarray[DTYPE_t, ndim=2] rhodust,
                   np.ndarray[DTYPE_t, ndim=2] dust_temp, int temp, 
<<<<<<< HEAD
                   Py_ssize_t i_dust, int source_nr):   
=======
                   Py_ssize_t i_dust, int source_nr,recurs_subli=False,**kwargs):   
>>>>>>> recurs_bare_allo
    """This is a specific function made to find the mass fraction of dust above
    90 K. This Cython implementation speeds up the code by a factor 1000(!).
    Note that you need to assign c-types to the variables and constants in 
    order to obtain the desired speed-up.
    Args:
          vol = 3-dim array of cell volumes.
          rhodust = 4-dim array of dust density. Last dim is dust species nr.
          dust_temp = 4-dim array of dust temperature. Last dim is singular, 
                    shape(dust_temp) = (x,y,z,1). I do not currently understand 
                    why this is necessary (see 
                    radmc3dPy.analyze.radmc3dData.scalarfieldReader()).
          ndust = dust species nr.
          temp = cutoff dust temperature
    Returns: 
          mas = The total dust mass above temp. [g] 
            
          """
    from numpy import isnan      
    cdef: 
        Py_ssize_t i
        double mas, cell_mass, vol_tot, rad
        list vol_all, mass
        set subli_list = set()
        
    mass = []
    vol_all = []
    above = True
    below = False

<<<<<<< HEAD
    for i in range(len(vol)):
        try:
            if above:
                if dust_temp[i,i_dust]>= temp:                                     
                    res = vol[i] * rhodust[i,i_dust]           
                    vol_all.append(vol[i])                      
                    mass.append(res)                 
            elif below:
                if dust_temp[i,i_dust]< temp:                                     
                    res = vol[i] * rhodust[i,i_dust]           
                    vol_all.append(vol[i])                      
                    mass.append(res)     
        
        except:
            print 'Error: No conditions set in find_evap_rad()'
            
   # vol_tot = np.sum(vol_all[:])    
    #rad = (3./(4.*np.pi)*(vol_tot))**(1./3.
=======
    for i in xrange(len(vol)):
        if above:
            if dust_temp[i,i_dust]>= temp:                                     
                cell_mass = vol[i] * rhodust[i,i_dust]           
                vol_all.append(vol[i])                      
                mass.append(cell_mass)           
                subli_list.add(i)
        elif below:
            if dust_temp[i,i_dust]< temp:                                     
                res = vol[i] * rhodust[i,i_dust]           
                vol_all.append(vol[i])                      
                mass.append(res)     
    
>>>>>>> recurs_bare_allo
    vol_tot = np.sum(vol_all)    
    if source_nr == 1:    
        rad = (3./(4.*PI)*(vol_tot))**(1./3.)
    #If two stars I divide the volume up between the stars.
    if source_nr == 2:
        rad = (3./(4.*PI)*(vol_tot/2.))**(1./3.)
     
    mas = np.sum(mass)
    assert isnan(mas) == False, 'Result is NaN.' 
                
    return mas, rad

def find_evap_rad(np.ndarray[DTYPE_t, ndim=3] vol,
                   np.ndarray[DTYPE_t, ndim=4] rhodust,
                   np.ndarray[DTYPE_t, ndim=4] dust_temp, int temp, 
                   Py_ssize_t i_dust, int source_nr):
    """This is a specific function made to find the mass fraction of dust above
    90 K. This Cython implementation speeds up the code by a factor 1000(!).
    Note that you need to assign c-types to the variables and constants in 
    order to obtain the desired speed-up.
    Args:
          vol = 3-dim array of cell volumes.
          rhodust = 4-dim array of dust density. Last dim is dust species nr.
          dust_temp = 4-dim array of dust temperature. Last dim is singular, 
                    shape(dust_temp) = (x,y,z,1). I do not currently understand 
                    why this is necessary (see 
                    radmc3dPy.analyze.radmc3dData.scalarfieldReader()).
          ndust = dust species nr.
          temp = cutoff dust temperature
    Returns: 
          mas = The total dust mass above temp. [g] 
            
          """
    from numpy import isnan      
          
    cdef Py_ssize_t i,j,k
    cdef int nx,nz,ny
    cdef double mas

    above = True
    below = False
    
    nx = len(dust_temp[:,0,0,:])
    ny = len(dust_temp[0,:,0,:])
    nz = len(dust_temp[0,0,:,:])
    mass = []
    vol_all = []
    for i in xrange(nx):
        for j in xrange(ny):
            for k in xrange(nz):
                try:
                    if above:
                        if dust_temp[i,j,k,i_dust]>= temp:                                     
                            res = vol[i,j,k] * rhodust[i,j,k,i_dust]           
                            vol_all.append(vol[i,j,k])                      
                            mass.append(res)             
                    elif below:
                        if dust_temp[i,j,k,i_dust]< temp:                                     
                            res = vol[i,j,k] * rhodust[i,j,k,i_dust]           
                            vol_all.append(vol[i,j,k])                      
                            mass.append(res)
                except:
                    print 'Error: No conditions set in find find_evap_rad()'

    vol_tot = np.sum(vol_all)    
    if source_nr == 1:    
        rad = (3./(4.*PI)*(vol_tot))**(1./3.)
    #If two stars I divide the volume up between the stars.
    if source_nr == 2:
        rad = (3./(4.*PI)*(vol_tot/2.))**(1./3.)
 
    mas = np.sum(mass)
    assert isnan(mas) == False, 'Result is NaN.' 
                
    return mas, rad