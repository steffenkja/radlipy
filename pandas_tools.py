#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 26 13:07:57 2017

@author: Steffen_KJ
"""

import pandas as pd
import numpy as np


def add_nan_row(dfs):
    """
    This function appends a row of NaNs to a set of dataframes, so they all
    have equal length. Useful, as many dataframe actions will not work with
    uneven dataframe row lengths.
    """

    m_len = max(map(len, dfs))
    for df in dfs:
        if len(df) < m_len:
            diff = m_len-len(df)
            x, y = df.shape
            for i in xrange(diff):
                df.loc[len(df)+i] = [np.nan for n in xrange(y)]
    return dfs
