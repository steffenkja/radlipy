# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 13:56:35 2015

@author: Steffen_KJ
"""

import warnings

def fxn():
    warnings.warn("deprecated", DeprecationWarning)

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    fxn()