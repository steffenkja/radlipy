# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 13:35:46 2015

@author: Steffen_KJ
"""

C_L = 299792458. #speed of light in vacuum [m/s]
k_b = 1.3806488*10.**(-23.)
AU = 149597870700. # meters
PC = 3.08567758*10.**(16.) #meter
M_SUN = 1.989e30 #kg
M_H2 = 2.0*1.660538921e-27 #Mass of molecular hydrogen in kg
L_SUN = 3.826*10.**26.0 # Watts
