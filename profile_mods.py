#!/usr/bin/python2
# -*- coding: utf-8 -*-

"""
Created on Sat Jun 25 11:58:20 2016

@author: Steffen_KJ
"""
from __future__ import print_function
import line_profiler
import os
import numpy as np
build = True
onedrive = "OneDrive - Københavns Universitet"
try:
    if os.path.expanduser('~') == '/Users/Steffen_KJ':
        os.system('python "/Users/Steffen_KJ/%s/PhD/Scientific_software/Python/modules/cython_setup.py" build_ext --inplace' % onedrive)
elif os.path.expanduser('~') == '/astro/cmh154':
    os.system('python /astro/cmh154/python/cython_setup.py build_ext --inplace')
else:
    build = False
assert build, "Build of cython_mod failed"

import cython_mod
    
noctree = 0 #round to get an exact int?
octree_key = []# np.zeros(noctree,dtype=np.int64)#np.int64
pos2 = 0L
levels = 3
res1 = []
args = {'noctree':noctree,'octree_key':octree_key,'pos2':pos2,'levels':levels}
#octree_key,noctree,pos2,res1 = cython_mod.construct_octree(octree_key,noctree,pos2,levels,res1)
octree_key = cython_mod.construct_octree(octree_key,levels)

print(octree_key)
profile = line_profiler.LineProfiler(cython_mod.construct_octree)
#profile.runcall(cython_mod.construct_octree,octree_key,noctree,pos2,levels,res1)
profile.runcall(cython_mod.construct_octree,octree_key,levels)

profile.print_stats()        
stats = profile.get_stats()
assert len(stats.timings) > 0, "No profile stats."
for key, timings in stats.timings.items():
    assert len(timings) > 0, "No stats for DustDensity."

import pstats, cProfile


#cProfile.runctx("cython_mod.construct_octree(octree_key,noctree,pos2,levels)", globals(), locals(), "Profile.prof")

#s = pstats.Stats("Profile.prof")
#s.strip_dirs().sort_stats("time").print_stats()