# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 12:28:34 2015

@author: Steffen_KJ
"""

AU  = 1.49597871*10.0**13.0 #[cm]
PC = 3.08567758*10.**18. #[cm]
R_SUN = 6.955*10.0**10.0 #Solar radius [cm]
M_SUN = 1.9891*10.0**33.0 #Solar Mass [g]
L_SUN = 3.828*10**33.0 #Erg/s         
#Seconds in a year, assuming 365.242199 days pr. year.
SEC_PR_YEAR = 60.0*60.0*24.0*365.242199
M_H2 = 2.0*1.660538921e-24 #Mass of molecular hydrogen in grams
PI = 3.14159265358979323846 
H_2_mass = 1.00794*2.*1.660539040*10**(-24) #atomic unit * 2* g/atomic unit
M_P = 1.00794*1.660539040*10**(-24) # Mass of a proton, atomic unit * g/atomic unit
K_B = 1.38064853*10.0**(-16.0) # Boltzmann constant, erg/K
GRAV_CNST = 6.67259*10.0**(-8.0) # [cm^3 g-1 s-2]
