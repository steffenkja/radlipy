#!/usr/bin/python2
# -*- coding: utf-8 -*-

from __future__ import division
import matplotlib.pyplot as plt
import numpy as np
import os
from scipy.integrate import dblquad
import sys
#import skimage.measure as sk_measure
from gistfile1 import fitgaussian
from time import localtime, strftime
import matplotlib.cm as cm
import scipy.spatial as sci_spat
import warnings
try:
    if os.path.expanduser('~') == '/Users/Steffen_KJ':
        os.system('python "/Users/Steffen_KJ/Dropbox/PhD/Scientific_software/Python/modules/cython_setup.py" build_ext --inplace')

    elif os.path.expanduser('~') == '/groups/astro/cmh154':
        os.system('python /groups/astro/cmh154/python/cython_setup.py build_ext --inplace')
except:
    print >> sys.stderr, "You are not in the expected wd"
    print >> sys.stderr, "Please relocate"
    exit()
# import radmc3dPy
import itertools
import astropy.io.fits as pyfits
import copy
from cgs_cnst import (AU, M_SUN, SEC_PR_YEAR, L_SUN, R_SUN, M_P, K_B,
                      GRAV_CNST, H_2_mass, PC)
try:
    from numpy import *
except:
    print 'ERROR'
    print ' Numpy cannot be imported '
    print ' To use the python module of RADMC-3D you need to install Numpy'

from radmc3dPy.natconst import *
try:
    from matplotlib.pylab import *
except:
    print ' WARNING'
    print ' matploblib.pylab cannot be imported ' 
    print ' To used the visualization functionality of the python module of RADMC-3D you need to install matplotlib'
    print ' Without matplotlib you can use the python module to set up a model but you will not be able to plot things or'
    print ' display images'

import radmc3d_analyze
from radmc3dPy.setup import *
from cgs_cnst import M_H2
# from matplotlib import pyplot
from matplotlib.ticker import AutoMinorLocator
from astropy.io.votable import parse_single_table
import matplotlib.ticker as plticker

# cython: linetrace=True
profile_active = False
import subprocess
from subprocess import Popen, PIPE, STDOUT
from cython_mod import (transf_sph_cart, find_evap_rad,
                        find_evap_rad_oct, z_x_axis_rot)
#from lime_tools import load_fits_im
#Load Robert Kern's line profiler
##%load_ext line_profiler
#import line_profiler
#Set compiler directives (cf. http://docs.cython.org/src/reference/compilation.html)
#from Cython.Compiler.Options import directive_defaults

#directive_defaults['linetrace'] = True
#directive_defaults['binding'] = True

##%%cython -a -f --compile-args=-DCYTHON_TRACE=1
#We need to define the macro CYTHON_TRACE=1 (cf. http://docs.cython.org/src/reference/compilation.html)

#-----------------------------------------------------------------------------#
#Define classes.


def problem_setup_dust(cav_rad, octree, p_stars, oct_rad_1, oct_rad_2,
                       subli_list, model='', nci=30, disk=False, binary=False,
                       cav_enable=False, streamline_view_enable=False,
                       cyth_profiler=True,
                       source_nr=2, recurs_subli=False, tor_dic=None,
                       azi_lim_1=0.0, azi_lim_2=np.pi, oct_inst=None,
                       only_wrt_radcm3dinp=False, use_radmc3d_gas_em=False,
                       **kwargs):
    """
    SKJ: This is a copy of radmc3dPy.setup.problemSetupDust() with moderate
    alterations.

    Function to set up a dust model for RADMC3D

    INPUT:
    ------
        model : Name of the model that should be used to create the density structure.
                The file should be in a directory from where it can directly be imported 
                (i.e. the directory should be in the PYTHON_PATH environment variable or
                it should be in the current working directory)
                and the file name should be 'model_xxx.py', where xxx stands for the string
                that should be specified in this variable
        cav_enable: Enable an inner envelope cavity of constant density.
        cav_rad: Radius of the inner envelope cavity.
        nci: Number of radial grid walls in the inner cavity grid.         
        binary : If True input files will be written in binary format, if False input files are
                written as formatted ascii text. 

        writeDustTemp: If True a separate dust_temperature.inp/dust_tempearture.binp file will be
                written under the condition that the model contains a function getDustTemperature() 
    OPTIONS:
    --------
        Any varible name in problem_params.inp can be used as a keyword argument.
        At first all variables are read from problem_params.in to a dictionary called ppar. Then 
        if there is any keyword argument set in the call of problem_setup_dust the ppar dictionary 
        is searched for this key. If found the value belonging to that key in the ppar dictionary 
        is changed to the value of the keyword argument. If no such key is found then the dictionary 
        is simply extended by the keyword argument. Finally the problem_params.inp file is updated
        with the new parameter values.
 
       
    FILES WRITTEN DURING THE SETUP:
    -------------------------------
        dustopac.inp          - dust opacity master file
        wavelength_micron.inp - wavelength grid
        amr_grid.inp          - spatial grid
        stars.inp             - input radiation field
        dust_density.inp      - dust density distribution
        radmc3d.inp           - parameters for RADMC3D (e.g. Nr of photons to be used, scattering type, etc)

    STEPS OF THE SETUP:
    -------------------
        1) Create the spatial and frequency grid
        2) Create the master opacity file and calculate opacities with the Mie-code if necessary
        3) Set up the input radiation field (generate stars.inp)
        4) Calculate the dust density
        5) If specified;  calculatest the dust temperature (e.g. for gas simulations, or if it is taken from an
            external input (e.g. from another model))
        6) Write all output files
    """
    # Read the parameters from the problem_params.inp file 
    modpar = radmc3d_analyze.readParams()
    # Make a local copy of the ppar dictionary
    ppar = modpar.ppar
    if model == '':
        print 'ERROR'
        print 'No model name is given'
        return

    if not ppar:
        print 'ERROR'
        print 'problem_params.inp was not found'
        return
# --------------------------------------------------------------------------------------------
#   If there is any additional keyword argument (**kwargs) then check
#   if there is such key in the ppar dictionary and if is change its value that of
#   the keyword argument. If there is no such key in the ppar dictionary then add the keyword
#   to the dictionary
# --------------------------------------------------------------------------------------------
    if binary:
        modpar.setPar(['rto_style', '3', '', ''])

    if kwargs:
        for ikey in kwargs.keys():
            modpar.ppar[ikey] = kwargs[ikey]        
            if type(kwargs[ikey]) is float:
                modpar.setPar([ikey, ("%.7e"%kwargs[ikey]), '', ''])
            elif type(kwargs[ikey]) is int:
                modpar.setPar([ikey, ("%d"%kwargs[ikey]), '', ''])
            elif type(kwargs[ikey]) is str:
                modpar.setPar([ikey, kwargs[ikey], '', ''])
            elif type(kwargs[ikey]) is list:
                dum = '['
                for i in range(len(kwargs[ikey])):
                    if type(kwargs[ikey][i]) is float:
                        dum = dum + ("%.7e"%kwargs[ikey][i])
                    elif type(kwargs[ikey][i]) is int:
                        dum = dum + ("%d"%kwargs[ikey][i])
                    elif type(kwargs[ikey][i]) is str:
                        dum = dum + (kwargs[ikey][i])
                    else:
                        print ' ERROR '
                        print ' Unknown data type in '+ikey
                        print kwargs[ikey][i]

                    if i<len(kwargs[ikey])-1:
                        dum = dum + ', '
                dum = dum + (']') 
                modpar.setPar([ikey, dum, '', ''])

        modpar.writeParfile()  
        ppar = modpar.ppar

    if not only_wrt_radcm3dinp:    
    # -------------------------------------------------------------------------
    # Create the grid
    # -------------------------------------------------------------------------
        # Call instance for grid construction    
        grid = radmc3d_analyze.radmc3dGrid()
        
        # Make wavelength grid
        grid.makeWavelengthGrid(ppar=ppar)
    
        # Make spatial grid
        # Note that if cav_rad is enabled, then grid.nx will change 
        # (more radial cells added in cavity region).
        grid.make_spatial_grid(ppar=ppar,cav_rad=cav_rad,nci=nci, 
                               cav_enable=cav_enable,
                               source_nr=source_nr, disk=disk)
        
        # Write the spatial grid to the file amr_grid.inp
        if not octree:    
            grid.write_spatial_grid(octree=octree,oct_rad_1=oct_rad_1,
                                    oct_rad_2=oct_rad_2,
                                    p_stars=p_stars,source_nr=source_nr)
    
            octree_indices = []
    
        if octree:
            octree_indices,n_branches,ref_nr = grid.write_spatial_grid(
                    octree=octree, oct_rad_1=oct_rad_1, oct_rad_2=oct_rad_2,
                    p_stars=p_stars, source_nr=source_nr, tor_dic=tor_dic,
                    azi_lim_1=azi_lim_1, azi_lim_2=azi_lim_2,
                    oct_inst=oct_inst)
                   
    # --------------------------------------------------------------------------------------------
    # Dust opacity
    # --------------------------------------------------------------------------------------------
        if ppar.has_key('dustkappa_ext'):
            opac=radmc3d_analyze.radmc3dDustOpac()
            #Master dust opacity file
            opac.writeMasterOpac(ext=ppar['dustkappa_ext'], 
                                 scattering_mode_max=ppar['scattering_mode_max'])
        else:
            opac=radmc3d_analyze.radmc3dDustOpac()
            # Calculate the opacities and write the master opacity file
            opac.makeOpac(ppar=ppar)
    
    # --------------------------------------------------------------------------------------------
    # Create the input radiation field (stars at this point) 
    # --------------------------------------------------------------------------------------------
    
        stars = radmc3d_analyze.radmc3dStars(ppar=ppar)
        stars.getStellarSpectrum(tstar=ppar['tstar'], rstar=ppar['rstar'],
                                 wav=grid.wav)
    
    # --------------------------------------------------------------------------------------------
    # Try to get the specified model
    # --------------------------------------------------------------------------------------------
        try:
            import model_IRAS_16293 as mdl
        except:
            print 'model_IRAS_16293.py not found in python path'        
    
        data = radmc3d_analyze.radmc3dData(grid)
    # --------------------------------------------------------------------------------------------
    # Create the dust density distribution 
    # --------------------------------------------------------------------------------------------
        
        # If we have octree refinement, then DustDensity() need help to
        # construct the correct dust density multidim array.
    
        if octree:     
            model_dir = os.getcwd()        
        else:
            ref_nr = 0
        data.rhodust, theta_bounds, stream_r, vel, vel_dic, disk_1_idcs, disk_2_idcs, tor_idcs = mdl.getDustDensity(
                grid=grid, ppar=ppar, octree_indices=octree_indices,
                ref_nr=ref_nr,p_stars=p_stars,subli_list=subli_list,
                oct_inst=oct_inst)
    
    # --------------------------------------------------------------------------------------------
    # Now write out everything 
    # --------------------------------------------------------------------------------------------
    
        #Frequency grid
        grid.writeWavelengthGrid()
    
        #Input radiation field
        stars.writeStarsinp(wav=grid.wav, pstar=ppar['pstar'], tstar=ppar['tstar'])
        #Dust density distribution
        
        ###SKJ edit [1/6/2015]: OBS! Changed the scalarfield writer methods, see bottom of this file.    
        if not octree:
            data.writeDustDens(binary=binary)
        if octree:
            wrt_dust_density(dust_density=data.rhodust, vel=vel,
                             vel_dic=vel_dic, gy=grid.y, model_dir=model_dir,
                             binary=binary)

    write_radmc3d_inp(modpar=modpar)

# --------------------------------------------------------------------------------------------
# Calculate optical depth for diagnostics purposes
# --------------------------------------------------------------------------------------------
    if only_wrt_radcm3dinp:
        return
    elif not only_wrt_radcm3dinp:
        stars = radmc3d_analyze.radmc3dStars()
        stars.readStarsinp()
        pwav = stars.findPeakStarspec()[0]
        if octree and not streamline_view_enable:
            if use_radmc3d_gas_em:
                return (octree_indices, ref_nr, disk_1_idcs, disk_2_idcs,
                        tor_idcs, data.rhodust)
            else:
                return (octree_indices, ref_nr, disk_1_idcs, disk_2_idcs,
                        tor_idcs)
        if not octree and streamline_view_enable:
            return theta_bounds, stream_r
        if octree and streamline_view_enable:
            return (theta_bounds, stream_r, octree_indices, disk_1_idcs,
                    disk_2_idcs, tor_idcs)


def wrt_dust_density(dust_density, vel, vel_dic, gy, model_dir, binary=False,
                     wrt_H2_dens=False):
    """
    Function to write dust_density.inp or .binp given model directory and
    radmc3d model object.

    Input:
      model_dir = radmc3d model directory (type: str)
      model = pyradmc3d model object. Should contain the method
              'get_dust_density' which returns dust_density array for
              dust_density.inp
    Keywords:
      binary = if true writes a binary file instead of an ascii file
      (Default: False)
    Returns:
      ndata = number of cells
      n_dust = number of dust species
    Credit:
        This module is an extended and custom version of a dust_distribution
        writer module by Soren Frimann.
    """

    dust_1 = dust_density[:, 0]
    dust_2 = dust_density[:, 1]

    shape = dust_density.shape
    if len(shape) == 1:
        n_dust = 1
        ndata = shape[0]

    elif len(shape) == 2:
        n_dust = shape[1]
        ndata = shape[0]
        tot_dust_dens = np.zeros(ndata)
        dust_density = np.ravel(dust_density.T)
        del_indices_1 = np.where(dust_1==0)[0]
        del_indices_2 = np.where(dust_2==0)[0]

        for i in xrange(len(dust_1)):
            assert dust_1[i] < 2.0e-50 or dust_2[i] < 2.0e-50, ( 
            'octree sequence error at index i. dust_1[i] is %.3e '
            'and dust_2[i] is %.3e' % (dust_1[i], dust_2[i]))

            tot_dust_dens[i] = dust_1[i] + dust_2[i]
        tot_dust_dens = np.ravel(tot_dust_dens.T)

        assert len(del_indices_1) == 0 and len(del_indices_2) == 0, (
        'Zeros detected in dust_density octree, %i in species 1 and %i in species 2.' 
        'Have you not traversed entire grid?' % (len(del_indices_1),
                                                 len(del_indices_2)))

    else:
        raise ValueError("wrt_dust_density() -- dust_density array must have shape (n_dust,ndata) or (ndata,)")

    iformat = 1
    bytesize = 8

    if wrt_H2_dens:
        H2_conv = 100.0 / H_2_mass  # Assume g2d mass-ratio of 100.
        # Convert dust density to H2 number density
        H2_numb_dens = [x*H2_conv for x in tot_dust_dens]

    if binary:
        with open(os.path.join(model_dir, 'dust_density.binp'), 'wb') as f:
            np.array(iformat,dtype='i8').tofile(f)           # iformat
            np.array(bytesize,dtype='i8').tofile(f)          # bytesize of main data
            np.array(ndata,dtype='i8').tofile(f)             # number of data points
            np.array(n_dust,dtype='i8').tofile(f)             # number of dust species
            np.asarray(dust_density,dtype='f8').tofile(f)    # dust densities

        if wrt_H2_dens:    
            with open(os.path.join(model_dir, 'numberdens_h2.binp'),'wb') as f:
                np.array(iformat,dtype='i8').tofile(f)           # iformat
                np.array(bytesize,dtype='i8').tofile(f)          # bytesize of main data
                np.array(ndata,dtype='i8').tofile(f)             # number of data points
                np.asarray(H2_numb_dens, dtype='f8').tofile(f)   # dust densities

    else:
        with open(os.path.join(model_dir, 'dust_density.inp'), 'w') as f:
            print >> f, str(iformat)        # iformat
            print >> f, ndata               # number of data points
            print >> f, str(n_dust)         # number of dust species
            np.savetxt(f, dust_density)      # dust densities
        if wrt_H2_dens:
            with open(os.path.join(model_dir, 'numberdens_h2.inp'), 'w') as f:
                print >> f, str(iformat)        # iformat
                print >> f, ndata               # number of data points
                np.savetxt(f, H2_numb_dens)      # dust densities

    with open(os.path.join('rot_col_vel.inp'), 'w') as f:
        print >> f, ndata
        for i in xrange(len(vel)):
            print >> f, "%.20f, %.20f, %.20f" % (vel[i][0], vel[i][1],
                                                 vel[i][2])
    if wrt_H2_dens:
        with open(os.path.join(model_dir, 'gas_velocity.inp'), 'w') as f:
            print >> f, 1
            print >> f, ndata
            for i in xrange(len(vel)):
                print >> f, "%.20f, %.20f, %.20f" % (vel[i][0]*100,
                                                     vel[i][1]*100,
                                                     vel[i][2]*100)
    return ndata, n_dust


def write_radmc3d_inp(modpar=None):
    """Writes the radmc3d.inp master command file for RADMC-3D
    Taken from writeRadmc3dInp by A. Juhazcz.
    Parameters
    ----------
    ppar   : dictionary
             Contains all parameters of a RADMC-3D run.

    """

    # Select those keywords, whose block name is 'Code parameters'
    ppar = {}

    for ikey in modpar.pblock.keys():
        if modpar.pblock[ikey]=='Code parameters':
            ppar[ikey] = modpar.ppar[ikey]

    print 'Writing radmc3d.inp'

    wfile = open('radmc3d.inp', 'w')
    keys = ppar.keys()
    keys.sort()
    for key in keys:
        #if modpar.ppar.has_key(key):
        wfile.write('%s %d\n'%(key+' =',ppar[key]))

    wfile.close()  
  

def wrt_model_file(source_nr, t_eff, lum_1, lum_2, inn_rad, out_rad, r_c,
                   m_star, m_dot, cav_rad, p_stars, evap_rad, n_stream,
                   n_stream_r, nx, ny, nz, n_dust, wbound, nw, phot_numb, prho,
                   rho0, cav_enable,
                   bare, rot_collapse, disk_dic, tor_dic, zbound, mrw,
                   recurs_subli, oct_rad_1, oct_rad_2, inn_disk_ref_1_rad,
                   inn_disk_ref_2_rad, disk_rot_coll, profiler_active=False):

    """This function writes a functional radmc3dPy model file (model_xxx.py), 
    to be called with various Radmc3dPy modules. 

    Args:        
        source_nr: Number of stellar sources [integer]. 
        t_eff: Effective surface temperature of the stellar source(s) 
            [Kelvin, float].
        lum_1: luminosity of first source. If a single source is set, the 
            according luminosity should be lum_1 [solar luminosity, float].
        lum_2: luminosity of second source.
        inn_rad: Inner radius of the model radial grid [cm].
        out_rad: Outer radius of the model radial grid [cm].  
        r_c: Co-rotation radius of the infalling, rotating envelope [cm,float].
        cav_rad: Radius of the central cavity [AU,float].
        p_stars: Star coordinates in the grid. Cartesian [cm,cm,cm].
        evap_rad: Evaporation radius of water away from the stellar 
            source(s). This is currently [10/4-2015] manually defined, but 
            the goal is to determine this distance recursively so various 
            luminosities can be used without inconsistency issues with this 
            freeze-out radius. [cm, float]    
        n_stream: number of calculated streamlines in the case of a 
            rotating collapse [integer].
        n_stream_r: number of evaluated radial grid nodes in each streamline
            [integer].
        nx: number of radial grid cells [integer].
        ny: number of meridional grid cells [integer].
        nz: number of azimuthal grid cells [integer].
        n_dust: number of dust species in the model [integer].
        wbound: Boundaries of the wavelength grid [list].
        nw: Number of wavelength points.
        phot_numb: Photon number in the RADMC-3D mctherm calculation [integer].
        prho: Power of the radial density distribution (in the case of a
            static envelope [float].
        rho0: Dust density at inn_rad [g/cm^3, float].
        cav_enable: If 'True', use an inner cavity of constant density.
            If 'False', use a either a static envelope or rotating infall
            all the way into inn_rad.  
        bare: If 'True', then use bare grain opacities as well
            as ice-grain opacities. If 'False', then use only ice-grains, 
            everywhere in model.
        rot_collapse: If 'True', then use a rotating collapse density
            structure. If 'False', then use a static envelope.
        disk_rot_coll: If True the use a rotating collapse around each star to
                      create a pseudodisk instead of a classical disk.  
        profiler_active: If 'True' a line profiler is used on 
            cython_mod.getDustDensity(). Be aware that this is very time        
            consuming.
         
    Returns:
        None. Constructs a model file named model_IRAS_16293.py

    """ 
    cnt = 0
    if source_nr == 1:
        st_size_enabl = 0
    else:
        st_size_enabl = 0

    #Write the model file for use in radmc3dpy.
    print 'Writing model_IRAS_16293.py'
    with open("model_IRAS_16293.py", "w+") as f:     
        f.write("# -*- coding: utf-8 -*-\n")
        f.write("\"\"\"\n")
        f.write("PYTHON module for RADMC3D\n") 
        f.write("Model: IRAS 16923-2422.\n")
        f.write("\n")
        f.write("Credits:\n")
        f.write("    Template layout from model_spher1d_1.py in the radmc3dpy package.\n") 
        f.write("    (c) Attila Juhasz, Kees Dullemond 2011,2012,2013,2014\n")
        f.write("    Original IDL model by Kees Dullemond, Python translation by Attila Juhasz\n")
        f.write("\n")
        f.write("\n")
        f.write("\n")
        f.write("\"\"\"\n")
        f.write("try:\n")
        f.write("    import numpy as np\n")
        f.write("    from radmc3dPy import crd_trans\n")
        f.write("except:\n")
        f.write("    print 'ERROR'\n")
        f.write("    print ' Numpy cannot be imported '\n")
        f.write("    print ' To use the python module of RADMC-3D you need to install Numpy'\n")
        f.write("\n")
        f.write("from radmc3dPy.natconst import *\n")
        f.write("import sys\n")
        f.write("import time as time\n")        
        f.write("import os\n")
        f.write("import pstats, cProfile\n")
        f.write("profiler_active = %s \n" % profiler_active)     
        f.write("try:\n")
        f.write("    if os.path.expanduser('~') == '/Users/Steffen_KJ':\n")
#        f.write("        os.system('python /Users/Steffen_KJ/%s/PhD/Scientific_software/Python/modules/cython_setup.py build_ext --inplace')\n" % onedrive)
        f.write("        os.system('python /Users/Steffen_KJ/Dropbox/PhD/Scientific_software/Python/modules/cython_setup.py build_ext --inplace')\n")

        f.write("    elif os.path.expanduser('~') == '/groups/astro/cmh154':\n")
        f.write("        os.system('python /groups/astro/cmh154/python/cython_setup.py build_ext --inplace')\n")
        f.write("except:\n")
        f.write("""    print >> sys.stderr, "You are not in the expected wd"\n""")
        f.write("""    print >> sys.stderr, "Please relocate"\n""")
        f.write("    sys.exit(1)\n")                
        f.write("import cython_mod \n")
        f.write("DustDensity = cython_mod.DustDensity \n")
      #  f.write("#Load Robert Kern's line profiler \n")
        f.write("import line_profiler \n")
        f.write("try: \n")
        f.write("    import line_profiler \n")
        f.write("except ImportError: \n")
        f.write("""    print("No line profiler, test will not work.") \n""")
        f.write("\n")   
        f.write("# ============================================================================================================================\n")
        f.write("#\n")
        f.write("# ============================================================================================================================\n")
        f.write("def getDefaultParams():\n")
        f.write("    \"\"\"\n")
        f.write("    Function to provide default parameter values \n")
        f.write("\n")
        f.write("    OUTPUT:\n")
        f.write("    -------\n")
        f.write("\n")
        f.write("    Returns a list whose elements are also lists with three elements:\n")
        f.write("    1) parameter name, 2) parameter value, 3) parameter description\n")
        f.write("    All three elements should be strings. The string of the parameter\n")
        f.write("    value will be directly written out to the parameter file if requested,\n")
        f.write("    and the value of the string expression will be evaluated and be put\n")
        f.write("    to radmc3dData.ppar. The third element contains the description of the\n")
        f.write("    parameter which will be written in the comment field of the line when\n")
        f.write("    a parameter file is written.\n")
        f.write("    \"\"\" \n")
        f.write("\n")
        f.write("    defpar = {}\n")  
        f.write("\n")    
        if source_nr == 1:    
            f.write("    defpar = [['mstar', '[1.0*ms]', 'Mass of the star(s)'],\n")
            f.write("    ['pstar','[0, 0, 0]'," 
            "'Position of the star(s) (cartesian coordinates)'],\n") 
            f.write("    ['rstar', '[%.4f*rs]', 'Radius of the star(s)'],\n" 
            % (radius_blackbody(t_eff,lum_1)))
            f.write("    ['tstar', '[%.4f]', "
            "'Effective temperature of the star(s)'],\n" % (t_eff))
            cnt +=1    
        if source_nr == 2:
            f.write("    defpar = [['mstar', '[1.0*ms,1.0*ms]', 'Mass of the star(s)'],\n")
            f.write("    ['pstar','[[%.4f, %.4f, %.4f],[%.4f, %.4f, %.4f]]'," 
            "'Position of the star(s) (cartesian coordinates)'],\n"
            % (p_stars[0,0],p_stars[0,1],p_stars[0,2],p_stars[1,0],p_stars[1,1],p_stars[1,2]))
            f.write("    ['rstar', '[%.4f*rs,%.4f*rs]', 'Radius of the star(s)'],\n" 
            % (radius_blackbody(t_eff,lum_1),radius_blackbody(t_eff,lum_2)))
            f.write("    ['tstar', '[%.4f,%.4f]'," 
            "'Effective temperature of the star(s)'],\n" % (t_eff,t_eff))
            cnt +=1
        assert cnt == 1, 'Error: star parameters not written. Did you choose correct number of sources?'     
        f.write("    ['crd_sys',"+"\"'sph'\""+", 'Coordinate system used (car/sph)'],\n")
        f.write("    ['nx', '%i', 'Number of grid points in the first dimension'],\n" % nx)
        f.write("    ['ny', '%i', 'Number of grid points in the second dimension'],\n" % ny)
        f.write("    ['nz', '%i', 'Number of grid points in the third dimension'],\n" % nz)
        f.write("    ['xbound', '[%.4f,%.4f]', 'Boundaries for the x-grid'],\n" 
        % (inn_rad,out_rad))
        f.write("    ['ybound', '[0.0, np.pi]', 'Boundaries for the y-grid'],\n")
        f.write("    ['zbound', '[%f, %f]', 'Boundaries for the z-grid'],\n"% (zbound[0],zbound[1])) 
        cnt = 1
        for x in nw:
            if cnt == 1:
                f.write("    ['nw', '[%i," % x)
            elif cnt != 1 and cnt != len(nw):
                f.write("%i," % x)
            elif cnt != 1 and cnt == len(nw):
                 f.write("%i]', 'Number of points in the wavelength grid'],\n" % x)
            cnt +=1
        cnt = 1
        for x in wbound:
            if cnt == 1:
                f.write("    ['wbound', '[%f," % x)
            elif cnt != 1 and cnt != len(wbound):
                f.write("%f," % x)
            elif cnt != 1 and cnt == len(wbound):
                 f.write("%f]', 'Boundaries for the wavelength grid'],\n" % x)
            cnt +=1
        if n_dust == 1:
            f.write("    ['dustkappa_ext', \"['OH94_thin_ice_dens_106']\", 'Dust opacity file name extension'],\n")   
        if n_dust == 2:
            f.write("    ['dustkappa_ext', \"['OH94_thin_ice_dens_106','OH94_no_ice_dens_106']\", 'Dust opacity file name extension'],\n") 
        f.write("    ['nphot', '%i', 'Number of photons in the thermal Monte Carlo simulation'],\n" % phot_numb)
        f.write("    ['modified_random_walk', '%i', 'If 1, set to modified random walk', 'Code parameters'],\n" %mrw)
        f.write("    ['scattering_mode_max', '0', '0 - no scattering, 1 - isotropic scattering, 2 - anizotropic scattering', 'Code parameters'],\n")
        f.write("    ['istar_sphere', '%i', '  1 - take into account the finite size of the star, 0 - take the star to be point-like', 'Code parameters'],\n" % st_size_enabl)
#        f.write("    ['camera_min_drr','0.03','Sets a limit on recursive subpixeling'],\n")
        f.write("    ['prho', '%e', ' Power exponent of the radial density distribution'],\n" % prho)
        f.write("    ['rho0', '%e', 'Central density']]\n" %rho0)
        f.write("\n")
        f.write("    return defpar\n")
        f.write("\n")
        f.write("#=============================================================================================================================\n")
        f.write("#\n")
        f.write("#============================================================================================================================\n")
        f.write("def getModelDesc():\n")
        f.write("    \"\"\"\n")
        f.write("    Function to provide a brief description of the model\n")
        f.write("    \"\"\"\n")
        f.write("    descr = \"This is a 1D spherical envelope model of IRAS 16293-2422 with powerlaw radial \\n")
        f.write("    density in a spherical grid.\" \n")
        f.write("    descr = \" \".join(descr.split())\n")
        f.write("    return descr\n")
        f.write("# ============================================================================================================================\n")
        f.write("#\n")
        f.write("# ============================================================================================================================\n")
        f.write("def getDustDensity(subli_list,oct_inst,grid=None, ppar=None,octree_indices= False,octree_grid= None,ref_nr=None,p_stars=None):\n")
        f.write("    \"\"\"\n")
        f.write("    Function to create the dust density distribution \n")
        f.write("    \n")
        f.write("    OUTPUT:\n")
        f.write("    -------\n")
        f.write("        returns the volume density in g/cm^3\n")
        f.write("    \"\"\"\n")
        f.write("    start = time.clock()\n")
        f.write("    nx = grid.nx \n")
        f.write("    ny = grid.ny \n")
        f.write("    nz = grid.nz \n")
        f.write("""    dust_setup_args = {"nx": nx,"ny": ny,"nz": nz,\n""")
        f.write("""    'g_ix':grid.xi,'g_iy':grid.yi,'g_iz':grid.zi,\n """)
        f.write("""    'gx':grid.x,'gy':grid.y,'gz':grid.z,\n """)
        f.write("""    "n_dust":{0}, "n_stream":{1}, "n_stream_r":{2}, \n""".format(n_dust, n_stream, n_stream_r))
        f.write("""    "evap_rad":{0},"cav_rad":{1}, "inn_rad":{2}, "out_rad":{3}, \n""".format(evap_rad, cav_rad, inn_rad, out_rad))
        f.write("""    "source_nr":{0}, "prho":{1}, "rho0":{2},"r_c":{3},"disk_dic":{4},"tor_dic":{5},\n""".format(source_nr, prho,rho0,r_c,disk_dic,tor_dic))
        f.write("""    "octree_indices":octree_indices,"ref_nr":ref_nr, \n""") 
        f.write("""    "oct_rad_1":{0},"oct_rad_2":{1},"inn_disk_ref_1_rad":{2},"inn_disk_ref_2_rad":{3}, \n """.format(oct_rad_1,oct_rad_2,inn_disk_ref_1_rad,inn_disk_ref_2_rad))
        f.write("""    "m_dot":{0}, "m_star":{1},"p_stars":p_stars, "bare":{2}, "rot_collapse":{3}, "cav_enable":{4}, "recurs_subli":{5}, "oct_inst":oct_inst, "subli_list":subli_list, \n""".format(m_dot, m_star, bare, rot_collapse, cav_enable, recurs_subli))
        f.write("""    "x_c":0.0, "y_c":0.0, "z_c":0.0, \n""")
        f.write("""    "disk_rot_coll":{0} {1} \n""".format(disk_rot_coll, '}'))

        f.write("    import warnings\n")
        f.write("""    warnings.filterwarnings("ignore", category=DeprecationWarning)\n""")
        f.write("    mdl_dust = DustDensity(**dust_setup_args)\n")
        f.write("    if not profiler_active: \n""")
        f.write("       rho, theta_bounds, stream_r,vel,vel_dic,disk_1_idcs,disk_2_idcs, tor_idcs = DustDensity.dust_setup(mdl_dust,**dust_setup_args)\n")
        f.write("       elapsed = (time.clock() - start)\n")
        f.write("       print 'Time elapsed in DustDensity.dust_setup() = %f seconds' % elapsed \n")
        f.write("    if profiler_active:\n")  
        f.write("    # Print profiling statistics using the `line_profiler` API\n")
        f.write("        print 'Profiling DustDensity.dust_setup(), please wait..' \n")
        f.write("       # profile = line_profiler.LineProfiler(cython_mod.DustDensity.dust_setup)\n")
        f.write("        profile = line_profiler.LineProfiler(mdl_dust.dust_setup)\n")
        f.write("        profile.runcall(mdl_dust.dust_setup,**dust_setup_args)\n") 
        f.write("        profile.print_stats() \n")        
        f.write("        stats = profile.get_stats()\n")
        f.write("""        assert len(stats.timings) > 0, "No profile stats." \n""")
        f.write("        for key, timings in stats.timings.items(): \n")
        f.write("            if key[-1] == 'mdl_dust.dust_setup': \n")
        f.write("                assert len(timings) > 0 \n")
        f.write("                break \n")
        f.write("        else: \n")
        f.write("""            raise ValueError("No stats for DustDensity.") \n""")
        f.write("\n")
        f.write("    return rho, theta_bounds, stream_r,vel, vel_dic,disk_1_idcs,disk_2_idcs, tor_idcs \n")
        f.write("\n") 
    return 
   
def radius_blackbody(t_eff, lum):
    """This function calculates the radius of a blackbody with a given
    surface temperature and luminosity.
    
    Args: 
        t_eff = Effective surface temperature [Kelvin, float].
        lum = Luminosity [Solar luminosity, float].
    
    Returns: 
        radius: Radius of the black-body emitter [Solar radii].
    """
    
    #Stefan-Boltzmann constant [W m^{-2} K^{-4}]          
    SIGMA = 5.67033*10.0**(-8.0)          
    #Solar radius.         
    R_SUN_SI = 6.955*10.0**8.0  #[m] 
    L_SUN_SI = 3.826*10.0**26.0 #[W] 
    #Luminosity of blackbody, L = 4*pi*R^2*sigma*t_eff^4. From this, derive
    #radius of the blackbody.
    radius = np.sqrt((lum*L_SUN_SI/(4.0*np.pi*SIGMA*t_eff**4.0)))/R_SUN_SI

    return radius


class Dust:
    """This class defines a variety of dust analysis tools.
            Args:
            vol: Volume of cells [array].
            rhodust: Dust density [array].
            temp: Cutoff temperature [Kelvin, integer].
            """
    def __init__(self, vol, rho_dust, temp_fract=False, temp=90., i_dust=0,
                 binary=False, octree=False, octree_len=0, source_nr=2,
                 total_mass=True, disk=False, disk_1_idcs=set(),
                 disk_2_idcs=set(), n_dust=1, find_tor_mass=False,
                 tor_idcs=set(), **kwargs):
        self.vol = vol
        self.rho_dust = rho_dust
        self.temp_fract = temp_fract
        self.temp = temp
        self.i_dust = i_dust  # The number of the dust species to investigate
        self.binary = binary
        self.octree = octree
        self.octree_len = octree_len
        self.source_nr = source_nr
        self.total_mass = total_mass
        self.disk = disk
        self.disk_1_idcs = disk_1_idcs
        self.disk_2_idcs = disk_2_idcs
        self.tor_idcs = tor_idcs
        self.n_dust = n_dust  # Total number of dust species in model.
        self.find_tor_mass = find_tor_mass

    def tot_mass(self):
        """
        This method calculates the total dust mass in the model.
        Returns:
            if temp_fract:
                return mas,rad
            else:
                rad
            if disk:
                disk_1_mass,disk_2_mass -> Only the dust mass of the disks.
                Multiply by gas-to-dust ratio to get total mass.
            mas: Total mass in the model [g, float].
            rad: Radius where the dust reaches a temperature of 90 Kelvin
            [cm, float].

        Credit:
            Uses code extracted from radmc3dPy.analyze.getSigmaDust().

        """

        print 'Integrating dust mass of species nr %i..' % self.i_dust

        if self.temp_fract or self.find_tor_mass:
            self.prox = radmc3d_analyze.radmc3dGrid()
            self.prox.read_grid()
            self.prox = radmc3d_analyze.radmc3dData(self.prox)

            # Open datafiles.
            radmc3d_analyze.radmc3dData.readDustTemp(self.prox,
                                                     binary=self.binary,
                                                     octree=self.octree,
                                                     octree_len=self.octree_len)

            self.dust_temp = self.prox.dusttemp  # Get dust temperature

        # Dustmass in each grid cell
        if not self.temp_fract:
            if not self.disk and not self.find_tor_mass:
                if not self.octree:
                    if len(self.rho_dust) > 3:
                        if not self.total_mass:
                            mass = self.vol * self.rho_dust[:, :, :, self.i_dust]
                        elif self.total_mass:
                            mass = 0
                            for j in xrange(self.n_dust):
                                m_res = self.vol * self.rho_dust[:, :, :, j]
                                mass += m_res

                    elif len(self.rho_dust) == 3:
                        mass = self.vol * self.rho_dust
                elif self.octree:
                    if not self.total_mass:
                        mass = self.vol[:] * self.rho_dust[:, self.i_dust]
                    elif self.total_mass:
                        mass = 0
                        for j in xrange(self.n_dust):
                            m_res = self.vol[:] * self.rho_dust[:, j]
                            mass += m_res

                # Sum all three dimensions.
                if not self.octree:
                    mas = np.sum(mass)
                else:
                    mas = sum(mass)

            elif self.disk and self.octree:
                disk_1_mass = 0.0
                disk_2_mass = 0.0
                disk_bare_mass = 0.0
                for i in xrange(len(self.vol)):
                    if i in self.disk_1_idcs:
                        for j in xrange(self.n_dust):
                            disk_1_cell_mass = self.vol[i]*self.rho_dust[i, j]
                            disk_1_mass += disk_1_cell_mass
                        disk_bare_mass += self.vol[i]*self.rho_dust[i, 1]

                        self.disk_1_idcs.remove(i)

                    elif i in self.disk_2_idcs:
                        for j in xrange(self.n_dust):
                            disk_2_cell_mass = self.vol[i]*self.rho_dust[i, j]
                            disk_2_mass += disk_2_cell_mass

                        disk_bare_mass += self.vol[i]*self.rho_dust[i, 1]
                        self.disk_2_idcs.remove(i)

            elif self.find_tor_mass:
                n_tor_cells = len(self.tor_idcs)
                tor_mass = 0.0
                sum_tor_temp = 0.0
                sum_tor_mw_temp = 0.0
                dust_temp_tor = []

                # Get total mass
                for i in xrange(len(self.vol)):
                    if i in self.tor_idcs:
                        for j in xrange(self.n_dust):
                            tor_cell_mass = self.vol[i]*self.rho_dust[i, j]
                            tor_mass += tor_cell_mass

                # Get mass weighted temperature
                for i in xrange(len(self.vol)):
                    if i in self.tor_idcs:
                        # for j in xrange(self.n_dust):
                        tor_cell_temp = max(self.dust_temp[i, :])
                        tor_cell_mass = max(self.vol[i]*self.rho_dust[i, :])
                        dust_temp_tor.append(tor_cell_temp)
                        sum_tor_temp += tor_cell_temp
                        sum_tor_mw_temp += tor_cell_temp*tor_cell_mass

                T_av = sum_tor_temp/n_tor_cells
                T_mw_av = sum_tor_mw_temp/(tor_mass)
                T_min = min(dust_temp_tor)
                print 'n_tor_cells is ', n_tor_cells

            elif self.disk and not self.octree:
                print 'Must have octree refinement to calculate disk mass'

        # If we wish to sum all material >= a T limit.
        elif self.temp_fract:

            if not self.octree:
                mas, rad, vol_tot = find_evap_rad(
                                         self.vol, self.rho_dust,
                                         self.dust_temp, temp=self.temp,
                                         i_dust=self.i_dust,
                                         source_nr=self.source_nr,
                                         n_dust=self.n_dust,
                                         total_mass=self.total_mass)
            elif self.octree:
                mas, rad, vol_tot = find_evap_rad_oct(
                                        self.vol, self.rho_dust,
                                        self.dust_temp, temp=self.temp,
                                        i_dust=self.i_dust,
                                        source_nr=self.source_nr,
                                        n_dust=self.n_dust,
                                        total_mass=self.total_mass)

        if self.temp_fract:
            return mas, rad, vol_tot
        elif not self.temp_fract and not self.disk and not self.find_tor_mass:
            return mas
        elif self.disk:
            return disk_1_mass, disk_2_mass, disk_bare_mass
        elif self.find_tor_mass:
            return tor_mass, T_av, T_mw_av, T_min

    def find_rho0(self, mass, p_rho, r_0, r_1, r_2):
        """This function finds rho_0 in the formula of
        rho = rho_0 *(r/r_0)**(-p), given a mass.
        mass = 4/3*r**3*rho. When rho is spatially dependent, this function
        changes into an integral of infinetisimally thin spheres.

        Args:
            mass: Known mass of object [g].
            p_rho: exponent of the radial density function [float].
            r_0: Radius where the local density is rho_0 [cm].
            r_1 = Start radius of the spherical object [cm].
            r_2 = End radius of the sperical object in [cm].
        
        Returns: 
            rho_0: The dust density at r_0 [cm].
        
        """
        
        rho_0 = mass*(3.0-p_rho)*r_0**(-p_rho)/(4.0*np.pi*(r_2**(3.0-p_rho)-
        r_1**(3.0-p_rho)))
        
        return rho_0                
        
        
class Tools:
    """This class defines a variety of useful radmc3d plotting tools and
    manipulations."""    

    def __init__(self, source_nr, ny, nz, n_dust=2):
        self.C = 299792458.0 #Speed of light [m/s].
        self.source_nr = source_nr
        self.n_dust = n_dust
        self.ny = ny
        self.nz = nz
        
    def open_sed_file(self, filename, dist):
        """
        This program inputs a standard spectrum.out file from RADMC-3D
        and outputs the wavelength array and flux array to python.
        Also converts the RADMC-3D values flux output to nu*F_nu(\lambda) values.

        Args: 
            modelname: Name of input file.                 
            dis: Distance to the object 
                   
        Returns: Two arrays. First is wavelength, second is flux. 
        
        Raises: If the filename does not have the correct format (first three
        lines should not be less than 10 characters, indicating that the output
        data begins at line 4).
        
        """    
    
        C = 299792458.0 #Speed of light [m/s].
        wavelength = []
        flux = []
        
        with open(filename, 'r') as f:
            data = f.readlines()[3:]   
                
        print 'open_sed_file() has opened '+filename
        for line in data:
            a, b = line.split()        
            wavelength.append(float(a))       
            flux.append(float(b))
                
        flux[:] = [x*(1.0/dist)**2.0 for x in flux]
        flux[:]= [x*(C/(y*10.0**(-6.0))) for x,y in zip(flux, wavelength)]
        
        return wavelength, flux
    
    def jy_to_SI_flux(self,flux, wavelength):
        """Function to convert Jansky to SI flux.  
           Args:           
                Flux = Array of Flux [Jy].
        """
        C = 299792458.0 #Speed of light [m/s].

        flux[:]= [x*10.0**(-23.0) for x in flux]
        flux[:]= [x*(C/(y*10.0**(-6.0))) for x,y in zip(flux, wavelength)]
        
        return flux
        
    def rd_crr_flx(self,filename): 
    #Read interferometry correlated flux.
        wav = []
        crr_flx = []
        with open(filename, 'r') as f:        
            for row in f:
                values = row.split()
                try:
                    a,b = values
                except ValueError:
                    print(len(values))
          
                wav.append(float(a))
                crr_flx.append(float(b))

        return wav, crr_flx

    def plot_sed(self, mod_filename, obs_filename, dist, start, splitter,
                 mark_size=6.0, unitsize=6.0, ewidth=3.0, x_lim_1=10.0,
                 x_lim_2=10.0**4.0, y_lim_1=10.0**(-15.0),
                 y_lim_2=10.0**(-7.0)):

        """This function takes an input radmc3d sed file and observational
        datafile and plots them.

        Args: mod_filename = name of model sed file.
              obs_filename = name of observational sed datafile.
              dist = distance to the object from observer, in pc.
              Also some selfexplanatory plot configurations.

        Returns: A plot of the sed with overplotted observational datapoints,
                 saved at sed_plot.png.
        """
        warnings.filterwarnings('ignore')
        import seaborn as sns
        warnings.filterwarnings('always')

        plt.rcParams['text.usetex'] = True  # Let TeX do the typsetting
        plt.rcParams['font.family'] = 'Times New Roman'
        plt.rcParams['font.weight'] = 'bold'
        plt.rcParams['font.size'] = 40

        # ------------------- Open datafiles.

        # Observational file.
        if type(obs_filename) != dict:
            wav_obs, flux_obs, flux_error_obs = open_csv_file(
                                                     obs_filename,
                                                     start, splitter)

            flux_obs = self.jy_to_SI_flux(flux_obs, wav_obs)
            flux_error_obs = self.jy_to_SI_flux(flux_error_obs, wav_obs)
        elif type(obs_filename) == dict:
            obs = {}
            for i in xrange(len(obs_filename)):
                wav_obs, flux_obs, flux_error_obs = open_csv_file(
                                            obs_filename['SED_dat_%i' % (i+1)],
                                            start, splitter)
                flux_obs = self.jy_to_SI_flux(flux_obs, wav_obs)
                flux_error_obs = self.jy_to_SI_flux(flux_error_obs, wav_obs)
                obs.update({'wav_obs_'+str(i): wav_obs,
                            'flux_obs'+str(i): flux_obs,
                            'flux_error_obs'+str(i): flux_error_obs})

        # radmc3d model sed file.
        wav_mod, flux_mod = self.open_sed_file(mod_filename, dist)

        plt.figure()
        plt.plot(wav_mod, flux_mod, label=str(r'\texttt{RADMC-3D model'),
                 color='red', linewidth=3)
        if type(obs_filename) != dict:

            # Overplot observations.
            plt.errorbar(wav_obs, flux_obs, yerr=flux_error_obs, marker='o',
                         color='blue', ecolor='blue',
                         label = 'Obs. data', markersize=mark_size,
                         elinewidth=ewidth)

        elif type(obs_filename) == dict:
            for i in xrange(len(obs_filename)):
                # Overplot observations.
                if i == 0:
                    plt.errorbar(obs['wav_obs_'+str(i)],
                                 obs['flux_obs'+str(i)],
                                 yerr=obs['flux_error_obs'+str(i)], marker='o',
                                 color='blue', ecolor='blue', fmt='',
                                 label='Archive data', markersize=mark_size,
                                 linestyle='None',
                                 elinewidth=ewidth)
                elif i == 1:
                    plt.errorbar(obs['wav_obs_'+str(i)],
                                 obs['flux_obs'+str(i)],
                                 yerr=obs['flux_error_obs'+str(i)],
                                 marker='o', color='grey', ecolor='grey',
                                 label='Spitzer data', markersize=mark_size,
                                 fmt='',
                                 linestyle='None',
                                 elinewidth=ewidth)

        plt.xticks(fontsize=16)
        plt.yticks(fontsize=16)
        plt.xlabel(r' $\lambda$ [$\mu$m]', fontsize=22)
        plt.ylabel(r'$\nu$ $F_{\nu}$ [erg cm$^{-2}$ s$^{-1}$]', fontsize=22)
        plt.yscale('log')
        plt.xscale('log')
        plt.grid(True)
        plt.xlim(x_lim_1, x_lim_2)
        plt.ylim(y_lim_1, y_lim_2)
        plt.legend(fontsize=24, bbox_to_anchor=(1.05, 1), loc=2,
                   borderaxespad=0.)
        plt.savefig('sed_plot.png', dpi=300, bbox_inches='tight')

    def plot_dens(self, rho_dust=None, grid=None, idust=0, r_c=10.*AU,
                  r_max=8000.*AU, p_rho=-1.7,
                  rho_0=2.79130088993e-13, cav_rad=600.*AU,
                  streamline_view_enable=False, n_stream=None,
                  theta_bounds=None, stream_r=None, binary=True,
                  rot_coll=False):

        """
        This function plots a 2D density contour plot from a 3D model.
        Args:
            rho_dust: Numpy array of dust density [x,y,z,idust], [g/cm^3].
            grid: Instance of radmc3dPy.analyze.radmc3dGrid.readGrid(), which
                reads the RADMC-3D model grid as defined in amr_grid.inp.
            idust: Dust species number.
            r_c: Co-rotation radius of the infalling, rotating envelope
                [cm,float].
            r_max: Maximum r_c value plotted.
            streamline_view_enable: Plot (True) or do not plot (False) the
                streamlines used to determine the dust density in the rotating
                collapse model.
            n_stream: number of calculated streamlines in the case of a
                rotating collapse [integer].
            theta_bounds: Numpy array of the meriodional streamline grid node
                positions [n_stream,n_stream_r].
            stream_r: 1D numpy array of radial streamline grid node positions,
                logarithmically spaced.
        Returns: None. Delivers a 2D density contour plot.
        """

        if rot_coll:
            # Convert the input radius to units of r_c (centrifugal radius).

            grid_x = []
            grid_x[:] = [x/r_c for x in grid.x]

    #============================== Meshgrid ==================================

            # Construct meshgrid of the radial and meridional dimensions.
            radius_matrix, theta_matrix = np.meshgrid(grid_x, grid.y)

            # Collapse the azimuthal dimension to a single value.
            X = radius_matrix * np.sin(theta_matrix) * np.cos(0.0)

            # Need a slice on the other side of the theta plane as well.
            X_2 = radius_matrix * np.sin(theta_matrix) * np.cos(np.pi)

            # Birds-eye view.
            radius_matrix_xy, phi_matrix = np.meshgrid(grid_x, grid.z)

            Y = radius_matrix_xy * np.sin(np.pi/2.) * np.sin(phi_matrix)
            Xy = radius_matrix_xy * np.sin(np.pi/2.) * np.cos(phi_matrix)

            Z = radius_matrix * np.cos(theta_matrix)
            Z_2 = radius_matrix * np.cos(theta_matrix)

            #A small hack; the dimension order is easiest to handle if the density
            #multi-dim array is transposed.
            rho_slice = np.transpose(rho_dust[0:grid.nx, 0:grid.ny, 0, idust])
            rho_slice_2 = np.transpose(rho_dust[0:grid.nx, 0:grid.ny,
                                                int(grid.nz/2.), idust])

            rho_slice_xy = np.transpose(rho_dust[:, int(grid.ny/2.0+1), :, idust])
            print 'SHAPES'
            print rho_dust.shape
            print rho_slice_xy.shape
            print Y.shape
            print X.shape

            # Recreate contour plot from fig. 4.7 in Hartmann.
            n_iter = 25  # Nr of contours
            font_size = 25
            rc_AU = 20.0  # assume r_c = 20 AU

            H_0 = 0.85/rc_AU
            r_0 = 10.0/rc_AU  # I.e. r_0 is 10 AU
            levels = []
            levels_class = []

            # Add classical disk contours
            rho_class = np.ndarray((len(grid.x), len(grid.y)))
            rho_class_2 = np.ndarray((len(grid.x), len(grid.y)))
            rho_class_xy = np.ndarray((len(grid.x), len(grid.z)))

            rho_class = class_disk_dens(X, 0.0, Z, 0.5, r_0, 5*10**(-18),
                                        H_0, 0.25)
            rho_class_2 = class_disk_dens(X, 0.0, Z, 0.5, r_0, 5*10**(-18),
                                          H_0, 0.25)
            rho_class_xy = class_disk_dens(Xy, Y, 0.0, 0.5, r_0, 5*10**(-18),
                                           H_0, 0.25, azi=True)
            rho_class_slice = rho_class
            rho_class_slice_2 = rho_class_2
            rho_class_xy_slice = rho_class_xy

            # Find the global maximum density in the model.
            levels_class = [max(map(max, rho_class_slice))]
            levels = [max(map(max, rho_slice))]

            # Create countour levels, going as incremental differences of
            # 2^(1/2).
            for x in xrange(n_iter):
                levels.append(levels[x]*(np.sqrt(2.0))**(-1.0))
                levels_class.append((levels_class[x]/12.0)*(np.sqrt(2.0))**(-1.0))

            levels = levels[::-1]
            levels_class = levels_class[::-1]

            # Plot the meshgrid and streamlines.
            if streamline_view_enable:
                plt.figure()
                plt.rc('text', usetex=True)
                plt.rcParams['font.size'] = font_size
                for y in xrange(n_stream):
                    plt.plot(stream_r / r_c * np.sin(theta_bounds[y, :]),
                             stream_r / r_c * np.cos(theta_bounds[y, :]))
                plt.xlabel(r'x/$r_c$')
                plt.ylabel(r'z/$r_c$')
                plt.title('Streamlines')
                plt.xlim([0, 10])
                plt.ylim([0, 10])
                plt.grid(True)
                plt.show()

                plt.figure()
                plt.rc('text', usetex=True)
                plt.rcParams['font.size'] = 16
                for y in xrange(n_stream):
                    plt.plot(stream_r/r_c*np.sin(theta_bounds[y, :]),
                             stream_r/r_c*np.cos(theta_bounds[y, :]), 'g. ',
                             markersize=0.4)
                plt.plot(X, Z, 'r. ', markersize=0.6)
                plt.xlabel(r'x/$r_c$')
                plt.ylabel(r'z/$r_c$')
                plt.title("radmc-3d grid nodes (red) and streamline grid nodes"
                          " (green)")
                plt.xlim([0, r_max])
                plt.ylim([0, r_max])
                plt.grid(True)
                plt.show()

            # ------------------ pseudodisk, vertical -------------------------

            plt.figure()
            fig, ax = plt.subplots()
            plt.rc('text', usetex=True)
           # plt.rcParams['font.size'] = font_size
            plt.contour(X, Z, rho_slice[0:grid.ny, 0:grid.nx], levels,
                        origin='lower', linewidths=1.25, cmap=cm.RdBu)
            plt.contour(X_2, Z, rho_slice_2[0:grid.ny, 0:grid.nx], levels,
                        origin='lower', linewidths=1.25, cmap=cm.RdBu)
            plt.xlabel(r'x/$r_c$', fontsize=22)
            plt.ylabel(r'z/$r_c$', fontsize=22)
            plt.xlim([-5.7, 5.7])
            plt.ylim([-5.7, 5.7])
            loc = plticker.MultipleLocator(base=1.0) # this locator puts ticks at regular intervals
            ax.yaxis.set_major_locator(loc)
            ax.xaxis.set_major_locator(loc)
            ax.tick_params(labelsize=16)

            plt.savefig('rot_col_contours.png', dpi=300, format='png',
                        bbox_inches='tight')
            
            # ------------------ Classical disk, vertical ---------------------
            plt.figure()
            fig, ax = plt.subplots()
            plt.rc('text', usetex=True)
       #     plt.rcParams['font.size'] = 32
            plt.contour(X, Z, rho_class_slice, levels, linewidths=1.25,
                        origin='lower', cmap=cm.RdBu)
            plt.contour(X_2, Z, rho_class_slice_2, levels, linewidths=1.25,
                        origin='lower', cmap=cm.RdBu)
            plt.xlabel(r'x/$r_c$', fontsize=22)
            plt.ylabel(r'z/$r_c$', fontsize=22)
            plt.xlim([-5.7, 5.7])
            plt.ylim([-5.7, 5.7])
            loc = plticker.MultipleLocator(base=1.0) # this locator puts ticks at regular intervals
            ax.yaxis.set_major_locator(loc)
            ax.xaxis.set_major_locator(loc)
            ax.tick_params(labelsize=16)
            plt.savefig('class_disk_contours.png', dpi=300, format='png',
                        bbox_inches='tight')
            
            # ------------------ pseudodisk from above ------------------------
            plt.figure()
            plt.rc('text', usetex=True)
            plt.rcParams['font.size'] = font_size
            plt.contour(Xy, Y, rho_slice_xy, levels,
                        origin='lower', linewidths=1.75, cmap=cm.RdBu)
            plt.xlabel(r'x/$r_c$')
            plt.ylabel(r'y/$r_c$')
            plt.xlim([-5.5, 5.5])
            plt.ylim([-5.5, 5.5])
            plt.show()

            #  class disk from above
            levels[:] = [x*12.0 for x in levels]
            plt.figure()
            plt.rc('text', usetex=True)
            plt.rcParams['font.size'] = font_size
            plt.contour(Xy, Y, rho_class_xy_slice, levels,
                        origin='lower', linewidths=1.75, cmap=cm.RdBu)

            plt.xlabel(r'x/$r_c$')
            plt.ylabel(r'y/$r_c$')
            plt.xlim([-5.5, 5.5])
            plt.ylim([-5.5, 5.5])
            plt.show()

        elif not rot_coll:
            #Show a classical powerlaw density distribution, with a density
            # plateau.

            rho = np.zeros(8000)
            dens_pow = lambda x: rho_0 * (x)**p_rho
            dens_cav = lambda x: rho_0 * (cav_rad/AU)**p_rho
            distance = arange(0,8000)
            for x in xrange(8000):
                if x>cav_rad/AU:
                    rho[x] = dens_pow(x)
                elif x<=cav_rad/AU:
                    rho[x] = dens_cav(x)

            plt.figure()
            fig, ax = plt.subplots()

            minorLocator = AutoMinorLocator()
            ax.xaxis.set_minor_locator(minorLocator)

            plt.tick_params(which='both', width=2)
            plt.tick_params(which='major', length=7)
            plt.tick_params(which='minor', length=4)
            plt.plot(distance, rho, label=str('radmc-3d model density'),
                     color='blue', linewidth=3)
            plt.rc('text', usetex=True)
            plt.rcParams['font.size'] = 26
            plt.xlabel(r'r [AU]')
            plt.ylabel(r'Dust density [g/cm$^{-3}$]')
            plt.xscale('log')
            plt.yscale('log')
            plt.grid(True)
            plt.show

    def plot_temp(self, p_stars, plot_dim=2, use_cm=False, binary=False,
                  octree=False, octree_len=0, n_dust=1, plot_width=2000,
                  use_50K=False, target_L483=False, use_rot_col=True):
        """
        This function takes an input radmc3d dust_temperature.dat file
        and plots it.

        Args:
            plot_dim = dimension of the temperature structure to be plotted.
            plot_width = width of the plot [AU]
        Returns: A plot of dust temp.
        """
        warnings.filterwarnings('ignore')
        import seaborn as sns
        warnings.filterwarnings('always')
        sns.set(font_scale=2)

        distance = []
        distance_1 = []
        distance_2 = []

        prox = radmc3d_analyze.radmc3dGrid()
        prox.read_grid()
        prox = radmc3d_analyze.radmc3dData(prox)
        # Open datafiles.
        print("sizes:")
        print(prox.grid.nx, prox.grid.ny, prox.grid.nz)

        radmc3d_analyze.radmc3dData.readDustTemp(prox, binary=binary,
                                                 octree=octree,
                                                 octree_len=octree_len)
        radmc3d_analyze.radmc3dData.readDustDens(prox, binary=binary,
                                                 octree=octree,
                                                 octree_len=octree_len)
        print(prox.dusttemp.shape)

        # ------------------------------- 1D plot -----------------------------

        if plot_dim == 1 and not octree:
            if prox.dusttemp.shape[1] == 3:
                temp_r = prox.dusttemp[:, 59, 6]
            elif prox.dusttemp.shape[1] == 1:
                temp_r = prox.dusttemp[:]
            if self.source_nr == 2:
                # investigate one halfplane.
                # assuming stars are on y-axis. Positive y.
                temp_1 = prox.dusttemp[:, self.ny/2, self.nz/8]
                # Then other halfplane
                # assuming stars are on y-axis. Negative -y.
                temp_2 = prox.dusttemp[:, self.ny/2, 5*self.nz/8]
                temp_2[:] = temp_2[::-1]
                temp_r = np.concatenate((temp_2, temp_1), axis=0)

                # We need to sum the temp in the meridional and azimuthal
                # direction
                r_temp = np.zeros(len(temp_1))
                for x in xrange(len(r_temp)):
                    r_temp[x] = sum(prox.dusttemp[x, :, :])/(self.ny * self.nz)

            if not use_cm:
                distance_1[:] = [x/AU for x in prox.grid.x]
                distance_2[:] = [-x/AU for x in reversed(prox.grid.x)]
                distance = distance_2 + distance_1  # merge the lists
                x_lim_1 = distance[0]/AU
                x_lim_2 = distance[-1]/AU
            elif use_cm:
                distance_1[:] = [x for x in prox.grid.x]
                distance_2[:] = [-x for x in reversed(prox.grid.x)]
                distance = distance_2 + distance_1  # merge the lists
                x_lim_1 = distance[0]
                x_lim_2 = distance[-1]
            if n_dust == 2:
                temp_r = temp_r[:, 0]

            # Remove odd values, extremely close to one. NOTE: This is due to
            # very small
            mask = np.ones(len(temp_r), dtype=bool)
            for x in xrange(len(temp_r)):
                if temp_r[x] < 1.0:
                    mask[x] = False

            temp_r = temp_r[mask]
            for x in xrange(len(distance)):
                if not mask[x]:
                    del distance[x]

            y_tcks = np.arange(100, 1000+1, 100)
            fig, ax = plt.subplots()

            plt.plot(distance, temp_r, label=str('Dust temperature'),
                     color='red', linewidth=2.0)
            if use_cm:
                plt.xlim(-8000.*AU, 8000.*AU)
            elif not use_cm:
                plt.xlim(-8000., 8000.)
            plt.ylim(8, 1000)
            plt.rcParams['font.size'] = 24
            plt.rc('text', usetex=True)
            minorLocator = AutoMinorLocator()
            ax.xaxis.set_minor_locator(minorLocator)

            plt.tick_params(which='both', width=2)
            plt.tick_params(which='major', length=7)
            plt.tick_params(which='minor', length=4)

            if not use_cm:
                plt.xlabel(r' Stellar axis position [AU]')
            elif use_cm:
                plt.xlabel(r' Stellar axis position [cm]')

            plt.ylabel(r'T [K]')
            plt.yscale('log')
            plt.yticks(y_tcks)
            plt.grid(True)
            plt.legend(fontsize=24)
            plt.show()

            # Zoom in on the clearest source.
            plt.figure()
            plt.plot(distance_1, temp_1, label=str('Dust temperature'),
                     color='red', linewidth=2.0)
            plt.axvline(x=6, color='b', linestyle='--', label='r = 6 AU',
                        linewidth=3)
            plt.axvline(x=30, color='g', linestyle=':', label='r = 30 AU',
                        linewidth=3)
            plt.ylim(0, 1000)
            plt.xlim(1, 500)

            plt.rcParams['font.size'] = 26
            plt.rc('text', usetex=True)
            minorLocator = AutoMinorLocator()

            ax.xaxis.set_minor_locator(minorLocator)
            plt.tick_params(which='both', width=2)
            plt.tick_params(which='major', length=7)
            plt.tick_params(which='minor', length=4)

            if not use_cm:
                plt.xlabel(r' Stellar axis position [AU]')
            elif use_cm:
                plt.xlabel(r' Stellar axis position [cm]')
            plt.ylabel(r'T [K]')
            plt.xscale('log')
            plt.grid(True)
            plt.legend(fontsize=24)
            plt.show()

# --------------------------------------- 2D plot -----------------------------

        elif plot_dim == 2:

            print 'temp shape is ', prox.dusttemp.shape
            print 'dens shape is ', prox.rhodust.shape
            if target_L483:
                print 'L483 is investigated'

            temp_0 = prox.dusttemp[:, :, :, 0]
            temp_1 = prox.dusttemp[:, :, :, 1]

            dens_0 = prox.rhodust[:, :, :, 0]
            dens_1 = prox.rhodust[:, :, :, 1]

            print 'Max and min of dens 0 is {}, {}'.format(dens_0.max(),
                                                           dens_0.min())
            print 'Max and min of dens 1 is {}, {}'.format(dens_1.max(),
                                                           dens_1.min())

            # Combine dust densities and temperatures, since we have both ice
            # and bare grains.
            dens_arr = prox.rhodust[:, :, :, 0]
            temp_arr = prox.dusttemp[:, :, :, 0]

            for i, j, k in itertools.product(xrange(prox.grid.nx),
                                             xrange(prox.grid.ny),
                                             xrange(prox.grid.nz)):
                temp_arr[i, j, k] = max(temp_0[i, j, k], temp_1[i, j, k])
                dens_arr[i, j, k] = max(dens_0[i, j, k], dens_1[i, j, k])

            # Make meshgrid
            if target_L483:
                radius_matrix, theta_matrix = np.meshgrid(prox.grid.x,
                                                          prox.grid.y)
                phi = 0.0  # np.pi/2.0

                # Collapse the azimuthal dimension to a single value
                X = radius_matrix/AU * np.sin(theta_matrix) * np.cos(phi)
                Y = radius_matrix/AU * np.sin(theta_matrix) * np.sin(phi)
                Z = radius_matrix/AU * np.cos(theta_matrix)

            else:
                # Hack to extend meshgrid to full circle.
                gridz = copy.deepcopy(prox.grid.z)
                gridz[0] = 0.0
                gridz[-1] = 2*np.pi
                radius_matrix, phi_matrix = np.meshgrid(prox.grid.x, gridz)
                theta = np.pi/2.0

                #Collapse the meridional dimension to a single value.
                X = radius_matrix/AU * np.sin(theta) * np.cos(phi_matrix)
                Y = radius_matrix/AU * np.sin(theta) * np.sin(phi_matrix)
                Z = radius_matrix/AU * np.cos(theta)

            # A small hack; the dimension order is easiest to handle if the
            # density multi-dim array is transposed.

            if target_L483:
                temp_arr_L483 = prox.dusttemp[:, :, 0, 0]

                for i, j in itertools.product(xrange(prox.grid.nx),
                                              xrange(prox.grid.ny)):

                    interp_val = []
                    interp_val.append(temp_arr[i, j, :])
                    temp_arr_L483[i, j] = sum(interp_val)/len(interp_val[0])
            else:
                for i, j, k in itertools.product(xrange(prox.grid.nx),
                                                 xrange(prox.grid.ny),
                                                 xrange(prox.grid.nz)):
                    assert not np.isnan(temp_arr[i, j, k]), 'nan found in array'
                    assert temp_arr[i, j, k] >= 0.0, 'negative temperature found'

                    if (1.1*np.pi/2.0> prox.grid.y[j] >0.9*np.pi/2.0 and
                        i != prox.grid.nx - 1 and j != prox.grid.ny - 1
                        and k != prox.grid.nz - 1):
                        interp_val = []
                        for ir in np.arange(-1, 2, 1):
                            for iy in np.arange(-1, 2, 1):
                                for iz in np.arange(-1, 2, 1):
                                    interp_val.append(temp_arr[i + ir, j + iy,
                                                               k + iz])
                        temp_arr[i, j, k] = sum(interp_val)/len(interp_val)

            if target_L483:
                # Take a slice through the meridional plane.
                temp_slc = np.transpose(temp_arr_L483[0:prox.grid.nx,
                                                      0:prox.grid.ny])
                dens_slc = np.transpose(dens_arr[0:prox.grid.nx,
                                                 0:prox.grid.ny,
                                                 0])
 #               temp_slc = temp_arr_L483[0:prox.grid.nx, 0:prox.grid.ny]
 #               dens_slc = dens_arr[0:prox.grid.nx, 0:prox.grid.ny, 0]
            else:
                temp_slc = np.transpose(temp_arr[0:prox.grid.nx,
                                        int(prox.grid.ny/2), 0:prox.grid.nz])
                dens_slc = np.transpose(dens_arr[0:prox.grid.nx,
                                             int(prox.grid.ny/2),
                                             0:prox.grid.nz])

            # Plot the temperature limit edges
            plt.figure()

            plt.rc('text', usetex=True)
            plt.rcParams['font.size'] = 16
            plt.rcParams['font.family'] = 'serif'

            if use_50K:
                cont_list = [30.0, 50.0, 90.0]
                labels = [r'T$_{\mathrm{dust}}$ = 30 K',
                          r'T$_{\mathrm{dust}}$ = 50 K',
                          r'T$_{\mathrm{dust}}$ = 90 K']
                colors = ('blue', 'green', 'r')

            else:
                if not target_L483:
                    cont_list = [30.0, 40.0, 55.0, 90.0]
                    labels = [
                              r'T$_{\mathrm{dust}}$ = 30 K',
                              r'T$_{\mathrm{dust}}$ = 40 K',
                              r'T$_{\mathrm{dust}}$ = 55 K',
                              r'T$_{\mathrm{dust}}$ = 90 K']
                    colors = ('blue', 'green', 'orange', 'r')
                elif target_L483 and not use_rot_col:
                    cont_list = [50.0, 60.0, 70.0, 80.0, 90.0, 100.0, 150.0,
                                 250]
                    labels = [
                              r'T$_{\mathrm{dust}}$ = 50 K',
                              r'T$_{\mathrm{dust}}$ = 60 K',
                              r'T$_{\mathrm{dust}}$ = 70 K',
                              r'T$_{\mathrm{dust}}$ = 80 K',
                              r'T$_{\mathrm{dust}}$ = 90 K',
                              r'T$_{\mathrm{dust}}$ = 100 K',
                              r'T$_{\mathrm{dust}}$ = 150 K',
                              r'T$_{\mathrm{dust}}$ = 250 K'
                              ]
                elif target_L483 and use_rot_col:
                    cont_list = [100.0, 150.0, 300.0]
                    labels = [
                              r'T$_{\mathrm{dust}}$ = 100 K',
                              r'T$_{\mathrm{dust}}$ = 150 K',
                              r'T$_{\mathrm{dust}}$ = 300 K',
                              ]

                    colors = ('blue', 'orange', 'red')

            if target_L483:
                plt.contour(X, Z, dens_slc, np.logspace(-18.0, -14.5, 25),
                            origin='lower', linewidths=2, alpha=0.4,
                            cmap=cm.summer)
            else:
                plt.contour(X, Y, dens_slc, np.logspace(-18.5, -14.8, 10),
                            origin='lower', linewidths=2, alpha=0.4,
                            cmap=cm.summer)

            if self.source_nr == 2:
                pos_x_1 = p_stars[0, 0]/AU
                pos_y_1 = p_stars[0, 1]/AU
                pos_x_2 = p_stars[1, 0]/AU
                pos_y_2 = p_stars[1, 1]/AU
                plt.plot(pos_x_1, pos_y_1, 'y*', markersize=6)
                plt.plot(pos_x_2, pos_y_2, 'y*', markersize=6)
            elif self.source_nr == 1:
                plt.plot([0.0], [0.0], 'y*', markersize=5)

            if target_L483 and use_rot_col:
                plt.xlim(0, plot_width)
                plt.ylim(0, plot_width)
            elif target_L483:
                plt.xlim(0, 40.0)
                plt.ylim(0, 25.0)
            else:
                plt.xlim(-1.*plot_width, plot_width)
                plt.ylim(-1.*plot_width, plot_width)

            plt.xlabel('AU')
            plt.ylabel('AU')

            if target_L483:
                CS = plt.contour(X, Z, temp_slc, cont_list,
                                 colors=colors, #cmap=cm.nipy_spectral,  # colors=colors,
                                 linestyles='--', linewidth=3.0)
            else:
                CS = plt.contour(X, Y, temp_slc, cont_list, cmap=cm.RdBu, #colors=colors,
                                 linestyles='--', linewidth=3.0)

#            CS = plt.contour(X, Y, temp_slc, cont_list, cmap=cm.RdBu, #colors=colors,
#                             linestyles='--', linewidth=3.0)

            for i in xrange(len(CS.collections)):
                CS.collections[i].set_label(labels[i])
            if target_L483:
                plt.legend(loc=1, fontsize=11)
            else:
                plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.0,
                           fontsize=11)
            plt.axes().set_aspect('equal')
            plt.savefig('temp_plot.png', dpi=300, format='png',
                        bbox_inches='tight')

def out_to_fits(model_file, dist, name,
                trash_dir='/Users/Steffen_KJ/work_trash'):
    """This function creates a fits file from a radmc3d image output file.
       Input: 
           model_file: name of .out file.
           dist: distance to object in parsec.
           name: name of the output file, excluding the .fits extension. 
           Requires: The module pyfits and numpy.
               The file expects equal length X and Y dimensions
               The output file cannot exist beforehand.
           Time complexity: O(n + n^2)
           
       TODO: There is an orientation error, .writeto() actually requires a 
       [y, x] format, not [x, y]. This was corrected previously by rotating the
       radmc3d output image file in the radmc3d image call.
       I should remove that in the radmc3d call and alter this function
       to an [y, x] format.
    """

    image = []
    subprocess.call(['mv', name+'.fits', trash_dir], shell = False)
    with open(model_file, 'r') as f:
        param = f.readlines()[:5]
        dim = param[1]  # Dimensions of the image.
        pix_size = param[3]  # Pixel length in cm.
        wav_len = param[4]#Wavelength of the image in microns.

    nx, ny = dim.split()
    nx = int(nx) #XDim
    ny = int(ny) #YDim
    x_pix_size,y_pix_size = pix_size.split()
    x_pix_sizecm = x_pix_size
    y_pix_sizecm = y_pix_size
    pix_size_AU = float(x_pix_size)/(1.496*10**13)            #Pixsize in AU.
    x_pix_size = float(x_pix_size)/(1.496*10**13)/dist        #Convert pixel length from cm to arcsec.
    y_pix_size = float(y_pix_size)/(1.496*10**13)/dist       
    wav_len = float(wav_len)#/(10**6)                      #Wavelength in micron.

    #The following should correspond to four line values.
    with open(model_file, 'r') as t:
        data1 = t.readlines()[6:(int(nx*ny + 6))]      

    if len(data1[20]) >= 80:
        #Read main data.
        with open(model_file1, 'r') as t:
            data = t.readlines()[5:(int(nx*ny + 5))]      
    
    #The following should correspond to a single line value, in image.out.
    if len(data1[20]) <= 40:
        for line in data1:
            a = line.strip()
            image.append(a)
    
    #The following should correspond to two line values.
    if len(data1[20]) > 40 and len(data[20]) <80 :
        for line in data1:
            a,b= line.split()
            image.append(a)
            image.append(b)
            
    #The following should correspond to four line values.
    if len(data1[20]) >= 80:
        for line in data:
            a,b,c,d = line.split()
            image.append(a)
            image.append(b)
            image.append(c)
            image.append(d)

    jysr_to_sqrd_arcsec_jy_to_jy = 2.35294*10**(12)*y_pix_size*x_pix_size #erg cm$^{-2}$ s$^{-1}$ Hz$^{-1}$ arcsec$^{-2}$*pixelsize_{arcsec}^2 
    image1 = map(float,image)
    image[:] = [x*jysr_to_sqrd_arcsec_jy_to_jy for x in image1] #The factor is a conversion from a flux pr. steradian to flux in Jansky (Jy) for each pixel.
    image2Dim = zeros([nx,ny])

    for y in range(int(ny)):
        for x in range(int(nx)):
            image2Dim[x,y] = image[y*int(nx) + x]

    hdu = pyfits.PrimaryHDU(image2Dim)
    #We then create a HDUList to contain the newly created primary HDU, and write to a new file:

    hdulist = pyfits.HDUList([hdu])
    prihdr = hdulist[0].header
    prihdr.set('observer', 'RADMC-3D')
    prihdr.set('CDELT1', float(x_pix_size)/3600.)
    prihdr.set('CDELT2', float(y_pix_size)/3600.)
    prihdr.set('BUNIT', 'JY/PIXEL')
    prihdr.set('CUNIT1', 'DEG')
    prihdr.set('CUNIT2', 'DEG')
    prihdr.set('LAMBDA', str(wav_len), 'Image wavelength in micrometer')
    prihdr.set('DPC', dist, 'Distance to source in Parsec')
    hdulist.writeto(name + '.fits')
    print '.out file has been written to', name + '.fits'
    hdulist.close()
    sub_res = subprocess.call(['mv', '%s' % model_file, trash_dir],
                              shell=False)
    assert sub_res == 0, 'out_to_fits() failed to move file'
    print 'Move radmc3d file : %s to trash' % model_file


def freq_wav_conv(data, freq=True, micron=False, GHz=False):

    """
    Function to convert frequency to wavelength or vice-versa.
    Assumes frequency to wavelength conversion.
    Input must be supplied in SI units.
    """

    c = 299792458.0  # m/s

    if freq and micron:
        res = c/data/(10.0**(-6.0))
        print 'Wavelength is %0.1f micron' % res
    elif freq and not micron:
        res = c/data
    elif not freq and GHz:
        res = c/data/(10.0**9.0)
        print 'frequency is %0.1f GHz' % res
    elif not freq and not GHz:
        res = c/data
        print 'frequency is %0.1f Hz' % res
    return res

# -----------------------------------------------------------------------------


def conv_freq_wav(inp, freq=True, micron=True, wav=False, input_micron=False):
    """This function converts frequency into wavelength and vice versa,
    depending on the input.
    Args:
         inp = input array.
         freq = If set to true, then convert frequency into wavelength.
                If false, convert wavelength into frequency [Hz]
         micron = If true, then give wavelength in microns.
    Returns: Array of either frequency or wavelength, depending on the
    input.
    """
    res = []
    C = 299792458.0  # Speed of light [m/s].
    if freq:
        if micron:
            res[:] = [C*10.**6./x for x in inp]
        else:
            res[:] = [C/x for x in inp]
    elif wav and input_micron:
        res[:] = [C/(x*10.**(-6.)) for x in inp]
    else:
        res[:] = [C/x for x in inp]
    return res


def conv_flux_freq_to_flux_wav(flux, freq=-99.0, wav=-99.0,
                               freq_flux_to_wav_flux=True,
                               wav_flux_to_freq_flux=False):

    light_speed = 299792458  # m/s
    assert freq_flux_to_wav_flux != wav_flux_to_freq_flux, 'Choose one option'
    if freq_flux_to_wav_flux:
        assert wav > 0.0, 'Must provide frequency'
        flux = light_speed*flux/wav**2.0

    elif wav_flux_to_freq_flux:
        assert freq > 0.0, 'Must provide wavelength'
        flux = light_speed*flux/freq**2.0

    return flux


def dump_res(md_res,file_name):
    """Write pandas dataframe to a csv file."""
    if os.path.isfile(file_name):
        hdr = True
    elif not os.path.isfile(file_name):
        hdr = False
        d_ctr = 0
    
    with open(file_name,'a') as f:
        if not hdr:
            f.write('Time,')
            for x in md_res.keys():
                print 'md_res.keys are', x
                if type(md_res[x]) == dict:
    
                    for y in md_res[x].keys():
                        print 'md_res[x].keys are', y
    
                        if type(md_res[x][y]) == dict:
                            d_ctr+=1
                            for z in md_res[x][y].keys():
                                print 'dic keys vals are',md_res[x][y][z] 
                                f.write('%s_%i,' %(z,d_ctr))    
                        else:        
                            f.write('%s,' %y)    
                else:
                    f.write('%s,' % x)
            f.write('\n')
            
    md_res.to_csv(path_or_buf=file_name,date_format = False,mode='a',
                        header=False,index=False)


def pd_wrt_args_file(md_res, file_name):
    """
    Write pandas dataframe to file. Takes into account the different datatypes
    that arise in my model arguments.
    md_res = A pandas dataframe containing all the model results.
    """
    
    cur_time = strftime("%Y-%m-%d %H:%M:%S", localtime())

    #New approach is to write to a csv file and then read in with pandas later
    if os.path.isfile(file_name):
        hdr = True
    elif not os.path.isfile(file_name):
        hdr = False
        d_ctr = 0

    with open(file_name,'a') as f:
        if not hdr:
            line = 'Time,'
            for x,dum in [(key, value) for (key, value) in sorted(md_res.items())]:
                if type(md_res[x]) == dict:
                    for y,dum in [(key, value) for (key, value) in sorted(md_res[x].items())]:
                        if type(md_res[x][y]) == dict:
                            d_ctr+=1
                            for z, dum in [(key, value) for (key, value) in sorted(md_res[x][y].items())]:
                                line += '%s_%i,' %(z, d_ctr)
                        else:        
                            line += '%s,' %y
                else:
                    line += '%s,' % x
        
            line = line[:-1]
            f.write(line + '\n')              
        val_line = '%s,' % cur_time
        
        for g, x in [(key, value) for (key, value) in sorted(md_res.items())]:
               
            if type(x) not in (list, dict, np.ndarray):  
                val_line += '%s,' %str(x)

            elif type(x) == list:
                val_line += '%s,' %str(x).replace(',', ';')               

            elif type(x) == np.ndarray:
                x = str(list(x))
                x = x.replace('array(','')
                x = x.replace(')','')
                x = x.replace(',',';')
                val_line += '%s,' %x
                
            elif type(x) == dict:
                for y,k in [(key, value) for (key, value) in sorted(x.items())]:
                    if type(x[y]) == dict:
                        for z,v_dum in [(key, value) for (key, value) in sorted(x[y].items())]:
                            val_line += '%s,' % str(x[y][z])
                    elif not type(x[y]) == dict:
                        val_line += '%s,' % str(x[y])

        val_line = val_line[:-1]
        f.write(val_line)
        f.write('\n')

def scalarfield_reader(nx,ny,nz,fname='', binary=True, octree=False,
                           octree_len=0):
        """
        Function to read a scalar field from file. Copy of radmc3dData._scalarfieldReader.

        INPUT:
        ------
            nx = length of grid x array
            ny = length of grid y array
            nz = length of grid z array
            
            fname  - Name of the file containing a scalar variable
            binary - If True the file is in binary format, if False the file format is formatted ASCII text
        
        OUTPUT:
        -------
            Returns a numpy Ndarray with the scalar field
        """

        if binary:
            # hdr[0] = format number
            # hdr[1] = data precision (4=single, 8=double)
            # hdr[2] = nr of cells
            # hdr[3] = nr of dust species
            hdr = fromfile(fname, count=4, dtype=int)
            if not octree:
                if hdr[2]!=(nx*ny*nz):
                    print ' ERROR'
                    print ' Number of grid points in '+fname+' is different from that in amr_grid.inp'
                    print nx*ny*nz
                    print hdr[2]
                    return

            elif octree:

                if hdr[2]!=(octree_len):
                    print ' ERROR'
                    print ' Number of grid points in '+fname+' is different from that in amr_grid.inp'
                    print octree_len
                    print hdr[2]
                    return

            if hdr[1]==8:
                data = fromfile(fname, count=-1, dtype=float64)
            elif hdr[1]==4:
                data = fromfile(fname, count=-1, dtype=float)
            else:
                print 'ERROR'
                print 'Unknown datatype in '+fname
                return
 
           
            #Did I mess with the following line by mistake? Compare with 
            #original if the following lines throw an error. [8/7/2015]
            if not octree:
                if data.shape[0]==(hdr[2]+3):
                    data = reshape(data[3:], [1,nz,ny,nx])
                elif data.shape[0]==(hdr[2]*hdr[3]+4):
                    data = reshape(data[4:], [hdr[3],nz,ny,nx])

                # We need to change the axis orders as Numpy always writes binaries in C-order while RADMC3D
                # uses Fortran-order
                data = swapaxes(data,0,3)
                data = swapaxes(data,1,2)
            elif octree:
                if data.shape[0]==hdr[2]+3:
                    data = reshape(data[3:],[1])
                elif data.shape[0]==hdr[2]*hdr[3]+4:
                    if hdr[3] == 2:
                        data_res = np.zeros([hdr[2],2],dtype=np.float64)
                        data_res[:,0] = data[4:hdr[2]+4]
                        data_res[:,1] = data[hdr[2]+4:]
                        data = data_res               

        if not binary:
            rfile = -1
            try :
                rfile = open(fname, 'r')
            except:
                print 'Error!' 
                print fname+' was not found!'
                
            if (rfile!=(-1)):

                hdr = fromfile(fname, count=3, sep="\n", dtype=int)
                if not octree:
                    if ((nx * ny * nz)!=hdr[1]):
                        print 'Error!'
                        print 'Number of grid points in amr_grid.inp is not equal to that in '+fname
                    else:
    
                        data = fromfile(fname, count=-1, sep="\n", dtype=float64)
                        if data.shape[0]==hdr[1]+2:
                            data = reshape(data[2:], [1, nz,ny,nx])
                        elif data.shape[0]==hdr[1]*hdr[2]+3:
                            data = reshape(data[3:], [hdr[2],nz,ny,nx])
                        # We need to change the axis orders as Numpy always reads  in C-order while RADMC3D
                        # uses Fortran-order
                        data = swapaxes(data,0,3)
                        data = swapaxes(data,1,2)
     
                if octree:
                    if ((octree_len)!=hdr[1]):
                        print 'Error!'
                        print 'Number of grid points in amr_grid.inp is not equal to that in '+fname
                    else:
                        data = fromfile(fname, count=-1, sep="\n", dtype=float64)
                        if data.shape[0]==hdr[1]+2:
                            data = reshape(data[2:],[1])
                        elif data.shape[0]==hdr[1]*hdr[2]+3:
                            if hdr[2] == 2:
                                data_res = np.zeros([hdr[1],2],dtype=np.float64)
                                data_res[:,0] = data[3:hdr[1]+3]
                                data_res[:,1] = data[hdr[1]+3:]
                                data = data_res
            else:
                data = -1

            if rfile != (-1):
                rfile.close()

        # Return data, nr of cells,nr of dust species.
        return data, hdr[2], hdr[3]


def extract_aspect_ratio(filename, star_pix=(950, 950), ran_x=100, ran_y=100,
                         gauss_fit_plot=False, L483_target=True,
                         gaussfit_res_filename=None):
    """
    This function returns the aspect ratio of disk within the 2D pixel range
    input of the given file.
    Args:
        filename = name of the file [fits]
        ran_x = x pixel range [tuple]
        ran_y = y pixel range [tuple]
    Returns:
        Aspect ratio
    """

    from lime_tools import load_fits_im
    assert np.allclose(ran_x, ran_y), 'Segment must be a perfect square.'
    data, hdr = load_fits_im(filename)

    section = data[star_pix[1] - ran_y:star_pix[1] + ran_y,
                   star_pix[0] - ran_x:star_pix[0] + ran_x]
    ny, nx = section.shape

    for x in xrange(nx):
        for y in xrange(ny):
            if np.isnan(section[y, x]):
                print 'found NaN'
                section[y, x] = 0.0

    # Find emission peak position
    where_res = np.where(data == section.max())
    peak_pos = (where_res[1][0], where_res[0][0])  # in x, y coords

    # Fits format is [y, x], while fitgaussian requires [x, y]
    # New slice around target peak emission pos.
    section = data[peak_pos[1] - ran_y:peak_pos[1] + ran_y,
                   peak_pos[0] - ran_x:peak_pos[0] + ran_x]

    # Transpose to (x, y) orientation
    section = section.T

    # Use a 2D gaussian fit
    aspect_ratio = np.nan # If gaussfit fails, return NaN.

    if section.sum() != 0.0:

        # Add noise to all pixels with zero value
        for x in xrange(nx):
            for y in xrange(ny):
                if section[x, y] == 0.0:
                    max_of_2darr
                    print('random value is',  max_of_2darr(section)*0.02*np.random.random())
                    section[x, y] = max_of_2darr(section)*0.02*np.random.random()

        height, center_x, center_y, width_x, width_y, rotation = fitgaussian(section)
        major_axis = max((abs(width_x), abs(width_y)))
        minor_axis = min((abs(width_x), abs(width_y)))
        aspect_ratio = major_axis/minor_axis
        
        if L483_target:
            assert ran_x >= 20 and ran_y >= 20, ('Too small segment around '
                                                 'target.')
    
        if gauss_fit_plot:
            lin_x = np.linspace(1, ran_x*2, ran_x*2)
            lin_y = np.linspace(1, ran_y*2, ran_y*2)
            grid_x, grid_y = np.meshgrid(lin_x, lin_y)
            gauss_data = gaussian(height, center_x, center_y, width_x, width_y,
                                  rotation)
            gauss_data_plot = gauss_data(grid_x, grid_y)
#            levels = np.arange(gauss_data_plot.max()*0.01, gauss_data_plot.max(),
#                               gauss_data_plot.max()*0.15)
            levels = np.arange(gauss_data_plot.max()*0.05, gauss_data_plot.max(),
                               gauss_data_plot.max()*0.05)    
            plt.figure()
            plt.imshow(section, aspect='auto', interpolation='nearest',
                       cmap=plt.cm.jet, origin='lower')
            plt.contour(grid_x, grid_y, gauss_data_plot, levels, linewidths=1.25,
                        origin='lower', cmap=cm.RdBu, alpha=0.5)
    
            if L483_target:
                print('Now writing gauss fit image to {}'.format('{}.png'.format(filename[:-5])))
                plt.savefig('{}.png'.format(filename[:-5]), dpi=300,
                            bbox_inches='tight')
            else:
                plt.show()

        if L483_target and type(gaussfit_res_filename) == str:
    
            if os.path.isfile(gaussfit_res_filename):
                with open(gaussfit_res_filename, 'a') as f:
                    f.write('{}, {}, {}, {} \n'.format(filename, major_axis,
                                                    minor_axis, rotation)) 
            else:
                with open(gaussfit_res_filename, 'w') as f:
                    f.write('file,major_axis,minor_axis,pos_angle \n')
                    f.write('{}, {}, {}, {} \n'.format(filename, major_axis,
                                                    minor_axis, rotation))
    else:
        'Input data is empty. Skipping Gaussfit..'

    return aspect_ratio


def extract_pix_val(filename, pix_1, pix_2):
    """
    This function extracts value of pix_1 and pix_2 from the given file
    and returns them.
    OBS: This data is imported from a fits file, note the unusual [y, x]
    indexing format.
    Args:
        filename = name of the file [fits]
        pix_1 = y, x coordinate of the pixel [tuple]
        pix_2 = y, x coordinate of the pixel [tuple]
    Returns:
        val_1, val_2. Tuple of the pixel values.
    """

    from lime_tools import load_fits_im
    data, hdr = load_fits_im(filename)
    val_1 = data[pix_1]
    val_2 = data[pix_2]

    return val_1, val_2

def H_2_numb_dens(dens, gas_to_dust_mass_ratio=100.0):
    """
    This function calculates the H_2 number density, based on the dust 
    mass density.
    """
    
    N_H2 = gas_to_dust_mass_ratio * dens / M_H2
    
    return N_H2
    
def convert_vtk_file_units(filename):
    """This function converts the log-10 values of radmc3d vtk files into
    regular units by finding the string LOOKUP_TABLE default in the vtk 
    file and raising everything below into the power of 10."""

    old_file = open(filename, 'r')

    new_file = open("model_conv.vtk", "w")
    convert = False
    header = False
    
    try:
        f = old_file.readlines()
       #Read one line to get number of variables.
        for line in f:
            assert line != 'SCALARS dust_density_0 float\n',('Dude! '
            ' Call to first dust species is numbered by 1, not 0!')

            if line == 'SCALARS dust_density_1 float\n':
                new_file.write('SCALARS dust_density_1 double\n')
                header = True
            elif line == 'SCALARS dust_density_2 float\n':
                new_file.write('SCALARS dust_density_2 double\n')
                header = True
            if convert == True:
                line = line.rstrip()
                line = 10.0**(float(line))
                new_file.write('%.10e\n' % line)
            elif not header:
                new_file.write('%s' % line)
            if line == 'LOOKUP_TABLE default\n':
                print 'Found scalar values. Raising to the power of 10..'
                convert = True
            header = False

    finally:
        print 'convert_vtk_file_units executed with success.'
        old_file.close()
        new_file.close()


def run_mctherm(use_mrw=1, paral=True, threads_nr=8, phot_numb=150000,
                timeout='12h', nice=''):
    try:
        if os.path.expanduser('~') == '/Users/Steffen_KJ':
            server = False
            timeout_command = 'gtimeout'
        elif os.path.expanduser('~') == '/groups/astro/cmh154':
            server = True
            timeout_command = 'timeout'
    except:
        print >> sys.stderr, "You are not in the expected wd"
        print >> sys.stderr, "Please relocate"
    print 'timeout command is', timeout

    if use_mrw:
        assert phot_numb > 10000, ('radmc3d will (for unknown reason) return '
                                   'fail code when phot_numb <= 10000. Please '
                                   'use higher phot_numv')
    if nice != '':
        print('radmc3d mctherm says it will be nice to other users (nice -19 '
              'prefix used)!')
    if use_mrw == 1:
        # Ensure that mrw is set, in case a previous crash set mrw to 0.
        if server:
            os.system("sed -i 's/modified_random_walk = 0/modified_random_walk = 1/g' radmc3d.inp")
        elif not server:
            os.system("sed -i '.bak' 's/modified_random_walk = 0/modified_random_walk = 1/g' radmc3d.inp")

    elif use_mrw == 0:
        if server:
            os.system("sed -i 's/modified_random_walk = 1/modified_random_walk = 0/g' radmc3d.inp")
        elif not server:
            os.system("sed -i '.bak' 's/modified_random_walk = 1/modified_random_walk = 0/g' radmc3d.inp")

    if paral:
        proc = exe_popen(nice + '%s %s radmc3d mctherm setthreads %i' % (timeout_command,
                  timeout, threads_nr))
#        proc = Popen(nice + '%s %s radmc3d mctherm setthreads %i' % (timeout_command,
#                     timeout, threads_nr), stdout=PIPE, stderr=STDOUT,
#                     shell=True)
      #  for stdout_line in iter(proc.stdout.readline, ""):
      #      yield stdout_line 
      #  proc.stdout.close()

    elif not paral:
        proc = exe_popen(nice + '%s %s radmc3d mctherm' % (timeout_command, timeout))
#        proc = Popen(nice + '%s %s radmc3d mctherm' % (timeout_command, timeout),
#                     stdout=PIPE, stderr=STDOUT, shell=True)
    tmp = proc.communicate()[0]
    sh_ret_val = proc.returncode

    assert sh_ret_val == 0, 'radmc3d mctherm timed out after running %s.' % timeout

    fin_tert = str(phot_numb - 3000)
    fin_sec = str(phot_numb - 2000)
    fin_phot = str(phot_numb - 1000)

    fin_tert_check = tmp.find(fin_tert)
    fin_sec_check = tmp.find(fin_sec)
    fin_phot_check = tmp.find(fin_phot)

    stop = tmp.find('STOP')
    error = tmp.find('ERROR')
    print tmp
    print fin_tert, fin_sec, fin_phot
    print fin_tert_check, fin_sec_check, fin_phot_check, stop, error
    if use_mrw == 1:
        try:
# =============================================================================
#             assert fin_phot_check != -1 and fin_sec_check != -1 and fin_tert_check != -1, (
#                 'Photon simulation not finished. Error in radmc3d mctherm. Full log follows.'
#                 '%s' %tmp)
# =============================================================================
            assert stop == -1 and error == -1, ("""radmc3d mctherm failed.
                                                Stopping program, Gator don't
                                                take no shit!""")
        except:
            # If mrw fails, redo mctherm with standard photon propagation
            print "WARNING:"
            print "Modified random walk crashed. Restarting with standard photon propagation instead."
            
            use_mrw = 0
            run_mctherm(use_mrw, paral, threads_nr, phot_numb, timeout=timeout,
                        nice=nice)

    elif use_mrw == 0:
# =============================================================================
#         assert fin_phot_check != -1 and fin_sec_check != -1 and fin_tert_check != -1, (
#             'Photon simulation not finished. Error in radmc3d mctherm. Full log is %s' % tmp)
# =============================================================================
        assert stop == -1 and error == -1, ("""radmc3d mctherm failed.
                                            Stopping program, Gator don't
                                            take no shit!""")
    else:
        assert use_mrw == 0 or use_mrw == 1, ('mrw must '
        'be set to 0 or 1.')


def exe_popen(command):

    proc = Popen(command, stdout=PIPE, stderr=STDOUT, shell=True)
    while True:
        output = proc.stdout.readline()
        if output == '' and proc.poll() is not None:
            break
        if output:
            print output.strip()
    readout = proc.poll()

    return proc


def accr_lum(mass, radius, accr_rate):
    """
    Calculate the accretion luminosity. Pay close attention to the units used
    and returned in this function.
    
    Args:
        mass: star mass [M_SUN]
        radius: star radius [R_SUN]
        accr_rate: Accretion rate [M_SUN/yr]
    Returns:
        lum: stellar luminosity [L_SUN]
    """
    G = 6.67259*10.0**(-8.0)  # [cm^3 g-1 s-2]

    lum = (G*mass*M_SUN*(accr_rate*M_SUN/SEC_PR_YEAR)/(radius*R_SUN))/L_SUN

    return lum


def read_vot_sed(vot):
    """
    This function reads in a vizier .vot file (datafile), and returns the
    wavelength in microns, flux and flux error values. If no error values are
    given (assumed to be read as NaNs) then a flux error value of 20 % is
    assumed.

    Will throw a warning when finding NaNs in input vot file, but these
    are handled.
    """
    # read vot table

    table = parse_single_table(vot)
    data = table.array

    arr_len = len(data['sed_freq'])
    freq = np.zeros(arr_len)
    sed_val = np.zeros(arr_len)
    sed_err_val = np.zeros(arr_len)

    # Convert GHz to Hz
    freq[:] = [x*10.0**9.0 for x in data['sed_freq']]

    wav = conv_freq_wav(freq)
    sed_val[:] = [x for x in data['sed_flux']]
    sed_err_val[:] = [x for x in data['sed_eflux']]

    # Remove duplicates, i.e. extra Sed points from vizier.
    # Chose a single data-point. Unclear on vizier photometryviewer
    # which should be used.

    uniq_arr = np.unique(wav, return_index=True)
    uniq = [x for x in uniq_arr[1]]
    wav = [wav[i] for i in uniq]

    for i in xrange(arr_len):
        if np.isnan(sed_err_val[i]):
            sed_err_val[i] = 0.2*sed_val[i]

    sed_val = [sed_val[i] for i in uniq]
    sed_err_val = [sed_err_val[i] for i in uniq]

    return wav, sed_val, sed_err_val


def open_csv_file(filename, start, splitter):
    """ This function opens a csv file of unknown length and returns a list
    of values. By default, any string than can be converted to float, will be.

    Args: filename = name of the file.
          start =  starting indice, if some lines in the csv file should be
          skipped.
          splitter = the command splitting the values. If comma, use "','"  if
          whitespace simply use "None".
    Returns: a list of arrays, as floats if possible, otherwise strings.

    """

    var_nam = {}
    list_res = []
    c = 0

    fil = open(filename, 'r')
    try:
        f = fil.readlines()[start:]
        # Read one line to get number of variables.
        for first_line in f:
            if c > 0:
                break
            length = len(first_line.split(splitter))
            c += 1

        for i in range(length):
            var_nam['var_'+str(i)] = []

        # Append values to arrays. Convert to float by default, if possible.
        for row in f:
            var_val = row.split(splitter)
            for i in range(len(var_val)):
                try:
                    var_nam['var_'+str(i)].append(float(var_val[i]))
                except ValueError:
                    var_nam['var_'+str(i)].append(var_val[i])
    finally:
        fil.close()

    # Create list of arrays.
    for i in range(length):
        list_res.append(var_nam['var_'+str(i)])
    return list_res[0:3]  # Return only first three arrays.


def mov_box_av(file_name, leng=150., width=50., dist=120, deg=True,
               model=True):
    """
    This function calculates the moving box average with a given length and
    width, in an given azimuthal range. 
    """
    from lime_tools import load_fits_im
    data, hdr = load_fits_im(file_name)

    if deg:
        arcsec_pr_pix = abs(hdr['CDELT1'] * 3600.)
        pix_size = dist * arcsec_pr_pix  # Convert deg to arcsec then to AU

    mod_cent = (hdr['NAXIS1']/2., hdr['NAXIS2']/2.)
    
    # Define azimuth from source A to B
    # If radmc3d model.
    if model:
        pix_A = (947, 948) # Source A
        pix_B = (1100, 1102) # Source B
    else:
        pix_A = (79, 77) # Source A
        pix_B = (114, 117) # Source B        

    # Define ellipse to track the box movement
    ellip_azi = np.arange(np.pi/4., np.pi*1.25, np.pi/3000.)
    sm_maj = np.sqrt((pix_B[0] - pix_A[0])**2. + 
                     (pix_B[1] - pix_A[1])**2.)/2.
    sm_min = 150./pix_size

    print ellip_azi
    print sm_min
    print sm_maj
    print 'Distance between stars is %.3f pixels' % (sm_maj * 2.0)
    print 'Distance between stars is %.3f arcsec' % (sm_maj * 2.0 * arcsec_pr_pix)
    print 'Distance between stars is %.3f AU' % (sm_maj * 2.0 * pix_size)
    ellip_coords = np.ndarray((len(ellip_azi), 2), dtype=np.float64)

    ellip_coords[:] = [(sm_maj*np.cos(x), sm_min*np.sin(x)) for x in ellip_azi]

    # Calculate the pixels in each box-step.
    x_coord = [x for x, y in ellip_coords]
    y_coord = [y for x, y in ellip_coords]

    # Construct meshgrid of the radial and meridional dimensions.
    x_matrix, y_matrix = np.meshgrid(x_coord, y_coord)

    plt.scatter(x_coord, y_coord)
    plt.xlim(-150, 110)
    plt.ylim(-150, 110)
    plt.show()


def kepler_orb_vel(mass, radius, SI=False):

    """ Kepler orbital velocity.

    Args:
        mass: central mass [solar masses]
        radius: orbital distance [AU]
    Returns:
        orbital velocity [cm/s (default) or m/s if SI is set to true]
    """

    G = 6.67259*10.0**(-8.0)  # [cm^3 g-1 s-2]

    orb_vel = np.sqrt(G*mass*M_SUN/(radius*AU))
    try:
        if not SI:
            return orb_vel
        elif SI:
            return orb_vel/100.
    except:
        assert type(SI) == bool, "SI must be bool, ie. set to True or False."
        
def dist_from_incl(proj_dist, incl):
    
    real_dist = proj_dist*np.sqrt(np.tan(incl*np.pi/180.)**2.+1.)
    
    
    return real_dist


def erg_to_jy(flux):
    """
    Function to convert erg to Jy flux.
       Args:
            Flux = Array of Flux [erg cm^-2 s^-1 Hz^-1].
    """

    flux[:] = [x*10.0**(23.0) for x in flux]

    return flux


def find_min_chi2_in_file(file_name, molecule, use_combined_CS_H13CN_imfit):

    star_mass_list = []
    disk_radii = []
    chi2_kepler_list = []
    chi2_infall_list = []
    rest_vel_val_list = []
    beta_list = []

    with open(file_name, 'r') as f:
        lines = f.readlines()[1:]
        for line in lines:
            (mass_file, radius_file, rest_vel_val, chi2_kepler_file,
             beta_val, chi2_infall_file) = line.split(',')
            star_mass_list.append(float(mass_file))
            disk_radii.append(float(radius_file))
            rest_vel_val_list.append(float(rest_vel_val))
            chi2_kepler_list.append(float(chi2_kepler_file))
            beta_list.append(float(beta_val))
            chi2_infall_list.append(float(chi2_infall_file))

    min_kep_chi2_val = 1e10
    min_infall_chi2_val = 1e10
    for i, val in enumerate(chi2_kepler_list):
        if val < min_kep_chi2_val:
            min_kep_chi2_val = val
            kep_index = i

    for i, val in enumerate(chi2_infall_list):
        if val < min_infall_chi2_val:
            min_infall_chi2_val = val
            infall_index = i

    print('==================================================================')
    print('Infall best fit is')
    print('Chi^2 = {}, beta = {}, v_lsr = {}'.format(min_infall_chi2_val,
                                                     beta_list[infall_index],
                                                     rest_vel_val_list[infall_index]))

    print('Kepler best fit is')
    print('Chi^2 = {}, mass = {}, disk radius = {}, v_lsr = {}'.format(
                                                           min_kep_chi2_val,
                                                           star_mass_list[kep_index],
                                                           disk_radii[kep_index],
                                                           rest_vel_val_list[kep_index]))
    print('==================================================================')
    if not use_combined_CS_H13CN_imfit:
        print('Creating plots with best-fit parameters..')
        os.chdir('/Users/Steffen_KJ/Dropbox/PhD/L483')
        print('Now in {}'.format(os.getcwd()))
    # =============================================================================
        print('Executing command: python mk_PV.py -plot -{} -m_star_kep {} -disk_rad_kep {} -m_star_infall {} -disk_rad_infall {} -infall_beta {}'.format(molecule,
                      star_mass_list[kep_index], disk_radii[kep_index],
                      star_mass_list[infall_index], disk_radii[infall_index],
                      beta_list[infall_index]))
    # =============================================================================
    
        returncode = os.system('python mk_PV.py -{} -plot -m_star_kep {} -disk_rad_kep {} -m_star_infall {} -disk_rad_infall {} -infall_beta {}'.format(molecule,
                     star_mass_list[kep_index], disk_radii[kep_index],
                     star_mass_list[infall_index], disk_radii[infall_index],
                     beta_list[infall_index]))
    
        assert returncode == 0, 'Execution error, check mk_pv.py args.'


def class_disk_dens(x, y, z, p, x_0, rho_0, H_0, flr, spher_coord=False,
                    azi=False):
    """
    This function returns the dust density of a classical PP-disk model.
    If cartesian, input must be x,y,z = radius, theta, phi
    If spherical, input must be x,y,z = radius, theta, phi
    """

    midplane_r = np.sqrt(x**2.0 + y**2.0)
    z_disk = z

    surf_dens = rho_0*(midplane_r/x_0)**(-p)
    H_d = H_0*(midplane_r/x_0)**(1.0 + flr)  # Was divided by 10 AU before

    rho_dust = surf_dens/(H_d*np.sqrt(2.*np.pi))*np.exp(-z_disk**2./(2.*H_d**2.))

    return rho_dust


import datetime

def time_stamped(fname, fmt='%Y-%m-%d-%H-%M-%S_{fname}'):
    """
    Taken from stackoverflow, see
    http://stackoverflow.com/questions/5214866/
    python-add-date-stamp-to-text-file
    """

    return datetime.datetime.now().strftime(fmt).format(fname=fname)


def temp_dens_to_ascii(disk_orientation, binary=True, octree=True,
                       oct_file='octree_idx.out',
                       ref_pos=[-224.86*AU, -224.86*AU, 0.0],
                       n_nn=6, vertical=False, out_put_all=False,
                       file_name='IRAS_16293_pathway.dat'):
    """
    This function reads in RADMC-3D dust temperature and density files and
    outputs them into an ascii file of [dist (to chosen star),
    density (g cm^-3), temperature (K)].

    Args:
        ref_pos = Position of reference point (usually the central star) in
                  grid. Tuple of AU values, (x, y, z)
                  coordinates in spherical coordinates.
        pathway = Tuple of (meriodional, azimuthal), defining a
                  pathway through the model in the meriodional, azimuth
                  direction away from the chosen center (likely a central
                  star). The 6 nearest neighbors are found using a kd-tree
                  from which the grid cell temperature and density values are
                  averaged in and written to the output file.
    Requires:
        temp_dens_to_ascii() must be executed in a folder containing
        dust_density.binp, dust_temperature.bdat, amr.inp file (required by the
        readDustTemp and readDustDens functions invoked by temp_dens_to_ascii).
    Returns: None. Writes values to ascii file of x [cm], y [cm], z[cm],
             dust density [g cm^(-3)], dust temperature [K]. If pathway is not
             an empty tuple, the output will be of r [cm],
             dust density [g cm^(-3)], dust temperature [K].

    """

    oct_r = []
    oct_theta = []
    oct_phi = []

    # Read in grid and add to radmc3dData instance
    prox = radmc3d_analyze.radmc3dGrid()
    prox.read_grid()
    prox = radmc3d_analyze.radmc3dData(prox)

# =============================================================================
#                            Open datafiles
# =============================================================================

    # Octree file has spherical coords
    with open(oct_file, 'r') as f:
        data = f.readlines()
        for line in data:
            r_val, theta_val, phi_val = line.split(',')
            oct_r.append(float(r_val))
            oct_theta.append(float(theta_val))
            oct_phi.append(float(phi_val))
        n_cells = len(oct_r)

    assert max(oct_theta) <= np.pi, ("Coordinate error - is input octree file "
                                     "in spherical coordinates?")
    assert max(oct_phi) <= 2.0*np.pi, ("Coordinate error - is input octree "
                                       "file in spherical coordinates?")

    # Prepare cartesian coords arrays

    oct_x = np.zeros(n_cells)
    oct_y = np.zeros(n_cells)
    oct_z = np.zeros(n_cells)

# =============================================================================
#                    Get coordinates relative to chosen coord center.
# =============================================================================
    if vertical:
        #Define a 15 AU midplane radius point in the disk reference frame.
        midplane_pt = [15.0*AU, 0.0, 0.0]
        # Convert
        midplane_pt[0], midplane_pt[1], midplane_pt[2] = z_x_axis_rot(x_c=midplane_pt[0],
                                      y_c=midplane_pt[1], z_c=midplane_pt[2],
                                      incl=-disk_orientation[0], # Use reverse angles to get back to global model coords.
                                      azith_turn=-disk_orientation[1],
                                      ref_pos=[0.0, 0.0, 0.0],
                                      return_cart=True)

        ref_pos[0] = ref_pos[0] + midplane_pt[0]
        ref_pos[1] = ref_pos[1] + midplane_pt[1]
        ref_pos[2] = ref_pos[2] + midplane_pt[2]

        print('new ref pos is', ref_pos[0]/AU, ref_pos[1]/AU, ref_pos[2]/AU)

    for i in xrange(n_cells):
        # Convert to cartesian coords
        oct_x[i], oct_y[i], oct_z[i] = transf_sph_cart(oct_r[i], oct_theta[i],
                                                       oct_phi[i])


        # Convert to disk reference frame.
        oct_x[i], oct_y[i], oct_z[i] = z_x_axis_rot(oct_x[i], oct_y[i],
                                                    oct_z[i],
                                                    incl=disk_orientation[0],
                                                    azith_turn=disk_orientation[1],
                                                    ref_pos=ref_pos,
                                                    return_cart=True)

# =============================================================================
#                   Extract temperature and density
# =============================================================================

    prox.readDustTemp(binary=binary, octree=octree, octree_len=n_cells)
    prox.readDustDens(binary=binary, octree=octree, octree_len=n_cells)

    # Create test of the grid, density and temperature values.
    temp = prox.dusttemp
    dens = prox.rhodust
    temp_fin = np.zeros(temp.shape[0])
    dens_fin = np.zeros(dens.shape[0])

    for i in xrange(n_cells):
        if dens[i, 0] > dens[i, 1]:
            max_dens_species = 0
        else:
            max_dens_species = 1

        temp_fin[i] = temp[i, max_dens_species]
        dens_fin[i] = dens[i, max_dens_species]

    assert min(temp_fin) >= 0.0, 'Negative temperature value found'
    assert min(dens_fin) >= 0.0, 'Negative density value found'

    # Define pathway averaged values, through model.
    if not out_put_all:

        tree_nodes = []
        temp_pathway = []
        dens_pathway = []

        if vertical:
            r_pathway = np.arange(0.0, 150.0*AU, 0.5*AU)  # was 2 AU
            polar_angle = 0.0  # Purely vertical direction
            azimuth = 0.0
        if not vertical:
            # Define pathway values
            r_pathway_1 = np.arange(0.0, 150.0*AU, 0.5*AU)  # was 2 AU
            r_pathway_2 = np.arange(150.0*AU, 300.0*AU, 10.0*AU)
            r_pathway_3 = np.arange(300.0*AU, 1000.0*AU, 700.0*AU/6.0)
            r_pathway_4 = np.arange(1000.0*AU, 7000.0*AU, 1000.0*AU)

            r_pathway = np.concatenate((r_pathway_1, r_pathway_2, r_pathway_3,
                                       r_pathway_4), axis=0)
            polar_angle = np.pi/2.0  # pathway[0]
            azimuth = np.pi/2.0

        pathway_nodes = [transf_sph_cart(r_node, polar_angle, azimuth) for
                         r_node in r_pathway]

        # Create kd-tree of model values.
        for i in xrange(n_cells):
            tree_nodes.append((oct_x[i], oct_y[i], oct_z[i]))

        tree = sci_spat.cKDTree(tree_nodes, copy_data=False,
                                balanced_tree=False)

        # Find six nearest neighbors, take the average value
        for node in pathway_nodes:
            dist, index = tree.query(node, k=n_nn, eps=0.05, p=2, n_jobs=1)
            temp_pathway.append(np.mean(temp_fin[index]))
            dens_pathway.append(np.mean(dens_fin[index]))


# =============================================================================
#                               Write file
# =============================================================================

    if out_put_all:
        file_name = 'IRAS_16293.dat'
        with open(file_name, 'w') as f:
            f.write('x [cm], y [cm], z[cm], Dust density [g cm^(-3)],'
                    'Dust temperature [K]\n')
            for i in xrange(n_cells):
                f.write('%.20e, %.20e, %.20e, %.20e, %.20e \n' % (oct_x[i],
                                                                  oct_y[i],
                                                                  oct_z[i],
                                                                  dens_fin[i],
                                                                  temp_fin[i]))
    else:
        with open(file_name, 'w') as f:
            f.write('r [cm], Dust density [g cm^(-3)], Dust temperature [K]\n')
            for i in xrange(len(r_pathway)):
                f.write('%.20e, %.20e, %.20e \n' % (r_pathway[i],
                                                    dens_pathway[i],
                                                    temp_pathway[i]))
    print 'Done. output written to %s' % file_name


def toomre(t_gas, m_star, radius, sigma):
    cs = sound_speed(t_gas)
    ang_vel = kep_ang_vel(m_star, radius)
    toomre_nr = cs*ang_vel/(np.pi*GRAV_CNST*sigma)

    return toomre_nr


def sound_speed(t_gas):
    """
    Calculate sound speed in an ideal gas.
    """

    mu = 2.3  # Mean molecular weight, here assuming H_2
    cs = np.sqrt(K_B*t_gas/(mu*M_P))

    return cs


def kep_ang_vel(m_star, radius):
    """
    Angular velocity in Kepler disk.
    """

    ang_vel = np.sqrt(GRAV_CNST*m_star/radius**3.0)

    return ang_vel


def infer_scale_height(cs, ang_vel):
    """
    Calculate the scale-height of dust and gas in a Keplerian disk.
    """

    return cs/ang_vel


def z_axis_rot(x, y, azith_turn):
    """
    Perform z-axis rotation. Azith_turn is defined as the rotation angle
    from the positive x-axis towards the positive y-axis, i.e.,
    counter-clockwise rotation.
    Args:
        x = x coordinate
        y = y coordinate
        azith_turn = Angle of rotation [radians]
    """
    x_c_zrot = x*np.cos(azith_turn) - y*np.sin(azith_turn)
    y_c_zrot = x*np.sin(azith_turn) + y*np.cos(azith_turn)

    return x_c_zrot, y_c_zrot


def points_dist(pos_1, pos_2):
    """
    Calculate the distance between two positions.
    """

    dist = np.sqrt((pos_2[0] - pos_1[0])**2.0 + (pos_2[1] - pos_1[1])**2.0)

    return dist

#def z_x_axis_rot(x_c, y_c, z_c, incl, azith_turn,
#                         ref_pos=(0.0, 0.0, 0.0), return_cart=False):
#    """
#    This function performs rotation around the z-axis and the rotation 
#    around the x-axis.
#    Transform coordinate system by rotating around axes.
#    First z-axis rotation (i.e. turn in azimuth).
#    Args: x_c, y_c, z_c (Cartesian coordinates of the input point)
#          ref_pos = reference position, usually that of a star.
#    """
    
#    x_c = x_c - ref_pos[0]
#    y_c = y_c - ref_pos[1]        
#    z_c = z_c - ref_pos[2]

    #Transform coordinate system by rotating around axes.
    #First z-axis rotation (i.e. turn in azimuth).
#    x_c_zrot = x_c*np.cos(azith_turn) + y_c*np.sin(azith_turn)
#    y_c_zrot = -x_c*np.sin(azith_turn) + y_c*np.cos(azith_turn) 
    
    #Then perform rotation around the z-axis.
#    y_c_xrot = y_c_zrot*np.cos(incl) + z_c*np.sin(incl)
#    z_c_xrot = -y_c_zrot*np.sin(incl) + z_c*np.cos(incl)

#    grid_radius = np.sqrt((x_c_zrot)**2.+(y_c_xrot)**2.+(z_c_xrot)**2.)
    
#    midplane_r = np.sqrt(x_c_zrot**2.0 + y_c_xrot**2.0)
#    theta_disk = np.arccos(z_c_xrot/grid_radius)
#    phi_disk = azi_from_cart(x_c_zrot, y_c_xrot)

#    if return_cart:
#        x_c_zrot, y_c_xrot, z_c_xrot
#    else:
#        return grid_radius, theta_disk, phi_disk, midplane_r


def gaussian(height, center_x, center_y, width_x, width_y, rotation):
    """
    Returns a gaussian function with the given parameters
    Credit: Andrew Gissel.
    """

    width_x = float(width_x)
    width_y = float(width_y)
    rotation = np.deg2rad(rotation)
    center_x = center_x * np.cos(rotation) - center_y * np.sin(rotation)
    center_y = center_x * np.sin(rotation) + center_y * np.cos(rotation)

    def rotgauss(x, y):
        xp = x * np.cos(rotation) - y * np.sin(rotation)
        yp = x * np.sin(rotation) + y * np.cos(rotation)
        g = height*np.exp(-(((center_x - xp)/width_x)**2 +
                            ((center_y - yp)/width_y)**2)/2.)
        return g

    return rotgauss


def log_interp(x_arr, y_arr, int_p):

    logz = np.log10(int_p)
    logx = np.log10(x_arr)
    logy = np.log10(y_arr)

   # lin_interp = sp.interpolate.interp1d(logx, logy, kind='linear')
   # log_interp = lambda zz: np.power(10.0, lin_interp(np.log10(zz))

    # return np.power(10.0, np.interp(int_p, logx, logy))
    return 10.0**np.interp(logz, logx, logy)


def spectral_radiance_freq(freq, temp):

    H_P = 6.6260755*10**(-27.)  # erg s^-1
    K_B = 1.380658*10**(-16.)  # erg K^-1
    C_cgs = 29979245800.0   # Speed of light [cm/s].
    spectral_radiance_val = ((2.0*H_P*freq**3.0)/(C_cgs**2.0))*1.0/(np.exp(H_P*freq/(K_B*temp)) - 1.0)

    return spectral_radiance_val


def spectral_radiance_wav(wav, temp):
    # Rayleigh-Jeans law for the spectral brightness of blackbody radiation
    H_P = 6.6260755*10**(-27.) # erg s^-1
    K_B = 1.380658*10**(-16.) # erg K^-1
    C_cgs = 29979245800.0 # Speed of light [cm/s].
    #rayleigh_jeans_cgs = 2.0*K_B*temp*freq**2.0/C_cgs**2.0

    spectral_radiance_val = (2.0*H_P*C_cgs**2.0)/(wav**5.0) * 1.0/(np.exp(H_P*C_cgs/(wav*K_B*temp)) - 1.0)

    return spectral_radiance_val


def disk_mass_opt_thin_em(freq, temp, tot_flux_jy, dist_PC, ice_fract,
                          bare_grain_fract, use_JKJ_deriv=False):
# =============================================================================
# Units are erg/(s cm^2 Hz) * cm^2/(cm^2 g^-1 erg s^-1 sr^-1 cm^-2 Hz^-1)
# ---> grams
# =============================================================================

    assert np.allclose(ice_fract + bare_grain_fract, 1.0), ('Total ice and '
                       'bare grain fraction is above 1.0')

    wav = freq_wav_conv(freq)
    BB_val = spectral_radiance_freq(freq, temp)

    tot_flux = tot_flux_jy*10**(-23.0)

    # Interpolate dust opacity values
    opac_ice_interp = log_interp([700, 1000], [2.57, 1.37], wav*10**6.0)
#    print('opac_ice_interp is {}'.format(opac_ice_interp))
    dist = dist_PC*PC
    opac_bare_interp = log_interp([700, 1000], [4.56, 2.74], wav*10**6.0)
    opac_mix_interp = (opac_ice_interp*ice_fract +
                       opac_bare_interp*bare_grain_fract)

    print('Average wavelength of observations is {} microns'.format(wav*10.0**6.0))

  #  print('Ice opacity at {} is {}'.format(wav, opac_ice_interp))
  #  print('Bare grain opacity at {} is {}'.format(wav, opac_bare_interp))
  #  print('Mix opacity at {} is {}'.format(wav, opac_mix_interp))
  #  print('BB_val is', BB_val)

    # Get a fitting average opacity of the dust
#    print('opac_mix_interp is {}'.format(opac_mix_interp))
    if use_JKJ_deriv:
        tot_mass = 0.18*(1.0/(opac_mix_interp/opac_ice_interp))*tot_flux_jy*(dist_PC/200.0)**2.0*(np.exp(0.55*(30.0/temp)) - 1.0)
#        print('Factor is {}'.format(0.18*(1.0/(opac_mix_interp/opac_ice_interp))*tot_flux_jy*(dist_PC/200.0)**2.0))
    else:
        tot_mass = tot_flux*dist**2.0/(opac_mix_interp*BB_val)*100.0/M_SUN

    return tot_mass


def max_of_2darr(data):
    data = np.array(data)
    assert len(data.shape) == 2, 'Only 2D arrays are allowed. Input is {}'.format(data)
    res = max(map(max, data))

    return res

