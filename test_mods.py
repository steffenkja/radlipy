#!/usr/bin/python2
# -*- coding: utf-8 -*-

"""
Created on Wed Apr 13 19:33:06 2016

@author: Steffen_KJ
"""
from __future__ import division
from __future__ import print_function
#from functools import partial as partial
import numpy as np
import os
import scipy as sp
#position = "OneDrive - Københavns Universitet"
position = "Dropbox"
os.system('python "/Users/Steffen_KJ/%s/PhD/Scientific_software/Python/modules/cython_setup.py" build_ext --inplace' % position)
from cgs_cnst import AU, M_SUN, SEC_PR_YEAR, L_SUN, R_SUN
from radmc3d_tools import wrt_model_file, accr_lum, z_axis_rot
from cython_mod import (transf_sph_cart, find_octree_reg,
                        azi_from_cart, transf_cart_sph, octree_nodes,
                        find_evap_rad_oct, mk_dust_arc_tree, DustDensity,
                        rot_vel_for_lime, find_torus, z_x_axis_rot)
from lime_tools import find_chan_vels, cube
import radmc3d_analyze

def test_transf_sph_cart():
    r = 100.
    theta = np.pi/2.
    phi = np.pi

    x_ct = r*np.sin(theta) * np.cos(phi)
    y_ct = r*np.sin(theta) * np.sin(phi) 
    z_ct = r*np.cos(theta)
    
    x_c,y_c,z_c = transf_sph_cart(r,theta,phi)
    
    #Now perform the test
    
    assert x_ct == x_c
    assert y_ct == y_c
    assert z_ct == z_c


 #   def jy_to_SI_flux(self,flux, wavelength):
 #       """Function to convert Jansky to SI flux.  
 #          Args:           
 #               Flux = Array of Flux [Jy].
 #       """
 #       C = 299792458.0 #Speed of light [m/s].
 #       flux = [1,2,3]
 #       wavelength = [0.0001,0002,0003]
 #       flux[:]= [x*10.0**(-23.0) for x in flux]
 #       flux1[:]= [x*(C/(y*10.0**(-6.0))) for x,y in zip(flux, wavelength)]
 #        
 #       return flux

def test_find_octree_reg():
    source_nr = 2
    p_stars = np.ndarray((2,3),np.float64)

    p_stars[0,:] = [x for x in [-212.132*AU,-212.132*AU,0.]]
    p_stars[1,:] = [x for x in [212.132*AU,212.132*AU,0.]]
    
    x_c = -150.*AU
    y_c = 0.0
    z_c = 0.0

    tor_dic = {'torus':True,'tor_c':300.*AU,'tor_rad':150.*AU,'max_dens':2.5*10.**(-17.),
               'tor_incl':0.0, 'sm_min':150.*AU, 'tor_p': -0.25,
               'fixed_dens_across_arc':True}
    star_1_azi = azi_from_cart(p_stars[0,0],p_stars[0,1])
    star_2_azi = azi_from_cart(p_stars[1,0],p_stars[1,1])
    azi_lim_1,azi_lim_2 = np.sort([star_1_azi,star_2_azi]) 
    rad,theta,phi = transf_cart_sph(x_c,y_c,z_c)  
    
    class oct_instance():
        def __init__(self):
            pass
    oct_inst = oct_instance()
    oct_inst.octree = True
    oct_inst.disks = True
    oct_inst.oct_rad_1 = 100.*AU
    oct_inst.oct_rad_2 = 100.*AU
    oct_inst.inn_disk_ref_1_rad = 30.*AU
    oct_inst.inn_disk_ref_2_rad = 30.*AU
    oct_inst.azi_lim_1 = azi_lim_1      
    oct_inst.azi_lim_2 = azi_lim_2      
    oct_inst.tor_dic = tor_dic
    oct_inst.source_nr = source_nr
    oct_inst.p_stars = p_stars   
    oct_inst.no_disk_level = 2
    oct_inst.inn_disk_1_lev = 6
    oct_inst.inn_disk_2_lev = 6
    oct_inst.disk_1_lev = 2
    oct_inst.disk_2_lev = 2
    oct_inst.tor_lev = 1               
    
    
    #Get kdTree
    tree = mk_dust_arc_tree(sm_maj=300.*AU, sm_min=150.*AU)
    reg_unref, levels, disk_1_inn_ref, disk_2_inn_ref = find_octree_reg(rad,
    theta,phi,oct_inst,
    p_stars,tor_dic, tree)

    assert levels > 0
    
def test_octree_nodes():
    x_arr_compare = [0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 2.5, 3.5, 2.5, 3.5, 
                     2.5, 3.5, 2.5, 3.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 
                     2.5, 3.5, 2.5, 3.5, 2.5, 3.5, 2.5, 3.5, 0.5, 1.5, 0.5, 1.5, 
                     0.5, 1.5, 0.5, 1.5, 2.5, 3.5, 2.5, 3.5, 2.5, 3.5, 2.5, 3.5, 
                     0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 2.5, 3.5, 2.5, 3.5, 
                     2.5, 3.5, 2.5, 3.5]
    x_arr,y_arr,z_arr,node_pos,vol_list = octree_nodes(xw_0=0, xw_1=4, yw_0=0, yw_1=4, 
                 zw_0=0, zw_1=4, levels=2, x_arr=[], y_arr=[],
                 z_arr=[])
    #print y_arr             
    assert x_arr == x_arr_compare               
    
    


def test_find_evap_rad_oct():
    rhodust = np.zeros((10,2))
    rhodust[:,:] = [[x,y] for x,y in zip([10]*10,[5]*10)]
    vol = np.zeros(10)
    dust_temp = np.zeros((10,2))
    vol[:] = [x for x in [10]*10]
    dust_temp[:] = [[x,y] for x,y in zip([100]*10,[100]*10)]
    i_dust = 0
    temp = 90
    n_dust = 2
    source_nr =2
    recurs_subli = False
    total_mass = False
    
    mass,rad,vol = find_evap_rad_oct(vol,rhodust,dust_temp,temp,i_dust,n_dust, 
                                     source_nr,recurs_subli,total_mass)
    assert mass == 1000

def DustDensity_methods(cav_rad, octree, p_stars, oct_rad_1, oct_rad_2,
                       subli_list, model='', nci= 30,
                       disk=False, binary=False, cav_enable=False,
                       streamline_view_enable=False, cyth_profiler = True,
                       source_nr=2, recurs_subli=False, tor_dic=None,
                       azi_lim_1 = 0.0, azi_lim_2 = np.pi, oct_inst=None,
                       DustDensity_args=None, grid=False, opac=False, 
                       stars=False, **kwargs):
    

    G = 6.67259*10.0**(-8.0)
    PI = np.pi
    PI_HALF = np.pi/2.
    incl = 0.0
    azith_turn = 0.0

    inside_a_disk = True
    # Read the parameters from the problem_params.inp file 
    modpar = radmc3d_analyze.readParams()

    # Make a local copy of the ppar dictionary
    ppar = modpar.ppar

    if binary:
        modpar.setPar(['rto_style', '3', '', ''])
    if kwargs:
        for ikey in kwargs.keys():
            modpar.ppar[ikey] = kwargs[ikey]        
            if type(kwargs[ikey]) is float:
                modpar.setPar([ikey, ("%.7e"%kwargs[ikey]), '', ''])
            elif type(kwargs[ikey]) is int:
                modpar.setPar([ikey, ("%d"%kwargs[ikey]), '', ''])
            elif type(kwargs[ikey]) is str:
                modpar.setPar([ikey, kwargs[ikey], '', ''])
            elif type(kwargs[ikey]) is list:
                dum = '['
                for i in range(len(kwargs[ikey])):
                    if type(kwargs[ikey][i]) is float:
                        dum = dum + ("%.7e"%kwargs[ikey][i])
                    elif type(kwargs[ikey][i]) is int:
                        dum = dum + ("%d"%kwargs[ikey][i])
                    elif type(kwargs[ikey][i]) is str:
                        dum = dum + (kwargs[ikey][i])
                    else:
                        print(' ERROR ')
                        print(' Unknown data type in '+ikey)
                        print(kwargs[ikey][i])
    
                    if i<len(kwargs[ikey])-1:
                        dum = dum + ', '
                dum = dum + (']') 
                modpar.setPar([ikey, dum, '', ''])
    
        modpar.writeParfile()
        ppar = modpar.ppar
    print(ppar)

# -----------------------------------------------------------------------------
# Create the grid
# -----------------------------------------------------------------------------

    if grid:
        # Call instance for grid construction    
        grid = radmc3d_analyze.radmc3dGrid()
        
        # Make wavelength grid
        grid.makeWavelengthGrid(ppar=ppar)
    
        # Make spatial grid
        # Note that if cav_rad is enabled, then grid.nx will change 
        # (more radial cells added in cavity region).
        grid.make_spatial_grid(ppar=ppar,cav_rad=cav_rad,nci=nci, 
                               cav_enable=cav_enable,
                               source_nr=source_nr, disk=disk)
        
        # Write the spatial grid to the file amr_grid.inp
        if not octree:    
            grid.write_spatial_grid(octree=octree,oct_rad_1=oct_rad_1,
                                    oct_rad_2=oct_rad_2,
                                    p_stars=p_stars,source_nr=source_nr)
    
            octree_indices = []
    
        if octree:
            octree_indices,n_branches,ref_nr = grid.write_spatial_grid(
                           octree=octree,
                           oct_rad_1=oct_rad_1,oct_rad_2=oct_rad_2,p_stars=p_stars,
                           source_nr=source_nr,tor_dic=tor_dic,azi_lim_1=azi_lim_1,
                           azi_lim_2=azi_lim_2,oct_inst=oct_inst)
               
# --------------------------------------------------------------------------------------------
# Dust opacity
# --------------------------------------------------------------------------------------------
    if opac:
    
        if ppar.has_key('dustkappa_ext'):
            opac=radmc3d_analyze.radmc3dDustOpac()
            #Master dust opacity file
            opac.writeMasterOpac(ext=ppar['dustkappa_ext'], 
                                 scattering_mode_max=ppar['scattering_mode_max'])
        else:
            opac=radmc3d_analyze.radmc3dDustOpac()
            # Calculate the opacities and write the master opacity file
            opac.makeOpac(ppar=ppar)

# --------------------------------------------------------------------------------------------
# Create the input radiation field (stars at this point) 
# --------------------------------------------------------------------------------------------
    if stars:
        stars = radmc3d_analyze.radmc3dStars(ppar=ppar)
        stars.getStellarSpectrum(tstar=ppar['tstar'], rstar=ppar['rstar'],
                                 wav=grid.wav)

# --------------------------------------------------------------------------------------------
# Try to get the specified model
# --------------------------------------------------------------------------------------------
    try:
        import model_IRAS_16293 as mdl
    except:
        print('model_IRAS_16293.py not found in python path')

    data = radmc3d_analyze.radmc3dData(grid)
# --------------------------------------------------------------------------------------------
# Create the dust density distribution 
# --------------------------------------------------------------------------------------------
    
    # If we have octree refinement, then DustDensity() need help to
    # construct the correct dust density multidim array.

    if octree:     
        model_dir = os.getcwd()        
    else:
        ref_nr = 0
        
    # SKIP USUAL STEP AND GO DIRECTLY TO CALLING DUSTDENSITY
    
    r_val = 30.0*AU
    radmc3d_grid_theta = np.pi/4.
    phi_cell = 0.0
    y_sph_c = radmc3d_grid_theta
    x_c, y_c, z_c = transf_sph_cart(r_val, radmc3d_grid_theta, phi_cell)
    DustDensity_args['x_c'] = x_c
    DustDensity_args['y_c'] = y_c
    DustDensity_args['z_c'] = z_c
    DustDensity_args['cav_enable'] = False
    md = DustDensity(**DustDensity_args)

    
    # Methods to be tested
    # _tilt_disk
    # _rot_col_nodes
    # _disk_dens    
    

    # ---------------------- TEST ROTATING COLLAPSE VALUES --------------------
    stream_r, theta_bounds, theta_0 = md._rot_col_nodes(r_in= 1.*AU,
                                                        r_out=150.*AU,
                                                r_c=DustDensity_args['r_c'])

    min_dist = 10.0**60.0
    for ir in xrange(len(stream_r)):
        r_node_dist = abs(r_val-stream_r[ir])
        if r_node_dist < min_dist:
            min_dist = r_node_dist
            r_cell = stream_r[ir]
            i_r_cell = ir

    min_dist = 10.0**60.0

    for i_theta_0 in xrange(len(theta_0)):
        thet_node_dist = abs(radmc3d_grid_theta - theta_bounds[i_theta_0, i_r_cell])

        # If the new streamline node is closer than the
        # previously chosen node, we assign it instead.

        if thet_node_dist < min_dist:
            min_dist = thet_node_dist
            theta_center_pos = theta_0[i_theta_0] # Starting angle of the streamline

            # The current meridional position of the streamline with the given radius.
            theta_cell = theta_bounds[i_theta_0, i_r_cell]    

    # Test disk rotating collapse density values.
    m_star = DustDensity_args['m_star']
    m_dot = DustDensity_args['m_dot']
    r_c = DustDensity_args['r_c']
    
    rho_cell = m_dot/(4.0*np.pi*np.sqrt(
        G*m_star*(r_cell)**3.0))*(
        1.0+np.cos(theta_cell)/np.cos(theta_center_pos))**(-0.5)*(
        np.cos(theta_cell)/np.cos(theta_center_pos)+
        2.*(np.cos(theta_center_pos))**2./(r_cell/r_c))**(-1.)

    vel_r_fac = (-np.sqrt(G*m_star/r_cell)*np.sqrt(1+np.cos(theta_cell)/
        np.cos(theta_center_pos)))/100.

    vel_theta_fac = (np.sqrt(G*m_star/r_cell)*(np.cos(theta_center_pos)-
        np.cos(theta_cell))*np.sqrt((np.cos(theta_center_pos)+np.cos(theta_cell))/
        (np.cos(theta_center_pos)*(np.sin(theta_cell))**2.)))/100.
        
    vel_phi_fac = (np.sqrt(G*m_star/r_cell)*np.sqrt(1- np.cos(theta_cell)/
        np.cos(theta_center_pos))*np.sin(theta_center_pos)/np.sin(theta_cell))/100.
      
    #Convert spherical velocity vectors to cartesian.
    #From http://www.astrosurf.com/jephem/library/li110spherCart_en.htm
    #and "Accretion processes page 70"
    #Bear in mind that vel_phi and vel_theta include some of the terms 
    #from astrosurf. eq.  

    #The used eq. used another meridional coordinate system.
    theta_s = radmc3d_grid_theta #Only evaluate upper midplane
    phi_s = phi_cell
    #Since the sph-cart conversion eq. has a different meridional range and direction,
    #I need to convert the meriodional coordinates and vel vectors before I calculate the cart vels.

    assert phi_s >= 0.0 and phi_s <= 2*PI, 'phi_s has wrong range.'
    assert theta_s >= 0.0 and theta_s <= PI/2., 'theta_s has wrong range.'
 
    if rho_cell < 10.**(-40):
        print('unexpectedly low density result')
        print('rho_cell is ', rho_cell)
        print('theta_center_pos is', theta_center_pos)
        print('theta_cell is', theta_cell)
    assert rho_cell > 10.**(-40), 'Shit'
    #Based on my own derivations with standard spherical coordinate system.
    vel_x = (vel_r_fac*np.sin(theta_s)*np.cos(phi_s) + np.cos(theta_s)*np.cos(phi_s)*vel_theta_fac
        - np.sin(phi_s)*vel_phi_fac)
    vel_y = (vel_r_fac*np.sin(theta_s)*np.sin(phi_s) + np.cos(theta_s)*np.sin(phi_s)*vel_theta_fac +
        np.cos(phi_s)*vel_phi_fac)
    vel_z = vel_r_fac*np.cos(theta_s) - np.sin(theta_s)*vel_theta_fac

    if inside_a_disk:
        if y_sph_c > PI_HALF:
            vel_z = -vel_z                
    elif not inside_a_disk:
        if z_c < 0.0:
            vel_z = -vel_z
    
    # Convert velocity vectors to global cartesian vectors.
    vel_x_fac, vel_y_fac, vel_z_fac = rot_vel_for_lime(
                     vel_x_disk=vel_x, vel_y_disk=vel_y, 
                     vel_z_disk=vel_z, incl=incl, 
                     azith_turn=azith_turn)      
        
    rho_model, vels = md._find_streamline(theta_bounds, 
                                    stream_r, theta_0, m_star, m_dot, r_c)
    vel_r, vel_theta, vel_phi, vel_x, vel_y, vel_z = vels[:]
    print('Global x, y, z vels are', vel_x, vel_y, vel_z)

    v_tot_fac = np.sqrt(2.0*G*m_star/(r_cell))/100.0
    v_tot_mod = np.sqrt(vel_r**2. + vel_theta**2.+vel_phi**2.)
    v_tot_fac_san = np.sqrt(vel_r_fac**2. + vel_theta_fac**2.+vel_phi_fac**2.)
    print('v_tot_fac', v_tot_fac)

    assert np.isclose(v_tot_fac, v_tot_mod)
    assert np.isclose(v_tot_fac, v_tot_fac_san)

    assert np.isclose(vel_x, vel_x_fac)
    assert np.isclose(vel_y, vel_y_fac)
    assert np.isclose(vel_z, vel_z_fac)
    
    assert np.isclose(rho_cell, rho_model), ('rho_cell is %e and rho_model '
                                            'is %e' % (rho_cell, rho_model))
    
    # --------------------- TEST ROTATED CLASSICAL DISK -----------------------    

    #Get meridional coordinate, relative to the disk midplane axis
    #around star
    theta_ran = np.arange(np.pi/4.0, np.pi/2.0, 0.01)
    for theta in theta_ran:
        r_sphr = 10.0*AU
       # theta = np.pi/2.0*0.6
        phi = 0.0
        p_stars = (0.0*AU, 0.0*AU, 0.0*AU)
        azith_turn = 0.0
        incl = 0.0
        x_c, y_c, z_c = transf_sph_cart(r_sphr, theta, phi)
    
        x_c = x_c - p_stars[0]
        y_c = y_c - p_stars[1]
        z_c = z_c - p_stars[2]
    
        #Transform coordinate system by rotating around axes.
        #First z-axis rotation (i.e. turn in azimuth).
        x_c_zrot = x_c*np.cos(azith_turn) + y_c*np.sin(azith_turn)
        y_c_zrot = -x_c*np.sin(azith_turn) + y_c*np.cos(azith_turn) 
    
        y_c_xrot = y_c_zrot*np.cos(incl) + z_c*np.sin(incl)
        z_c_xrot = -y_c_zrot*np.sin(incl) + z_c*np.cos(incl)
    
        r_disk = np.sqrt((x_c_zrot)**2.+
            (y_c_xrot)**2.+(z_c_xrot)**2.)
       
        theta_transf = np.arccos(z_c_xrot/r_disk)
       
        phi_native = azi_from_cart(x_c_zrot, y_c_xrot)
    
        # ----------------------------- _disk_dens() ------------------------------
        
        rho_0 = np.float64(10.0**(-18))
        H_0 = np.float64(0.85*AU)
        r_0 = np.float64(10.0*AU)
        flr = np.float64(0.25)
        p = np.float64(-0.5)
        print('r_0', r_0)
        z_disk = np.float64(r_disk*np.cos(theta_transf))
        midplane_r = np.float64(r_disk*np.sin(theta_transf))
        surf_dens = np.float64(rho_0*(midplane_r/r_0)**p)

        disk_dic = {'rho_0': rho_0,
                    'H_0': H_0,
                    'r_0': r_0,
                    'flr': flr,
                    'p': p,
                    'r_disk': r_disk,
                    'theta_disk': theta_transf,
                    'r_in': 1.0*AU
                    }

        H_d = np.float64(H_0*(midplane_r/(r_0))**(1.+flr))  # Was divided by 10 AU before
        disk_rho =  np.float64(surf_dens/(H_d*np.sqrt(2.*np.pi))*np.exp(-z_disk**2./(2.*H_d**2.)))

        disk_rho_model, vel_dum = md._disk_dens(disk_dic, theta_bounds,
                                                stream_r, theta_0,
                                                test_class_disk=True)

        print('disk_rho_model', disk_rho_model)
        print('disk_rho', disk_rho)

        assert np.allclose(disk_rho, disk_rho_model, atol=0.0, rtol=1e-6)


def test_DustDensity_methods():
    # ---------------------------- DEFINE VARIABLES ---------------------------

    cav_rad = 600.0*AU
    octree = True
    oct_rad_1 = 150.*AU
    oct_rad_2 = 150.*AU
    cav_enable = True
    source_nr = 2
    n_dust = 2
    recurs_subli = True
    m_star = 1.1*M_SUN
    m_dot = 2.5 * 10.0**(-6.)* M_SUN / (SEC_PR_YEAR)
    inn_rad = 1.0*AU
    out_rad = 8000.0*AU
    nx = 60
    ny = 140
    nz = 140
    nci=110
    n_stream = 80
    n_stream_r = 80
    inn_disk_ref_1_rad = 6.*AU
    inn_disk_ref_2_rad = 6.*AU
    n_dust = 2
    disk_rot_coll = True
    bare = True
    rot_collapse = False
    evap_rad = 70.*AU
    azi_lim_1 = 0.0
    azi_lim_2 = np.pi
    r_c = 40.*AU
    phot_numb = 150000

    p_stars = np.ndarray((2, 3), np.float64)
    interbin_dist = 636.0 * AU
    star_1_x_y_pos = interbin_dist / 2. / (np.sqrt(2.))

    p_stars[0, :] = [x for x in [-star_1_x_y_pos, -star_1_x_y_pos, 0.]]
    p_stars[1, :] = [x for x in [star_1_x_y_pos, star_1_x_y_pos, 0.]]
    
    #p_stars[0, :] = [x for x in [-212.132 * AU, -212.132 * AU, 0.]]
   # p_stars[1, :] = [x for x in [212.132 * AU, 212.132 * AU, 0.]]
    
    
    
    rho0 = 2.5 * 10.**(-14.)
    prho = -1.7
    tor_dic = {'torus': True, 'tor_c': 300. * AU, 'tor_rad': 130.*AU,
               'max_dens': 2.5 * 10.**(-17.), 'tor_incl': 0.0, 'sm_min': 150*AU,
               'tor_p': -0.25}
               
    disk_dic = {'disk_dic_1': {'r_out': 150. * AU, 'r_in': 1.*AU,
                               'incl': np.pi / 2.,  # was 150 AU before rin was 0.3
                               'azith_turn': np.pi*0.75, 'rho_0': 0.01,
                               'r_fill': 0.0 * AU, 'r_0': 10*AU, 'H_0': 2.*AU,
                               'p': -0.5, 'flr': 0.0,
                               'theta_bounds': 'dummy', 'stream_r': 'dummy',
                               'theta_0': 'dummy'},
                'disk_dic_2': {'r_out': 50. * AU, 'r_in': 1.*AU,
                               'incl': np.pi / 2. * 0.0,  # rin was 0.2
                               'azith_turn': 0.5 * np.pi, 'rho_0': 0.01,
                               'r_fill': 0.0 * AU, 'r_0': 10*AU, 'H_0': 2.*AU,
                               'p': -0.5, 'flr': 0.0,
                               'theta_bounds': 'dummy', 'stream_r': 'dummy',
                               'theta_0': 'dummy'},
                'disk': True}

    
    DustDensity_args = {
                        'prho':prho, 'rho0': rho0,
                        'm_dot': m_dot,
                        'm_star': m_star, 'evap_rad': evap_rad,
                        'cav_rad': cav_rad, 'inn_rad': inn_rad,
                        'out_rad': out_rad, 'nx': nx, 'ny': nx,
                        'nz': nx, 'n_stream': n_stream, 'n_stream_r': n_stream_r, 'ref_nr': 50,
                        'n_dust': n_dust, 'source_nr': source_nr, 'inn_disk_ref_1_rad': inn_disk_ref_1_rad,
                        'inn_disk_ref_2_rad': inn_disk_ref_2_rad, 'oct_rad_1': oct_rad_1,
                        'oct_rad_2': oct_rad_2, 'disk_rot_coll': disk_rot_coll,
                        'recurs_subli': recurs_subli, 'bare': bare, 'rot_collapse': rot_collapse,
                        'cav_enable': cav_enable, 'r_c':r_c
                        }    
    
    
    
    
    class oct_instance():
    
        def __init__(self):
            pass
    
    oct_inst = oct_instance()
    oct_inst.octree = octree
    oct_inst.oct_rad_1 = oct_rad_1
    oct_inst.oct_rad_2 = oct_rad_2
    oct_inst.inn_disk_ref_1_rad = inn_disk_ref_1_rad
    oct_inst.inn_disk_ref_2_rad = inn_disk_ref_2_rad
    oct_inst.azi_lim_1 = azi_lim_1
    oct_inst.azi_lim_2 = azi_lim_2
    oct_inst.tor_dic = tor_dic
    oct_inst.source_nr = source_nr
    oct_inst.p_stars = p_stars
    if disk_rot_coll:
        oct_inst.inn_disk_1_lev = 2
        oct_inst.inn_disk_2_lev = 2
        oct_inst.disk_1_lev = 2
        oct_inst.disk_2_lev = 2
        oct_inst.tor_lev = 2
    
    wbound = [0.1, 20.0, 100.0, 9.0 * 1e3]
    nw = [20, 30, 30]
    zbound = [0.0, 2. * np.pi]
    
    test_DustDensity_methods_args = {
                           'cav_rad': cav_rad, 'octree': octree,
                           'p_stars': p_stars, 'oct_rad_1': oct_rad_1,
                           'oct_rad_2': oct_rad_2,
                           'subli_list': set(),
                           'disk': True, 'binary': False, 'cav_enable': cav_enable,
                           'streamline_view_enable': False, 'cyth_profiler': True,
                           'source_nr': source_nr, 'recurs_subli': recurs_subli,
                           'tor_dic': tor_dic, 'oct_inst': oct_inst, 'nci':nci,
                           'DustDensity_args': DustDensity_args
                           }
    
    # --------------------------- WRITE MODEL PARS --------------------------------
    
    wrt_model_par = {"t_eff": 5000., "lum_1": 18.0, "lum_2": 3.0,
                     "source_nr": source_nr, "r_c": r_c, "m_star": m_star,
                     "m_dot": m_dot,
                     "cav_rad": cav_rad, "inn_rad": inn_rad, "out_rad": out_rad,
                     "n_stream": n_stream, "n_stream_r": n_stream_r, "nx": nx,
                     "ny": ny, "nz": nz, "cav_enable": cav_enable,
                     "bare": bare, "p_stars": p_stars,
                     "evap_rad": evap_rad, "n_dust": n_dust,
                     "rot_collapse": rot_collapse, "wbound": wbound, "nw": nw,
                     "phot_numb": phot_numb, "prho": prho, "rho0": rho0,
                     "zbound": zbound, "profiler_active": False,
                     'disk_dic': disk_dic, 'tor_dic': tor_dic, 'mrw': 1,
                     'recurs_subli': recurs_subli, 'oct_rad_1': oct_rad_1,
                     'oct_rad_2': oct_rad_2,
                     'inn_disk_ref_1_rad': inn_disk_ref_1_rad,
                     'inn_disk_ref_2_rad': inn_disk_ref_2_rad,
                     'disk_rot_coll': disk_rot_coll}

    wrt_model_file(**wrt_model_par)
    model = 'IRAS_16293'
    radmc3d_analyze.writeDefaultParfile(model)

    DustDensity_methods(**test_DustDensity_methods_args)

    # Test dust_tot_mass
    
    # Volume integration?
    
    # Finish test on filament mass.

#==============================================================================
#     tree = mk_dust_arc_tree(sm_min=170.0*AU)
# 
#     ranges = [[0.0*AU, 300.0*AU], [0.49*np.pi, 0.51*np.pi], [0.99*np.pi,
#               1.1*np.pi]]
#     find_torus_partial = partial(find_torus)
#     # Integrate region
# 
#     # Make partial function object, with fixed values.
#     find_torus_partial = partial(find_torus, tor_rad=130.*AU, tor_incl=0.0,
#                                  tree=tree, p_stars=p_stars,
#                                  max_dens=tor_dic['max_dens'],
#                                  for_testing=True)
# 
#     print(type(find_torus_partial))
#     print(type(find_torus_partial(20.*AU, np.pi/2., 0.75*np.pi)))
#     print(sp.integrate.nquad(lambda x,y: x*y, [[0.,1.], [0.,1.]]))
#     options={'limit':200, 'epsrel':1.0e-02}
#     tor_mass = sp.integrate.nquad(find_torus_partial, ranges,
#                                   opts=options)
# 
#     print('tor_mass is', tor_mass)    
#==============================================================================


def test_accr_lum():
    G = 6.67259*10.0**(-8.0) # [cm^3 g-1 s-2]
    accr_rate = 3.25e-6
    mass = 1.0
    radius = 4.75
    lum = (G*mass*M_SUN*(accr_rate*M_SUN/SEC_PR_YEAR)/(radius*R_SUN))/L_SUN
    lum_model = accr_lum(mass, radius, accr_rate)
    assert np.allclose(lum, lum_model)


def test_rot_vel_for_lime():

    vel_x_disk = 10.0
    vel_y_disk = 10.0
    vel_z_disk = -3.0
    incl = np.pi/2.
    azith_turn = np.pi/4.

    vel_y_xback = (vel_y_disk * np.cos(2. * np.pi - incl) +
                   vel_z_disk * np.sin(2. * np.pi - incl))
    vel_z_xback = (-vel_y_disk * np.sin(2. * np.pi - incl) +
                   vel_z_disk * np.cos(2. * np.pi - incl))

    # Then use the de-tilted y vel-component to find the vel x-component
    vel_x_rot_fin = (vel_x_disk * np.cos(2. * np.pi - azith_turn) +
                     vel_y_xback * np.sin(2. * np.pi - azith_turn))
    vel_y_rot_fin = (-vel_x_disk * np.sin(2. * np.pi - azith_turn) +
                     vel_y_xback * np.cos(2. * np.pi - azith_turn))
    vel_z_rot_fin = vel_z_xback

    vel_x, vel_y, vel_z = rot_vel_for_lime(vel_x_disk, vel_y_disk, vel_z_disk,
                                           incl, azith_turn)
    print('vels are', vel_x, vel_x_rot_fin)
    assert np.allclose(vel_x, vel_x_rot_fin)
    assert np.allclose(vel_y, vel_y_rot_fin)
    assert np.allclose(vel_z, vel_z_rot_fin)


def test_z_x_axis_rot():
    """
    Expected axis rotation obtained from
    http://www.nh.cas.cz/people/lazar/celler/online_tools.php
    Be aware that the rotation direction in the above link is always clockwise,
    as seen from the direction of the rotation axis, i.e. z-axis rotation will
    appear as counter-clockwise rotation when looking down upon the x-y plane.
    """

    # Use a starting vector of (15, 10, 10), with a 45 degree counter-clockwise
    # rotation

    # Results in (17.67767,-3.535534,10).
    # Then performing x-axis rotation, with a 20 degree counter-clockwise rotation.
    # Results are (17.67767,0.097886,10.60615)

    true_vector = (17.67767, 0.097886, 10.60615)

    grid_radius, theta_disk, phi_disk, midplane_r = z_x_axis_rot(x_c=15.0,
                                                                 y_c=10.0,
                                                                 z_c=10.0,
                                                    incl=20.0*np.pi/180.0,
                                                    azith_turn=np.pi/4.0,
                                                    return_cart=False)

    # Convert back to cartesian coords for comparison
    print(grid_radius, theta_disk, phi_disk)
    x_res, y_res, z_res = transf_sph_cart(grid_radius, theta_disk, phi_disk)
    print(x_res, y_res, z_res)
    assert np.allclose(true_vector[0], x_res), 'true_vector[0] is {} x_res is {}'.format(true_vector[0], x_res)
    assert np.allclose(true_vector[1], y_res), 'true_vector[1] is {} y_res is {}'.format(true_vector[1], y_res)
    assert np.allclose(true_vector[2], z_res), 'true_vector[2] is {} z_res is {}'.format(true_vector[2], z_res)

    grid_radius, theta_disk, phi_disk, midplane_r = z_x_axis_rot(x_c=true_vector[0],
                                                                 y_c=true_vector[1],
                                                                 z_c=true_vector[2],
                                                    incl=2.0*np.pi - 20.0*np.pi/180.0,
                                                    azith_turn=2.0*np.pi - np.pi/4.0,
                                                    return_cart=False)
    
    x_res, y_res, z_res = transf_sph_cart(grid_radius, theta_disk, phi_disk)
    print(x_res, y_res, z_res)

    # Check that negative angles lead back to starting vector.
  #  assert np.allclose(15.0, x_res), 'true_vector[0] is {} x_res is {}'.format(15.0, x_res)
  #  assert np.allclose(10.0, y_res), 'true_vector[1] is {} y_res is {}'.format(10.0, y_res)
  #  assert np.allclose(10.0, z_res), 'true_vector[2] is {} z_res is {}'.format(10.0, z_res)
    
    grid_radius, theta_disk, phi_disk, midplane_r = z_x_axis_rot(x_c=7.071068,
                                                                 y_c=-7.071068,
                                                                 z_c=0,
                                                    incl=0.0,
                                                    azith_turn=2.0*np.pi - np.pi/4.0,
                                                    return_cart=False)
    
    x_res, y_res, z_res = transf_sph_cart(grid_radius, theta_disk, phi_disk)
    print(x_res, y_res, z_res)    
    assert np.allclose(10.0, x_res), 'true_vector[0] is {} x_res is {}'.format(10.0, x_res)
    assert np.allclose(0.0, y_res), 'true_vector[1] is {} y_res is {}'.format(0.0, y_res)
    assert np.allclose(0.0, z_res), 'true_vector[2] is {} z_res is {}'.format(0.0, z_res)


def test_z_axis_rot():
    # Check that rotation is counterclockwise

    azith_turn = np.pi/2.0
    x = -1.0
    y = -1.0
    x_new, y_new = z_axis_rot(x, y, azith_turn)

    x_facit = 1.0
    y_facit = -1.0

    assert np.allclose(x_facit, x_new)
    assert np.allclose(y_facit, y_new)

    azith_turn = -np.pi/2.0
    x = -1.0
    y = -1.0
    x_new, y_new = z_axis_rot(x, y, azith_turn)

    x_facit = -1.0
    y_facit = 1.0

    assert np.allclose(x_facit, x_new)
    assert np.allclose(y_facit, y_new)


def test_find_chan_vels():

    full_cube_name =  '/Users/Steffen_KJ/L483_data/valhalla_data/L483_H13CN43_full_clean.phase1.contsub.robust.pbcor.comb.fits'

    dist_pc = 200.0
    line_rest_freq = 345.3397750e9
    rest_vel = 5800.0
    cube_for_an = cube(full_cube_name, dist_pc=dist_pc,
                       line_rest_freq=line_rest_freq, rest_vel=rest_vel)

    cube_for_an.extract_data()

    # Casa channel values
    chan_0 = -94835.7  # [m]
    chan_959 = 108416  # [m]

    chan_vel_res = find_chan_vels(ref_chan=cube_for_an.ref_chan,
                                  chan_width_vel=cube_for_an.chan_width_vel,
                                  ref_vel=cube_for_an.ref_vel,
                                  chan_list=[0, 959])
    assert np.allclose(chan_0, chan_vel_res[0])
    assert np.allclose(chan_959, chan_vel_res[1])


def test_freq_derivation():
    """
    Test derivation of freq. in the cube() class.
    """

    dist_pc = 200.0
    rest_vel = 5800.0

    # --------------------------------- HCN -----------------------------------
    cube_HCN_freq_0 = 354.261e9  # Hz
    cube_HCN_freq_1 = 354.73e9  # Hz

    cube_HCN_vel_0 = -189787.0 # m
    cube_HCN_vel_1 = 206423.0 # m

    cube_HCN_freq_1 = 354.73e9  # Hz
    full_cube_name = '/Users/Steffen_KJ/L483_data/valhalla_data/cycle_3_only/L483_HCN43_full_clean.phase12amp.contsub.robust.pbcor.cyc3.fits'
    line_rest_freq = 354.50547730e9

    cube_for_an = cube(full_cube_name, dist_pc=dist_pc,
                       line_rest_freq=line_rest_freq, rest_vel=rest_vel)
    cube_for_an.extract_data()

    # Check derived frequencies, tolerance set at 0.5 MHz (cannot get better
    # precision of cube frequency from casaviewer).

    assert np.allclose(min(cube_for_an.freq), cube_HCN_freq_0, rtol=0.0,
                       atol=0.4999e6), 'min(cube_for_an.freq) is {}'.format(min(cube_for_an.freq))
    assert np.allclose(max(cube_for_an.freq), cube_HCN_freq_1, rtol=0.0,
                       atol=0.4999e6), 'max(cube_for_an.freq) is {}'.format(max(cube_for_an.freq))
    
    # Velocity check
    assert np.allclose(min(cube_for_an.vel), cube_HCN_vel_0, rtol=0.0,
                       atol=1.0), 'min(cube_for_an.vel) is {}'.format(min(cube_for_an.vel))
    assert np.allclose(max(cube_for_an.vel), cube_HCN_vel_1, rtol=0.0,
                       atol=1.0), 'max(cube_for_an.vel) is {}'.format(max(cube_for_an.vel))

    # --------------------------------- H13CN ---------------------------------
    cube_H13CN_freq_0 = 345.096e9  # Hz
    cube_H13CN_freq_1 = 345.565e9  # Hz

    cube_H13CN_vel_0 = -195395.0  # m
    cube_H13CN_vel_1 = 211332.0  # m

    full_cube_name = '/Users/Steffen_KJ/L483_data/valhalla_data/cycle_3_only/L483_H13CN43_full_clean.phase12amp.contsub.robust.pbcor.cyc3.fits'
    line_rest_freq = 345.3397750e9

    cube_for_an = cube(full_cube_name, dist_pc=dist_pc,
                       line_rest_freq=line_rest_freq, rest_vel=rest_vel)
    cube_for_an.extract_data()

    # Check derived frequencies, tolerance set at 0.5 MHz (cannot get better
    # precision of cube frequency from casaviewer).

    assert np.allclose(min(cube_for_an.freq), cube_H13CN_freq_0, rtol=0.0,
                       atol=1.4999e6), 'min(cube_for_an.freq) is {}'.format(min(cube_for_an.freq))
    assert np.allclose(max(cube_for_an.freq), cube_H13CN_freq_1, rtol=0.0,
                       atol=1.4999e6), 'max(cube_for_an.freq) is {}'.format(max(cube_for_an.freq))

    # Velocity check
    assert np.allclose(min(cube_for_an.vel), cube_H13CN_vel_0, rtol=0.0,
                       atol=1.0), 'min(cube_for_an.vel) is {}'.format(min(cube_for_an.vel))
    assert np.allclose(max(cube_for_an.vel), cube_H13CN_vel_1, rtol=0.0,
                       atol=1.0), 'max(cube_for_an.vel) is {}'.format(max(cube_for_an.vel))

    # --------------------------------- HCO+ ----------------------------------
    cube_HCO_freq_0 = 356.490e9  # Hz
    cube_HCO_freq_1 = 356.959e9  # Hz

    cube_HCO_vel_0 = -188517.0  # m
    cube_HCO_vel_1 = 205218.0  # m

    full_cube_name = '/Users/Steffen_KJ/L483_data/valhalla_data/cycle_3_only/L483_HCOp43_full_clean.phase12amp.contsub.robust.pbcor.cyc3.fits'
    line_rest_freq = 356.7342880e9

    cube_for_an = cube(full_cube_name, dist_pc=dist_pc,
                       line_rest_freq=line_rest_freq, rest_vel=rest_vel)
    cube_for_an.extract_data()

    # Check derived frequencies, tolerance set at 0.5 MHz (cannot get better
    # precision of cube frequency from casaviewer).

    assert np.allclose(min(cube_for_an.freq), cube_HCO_freq_0, rtol=0.0,
                       atol=0.4999e6), 'min(cube_for_an.freq) is {}'.format(min(cube_for_an.freq))
    assert np.allclose(max(cube_for_an.freq), cube_HCO_freq_1, rtol=0.0,
                       atol=0.4999e6), 'max(cube_for_an.freq) is {}'.format(max(cube_for_an.freq))

    # Velocity check
    assert np.allclose(min(cube_for_an.vel), cube_HCO_vel_0, rtol=0.0,
                       atol=1.0), 'min(cube_for_an.vel) is {}'.format(min(cube_for_an.vel))
    assert np.allclose(max(cube_for_an.vel), cube_HCO_vel_1, rtol=0.0,
                       atol=1.0), 'max(cube_for_an.vel) is {}'.format(max(cube_for_an.vel))

    # --------------------------------- CS ----------------------------------
    cube_CS_freq_0 = 342.757e9  # Hz
    cube_CS_freq_1 = 342.991e9  # Hz
    
    cube_CS_vel_0 = -94376.4  # m
    cube_CS_vel_1 = 110337.0  # m    

    full_cube_name = '/Users/Steffen_KJ/L483_data/valhalla_data/cycle_3_only/L483_CS76_full_clean.phase12amp.contsub.robust.pbcor.cyc3.fits'
    line_rest_freq = 342.8828503e9

    cube_for_an = cube(full_cube_name, dist_pc=dist_pc,
                       line_rest_freq=line_rest_freq, rest_vel=rest_vel)
    cube_for_an.extract_data()

    # Check derived frequencies, tolerance set at 0.5 MHz (cannot get better
    # precision of cube frequency from casaviewer).

    assert np.allclose(min(cube_for_an.freq), cube_CS_freq_0, rtol=0.0,
                       atol=0.4999e6), 'min(cube_for_an.freq) is {}'.format(min(cube_for_an.freq))
    assert np.allclose(max(cube_for_an.freq), cube_CS_freq_1, rtol=0.0,
                       atol=0.4999e6), 'max(cube_for_an.freq) is {}'.format(max(cube_for_an.freq))

    # Velocity check
    assert np.allclose(min(cube_for_an.vel), cube_CS_vel_0, rtol=0.0,
                       atol=1.0), 'min(cube_for_an.vel) is {}'.format(min(cube_for_an.vel))
    assert np.allclose(max(cube_for_an.vel), cube_CS_vel_1, rtol=0.0,
                       atol=1.0), 'max(cube_for_an.vel) is {}'.format(max(cube_for_an.vel))

