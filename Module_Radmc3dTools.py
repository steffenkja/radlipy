from __future__ import division
import matplotlib.pyplot as plt
import numpy as np
import radmc3dPy
import time as time

try:
    from numpy import *
except:
    print 'ERROR'
    print ' Numpy cannot be imported '
    print ' To use the python module of RADMC-3D you need to install Numpy'


from radmc3dPy.natconst import *
try:
    from matplotlib.pylab import *
except:
    print ' WARNING'
    print ' matploblib.pylab cannot be imported ' 
    print ' To used the visualization functionality of the python module of RADMC-3D you need to install matplotlib'
    print ' Without matplotlib you can use the python module to set up a model but you will not be able to plot things or'
    print ' display images'

from radmc3dPy.analyze import *
from radmc3dPy.setup import *
from subprocess import Popen, PIPE
import os, sys, copy
import pyximport
from matplotlib import pyplot

pyximport.install(setup_args={"include_dirs":np.get_include()},
                  reload_support=True)
import cython_mod


#------------------------------------------------------------------------------#
#Define classes.

def problem_setup_dust(cav_rad,model='',nci= 30, amr = 0, oct_rad= 0.0,
                       levels=1, binary=True, cav_enable = False, 
                       streamline_view_enable = False, **kwargs):
    """
    SKJ: This is a copy of radmc3dPy.setup.problemSetupDust() with minor alterations.
    
    Function to set up a dust model for RADMC3D 
    
    INPUT:
    ------
        model : Name of the model that should be used to create the density structure.
                The file should be in a directory from where it can directly be imported 
                (i.e. the directory should be in the PYTHON_PATH environment variable or
                it should be in the current working directory)
                and the file name should be 'model_xxx.py', where xxx stands for the string
                that should be specified in this variable
        cav_enable: Enable an inner envelope cavity of constant density.
        cav_rad: Radius of the inner envelope cavity.
        nci: Number of radial grid walls in the inner cavity grid.         
        binary : If True input files will be written in binary format, if False input files are
                written as formatted ascii text. 

        writeDustTemp: If True a separate dust_temperature.inp/dust_tempearture.binp file will be
                written under the condition that the model contains a function getDustTemperature() 
    OPTIONS:
    --------
        Any varible name in problem_params.inp can be used as a keyword argument.
        At first all variables are read from problem_params.in to a dictionary called ppar. Then 
        if there is any keyword argument set in the call of problem_setup_dust the ppar dictionary 
        is searched for this key. If found the value belonging to that key in the ppar dictionary 
        is changed to the value of the keyword argument. If no such key is found then the dictionary 
        is simply extended by the keyword argument. Finally the problem_params.inp file is updated
        with the new parameter values.
 
       
    FILES WRITTEN DURING THE SETUP:
    -------------------------------
        dustopac.inp          - dust opacity master file
        wavelength_micron.inp - wavelength grid
        amr_grid.inp          - spatial grid
        stars.inp             - input radiation field
        dust_density.inp      - dust density distribution
        radmc3d.inp           - parameters for RADMC3D (e.g. Nr of photons to be used, scattering type, etc)

    STEPS OF THE SETUP:
    -------------------
        1) Create the spatial and frequency grid
        2) Create the master opacity file and calculate opacities with the Mie-code if necessary
        3) Set up the input radiation field (generate stars.inp)
        4) Calculate the dust density
        5) If specified;  calculatest the dust temperature (e.g. for gas simulations, or if it is taken from an
            external input (e.g. from another model))
        6) Write all output files
    """
  
    # Read the parameters from the problem_params.inp file 
    modpar = readParams()

    # Make a local copy of the ppar dictionary
    ppar = modpar.ppar


    if model=='':
        print 'ERROR'
        print 'No model name is given'
        return

    if not ppar:
        print 'ERROR'
        print 'problem_params.inp was not found'
        return 
# --------------------------------------------------------------------------------------------
# If there is any additional keyword argument (**kwargs) then check
#   if there is such key in the ppar dictionary and if is change its value that of
#   the keyword argument. If there is no such key in the ppar dictionary then add the keyword
#   to the dictionary
# --------------------------------------------------------------------------------------------
    if binary:
        modpar.setPar(['rto_style', '3', '', ''])

    if kwargs:
        for ikey in kwargs.keys():
            modpar.ppar[ikey] = kwargs[ikey]
            
            if type(kwargs[ikey]) is float:
                modpar.setPar([ikey, ("%.7e"%kwargs[ikey]), '', ''])
            elif type(kwargs[ikey]) is int:
                modpar.setPar([ikey, ("%d"%kwargs[ikey]), '', ''])
            elif type(kwargs[ikey]) is str:
                modpar.setPar([ikey, kwargs[ikey], '', ''])
            elif type(kwargs[ikey]) is list:
                dum = '['
                for i in range(len(kwargs[ikey])):
                    if type(kwargs[ikey][i]) is float:
                        dum = dum + ("%.7e"%kwargs[ikey][i])
                    elif type(kwargs[ikey][i]) is int:
                        dum = dum + ("%d"%kwargs[ikey][i])
                    elif type(kwargs[ikey][i]) is str:
                        dum = dum + (kwargs[ikey][i])
                    else:
                        print ' ERROR '
                        print ' Unknown data type in '+ikey
                        print kwargs[ikey][i]

                    if i<len(kwargs[ikey])-1:
                        dum = dum + ', '
                dum = dum + (']') 
                modpar.setPar([ikey, dum, '', ''])

        modpar.writeParfile()
        ppar = modpar.ppar

# --------------------------------------------------------------------------------------------
# Create the grid
# --------------------------------------------------------------------------------------------
    grid = radmc3dGrid()
    
    # Wavelength grid
    grid.makeWavelengthGrid(ppar=ppar)
    make_grid = MakeGrid()
    # Spatial grid
    make_grid.make_spatial_grid_inn_cav(ppar=ppar,cav_rad = cav_rad,nci = nci, cav_enable = cav_enable)

# --------------------------------------------------------------------------------------------
# Dust opacity
# --------------------------------------------------------------------------------------------
    if ppar.has_key('dustkappa_ext'):
        opac=radmc3dDustOpac()
        #Master dust opacity file
        opac.writeMasterOpac(ext=ppar['dustkappa_ext'], scattering_mode_max=ppar['scattering_mode_max'])
    else:
        opac=radmc3dDustOpac()
        # Calculate the opacities and write the master opacity file
        opac.makeOpac(ppar=ppar)
# --------------------------------------------------------------------------------------------
# Create the input radiation field (stars at this point) 
# --------------------------------------------------------------------------------------------

    stars = radmc3dStars(ppar=ppar)
    stars.getStellarSpectrum(tstar=ppar['tstar'], rstar=ppar['rstar'], wav=grid.wav)

# --------------------------------------------------------------------------------------------
# Try to get the specified model
# --------------------------------------------------------------------------------------------
    print model        
    try:
        mdl = __import__('model_'+model)
    except:
        try:
            mdl  = __import__('radmc3dPy.model_'+model, fromlist=['']) 
        except:
            print 'ERROR'
            print ' model_'+model+'.py could not be imported'
            print ' The model files should either be in the current working directory or'
            print ' in the radmc3d python module directory'
            return

    #data = radmc3dData(grid)
    data = radmc3dData(make_grid)

# --------------------------------------------------------------------------------------------
# Create the dust density distribution 
# --------------------------------------------------------------------------------------------
    if dir(mdl).__contains__('getDustDensity'):
        if callable(getattr(mdl, 'getDustDensity')):
            data.rhodust, theta_bounds, stream_r = mdl.getDustDensity(grid=make_grid, ppar=ppar)
        else:
            print 'WARNING'
            print 'model_'+model+'.py does not contain a getDustDensity() function, therefore, '
            print ' dust_density.inp cannot be written'
            return 
    else:
        print 'WARNING'
        print 'model_'+model+'.py does not contain a getDustDensity() function, therefore, '
        print ' dust_density.inp cannot be written'
        return 
# --------------------------------------------------------------------------------------------
# Create the dust temperature distribution if the model has such function
# --------------------------------------------------------------------------------------------
# NOT INCLUDED BY SKJ
# --------------------------------------------------------------------------------------------
# Now write out everything 
# --------------------------------------------------------------------------------------------

    #Frequency grid
    grid.writeWavelengthGrid()
    #Spatial grid
    #grid.writeSpatialGrid()
        
    make_grid.write_spatial_grid(amr=amr,oct_rad=oct_rad,levels =levels)
    #Input radiation field
    stars.writeStarsinp(wav=grid.wav, pstar=ppar['pstar'], tstar=ppar['tstar'])
    #Dust density distribution
    data.writeDustDens(binary=binary)
    #Dust temperature distribution
    #radmc3d.inp
    writeRadmc3dInp(modpar=modpar)

# --------------------------------------------------------------------------------------------
# Calculate optical depth for diagnostics purposes
# --------------------------------------------------------------------------------------------
    
    stars = radmc3dStars()
    stars.readStarsinp()
    pwav = stars.findPeakStarspec()[0]
    
    if streamline_view_enable:
        return theta_bounds, stream_r


class MakeGrid():
    """This class constructs a grid to be used in radmc-3d.
    The goal is to support generic protoplanetary disks and envelopes, in an
    interchangeably and universal manner (no hardcoding). Uses previous
    IDL coding from my master's thesis project (see TW Hya models) inspired by
    Dullemonds examples as well as A. Juhacz python examples."""     
    def __init__(self):
        # Constants.
        self.AU  = 1.49597871*10.0**13.0 #[cm]
           
    
    #Radial grid refinement
    def make_spatial_grid_inn_cav(self,cav_rad,nci,cav_enable = False, crd_sys=None,xbound=None,ybound=None,zbound=None,nxi=None,nyi=None,nzi=None,ppar=None):
        """
        This function is a copy of radmc3dPy.analyze.makeSpatialGrid(), with the
        added functionality of a constant inner grid size (for the constant inner dust
        density).
        Function to create the spatial grid
    
        INPUT:
        ------
            cav_rad: Radius of the inner envelope cavity.
            nci: Number of radial grid walls in the inner cavity grid. 
            crd_sys     - 'car'/'sph'  Coordinate system of the spatial grid
            xbound      - List (with at least two elements) of boundaries for the grid along the first dimension
            ybound      - List (with at least two elements) of boundaries for the grid along the second dimension
            zbound      - List (with at least two elements) of boundaries for the grid along the third dimension
            nxi         - Number of grid points along the first dimension. List with len(xbound)-1 elements with 
                            nxi[i] being the number of grid points between xbound[i] and xbound[i+1]
            nyi         - Same as nxi but for the second dimension
            nzi         - Same as nxi but for the third dimension
        
        OPTIONS:
        --------
            ppar        - Dictionary containing all input parameters of the model (from the problem_params.inp file)
                          if ppar is set all keyword arguments that are not set will be taken from this dictionary
        """
        self.act_dim = [1,1,1]
        if ppar:
            if not crd_sys : crd_sys = ppar['crd_sys']
            self.crd_sys =  crd_sys
           
            if not xbound : 
                if ppar.has_key('xbound'):
                    xbound = ppar['xbound']
                else:
                    print ' No boundary for the first dimension is given, first dimension is deactivated.'
                    self.act_dim[0] = 0
            if not nxi:
                if ppar.has_key('nx'):
                    if (type(ppar['nx']).__name__!='list'): 
                        ppar['nx'] = [ppar['nx']]
                    nxi = [i+1 for i in ppar['nx']] #nxi = ppar['nx']+1
                    if ppar['nx'][0]==0:
                        self.act_dim[0] = 0
                else:
                    self.act_dim[0] = 0


            if not ybound : 
                if ppar.has_key('ybound'):
                    ybound = ppar['ybound']
                else:
                    print ' No boundary for the second dimension is given, second dimension is deactivated.'
                    self.act_dim[1] = 0
            if not nyi:
                if ppar.has_key('ny'):
                    if (type(ppar['ny']).__name__!='list'): 
                        nyi = [ppar['ny']+1]
                        ppar['ny'] = [ppar['ny']]

                    else:
                        ppar['ny'] = ppar['ny']
                        nyi = [i for i in ppar['ny']] #ppar['ny']+1
                    
                    if ppar['ny'][0]==0:
                        self.act_dim[1] = 0
                else:
                    self.act_dim[1] = 0

            if not zbound : 
                if ppar.has_key('zbound'):
                    zbound = ppar['zbound']
                else:
                    print ' No boundary for the third dimension is given, third dimension is deactivated.'
                    self.act_dim[2] = 0
            if not nzi:
                if (ppar.has_key('nz'))&(ppar['nz']>0.):
                    if (type(ppar['nz']).__name__!='list'): 
                        ppar['nz'] = [ppar['nz']]
                    nzi = [i+1 for i in ppar['nz']] #nzi = ppar['nz']+1
                    if ppar['nz'][0]==0:
                        self.act_dim[2] = 0
                else:
                    self.act_dim[2] = 0
                    nzi = [0]

       
#
# r->x, theta->y, phi-z            
#
        if (xbound==None): 
            print 'ERROR'
            print 'Boundaries on the radius is not specified'
            print 'Without the boundaries no grid can be created'
            return

        if (ybound==None): ybound = [0.0, pi]
        if (zbound==None): zbound = [0.0, 2.0*pi]

        if ((nxi==None)|(nyi==None)|(nzi==None)):
            print 'ERROR'
            print 'Number of grid points is not specified'
            return

#
# Type checking (what is in the dimension numbers)
#

        if (type(nxi).__name__=='int'):  nxi = [nxi]
        if (type(nyi).__name__=='int'):  nyi = [nyi]
        if (type(nzi).__name__=='int'):  nzi = [nzi]
#
# Create the x axis
#
        if (len(nxi)>1): 
            self.nxi = sum(nxi)
            self.nx  = self.nxi-1
            self.xi  = xbound[0] * (xbound[1] / xbound[0])**(arange(nxi[0], dtype=float64)/float(nxi[0]))
            for ipart in range(1,len(nxi)-1):
                dum = xbound[ipart] * (xbound[ipart+1] / xbound[ipart])**(arange(nxi[ipart], dtype=float64)/float(nxi[ipart]))
                self.xi = append(self.xi, dum)

            ipart = len(nxi)-1 
            dum = xbound[ipart] * (xbound[ipart+1] / xbound[ipart])**(arange(nxi[ipart], dtype=float64)/float(nxi[ipart]-1))
            self.xi = append(self.xi, dum)
            self.x  = sqrt(self.xi[0:self.nx] * self.xi[1:self.nx+1])
        else:
            if self.act_dim[0]==1:
                self.nxi = nxi[0]
                self.xi = xbound[0] * (xbound[1] / xbound[0])**(arange(self.nxi, dtype=float64)/float(self.nxi-1.))
                self.nx = self.nxi-1
                self.x  = sqrt(self.xi[0:self.nx] * self.xi[1:self.nx+1])
            else:
                self.x = [0.]
                self.xi = [0., 0.,]
                self.nx = 1
                self.nxi = 2
            
#
# Create the y axis
#
        if (len(nyi)>1):
            
            # Check if we go to the full [0,pi] interval or only use the upper half-plane [0, pi/2]
            
            if ybound[len(ybound)-1]!=pi/2.:
                self.nyi = sum(nyi)+1
                self.ny  = self.nyi-1
                self.yi  = ybound[0] + (ybound[1] - ybound[0])*(arange(nyi[0], dtype=float64)/float(nyi[0]))                
                for ipart in range(1,len(nyi)-1):
                    # Now make sure that pi/2 will be a cell interface
                    # 
                    # BUGFIX! 16-05-2012
                    # The grid was not symmetric to pi/2 when the grid contained multiple sections (i.e. len(nyi)>1)
                    # This is now fixed
                    if (ybound[ipart]<pi/2.):
                        dum = ybound[ipart] + (ybound[ipart+1] - ybound[ipart])*(arange(nyi[ipart], dtype=float64)/float(nyi[ipart]))
                    else:
                        if (ybound[ipart]==pi/2.):
                            dum = ybound[ipart] + (ybound[ipart+1] - ybound[ipart])*((arange(nyi[ipart]+1, dtype=float64))/(float(nyi[ipart])))
                        else:
                            dum = ybound[ipart] + (ybound[ipart+1] - ybound[ipart])*((arange(nyi[ipart], dtype=float64)+1.)/float(nyi[ipart]))

                    self.yi = append(self.yi, dum)

                ipart   = len(nyi)-1 
                if len(nyi)==2:
                    dum     = ybound[ipart] + (ybound[ipart+1] - ybound[ipart])*((arange(nyi[ipart], dtype=float64))/(float(nyi[ipart])-1.))
                else:
                    dum     = ybound[ipart] + (ybound[ipart+1] - ybound[ipart])*((arange(nyi[ipart], dtype=float64)+1.)/float(nyi[ipart]))

            else:
                self.nyi = sum(nyi)+1
                self.ny  = self.nyi-1
                self.yi  = ybound[0] + (ybound[1] - ybound[0])*(arange(nyi[0], dtype=float64)/float(nyi[0]))                
                for ipart in range(1,len(nyi)-1):
                    # Now make sure that pi/2 will be a cell interface
                    # 
                    # BUGFIX! 16-05-2012
                    # The grid was not symmetric to pi/2 when the grid contained multiple sections (i.e. len(nyi)>1)
                    # This is now fixed
                    if (ybound[ipart]<pi/2.):
                        dum = ybound[ipart] + (ybound[ipart+1] - ybound[ipart])*(arange(nyi[ipart], dtype=float64)/float(nyi[ipart]))
                    else:
                        dum = ybound[ipart] + (ybound[ipart+1] - ybound[ipart])*((arange(nyi[ipart]+1, dtype=float64))/(float(nyi[ipart])))
                    
                    self.yi = append(self.yi, dum)

                ipart   = len(nyi)-1 

                if len(nyi)==2:
                    dum     = ybound[ipart] + (ybound[ipart+1] - ybound[ipart])*((arange(nyi[ipart]+1, dtype=float64))/(float(nyi[ipart])))
                else:
                    dum     = ybound[ipart] + (ybound[ipart+1] - ybound[ipart])*((arange(nyi[ipart], dtype=float64)+1.)/float(nyi[ipart]))

        
            self.yi = append(self.yi, dum)
            self.y  = 0.5*(self.yi[0:self.ny] + self.yi[1:self.ny+1])

        else:
            if self.act_dim[1]==1:
                self.nyi = nyi[0]
                self.yi = ybound[0] + (ybound[1] - ybound[0])*(arange(self.nyi, dtype=float64)/float(self.nyi-1.))
                self.ny = self.nyi-1
                self.y  = 0.5*(self.yi[0:self.ny] + self.yi[1:self.ny+1])
            else:
                self.y = [0.]
                self.yi = [0., 0.,]
                self.ny = 1
                self.nyi = 2
#
# Create the z axis

        if (len(nzi)>1):
            self.nzi = sum(nzi)
            self.nz  = self.nzi-1

            self.zi  = zbound[0] + (zbound[1] - zbound[0])*(arange(nzi[0], dtzpe=float64)/float(nzi[0]))                
            for ipart in range(1,len(nzi)-1):
                dum = zbound[ipart] + (zbound[ipart+1] - zbound[ipart])*(arange(nzi[ipart], dtzpe=float64)/float(nzi[ipart]))
                self.zi = append(self.zi, dum)
            ipart   = len(nzi)-1 
            dum     = zbound[ipart] + (zbound[ipart+1] - zbound[ipart])*(arange(nzi[ipart], dtzpe=float64)/float(nzi[ipart]-1))
            self.zi = append(self.zi, dum)
            self.z  = 0.5*(self.zi[0:self.nz] + self.zi[1:self.nz+1])
        else:
            if self.act_dim[2]==1:
                self.nzi = nzi[0]
                self.zi = zbound[0] + (zbound[1] - zbound[0])*(arange(self.nzi, dtype=float64)/float(self.nzi-1))
                self.nz = self.nzi-1
                self.z  = 0.5*(self.zi[0:self.nz] + self.zi[1:self.nz+1])
            else:
                self.z = [0.]
                self.zi = [0., np.pi*2.]
                self.nz = 1
                self.nzi = 2
        
        #Added by SKJ.
        #Change the grid size inside an radial limit.
        if cav_enable:
            del_indices = []
            for i in range(self.nxi): 
                if self.xi[i] < cav_rad:
                    del_indices.append(i)             
            r_wall_i = (cav_rad-xbound[0])/nci * np.arange(nci, dtype=np.float64) + xbound[0]#(cav_rad / r_in)**(np.arange(nci, dtype=np.float64)/float(nci-1.))        
            self.xi = np.delete(self.xi,del_indices)         
            self.xi = np.hstack((r_wall_i,self.xi)) #Splice the two arrays into a final array for the wall positions.   
            self.nxi = len(self.xi)
            self.nx = self.nxi-1
            self.x  = 0.5*(self.xi[0:self.nx] + self.xi[1:self.nx+1])     
        
# --------------------------------------------------------------------------------------------------
    def write_spatial_grid(self,fname='',amr=0,oct_rad = 0.0, levels = 1,levelmax=1,nleafsmax=305280,nbranchmax=305920):
        """
        Copy of radmc3dPy.analyze.writeSpatialGrid().
        Function to write the wavelength grid to a file (e.g. amr_grid.inp)

        OPTIONS:
        --------
            fname - File name into which the spatial grid should be written. If omitted 'amr_grid.inp' will be used. 
        """
        


        if fname=='':
            fname = 'amr_grid.inp'

        print 'Writing '+fname
        wfile = open(fname, 'w')
        wfile.write('%d\n'%1)                    # Format number
        wfile.write('%d\n'%amr)                    # AMR self.style (0=regular self. NO AMR)
        if self.crd_sys=='car':
            wfile.write('%d\n'%0)                  # Coordinate system (0-99 cartesian, 100-199 spherical, 200-299 cylindrical)
        if self.crd_sys=='sph':
            wfile.write('%d\n'%100)                  # Coordinate system (0-99 cartesian, 100-199 spherical, 200-299 cylindrical)
        if self.crd_sys=='cyl':
            wfile.write('%d\n'%200)                  # Coordinate system (0-99 cartesian, 100-199 spherical, 200-299 cylindrical)
        wfile.write('%d\n'%0)                    # Gridinfo
        
        wfile.write('%d %d %d \n'%(self.act_dim[0], self.act_dim[1], self.act_dim[2]))       # Which dimension is active
        wfile.write('%d %d %d \n'%(self.nx,self.ny,self.nz))    # Grid size (x,y,z or r,phi,theta, or r,phi,z)
        if amr == 1: 
            wfile.write('%i %i %i \n'%(levelmax,nleafsmax,nbranchmax))    # Grid size (x,y,z or r,phi,theta, or r,phi,z)
        for i in range(self.nxi): wfile.write('%.9e\n'%self.xi[i])
        for i in range(self.nyi): wfile.write('%.9e\n'%self.yi[i])
        for i in range(self.nzi): wfile.write('%.9e\n'%self.zi[i])
        if amr == 1:
            oct_tree_indic = cython_mod.insert_oct_tree(self.x,self.y,self.z,self.nx,self.ny,self.nz,oct_rad, levels,p_stars=p_stars)
            for i in oct_tree_indic:
                wfile.write('%i\n'%i)
                
        wfile.close()                

# --------------------------------------------------------------------------------------------------
    def read_grid(self, fname=''):
        """
        Function to read the spatial (amr_grid.inp) and frequency grid (wavelength_micron.inp).
        
        OPTIONS:
        --------
            fname - File name from which the spatial grid should be read. If omitted 'amr_grid.inp' will be used. 
        """
#
# Natural constants
#

        cc = 29979245800.

        if fname=='':
            fname = 'amr_grid.inp'

# 
# Read the spatial grid 
#
        try :
            rfile = open(fname, 'r')
        except:
            print 'Error!' 
            print 'amr_grid.inp was not found!'
            return 
    
        form        = float(rfile.readline())
        grid_style  = float(rfile.readline())
        crd_system  = int(rfile.readline())
        if crd_system<100:
            self.crd_sys = 'car'
        elif ((crd_system>=100)&(crd_system<200)):
            self.crd_sys = 'sph'
        elif ((crd_system>=200)&(crd_system<300)):
            self.crd_sys = 'cyl'
        else:
            rfile.close()
            print 'ERROR'
            print ' unsupported coordinate system in the amr_grid.inp file'
            print crd_system
            return

        grid_info   = float(rfile.readline())
        dum         = rfile.readline().split()
        self.act_dim = [int(dum[i]) for i in range(len(dum))]
        dum         = rfile.readline().split()
        if grid_info == 1:         
            oct_tree_info = rfile.readline() #Read the oct-tree information.
        
        self.nx,self.ny,self.nz    = int(dum[0]), int(dum[1]), int(dum[2])
        self.nxi,self.nyi,self.nzi = self.nx+1, self.ny+1, self.nz+1

        self.xi           = zeros(self.nx+1, dtype=float64)
        self.yi           = zeros(self.ny+1, dtype=float64)
        self.zi           = zeros(self.nz+1, dtype=float64)
       
        for i in range(self.nxi): self.xi[i] = float(rfile.readline())
        for i in range(self.nyi): self.yi[i] = float(rfile.readline())
        for i in range(self.nzi): self.zi[i] = float(rfile.readline())

        if self.crd_sys=='car':
            self.x = (self.xi[0:self.nx] +  self.xi[1:self.nx+1]) * 0.5
            self.y = (self.yi[0:self.ny] +  self.yi[1:self.ny+1]) * 0.5
            self.z = (self.zi[0:self.nz] +  self.zi[1:self.nz+1]) * 0.5
        else: 
            self.x = sqrt(self.xi[0:self.nx] * self.xi[1:self.nx+1])
            self.y = (self.yi[0:self.ny] +  self.yi[1:self.ny+1]) * 0.5
            self.z = (self.zi[0:self.nz] +  self.zi[1:self.nz+1]) * 0.5

        rfile.close()

# 
# Read the frequency grid 
#

        try :
            rfile = open('wavelength_micron.inp', 'r')
        except:
            print 'Error!' 
            print 'wavelength_micron.inp was not found!'
            return 

        self.nwav = int(rfile.readline())
        self.nfreq = self.nwav
        self.wav  = zeros(self.nwav, dtype=float64)

        for i in range(self.nwav): self.wav[i] = float(rfile.readline())

        self.freq = cc / self.wav

        rfile.close()

# --------------------------------------------------------------------------------------------------
    def get_cell_volume(self):
        """
        Function to calculate the volume of grid cells
        """

        if self.crd_sys=='sph':

            if self.act_dim[0]==0:
                print '----------------------------------------------------------'
                print 'ERROR'
                print 'The r-dimension of a spherical grid is switched off'
                print 'This model (ppdisk) is not perpared for such grid style'
                print '----------------------------------------------------------'
            elif self.act_dim[1]==0:
                print '----------------------------------------------------------'
                print 'ERROR'
                print 'The theta-dimension of a spherical grid is switched off'
                print 'This model (ppdisk) is not perpared for such grid style'
                print '----------------------------------------------------------'
            elif self.act_dim[2]==0:
                vol = zeros([self.nx, self.ny, self.nz], dtype=float64)
                diff_r3   = self.xi[1:]**3 - self.xi[:-1]**3
                diff_cost = cos(self.yi[:-1]) - cos(self.yi[1:])
                diff_phi  = 2.*pi
                for ix in range(self.nx):
                    for iy in range(self.ny):
                        vol[ix,iy,:] = 1./3. * diff_r3[ix] * diff_cost[iy] * diff_phi

            else:
                vol = zeros([self.nx, self.ny, self.nz], dtype=float64)
                diff_r3   = self.xi[1:]**3 - self.xi[:-1]**3
                diff_cost = cos(self.yi[:-1]) - cos(self.yi[1:])
                diff_phi  = self.zi[1:] - self.zi[:-1] 
                for ix in range(self.nx):
                    for iy in range(self.ny):
                        vol[ix,iy,:] = 1./3. * diff_r3[ix] * diff_cost[iy] * diff_phi
        else:
            print 'ERROR!'
            print "coordinate system '" + self.crd_sys+ "' is not yet supported"
            return 0

        return vol


    def rad_log(self,grid_nr,x_0 = 0., y_0 = 0., z_0 = 0.):
        """Logarithmic radial spacing away from a given central position. 
        The goal is to make the grid produced by rad_log() insertable into a 
        larger grid model."""
        #Spatial grid parameters (spherical)
        r_wall_i = []
        grid_r_in = 0.05#*self.AU            #Inner radius of the grid
        grid_r_out = 250#.*self.AU           #Outer radius of the grid
        #grid_nr = 230                 #Number of radial grid points
        #grid_t_max = 0.9               #Upper boundary of theta (meridional angular coordinate)
        #grid_nt = 140                  #Number of theta grid points One-sided only. So the "real" value is twice this.
        #grid_np = 32 #Number of phi grid points 
        offset = np.sqrt(x_0**2.+y_0**2.+z_0**2.)
        #Construct grid cell walls.
        r_wall_i[:] = [grid_r_in * (grid_r_out/grid_r_in)**(grid_range/(grid_nr-1)) 
        + offset for grid_range in np.arange(grid_nr+1)]
        #r_wall_i[:] = [x/self.AU for x in r_wall_i]
        return r_wall_i



        
    def insert_inn_cav(self,cav_rad,r_in,nci):
        """This function inserts a spherical grid into an existing (larger) 
        grid."""

        for i in range(self.nxi): 
            if self.xi[i] < cav_rad:
                del self.xi[i]             
        r_wall_i = (cav_rad-r_in)/nci * np.arange(nci, dtype=np.float64) #(cav_rad / r_in)**(np.arange(nci, dtype=np.float64)/float(nci-1.))        
        self.xi = np.hstack((r_wall_i,self.xi)) #Splice the two arrays into a final array for the wall positions.
        self.nxi = len(self.xi)

class StarAnalysis:
    """This class defines a variety of star analysis tools."""   
    def __init__(self):
        pass
    
    def radius_blackbody(self,t_eff,lum):
        """This function calculates the radius of a blackbody with a given
        surface temperature and luminosity.
        
        Args: t_eff = effective surface temperature in Kelvin.
              lum = luminosity in units of solar luminosity.
        
        Returns: radius in units of solar radii.
        """
        
        #Stefan-Boltzmann constant [W m^{-2} K^{-4}]          
        sigma = 5.67033*10.0**(-8.0)          
        #Solar radius.         
        r_sun = 6.955*10.0**8.0  #[m] 
        l_sun = 3.826*10.0**26.0 #[W] 
        #Luminosity of blackbody, L = 4*pi*R^2*sigma*t_eff^4 
        radius = np.sqrt((lum*l_sun/(4.0*np.pi*sigma*t_eff**4.0)))/r_sun
        
        return radius
        
class Dust:
    """This class defines a variety of dust analysis tools.""" 
    def __init__(self):
        pass
    
#    def evap_ice_grid(self,vol)    
    
    def tot_mass(self, vol, rhodust, temp_fract = False, temp = 90., idust=0):
        """This function calculates the total dust mass in the model.
        Uses code extracted from radmc3dPy.analyze.getSigmaDust().
        Requires: vol  = prior call to self.grid.getCellVolume() 
                  and self.rhodust().
        
        Args: vol = volume of cells [array].
              rhodust = dust density array.
              temp = Cutoff temperature, must be integer [K].
        
        Returns: Total mass in the model, in grams.
        """
        
        # Dustmass in each grid cell
        if not temp_fract:
            if len(rhodust)>3:
                if idust>=0:
                    mass = vol * rhodust[:,:,:,idust]
                else:
                    mass = vol * rhodust.sum(3)
            else:
                mass = vol * rhodust
                
            #Sum all three dimensions.
            mas = np.sum(mass)
        #If we wish to sum all material > 90 K.
        
        if temp_fract:
            #import pyximport 
            #pyximport.install()
 #OBS!           import cython_mod
            prox = radmc3dPy.analyze.radmc3dData()
            #Open datafiles.
            radmc3dPy.analyze.radmc3dData.readDustTemp(prox)
            dust_temp = prox.dusttemp

            #Use Cython due to triple nested for loop.            
            start = time.clock()
            mas, rad =  cython_mod.mult_trip_loop(vol,rhodust,dust_temp,temp=temp, i_dust = idust)
            elapsed = (time.clock() - start)
            print 'Time elapsed in Cython = %f seconds' % elapsed   
        if temp_fract:
            return mas,rad
        else:
            return mas        
        
    
    def find_rho0(self,mass,p_rho,r_0,r_1,r_2):
        """This function finds rho_0 in the formula of 
        rho = rho_0 *(r/r_0)**(-p), given a mass.
        mass = 4/3*r**3*rho. When rho is spatially dependent, this function
        changes into an integral of infinetisimally thin spheres.
        
        Args: mass = known mass of object in gram.
              p_rho = exponent of the radial density function.
              r_0 = radius where the local density is rho_0 in cm.
              r_1 = start radius of the spherical object in cm.
              r_2 = end radius of the sperical object in cm.
        
        Returns: rho_0 in cm.
        

        """
        
        rho_0 = mass*(3.0-p_rho)*r_0**(-p_rho)/(4.0*np.pi*(r_2**(3.0-p_rho)-
        r_1**(3.0-p_rho)))
        
        return rho_0
      
class Tools:
    """This class defines a variety of useful radmc3d plotting tools and
    manipulations."""    

    def __init__(self):
        pass
        
    def open_sed_file(self,filename, dist):
        """
        This program inputs a standard spectrum.out file from RADMC-3D
        and outputs the wavelength array and flux array to python.
        Also converts the RADMC-3D values flux output to nu*F_nu(\lambda) values.

        Args: modelname = name of input file.                 
              dis = distance to the object 
                   
        Returns: Two arrays. First is wavelength, second is flux. 
        
        Raises: If the filename does not have the correct format (first three
        lines should not be less than 10 characters, indicating that the output
        data begins at line 4).
        
        """    
    
        C = 299792458.0 #Speed of light [m/s].
        wavelength = []
        flux = []
        
        with open(filename, 'r') as f:
            data = f.readlines()[3:]   
                
        print 'open_sed_file() has opened '+filename
        for line in data:
            a, b = line.split()        
            wavelength.append(float(a))       
            flux.append(float(b))
                
        flux[:] = [x*(1.0/dist)**2.0 for x in flux]
        flux[:]= [x*(C/(y*10.0**(-6.0))) for x,y in zip(flux, wavelength)]
        
        return wavelength, flux
    
    def open_csv_file(self,filename,start,splitter):
        """This function opens a csv file of unknown length and returns a list of
        values. By default, any string than can be converted to float, will be.
        Args: filename = name of the file.
              start =  starting indice, if some lines in the csv file should be 
              skipped.
              splitter = the command splitting the values. If comma, use "','"  if
              whitespace simply use "None".       
        Returns: a list of arrays, as floats if possible, otherwise strings.     
        
        """    

        var_nam = {}       
        list_res = []
        c = 0
       
        fil = open(filename, 'r')
        try:
            f = fil.readlines()[start:]
           #Read one line to get number of variables.
            for first_line in f:
                if c >0:
                    break     
                length = len(first_line.split(splitter))                    
                c += 1
                
            for i in range(length):
                var_nam['var_'+str(i)] = []   
            
            #Append values to arrays. Convert to float by default, if possible.
            for row in f:
                var_val = row.split(splitter)    
                for i in range(len(var_val)):
                    try:
                        var_nam['var_'+str(i)].append(float(var_val[i]))  
                    except ValueError:
                        var_nam['var_'+str(i)].append(var_val[i])
        finally:        
            fil.close()    
        
        #Create list of arrays. 
        for i in range(length):
            list_res.append(var_nam['var_'+str(i)])   
        return list_res    
    
    
    def jy_to_SI_flux(self,flux, wavelength):
        """Function to convert Jansky to SI flux.  
           Args:           
                Flux = Array of Flux [Jy].
        """
        C = 299792458.0 #Speed of light [m/s].
        flux[:]= [x*10.0**(-23.0) for x in flux]
        flux[:]= [x*(C/(y*10.0**(-6.0))) for x,y in zip(flux, wavelength)]
        
        return flux
    
    
    def conv_freq_wav(self,inp, freq=True,micron=True):
        """This function converts frequency into wavelength and vice versa,
        depending on the input.
        Args: 
             inp = Name of input file.
             freq = If set to true, then convert frequency into wavelength.
                    If false, convert wavelength into frequency [Hz]
             micron = If true, then give wavelength in microns.
        Returns: Array of either frequency or wavelength, depending on the 
        input. 
        """
        res = []
        C = 299792458.0 #Speed of light [m/s].        
        if freq == True:
            if micron == True:
                res[:] = [C*10.**6./x for x in inp]
            else:
                res[:] = [C/x for x in inp]
        else:
            res[:] = [C/x for x in inp]
        
        return res
        

    def rd_crr_flx(self,filename): 
    #Read interferometry correlated flux.
        wav = []
        crr_flx = []
        with open(filename, 'r') as f:        
            for row in f:
                values = row.split()
                try:
                    a,b = values
                except ValueError:
                    print(len(values))
            
                wav.append(float(a))
                crr_flx.append(float(b))
  
        return wav, crr_flx

    def plot_sed(self,mod_filename,obs_filename,dist,start,splitter,
                 mark_size=6.0,unitsize = 6.0, ewidth = 3.0, x_lim_1=0.1,x_lim_2=5.0*10.0**4.0,
                 y_lim_1=10.0**(-15.0),y_lim_2=10.0**(-7.0)):
        """This function takes an input radmc3d sed file and observational 
        datafile and plots them.
        
        Args: mod_filename = name of model sed file.
              obs_filename = name of observational sed datafile.
              dist = distance to the object from observer.
              Also some selfexplanatory plot configurations.
       
        Returns: A plot of the sed with overplotted observational datapoints.
        """
        
        #Open datafiles.
        #Observational file.          
        wav_obs, flux_obs, flux_error_obs = self.open_csv_file(obs_filename,start,splitter)       
        flux_obs = self.jy_to_SI_flux(flux_obs,wav_obs)      
        flux_error_obs = self.jy_to_SI_flux(flux_error_obs,wav_obs)

        #radmc3d model sed file.
        wav_mod,flux_mod = self.open_sed_file(mod_filename, dist)
        #Perform plot.         
        plt.figure()        
        plt.plot(wav_mod,flux_mod,label=str('radmc-3d model'),color ='red',
                 linewidth=3)
        #Overplot observations.
        plt.errorbar(wav_obs,flux_obs,yerr= flux_error_obs,marker='o',
                     color='blue',ecolor='blue',linestyle='.', 
                     label = 'obs. data',markersize=mark_size, elinewidth=ewidth)
        
        plt.rcParams['font.size'] = 27
        plt.rc('text', usetex=True)
        plt.xlabel(r' $\lambda$ [$\mu$m]')
        plt.ylabel(r'$\nu$ $F_{\nu}$ [erg cm$^{-2}$ s$^{-1}$]')
        plt.yscale('log')
        plt.xscale('log')
        plt.grid(True)
        plt.xlim(x_lim_1,x_lim_2)
        plt.ylim(y_lim_1,y_lim_2)
        plt.legend(fontsize = 24)        
        plt.show()
        
    def plot_dens(self,rhodust,grid,idust,r_c,r_max,streamline_view_enable = False,n_stream= None, theta_bounds=None, stream_r=None):
        """This function plots a 2D density contour plot from a 3D model."""
        
        AU  = 1.49597871*10.0**13.0 #[cm]
        #Convert the radius to units of r_c.
        grid_x = []
        grid_x[:] = [x/r_c for x in grid.x]
        print 'grid_x',grid_x
        radius_matrix, theta_matrix = np.meshgrid(grid_x,grid.y)
        phi = 0#np.pi*3.0/4.0 #Collapse the azimuthal dimension to a single value.        
        X = radius_matrix * np.sin(theta_matrix) * np.cos(phi)
        Y = radius_matrix * np.sin(theta_matrix) * np.sin(phi)
        Z = radius_matrix * np.cos(theta_matrix)

       # print 'shape of radius_matrix = ', radius_matrix.shape
       # print 'shape of theta_matrix = ', theta_matrix.shape
        rho_slice = np.transpose(rhodust[0:grid.nx,0:grid.ny,0,idust])

       # print 'shape of rhodust slice = ', rho_slice.shape
       # print 'shape of X = ', X.shape        
       # print 'shape of Z = ', Z.shape     
       # print 'X',X
        
        #Recreate contour plot from fig. 4.7 in Hartmann.
        n_iter = 20
        levels = []
        levels = [max(map(max,rho_slice))]
        for x in xrange(n_iter):
            levels.append(levels[x]*(np.sqrt(2.0))**(-1.0))
        #levels = np.logspace(np.log10(min(map(min,rho_slice))),np.log10(max(map(max,rho_slice))),num = n_iter)      
        print 'max(map(max,rho_slice))', max(map(max,rho_slice))
        print 'rho_slice',rho_slice
        print 'levels', levels
        R_SUN = 6.955*10.0**10.0 #[cm]
        #Plot the meshgrid and streamlines.
        if streamline_view_enable:
            #print 'n_stream = ', n_stream
            #print 'stream_r = ', stream_r/r_c
            #print 'theta_bounds[2,:] = ', theta_bounds[8,:]
            #print 'Shape of theta_bounds = ', theta_bounds.shape
            plt.figure()
            for y in xrange(n_stream):
                plt.plot(stream_r/r_c*np.sin(theta_bounds[y,:]),stream_r/r_c*np.cos(theta_bounds[y,:]))
            plt.xlabel('x/r_c')        
            plt.ylabel('z/r_c')
            plt.title('Streamlines')
            plt.xlim([0,4])
            plt.ylim([0,4])
            plt.grid(True)            
            plt.show()
            
            plt.figure()
            for y in xrange(n_stream):
                plt.plot(stream_r/r_c*np.sin(theta_bounds[y,:]),stream_r/r_c*np.cos(theta_bounds[y,:]), 'g. ', markersize = 0.4)
                if y == n_stream-1:
                    plt.plot(stream_r/r_c*np.sin(theta_bounds[y,:]),stream_r/r_c*np.cos(theta_bounds[y,:]), 'g. ', markersize = 0.4)                   
            plt.plot(X,Z, 'r. ', markersize = 0.6)
            plt.xlabel('x/r_c')        
            plt.ylabel('z/r_c')
            plt.title('radmc-3d grid nodes (red) and streamline grid nodes (green)')
            plt.xlim([0,r_max])
            plt.ylim([0,r_max])
            plt.grid(True)
            plt.show        

        plt.figure()        
        CS = plt.contour(X,Z,rho_slice[0:grid.ny,0:grid.nx],levels, origin = 'lower', linewidth = 2, label = 'Contours of 2^(1/2) factor decrease')
        #plt.contourf(X,Z,rho_slice[0:grid.ny,0:grid.nx])#,levels, origin = 'lower', linewidth = 2)            
        plt.xlabel('x/r_c')        
        plt.ylabel('z/r_c')
        plt.title('Contours of 2^(1/2) factor decrease')
        plt.grid(True)
        plt.xlim([0,r_max])
        plt.ylim([0,r_max])
        plt.show        



    
    def plot_temp(self,x_lim_1 = 0.0,x_lim_2 = 8000.0,is_three_dim = False, 
                  ninety_K=False, cm=False):
        """This function takes an input radmc3d sed file and observational 
        datafile and plots them.
        
        Args: mod_filename = name of model sed file.
              obs_filename = name of observational sed datafile.
              dist = distance to the object from observer.
              Also some selfexplanatory plot configurations.
       
        Returns: A plot of the sed with overplotted observational datapoints.
        """
        distance = [] 
        #Constants.
        AU  = 1.49597871*10.0**13.0 #[cm]
        prox = radmc3dPy.analyze.radmc3dData()
        proxG = radmc3dPy.analyze.radmc3dGrid()
               
        #Open datafiles.
        radmc3dPy.analyze.radmc3dData.readDustTemp(prox)
        if is_three_dim:        
            temp_r = prox.dusttemp[:,59,6]        
        if not is_three_dim:        
            temp_r = prox.dusttemp[:]        
        
        #Read grid if not done already
        radmc3dPy.analyze.radmc3dGrid.readGrid(proxG)
        
        if cm == False:        
            distance[:] = [x/AU for x in prox.grid.x]
        if cm == True:        
            distance[:] = [x for x in prox.grid.x]
            x_lim_1 = x_lim_1*AU
            x_lim_2 = x_lim_2*AU 
        plt.figure()        
        plt.plot(distance,temp_r,label=str('radmc-3d model temperature'),color ='red',
                 linewidth=3)
        plt.xlim(x_lim_1,x_lim_2)
        plt.ylim(8,300)
        plt.rcParams['font.size'] = 27
        plt.rc('text', usetex=True)
        if cm == False:        
            plt.xlabel(r' r [AU]')
        if cm == True:        
            plt.xlabel(r' r [cm]')
        plt.ylabel(r'T [K]')
        plt.xscale('log')        
        plt.yscale('log')
        plt.grid(True)
        plt.legend(fontsize = 24)        
        plt.show()                    