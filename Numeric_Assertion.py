# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 10:13:22 2015

@author: Steffen_KJ
"""

import math


class NumericAssertions:
    """
    This class is following the UnitTest naming conventions.
    It is meant to be used along with unittest.TestCase like so :
    class MyTestCase(unittest.TestCase, NumericAssertions):
        ...
    It needs python >= 2.6
    """

    def assertIsNan(self, value, msg=None):
        """
        Fail if provided value is not nan
        """
        standardMsg = "%s is not nan" % str(value)
        try:
            if not math.isnan(value):
                self.fail(self._formatMessage(msg, standardMsg))
        except:
            self.fail(self._formatMessage(msg, standardMsg))

    def assertIsNotNan(self, value, msg=None):
        """
        Fail if provided value is nan
        """
        standardMsg = "Provided value is nan"
        try:
            if math.isnan(value):
                self.fail(self._formatMessage(msg, standardMsg))
        except:
            pass