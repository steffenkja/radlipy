# -*- coding: utf-8 -*-
"""
PYTHON module for RADMC3D
Model: IRAS 16923-2422.

Credits:
    Template layout from model_spher1d_1.py in the radmc3dpy package.
    (c) Attila Juhasz, Kees Dullemond 2011,2012,2013,2014
    Original IDL model by Kees Dullemond, Python translation by Attila Juhasz



"""
try:
    import numpy as np
    from radmc3dPy import crd_trans
except:
    print 'ERROR'
    print ' Numpy cannot be imported '
    print ' To use the python module of RADMC-3D you need to install Numpy'

from radmc3dPy.natconst import *
import sys
import time as time
import os
import pstats, cProfile
profiler_active = False 
try:
    if os.path.expanduser('~') == '/Users/Steffen_KJ':
        os.system('python /Users/Steffen_KJ/Dropbox/PhD/Scientific_software/Python/modules/cython_setup.py build_ext --inplace')
    elif os.path.expanduser('~') == '/groups/astro/cmh154':
        os.system('python /groups/astro/cmh154/python/cython_setup.py build_ext --inplace')
except:
    print >> sys.stderr, "You are not in the expected wd"
    print >> sys.stderr, "Please relocate"
    sys.exit(1)
import cython_mod 
DustDensity = cython_mod.DustDensity 
import line_profiler 
try: 
    import line_profiler 
except ImportError: 
    print("No line profiler, test will not work.") 

# ============================================================================================================================
#
# ============================================================================================================================
def getDefaultParams():
    """
    Function to provide default parameter values 

    OUTPUT:
    -------

    Returns a list whose elements are also lists with three elements:
    1) parameter name, 2) parameter value, 3) parameter description
    All three elements should be strings. The string of the parameter
    value will be directly written out to the parameter file if requested,
    and the value of the string expression will be evaluated and be put
    to radmc3dData.ppar. The third element contains the description of the
    parameter which will be written in the comment field of the line when
    a parameter file is written.
    """ 

    defpar = {}

    defpar = [['mstar', '[1.0*ms,1.0*ms]', 'Mass of the star(s)'],
    ['pstar','[[-3363857075318417.5000, -3363857075318417.5000, 0.0000],[3363857075318417.5000, 3363857075318417.5000, 0.0000]]','Position of the star(s) (cartesian coordinates)'],
    ['rstar', '[5.6541*rs,2.3083*rs]', 'Radius of the star(s)'],
    ['tstar', '[5000.0000,5000.0000]','Effective temperature of the star(s)'],
    ['crd_sys',"'sph'", 'Coordinate system used (car/sph)'],
    ['nx', '60', 'Number of grid points in the first dimension'],
    ['ny', '140', 'Number of grid points in the second dimension'],
    ['nz', '140', 'Number of grid points in the third dimension'],
    ['xbound', '[14959787100000.0000,119678296800000000.0000]', 'Boundaries for the x-grid'],
    ['ybound', '[0.0, np.pi]', 'Boundaries for the y-grid'],
    ['zbound', '[0.000000, 6.283185]', 'Boundaries for the z-grid'],
    ['nw', '[20,30,30]', 'Number of points in the wavelength grid'],
    ['wbound', '[0.100000,20.000000,100.000000,9000.000000]', 'Boundaries for the wavelength grid'],
    ['dustkappa_ext', "['OH94_thin_ice_dens_106','OH94_no_ice_dens_106']", 'Dust opacity file name extension'],
    ['nphot', '150000', 'Number of photons in the thermal Monte Carlo simulation'],
    ['modified_random_walk', '1', 'If 1, set to modified random walk', 'Code parameters'],
    ['scattering_mode_max', '0', '0 - no scattering, 1 - isotropic scattering, 2 - anizotropic scattering', 'Code parameters'],
    ['istar_sphere', '0', '  1 - take into account the finite size of the star, 0 - take the star to be point-like', 'Code parameters'],
    ['prho', '-1.700000e+00', ' Power exponent of the radial density distribution'],
    ['rho0', '2.500000e-14', 'Central density']]

    return defpar

#=============================================================================================================================
#
#============================================================================================================================
def getModelDesc():
    """
    Function to provide a brief description of the model
    """
    descr = "This is a 1D spherical envelope model of IRAS 16293-2422 with powerlaw radial \n    density in a spherical grid." 
    descr = " ".join(descr.split())
    return descr
# ============================================================================================================================
#
# ============================================================================================================================
def getDustDensity(subli_list,oct_inst,grid=None, ppar=None,octree_indices= False,octree_grid= None,ref_nr=None,p_stars=None):
    """
    Function to create the dust density distribution 
    
    OUTPUT:
    -------
        returns the volume density in g/cm^3
    """
    start = time.clock()
    nx = grid.nx 
    ny = grid.ny 
    nz = grid.nz 
    dust_setup_args = {"nx": nx,"ny": ny,"nz": nz,
    'g_ix':grid.xi,'g_iy':grid.yi,'g_iz':grid.zi,
     'gx':grid.x,'gy':grid.y,'gz':grid.z,
     "n_dust":2, "n_stream":80, "n_stream_r":80, 
    "evap_rad":1.047185097e+15,"cav_rad":8.97587226e+15, "inn_rad":1.49597871e+13, "out_rad":1.196782968e+17, 
    "source_nr":2, "prho":-1.7, "rho0":2.5e-14,"r_c":5.98391484e+14,"disk_dic":{'disk': True, 'disk_dic_1': {'r_fill': 0.0, 'r_out': 2243968065000000.0, 'rho_0': 0.01, 'azith_turn': 2.356194490192345, 'theta_bounds': 'dummy', 'H_0': 29919574200000.0, 'stream_r': 'dummy', 'r_0': 149597871000000.0, 'r_in': 14959787100000.0, 'flr': 0.0, 'p': -0.5, 'theta_0': 'dummy', 'incl': 1.5707963267948966}, 'disk_dic_2': {'r_fill': 0.0, 'r_out': 747989355000000.0, 'rho_0': 0.01, 'azith_turn': 1.5707963267948966, 'theta_bounds': 'dummy', 'H_0': 29919574200000.0, 'stream_r': 'dummy', 'r_0': 149597871000000.0, 'r_in': 14959787100000.0, 'flr': 0.0, 'p': -0.5, 'theta_0': 'dummy', 'incl': 0.0}},"tor_dic":{'max_dens': 2.5000000000000003e-17, 'sm_min': 2243968065000000.0, 'tor_rad': 1944772323000000.0, 'torus': True, 'tor_p': -0.25, 'tor_incl': 0.0, 'tor_c': 4487936130000000.0},
    "octree_indices":octree_indices,"ref_nr":ref_nr, 
    "oct_rad_1":2.243968065e+15,"oct_rad_2":2.243968065e+15,"inn_disk_ref_1_rad":8.97587226e+13,"inn_disk_ref_2_rad":8.97587226e+13, 
     "m_dot":1.57580304273e+20, "m_star":2.18801e+33,"p_stars":p_stars, "bare":True,"rot_collapse":False,"cav_enable":True,"recurs_subli":True,"oct_inst":oct_inst,"subli_list":subli_list, 
    "x_c":0.0, "y_c":0.0, "z_c":0.0, 
    "disk_rot_coll":True } 
    import warnings
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    mdl_dust = DustDensity(**dust_setup_args)
    if not profiler_active: 
       rho, theta_bounds, stream_r,vel,vel_dic,disk_1_idcs,disk_2_idcs, tor_idcs = DustDensity.dust_setup(mdl_dust,**dust_setup_args)
       elapsed = (time.clock() - start)
       print 'Time elapsed in DustDensity.dust_setup() = %f seconds' % elapsed 
    if profiler_active:
    # Print profiling statistics using the `line_profiler` API
        print 'Profiling DustDensity.dust_setup(), please wait..' 
       # profile = line_profiler.LineProfiler(cython_mod.DustDensity.dust_setup)
        profile = line_profiler.LineProfiler(mdl_dust.dust_setup)
        profile.runcall(mdl_dust.dust_setup,**dust_setup_args)
        profile.print_stats() 
        stats = profile.get_stats()
        assert len(stats.timings) > 0, "No profile stats." 
        for key, timings in stats.timings.items(): 
            if key[-1] == 'mdl_dust.dust_setup': 
                assert len(timings) > 0 
                break 
        else: 
            raise ValueError("No stats for DustDensity.") 

    return rho, theta_bounds, stream_r,vel, vel_dic,disk_1_idcs,disk_2_idcs, tor_idcs 

