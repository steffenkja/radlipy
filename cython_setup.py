# -*- coding: utf-8 -*-
# !/usr/bin/python2

"""
Created on Mon Mar  2 09:40:47 2015

@author: Steffen_KJ
"""
# from __future__ import unicode_literals
from distutils.core import setup
from Cython.Build import cythonize
from Cython.Distutils import build_ext

import Cython.Compiler.Options as cyth_options
# from Cython.Compiler.Options import directive_defaults
from distutils.extension import Extension
import numpy
import sys
import os

directive_defaults = cyth_options.get_directive_defaults()
# Use cythonize feature or not. Currently I do not know the difference between
# cythonize and build_ext. TEST
cython_ize = False
directive_defaults['linetrace'] = True
directive_defaults['binding'] = True

try:
    if os.path.expanduser("~") == '/Users/Steffen_KJ':
        target_file = '/Users/Steffen_KJ/Dropbox/PhD/Scientific_software/Python/modules/cython_mod.pyx'

    elif os.path.expanduser("~") == '/groups/astro/cmh154':
        target_file = '/groups/astro/cmh154/python/cython_mod.pyx'
except:
    print >> sys.stderr, "You are not in the expected wd"
    print >> sys.stderr, "Please relocate"
    exit()

if cython_ize:
    setup(
        ext_modules=cythonize("cython_mod.pyx"),
        include_dirs=[numpy.get_include()],
        extra_compile_args=["-Wno-unused-but-set-variable",
                            "-Wno-unused-function"]
    )

elif not cython_ize:
    setup(
      name='Build Cython',
      ext_modules=[
        Extension('cython_mod', define_macros=[('CYTHON_TRACE', '1')],
                  sources=[target_file], #  gdb_debug=True, gdb_debug was not recognized.
#                 libraries=["m"],
                  include_dirs=[numpy.get_include()],
#                  extra_compile_args=['-Wno-unused-const-variable',
#                                      '-Wno-unused-function',
#                                      '-Wsometimes-uninitialized',
#                                      '-Wno-#warnings'],
#		  extra_link_args=['-fopenmp'],
                  language='c')
        ],
      cmdclass={'build_ext': build_ext}
    )
