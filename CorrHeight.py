from __future__ import division
#PYTHONPATH=/home/steffenkj/Dropbox/KandidatSpeciale/radmc-3d/version_0.35/examples/MMFin/
from scipy.optimize import fsolve
import numpy as np
print 'Always check that the boundary conditions are up-to-date' 

def InnDiskBounC(d):
    '''This small program computes the Inner Rim wall scale height z_0(10 AU) and its flaring constant, Psi.
    The program fsolve is uses, which requires initial guesses. Be aware that the solutions may not be unique, but those offered 
    satisfy the boundary conditions between the Rim end and the Inner disk start radius.
    
    Based on z_d(r) = z_0*(r/r0)^(Psi)
    '''
    hrdisk10AURim, Rimplh = d
    hr055 = 0.012
    hr155 = 0.004618

    InnStart = 0.055/10.
    InnEnd = 0.155/10.
    
    return (hrdisk10AURim*((InnStart)**(Rimplh)) - hr055, hrdisk10AURim*((InnEnd)**(Rimplh)) - hr155)

r, o =  fsolve(InnDiskBounC, (10**(-5.), -1.))


def OutDiskBounC(p):
    '''This small program computes the Outer disk wall scale height z_0(10 AU) and its flaring constant, Psi.
    The program fsolve is uses, which requires initial guesses. Be aware that the solutions may not be unique, but those offered 
    satisfy the boundary conditions between the Outer disk wall end and the Outer disk start radius.
    
    Based on z_d(r) = z_0*(r/r0)^(Psi)
    '''
    
    hrdisk10AUWall, Wallplh = p
    hr4 = 0.490
    hr5 = 0.235553
    Outend = 5.0/10.
    OutStart = 4.0/10.
    return (hrdisk10AUWall*((OutStart)**(Wallplh)) - hr4, hrdisk10AUWall*((Outend)**(Wallplh)) - hr5)

x, y =  fsolve(OutDiskBounC, (0.2, -3))

def An12Wall(p):
    '''This small program computes the wall scale height z_0(10 AU) and exponential constant, Psi.
    The program fsolve is used, which requires initial guesses. Be aware that the solutions may not be unique, but those offered 
    satisfy the boundary conditions between the Outer disk wall end and the Outer disk start radius.
    
    Based on z_d(r) = z_0*e^(-y*r/r0)
    '''
    
    hrdisk10AUWall,y = p
    hr4 = 0.35
    hr5 = 0.294314            #Make sure this number is up to date.
    Outend = 5.0/10.
    OutStart = 4.0/10.
    return (hrdisk10AUWall*np.exp(OutStart*y) - hr4, hrdisk10AUWall*np.exp(Outend*y) - hr5)

xa, ya =  fsolve(An12Wall, (0.2, -0.3))
print 'AN12',xa,ya
def MMFinWall(p):
    '''This small program computes the wall scale height z_0(10 AU) and exponential constant, Psi.
    The program fsolve is used, which requires initial guesses. Be aware that the solutions may not be unique, but those offered 
    satisfy the boundary conditions between the Outer disk wall end and the Outer disk start radius.
    
    Based on z_d(r) = z_0*e^(-y*r/r0)
    '''
    
    hrdisk10AUWall,y = p
    
    OutStart = 4.0/10.    
    Outend = 5.0/10.
    hr10 =   0.85
    hrnw = hr10*(OutStart)**(1.20)
    
    hr4 = 0.5
    hr5 = hr10*(Outend)**(1.20)

    return (hrdisk10AUWall*np.exp(OutStart*y) - hr4, hrdisk10AUWall*np.exp(Outend*y) - hr5)

xa, ya =  fsolve(MMFinWall, (0.9, -0.2))

hr10 =  0.85
Outend = 5.0/10.
OutStart = 4.0/10.
hr5 = hr10*(Outend)**(1.20)
print 'hr5',hr5 
print 'MMFinWallZ0 = ',xa
print 'MMFinWally =',ya
print 'hr4 =', xa*np.exp(OutStart*ya)
def MMFinRIM(d):
    '''This small program computes the wall scale height z_0(10 AU) and exponential constant, Psi.
    The program fsolve is used, which requires initial guesses. Be aware that the solutions may not be unique, but those offered 
    satisfy the boundary conditions between the Outer disk wall end and the Outer disk start radius.
    
    Based on z_d(r) = z_0*e^(-y*r/r0)
    '''  
    InnStart = 0.67/10.
    InnEnd = 0.77/10.
    hrdisk10AURim, y = d
    hr10 =  0.62
    hrn = hr10*(InnStart)**(1.25)
    
    hr1 = 1.75*hrn
    hr2 = hr10*(InnEnd)**(1.25)

    return (hrdisk10AURim**np.exp(InnStart*y) - hr1, hrdisk10AURim*np.exp(InnEnd*y) - hr2)

xb, yb =  fsolve(MMFinRIM, (10**(-2.), -1.))

hr10 =  0.62
InnEnd = 0.77/10.
InnStart = 0.67/10.
hrInnE = hr10*(InnEnd)**(1.0)
#print 'hrInnE',hrInnE
#print 'MMFinRimZ0 = ',xb
#print 'MMFinRimy =',yb
#print 'HrInnStart =', xb*np.exp(InnStart*yb)