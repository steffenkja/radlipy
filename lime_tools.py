# -*- coding: utf-8 -*-
# !/usr/bin/env python -W ignore::DeprecationWarning

"""
Created on Mon Aug 10 16:46:46 2015

@author: Steffen_KJ
"""

# This is a test line to track git branching.

# This line should only be seen in branch lime_dump.

from __future__ import division
from __future__ import print_function
import warnings
# import pyfits from astropy instead to accommodate server software
import astropy.io.fits as pyfits
from astropy.utils.exceptions import AstropyWarning
#from PIL import Image
from matplotlib.colors import LogNorm
import scipy.interpolate as inter
import numpy as np
#import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
import pylab as pl
import os
#from mpl_toolkits.axes_grid1 import ImageGrid
#import scipy as sp
import itertools
warnings.filterwarnings('ignore')
import seaborn.apionly as sns
warnings.filterwarnings('always')
from si_cnst import M_SUN as M_SUN_SI
import sys
import copy
import math

plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'

try:
    if os.path.expanduser('~') == '/Users/Steffen_KJ':
        os.system('python /Users/Steffen_KJ/Dropbox/PhD/Scientific_software/'
                  'Python/modules/cython_setup.py build_ext --inplace')
    elif os.path.expanduser('~') == '/groups/astro/cmh154':
        os.system('python /groups/astro/cmh154/python/cython_setup.py '
                  'build_ext --inplace')
except:
    print >> sys.stderr, "You are not in the expected wd"
    print >> sys.stderr, "Please relocate"

import subprocess
from radmc3d_tools import (scalarfield_reader, conv_freq_wav, freq_wav_conv,
                           out_to_fits)
import gc
from cgs_cnst import AU, M_H2
from si_cnst import C_L, k_b
from si_cnst import AU as AU_SI
from cython_mod import transf_sph_cart, vel_profile
# from collections import defaultdict
warnings.filterwarnings('ignore')
# Fix issue with Yt plotting old data. Previously needed to restart python.
from mpl_toolkits.axes_grid1 import AxesGrid
#from mpl_toolkits.axes_grid.anchored_artists import AnchoredAuxTransformBox
from matplotlib.patches import Ellipse
warnings.filterwarnings('always')
warnings.simplefilter('ignore', category=AstropyWarning)
warnings.filterwarnings("ignore", category=DeprecationWarning)


class lime_tools():
    """This class defines a variety of lime analysis tools.
    Be advised that filenames should be given without the folder path. Instead
    the work_dir parameter is used to switch back and forth in the folders.
    This is done due to a size limitation of the miriad file length.

    Args:
        file_name: The main lime model file - should be continuum subtracted
            and convolved with the beamsize of the telescope used to observe
            the observation data spectrum.
        cont_file: The lime continuum model file.
        data_file: The observation data file. Can be a pointer spectrum or a
            map. The booleans "point_spectrum" and "single_dish_map" must be
            set accordingly.
        work_dir: Working directory
    """  # TODO Write more comments

    def __init__(self, file_name, data_file, work_dir, timass=False,
                 PILS=True, **kwargs):
        self.file = file_name
        self.data_file = data_file
        self.work_dir = work_dir
        self.timass = timass
        self.PILS = PILS

    def plot_spectral_data(self, is_lime_mdl, point_spectrum, cont,
                           cube, beam_eff, fwhmbmaj, show_int_lim=False,
                           chan_start=-1, chan_end=-1, box=0, CO=17,
                           pix_correction_factor=1.0/9.0, **kwargs):
        """This method will plot the spectral data. Handles pointing spectra
        (1D scalars) and data cubes (3D scalars).
        TBD: More illustrative
        Args:
            is_lime_mdl: Boolean stating whether or not it is a lime model.
            point_spectrum: Boolean stating if we plot a pointing spectrum (1D)
            cont: Boolean. Use continuum file or not.
            cube: Boolean, states whether or not the datafile a cube.
        Returns:
            None. Produces plots.
        """
#        sns.set_style("darkgrid", {'axes.grid': True})
        # Lime fits files come with M/S in the header, but S will be
        # interpreted as Siemens. S is instead converted to s which is
        # interpreted as seconds.
        os.chdir(self.work_dir)
        f = pyfits.open(self.file, mode='update')
        prihdr = f[0].header
        prihdr['CUNIT3'] = 'm/s'  # Convert M/S to m/s.
        vel_px = float(prihdr['CDELT3'])  # Get stepsize pr. slice (channel width)
        rest_freq = float(prihdr['RESTFREQ'])  # Get rest frequency of emission
        # line ()
        print('converted header units in %s to %s' %
              (self.file, prihdr['CUNIT3']))
        f.flush()
        f.close()

        # Extract lime model fits data.
        lime_cube, hdr = load_fits_cube(self.file)
        # Get the flux units
        B_unit = hdr['BUNIT']

        if B_unit == 'K':
            flux_unit = 'Temperature'
        elif B_unit == 'JY/PIXEL':
            flux_unit = 'Flux'
        elif B_unit == 'JY/BEAM':
            flux_unit = 'Flux/beam'
            B_unit = 'K/beam'  # TODO change the fits header after miriad convol
            # when K is used. Default is Jy/beam even though K is used in lIME.

        # Get cube dimensions
        print('Shape of the lime datacube is ', lime_cube.shape)
        cl_m, yl_m, xl_m = lime_cube.shape

        freq_for_plot = np.zeros(cl_m)
        lime_spec = np.zeros(cl_m)
        # TODO: Check that I actually hit the central pixel below

        lime_cntrl_pix = np.zeros(cl_m)
        lime_cntrl_pix[:] = lime_cube[cl_m, int(yl_m/2.0), int(xl_m/2.0)]

        for i in xrange(cl_m):
            lime_spec[i] = sum(sum(lime_cube[i, 400:600, 400:600]))*pix_correction_factor

        # Collapse the image dimensions, to get the integrated spectrum of the
        # entire cube.

        # Convert the velocity to frequency units
        if is_lime_mdl:

            # Be aware that the lime datacube is delivered from blueshifted
            # (negative) velocities to redshifted (positive) velocities.
            # This means high freq to low freq.
            vel = np.zeros(len(lime_spec))
            freq = np.zeros(len(lime_spec))
            wav_m = np.zeros(len(lime_spec))
            T_mb_m = np.zeros(len(lime_spec))

            if self.timass:
                vel[:] = [x * vel_px - len(lime_spec) * vel_px / 2. for x in
                          xrange(len(lime_spec))]
            elif self.PILS:
                vel[:] = [x * vel_px + 4.00e3 - len(lime_spec) * vel_px / 2. for x in xrange(len(lime_spec))]                
            # Induce velocity shift due to the bulk movement of the object.
            # 2.85*10e3: systemic source velocity in m/s, taken as the average
            # of the two sources from Joergensen 2012, for IRAS-16293.

            # TODO why is the vel shift unneccessary if timass? Corrected by 
            # JCMT in their pointer spectrum?
            # vel[:] = [x - 2.85*10e3 for x in vel]
            
            # Convert velocity to frequency. I use relativistic velocities
            # which should reduce to the classical formulae for doppler shift
            # in the case of non-relativistic speeds.
            # OBS! The equation below requires that objects moving towards the
            # observer are classified as negative radial velocity.
            freq[:] = [rest_freq * np.sqrt((1. - v / C_L) / (1. + v / C_L))
                       for v in vel]
            assert freq[int(len(freq) / 2)] < freq[int(len(freq) / 2) - 1], ('Freq array'
                        ' must go from blueshifted to redshifted emission')
            wav_m[:] = conv_freq_wav(freq[:], freq=True,
                                           micron=False)  # Returns wavelength in meters.

            # The solid angle of the beam in steradian.
            # Note that I use natural log.
            omega_mb = (np.pi * (fwhmbmaj / (60. * 60.))**2. * (np.pi / 180.)**2. /
                        (4. * np.log(2.)))

            # Now convert Jy to main beam temperature. I use SI units. See
            # "various_notes.tex" for more explanations.
            T_mb_m[:] = wav_m[:]**2. * lime_cntrl_pix[:] * \
                10.**(-26.) / (2. * k_b * omega_mb)

        freq_for_plot[:] = freq[:] / (10.**(9.))
        # Extract continuum data.
        if cont:
            self.mk_cont()  # Create cont file.
            lime_cube_c, hdr_c = load_fits_im(self.cont_file)
            cont_val = np.sum(lime_cube_c)
            cont_val_arr = [cont_val for i in xrange(cl_m)]

# --------------------------- Extract obs data --------------------------------
        if point_spectrum:
            data_ptr, hdr_ptr = pyfits.getdata(self.data_file, 0, header=True)

            ptr_freq_px = float(hdr_ptr['CDELT1'])  # Get freq unit
            ptr_rest_freq = float(hdr_ptr['RESTFREQ'])  # Get rest freq
            ptr_ref_px = int(hdr_ptr['CRPIX1']) - 1 # The reference pixel
            # where the frequency and systemic velocity is set. Offset by one
            # to conform to zero-indexing arrays.

#            zl, xl, yl, c = data_ptr.shape
            ptr_spec = np.zeros(data_ptr.shape[-1])
            # The TIMASSS survey supplies the values in antennae temperature,
            # T_A. However, I choose to convert both the pointer spectrum and
            # the lime datacube to main beam temperature.
            # Since beam_eff = T_A/T_mb
            # OBS: What if last dimension is stokes?
            ptr_spec[:] = data_ptr[0, 0, 0, :] / beam_eff

            ptr_freq = np.zeros(len(ptr_spec))
            ptr_freq[:] = [(x * ptr_freq_px + ptr_rest_freq - ptr_ref_px * ptr_freq_px) /
                           (10.**(9.)) for x in xrange(len(ptr_spec))]

        elif cube:

            cube_inst = cube(self.data_file, dist_pc=120.0,
                             line_rest_freq=rest_freq, rest_vel=3100.0)
            cube_inst.extract_data()

# ==============================================================================
#                                   PLOTS
# ==============================================================================
        if is_lime_mdl:
            # Plot velocity diagram with intensity.
            if CO == 13:
                mol = 'C$^{13}$O'
            elif CO == 17:
                mol = 'C$^{17}$O'
            elif CO == 18:
                mol = 'C$^{18}$O'
            print('CO is {} and molstring is {}'.format(CO, mol))
            vel_plot_0 = -1e4
            vel_plot_1 = 2e4
            if cube:
                plot_vel_emission(vel, lime_spec, v_0=vel_plot_0,
                                  v_1=vel_plot_1, oplot=True,
                                  vel_2=cube_inst.vel,
                                  spec_2=cube_inst.int_spectrum,
                                  label_1='Model {}'.format(mol),
                                  label_2='Obs. {}'.format(mol),
                                  show_int_lim=show_int_lim,
                                  int_0=cube_inst.vel_cube[chan_start],
                                  int_1=cube_inst.vel_cube[chan_end])
            else:
                plot_vel_emission(vel, lime_spec, v_0=vel_plot_0,
                                  v_1=vel_plot_1,
                                  label_1='Model {}'.format(mol))

            fig, ax = pl.subplots()
            ax.format_coord = lambda x, y: '%.10e, %.10e' % (x, y)

            pl.plot(freq_for_plot, lime_spec, 'b')
            if cont:
                pl.plot(freq_for_plot, cont_val_arr, 'g')
            if cube:
                pl.plot(cube_inst.freq/1e9, cube_inst.int_spectrum,
                        label='Obs. {}'.format(mol))

            ax.axis('tight')
            pl.draw()
            pl.xlabel('freq [GHz]')
            pl.ylabel('%s [%s]' % (flux_unit, B_unit))
            pl.grid()

        elif point_spectrum:

            pl.figure()
            fig, ax = pl.subplots()
            pl.plot(freq_for_plot, T_mb_m, label='LIME spectrum')
            pl.plot(ptr_freq, ptr_spec, label='JCMT pointer spectrum')
            ax.axis('tight')
            ax.get_xaxis().get_major_formatter().set_useOffset(False)
            pl.draw()

            pl.xlabel('Freq [GHz]')
            pl.ylabel('Main beam temperature [K]')
            pl.legend()
            pl.grid()

        pl.show()

    def cnv_and_mom_cube(self, fwhmbmaj, fwhmbmin, trash_dir, rm_old=True,
                         mir=True, mom=True, cnv=True, PV=False,
                         pa=0.0, **kwargs):
        """
        Convolves the image fits file with the supplied beamsize.
           Creates a single zero-moment image of the convolved datacube.
           Input: file_name = Name of fits file.
                  fwhmbmaj = fwhm major axis of the beam.
                  fwhmbmin = fwhm minor axis of the beam
                  rm_old = Boolean. If set, delete previous run files
                      (cnvPlusInter() will not work if the output files
                      already exists)
                  mir = Boolean. If set, run miriad commands (handle kept so
                  cnvPlusInter() can be used as a clean-up tool).
           Output: No prompt output, but a filenamecnv.fits and
               filename_int.fits cube and fits file.
           Requirements: Miriad package that can run from bash.
        """
        print('')
        print('-------------------------- MIRIAD ----------------------------')
        print('')

        # Remove ".fits" from the string for easier naming.
        item = self.file[:-5]
        gc.collect()

        # Due to buffer issues with long filepaths, I will instead change the
        # directory and call the file by its name only, not path.
        print('Work_dir is ', self.work_dir)
        print('self.file is' ,self.file)
        os.chdir(self.work_dir)
        if rm_old:
            # Move to trash dir
            subprocess.call('mv ' + item + '.mir/* ' + trash_dir, shell=True)
            subprocess.call(['rmdir', item + '.mir'], shell=False)

            if cnv:
                subprocess.call('mv ' + item + '_cnv.fits ' +
                                trash_dir, shell=True)
                subprocess.call('mv ' + item + '_cnv.mir/* ' +
                                trash_dir, shell=True)
                subprocess.call(['rmdir', item + '_cnv.mir'], shell=False)
            if cnv and mom:
                subprocess.call('mv ' + item + '_cnv_int.fits ' +
                                trash_dir, shell=True)
                subprocess.call('mv ' + item + '_cnv_int.mir/* ' +
                                trash_dir, shell=True)
                subprocess.call(['rmdir', item + '_cnv_int.mir'], shell=False)

            elif mom and not cnv:
                subprocess.call('mv ' + item + '_int.fits ' +
                                trash_dir, shell=True)
                subprocess.call('mv ' + item + '_int.mir/* ' +
                                trash_dir, shell=True)
                subprocess.call(['rmdir', item + '_int.mir'], shell=False)

        if mir:
            print('item is ', item + '.fits')
            v = subprocess.call(
                ['fits', 'op=xyin', 'in=' + item + '.fits',
                 'out=' + item + '.mir'], shell=False)
            assert v == 0, ('Call to shell from Python failed in '
                            'cnv_and_mom_cube()')
            if cnv:
                subprocess.call(['convol', 'map=' + item + '.mir',
                                 'out=' + item + '_cnv.mir',
                                 'fwhm=' + str(fwhmbmaj) + ',' + str(fwhmbmin),
                                 'pa=' + str(pa) + ',',
                                 'options=final'], shell=False)
                subprocess.call(
                    ['fits', 'op=xyout', 'in=' + item + '_cnv.mir',
                     'out=' + item + '_cnv.fits'], shell=False)
                if mom:
                    subprocess.call(
                        ['moment', 'in=' + item + '_cnv.mir', 'out=' + item +
                         '_cnv_int.mir', 'mom=0'], shell=False)
                    subprocess.call(
                        ['fits', 'op=xyout', 'in=' + item + '_cnv_int.mir',
                         'out=' + item + '_cnv_int.fits'], shell=False)
                elif PV:
                    subprocess.call(
                        ['moment', 'in=' + item + '_cnv.mir',
                         'out=' + item + '_cnv_PV.mir', 'mom=0',
                         'axis=1'], shell=False)
                    subprocess.call(
                        ['fits', 'op=xyout', 'in=' + item + '_cnv_PV.mir',
                         'out=' + item + '_cnv_PV.fits'], shell=False)

            elif not cnv and mom:
                subprocess.call(
                    ['moment', 'in=' + item + '.mir',
                     'out=' + item + '_int.mir', 'mom=0'], shell=False)
                subprocess.call(
                    ['fits', 'op=xyout', 'in=' + item + '_int.mir',
                     'out=' + item + '_int.fits'], shell=False)
            elif not cnv and PV:
                subprocess.call(
                    ['moment', 'in=' + item + '.mir',
                     'out=' + item + '_PV.mir', 'mom=0', 'axis=1'],
                    shell=False)
                subprocess.call(
                    ['fits', 'op=xyout', 'in=' + item + '_PV.mir',
                     'out=' + item + '_PV.fits'], shell=False)

# ------------------- Remove redundant mir files ------------------------------

        subprocess.call('mv ' + item + '.mir/* ' + trash_dir, shell=True)
        subprocess.call(['rmdir', item + '.mir'])

        if cnv:
            subprocess.call('mv ' + item + '_cnv.mir/* ' +
                            trash_dir, shell=True)
            subprocess.call(['rmdir', item + '_cnv.mir'])

        if cnv and mom:
            subprocess.call('mv ' + item + '_cnv_int.mir/* ' +
                            trash_dir, shell=True)
            subprocess.call(['rmdir', item + '_cnv_int.mir'])
        elif not cnv and mom:
            subprocess.call('mv ' + item + '_int.mir/* ' +
                            trash_dir, shell=True)
            subprocess.call(['rmdir', item + '_int.mir'])

        # Remove "_c" for explicit deletion of continuum subtracted files
        item = item[:-2]

        print('')
        print('------------------------ MIRIAD END --------------------------')
        print('')

    def rnLIMEmod(n_models, model_name, offset=0, check=0):
        """This function runs a range of LIME models from the bash shell.
        Requires 'silent' set to no output in lime.h file, in the lime/src
        directory.
        Args:
            model_name: The name of the files, excluding the nr and .c suffix.
                E.g. the model name list [im_HCO+1.c,im_HCO+1.c] should be
                inputted as 'im_HCO+'.

            """

        if check != 1:
            for i in range(n_models):
                subprocess.call(['lime', model_name + str(int(i + 1 + offset)) + ".c"],
                                shell=False)

        if check == 1:
            for i in range(int(n_models)):
                print('lime', model_name + str(int(i + 1 + offset)) + ".c")

    def mk_cont(self):
        """
        This method creates a continuum file.
        """

        self.cont_file = self.file[:-5] + '_c.fits'
        extract_first_chan_as_cont(cube=self.file,
                                   out_name=self.cont_file)

    def subtr_cont(self):
        """
        This function subtracts continuum emission, using the first channel
        of the input cube.
        Args:
            file_name: Name of the input fits file.
        Returns:
            None. Writes a new continuum subtracted fits file named
            file_name_c.
        """

        os.chdir(self.work_dir)
        self.mk_cont()
        data_cont, chdr = load_fits_im(self.cont_file)
        data_cube, hdr = load_fits_cube(self.file)

        pyfits.info(self.file)
        zl, yl, xl = data_cube.shape
        data_res = np.zeros((zl, yl, xl))

        for z in xrange(zl):
            data_res[z, :, :] = data_cube[z, :, :] - data_cont[:, :]

        wrt_to_fits(data_res, hdr, self.file[:-5] + '_c.fits')

# =============================================================================
#         # We then create a HDUList to contain the newly created primary
#         # HDU, and write to a new file:
#         hdu = pyfits.PrimaryHDU(data_res)
#         hdulist = pyfits.HDUList([hdu])
#         hdulist[0].header = hdr
# 
#         subprocess.call(['rm', self.file[:-5] + '_c.fits'])
#         hdulist.writeto(self.file[:-5] + '_c.fits')
#         hdulist.close()
# =============================================================================

        # Update fits file name.
        self.file = self.file[:-5] + '_c.fits'

    def zeroth_mom(self, ALMA=0, binsize=2):
        """This function reads in a .fits file and creates a zero moment map

        """
        os.chdir(self.work_dir)
        data_cube, hdr = load_fits_cube(self.file)
        os.system('rm im_mom.fits')
        pyfits.info(self.file)
        zl, yl, xl = data_cube.shape
        casa = True

        if casa:
            vel = 216.70902198602516 / 1000
        else:
            vel = 0.029647684

        data_mom = np.zeros((yl, xl), dtype=np.float64)

        for x, y in itertools.product(xrange(xl), xrange(yl)):
            data_mom[y, x] = sum(data_cube[:, y, x]) * vel

        wrt_to_fits(data_mom, hdr, 'im_mom.fits')
# =============================================================================
#         # We then create a HDUList to contain the newly created primary
#         # HDU, and write to a new file:
#         hdu = pyfits.PrimaryHDU(data_mom)
#         hdulist = pyfits.HDUList([hdu])
#         hdulist[0].header = hdr
# 
#         hdulist.writeto('im_mom.fits')
#         hdulist.close()
#         gc.collect()
# =============================================================================
        print('zeroth moment map is done')

    def mom_rad_I(self, ALMA=0, binsize=2):
        """This function reads in a .fits file of the zero-moment map
        and computes the radial intensity as a distance from the image
        center."""

# ----------------------------------------------------------------------------#
#                                   HANDLES
# ----------------------------------------------------------------------------#
        ValC = []
        distan = []
        ValR = []
        coord = []
        distLim = 1000
        # Is the image a RADMC-3D image (=0) or made from a LIME datacube ( !=
        # 0).
        radmc = 0

# ----------------------------------------------------------------------------#
#                             ANNULAR INTEGRATION
# ----------------------------------------------------------------------------#

        data_cube, header_data_cube = pyfits.getdata(self.file, 0, header=True)

        pyfits.info(self.file)

        if radmc == 1:
            yl, xl = data_cube.shape
            data = np.array(range(xl * yl), dtype=np.float64)
            data = data.reshape(xl, yl)

            for x in range(int(xl)):
                for y in range(int(yl)):
                    data[x][y] = data_cube[y][x]

        if radmc != 1 and ALMA == 1:
            b, c, yl, xl = data_cube.shape
            x0 = xl / 2.0
            y0 = yl / 2.0
            data = np.array(range(xl * yl), dtype=np.float64)
            data = data.reshape(xl, yl)

            for x in range(int(xl)):
                for y in range(int(yl)):
                    data[x][y] = data_cube[0][0][y][x]

        if radmc != 1 and ALMA != 1:
            c, yl, xl = data_cube.shape
            x0 = xl / 2.0
            y0 = yl / 2.0
            data = np.array(range(xl * yl), dtype=np.float64)
            data = data.reshape(xl, yl)

            for x in range(int(xl)):
                for y in range(int(yl)):
                    data[x][y] = data_cube[0][y][x]

        for x in range(xl):
            for y in range(yl):
                R = np.sqrt((x - x0)**2 + (y - y0)**2)
                distan.append(R)  # This is pixel nr distance.
                res = data[x][y]
                ValC.append(res)

        # Sort lowest to highest, i.e. from low kilolambda to higher.
        order = sorted(range(len(distan)), key=lambda k: distan[k])
        ValR = []
        coord = []
        resCoordVal = []

        for z in range(int(len(distan))):
            if z == 0:
                listofval = order[0:1]
            if z != 0:
                listofval = order[binsize * z:binsize * (z + 1)]
            resCoordVal[:] = [distan[yn] for yn in listofval]

            if len(resCoordVal) != 0:
                # Average radial distance R. len(listofval) earlier
                resCoord = sum(resCoordVal) / len(resCoordVal)

            if resCoord <= distLim and len(resCoordVal) != 0:
                ValCsum = np.zeros(len(listofval))
                ValCsum[:] = [ValC[yk] for yk in listofval]

                resAF = sum(ValCsum) / len(ValCsum)  # Flux in distance R.
                ValR.append(resAF)
                coord.append(resCoord)

        return coord, ValR

    def prt_idx():
        """Print model values of LIME model."""
        xi_0 = ["5.0e-4", "1.0e-5", "5.0e-5", ]
        xi_expont = ["0.0"]  # ["0.0","0.5","1"]
        sig0 = ['0.0015']  # ,'0.002','0.003']
        xi_FWHM = ['(35.0*AU)', '(75.0*AU)', '(150.0*AU)']
        xi_offs = ['(0.0*AU)', '(20.0*AU)']
        counter = 1

        for i in range(len(xi_0)):
            for j in range(len(xi_expont)):
                for k in range(len(sig0)):
                    for m in range(len(xi_FWHM)):
                        for n in range(len(xi_offs)):
                            print('model' + str(counter), 'sig0:' + str(sig0[k]), '   xi_expont:' + str(xi_expont[
                                  j]), '    xi_0' + str(xi_0[i]), 'xi_FWHM' + str(xi_FWHM[m]), 'xi_offs' + str(xi_offs[n]))


class casa_tools():
    """
    This class reads casa output and performs various tasks on them.
    """
    def __init__(self, cube, output_dir, nchan, work_dir):
        self.cube = cube
        self.output_dir = output_dir
        self.nchan = nchan
        self.work_dir = work_dir

    def _imfit(self, box):
        """
        This function writes a small executable CASA script for performing
        imfit on a cube, dumping the results in an input dir.
        """

        with open('exe_casa_imfit.py', 'w') as f:
            f.write("output_dir = '{}'\n".format(self.output_dir))
            f.write("work_dir = '{}'\n".format(self.work_dir))
            f.write("cube = '{}' + '{}'\n".format(self.work_dir, self.cube))
            f.write("os.chdir(output_dir)\n")
            f.write("channel_list = range(0, {})\n".format(self.nchan))
            f.write("box = '{}'\n".format(box))
            f.write("for chan in channel_list:\n")
            f.write("    imfit(imagename=cube, box=box, newestimates='out.newestimate', chans=chan, logfile='chan_{}_log.dat'.format(chan))\n")
        os.system("casapy --nologger -c 'exe_casa_imfit.py'")

    def _wrt_logfiles_to_res(self, logfile_list):
        """
        This function reads logfiles outputted from the casa task imfit, and
        concatenates them into a single logfile.
        """

        chan_nr = []
        ra_pos = []
        dec_pos = []

        for logfile in logfile_list:
            with open(os.path.join(self.output_dir, logfile), 'r') as f:
                lines = f.readlines()

                # Extract pixel pos and uncertainty
                for i, line in enumerate(lines):
                    fit_failed = False
                    if 'Details of fit for channel number' in line:
                        line_splitted = line.split(' ')
                        for elem in line_splitted:
                            try:
                                chan_nr.append(float(elem))
                            except ValueError:
                                pass
                    if 'FIT FAILED' in line:
                        ra_pos.append(-1)
                        dec_pos.append(-1)
                        fit_failed = True
                    if '--- ra' in line and 'pixels' in line and not fit_failed:
                        line_splitted = line.split(' ')
                        ra_pos_temp = []
                        for elem in line_splitted:
                            try:
                                ra_pos_temp.append(float(elem))
                            except ValueError:
                                pass
                        assert len(ra_pos_temp)==2, 'Wrong number of floats found'
                        ra_pos.append(tuple(ra_pos_temp))
                    elif '--- dec' in line and 'pixels' in line and not fit_failed:
                        line_splitted = line.split(' ')
                        dec_pos_temp = []
                        for elem in line_splitted:
                            try:
                                dec_pos_temp.append(float(elem))
                            except ValueError:
                                pass
                        assert len(dec_pos_temp)==2, 'Wrong number of floats found'
                        dec_pos.append(tuple(dec_pos_temp))

            # We now have three lists of channel nr, ra pos and dec pos
            # Output results to a new dat file.
        print('ra_pos and len', ra_pos, len(ra_pos))
        print('chan_n is', len(chan_nr))

        with open(os.path.join(self.output_dir, 'casa_imfit_res.dat'), 'w') as f:
            f.write("# channel nr, ra pos [pixel], ra pos error [pixel], "
                    "dec pos [pixel], dec pos error [pixel] \n")
            for i in xrange(len(chan_nr)):
                print(ra_pos[i])
                if ra_pos[i] != -1:
                    f.write("{}, {}, {}, {}, {} \n".format(int(chan_nr[i]),
                            ra_pos[i][0], ra_pos[i][1], dec_pos[i][0],
                            dec_pos[i][1]))


class zsh_commands():

    def __init__(self):
        pass

    def TrfMdServToLoc(ModelNr):
        """This function downloads LIME models from server."""
        # Upload models. Must be named pHCO+model1.c, pHCO+model2.c,
        # pHCO+model3.c etc.
        for i in range(ModelNr):
            subprocess.call(['scp', '-p',
                             "cmh154@astro08.hpc.ku.dk:'/astro/cmh154/LimePackage/LIMEmod/'" + 'imHCO+' + str(int(i + 1)) + '.fits', 'imHCO+' + str(int(i + 1)) + '.fits'],
                            shell=False)

        # counter = counter + 1

    def TrfMdToServ(ModelNr):
        """This function uploads LIME models to server."""
        # Upload models. Must be named pHCO+model1.c, pHCO+model2.c,
        # pHCO+model3.c etc.
        for i in range(ModelNr):
            subprocess.call(['scp', '-p', 'pHCO+model' + str(int(i + 1)) + '.c',
                             "cmh154@astro08.hpc.ku.dk:'/astro/cmh154/LimePackage/LIMEmod/'"], shell=False)

        # Upload input data files, to make sure they are always updated.
        subprocess.call(['scp', '-p', 'OpacOutDisk.tab',
                         "cmh154@astro06.hpc.ku.dk:'/astro/cmh154/LimePackage/LIMEmod/'"], shell=False)
        subprocess.call(['scp', '-p', 'DustTCoord.txt',
                         "cmh154@astro06.hpc.ku.dk:'/astro/cmh154/LimePackage/LIMEmod/'"], shell=False)
        subprocess.call(['scp', '-p', 'rnLIMEmod.py',
                         "cmh154@astro06.hpc.ku.dk:'/astro/cmh154/LimePackage/LIMEmod/'"], shell=False)


def lime_setup(dens_file, temp_file, grid_file, mcmono_file, p_stars, evap_rad,
               header_file_name, rot_vel_file, oct_idx_file,
               rad_file_name=None, rad_dump=False, lime_dump=True,
               source_nr=2, binary=False, ca_coords_file_name='',
               radmc3d=True, krome=False, mol='HCN', model_dir='',
               TW_Hya=False, HL_Tau=False):

    """
    This function reads the RADMC-3D output files (dust_temperature.dat,
    amr_grid.inp and dust_density.inp) into a header file as C arrays.
    In this manner, the header file arrays can be imported directly into
    LIME model files.
    Second functionality is the reading and writing of KROME output files
    into a C header file for LIME to read.
    Tertiary functionality is the setup of input to KROME, in form of
    [r [cm] (radial distance to star), rho_dust [g cm^-3], n_H2 [cm^-3],T [K],
    loc. rad. field in a list of frequencies]


    Args:
        dens_file : Name of the input dust density file.
        temp_file : Name of the input dust temperature file.
        grid_file : Name of the input grid file (amr_grid.inp).
        rot_vel_file: Name of the file containing the rotation velocity.
        oct_idx_file : Octree filename, containing the base level octree keys.
        mcmono_file: Name of the mean_intensity.out file from RADMC-3D.
        p_stars: Nested list of positions lists of the stellar sources.
            Cartesian coordinates.
        header_file_name: String. Name and location of output header file.
        rad_file_name: String. Name and location of output krome file.
        rad_dump: Boolean - dump input files to KROME.
        lime_dump: Boolean - dump input files to LIME.
        source_nr: Int- number of stars in the model.
    Returns:
        None. Writes a file, header_model.c (if lime_dump is True).
        Writes a file, rad_dump.inp (if rad_dump is True).
    Notes:
        The distance values (r) are to be used in LIME, therefore RADMC-3D
        cm unit values are converted to unit of meter.

    Speed-up:
        Make separate subarrays for r,theta,phi,T and rho for regions that
        are octree refined. This will speed up the search for the appropriate
        LIME grid values, as these regions could potentially have a lot of
        octree cells.

    """

    r_wall = []
    theta_wall = []
    phi_wall = []
    r_c = []
    theta_c = []
    phi_c = []
    octree_x_list, octree_y_list, octree_z_list = [], [], []

    if not rad_dump:
        rad_field = False
    else:
        rad_field = True
# =============================================================================
#                                READ DATA
# =============================================================================
    if radmc3d:
        vel_NNS = True  # Use a nearest neighbor search for the velocity.
        # Open the dust temperature file to extract the number of cells
        # and dust species.
        # Open the coordinate grid file
        with open(grid_file, 'r') as f:
            rows = f.readlines()
            grd_sty = int(rows[1])
            if grd_sty == 1:
                print('Octree refinement detected..')
            else:
                print('No octree refinement..')
            row = rows[5]
            a, b, c = row.split()
            n_r = int(float(a))
            n_theta = int(float(b))
            n_phi = int(float(c))

            pos_wall = rows[7:]  # Cell wall positions.

            for x in xrange(int(n_r + 1)):
                r_wall.append(float(pos_wall[x]))
            for y in xrange(int(n_theta + 1)):
                theta_wall.append(float(pos_wall[y + int(n_r + 1)]))
            for z in xrange(int(n_phi + 1)):
                # TODO fix for no oct-tree ref.
                phi_wall.append(
                    float(pos_wall[z + int(n_r + 1) + int(n_theta + 1)]))

        # Open the octree indices file.
        if grd_sty == 1:
            with open(oct_idx_file, 'r') as f:
                for line in f.readlines():  # Octree coords
                    oct_x, oct_y, oct_z = line.split(",")

                    octree_x_list.append(float(oct_x))
                    octree_y_list.append(float(oct_y))
                    octree_z_list.append(float(oct_z))

                # len(octree_indices) - np.count_nonzero(octree_indices)
                octree_len = len(octree_x_list)
                n_cells = octree_len

        if grd_sty != 1:
            n_cells = n_r * n_theta * n_phi
            octree_len = 0

        # --------------------------- Temperature -----------------------------
        if binary:
            # Open temp file
            data_t_dum, n_cells, n_specs = scalarfield_reader(nx=n_r,
                       ny=n_theta, nz=n_phi, fname=temp_file, binary=True,
                       octree=bool(grd_sty), octree_len=octree_len)

            # Open dens file
            data_rho_dum, n_cells_dens, n_specs_dens = scalarfield_reader(
                         nx=n_r, ny=n_theta, nz=n_phi, fname=dens_file,
                         binary=True, octree=bool(grd_sty),
                         octree_len=octree_len)

            assert n_cells_dens == n_cells and n_specs_dens == n_specs, ('Mismatch ' +
                   'in dust species nr and cell nr in the temp and dens files')

        elif not binary:
            with open(temp_file, 'r') as f:
                data_temp = f.readlines()
                n_cells = int(data_temp[1])
                n_specs = int(data_temp[2])  # Nr of dust species in the model.

            data_t_dum = np.ndarray([n_cells, n_specs], dtype=np.float64)

            for i in xrange(n_specs):
                data_t_dum[:, i] = data_temp[
                    3 + n_cells * (i):n_cells * (i + 1) + 3]
                data_t_dum[:, i] = [float(x) for x in data_t_dum[:, i]]

            # Prepare the dust density array.
            data_rho_dum = np.ndarray([n_cells, n_specs], dtype=np.float64)

            with open(dens_file, 'r') as f:
                data_dens = f.readlines()
                for i in xrange(n_specs):
                    data_rho_dum[:, i] = data_dens[
                        3 + n_cells * (i):n_cells * (i + 1) + 3]
                    data_rho_dum[:, i] = [float(x) for x in data_rho_dum[:, i]]

        # Open the velocity file in the case of a rotating collapse.
        with open(rot_vel_file, 'r') as f:
            model_vel = f.readlines()
            n_vel_cells = int(model_vel[0])
            model_vel_c = np.ndarray([n_cells, 3], dtype=np.float64)
            for i in xrange(n_cells):
                vel_x_val, vel_y_val, vel_z_val = model_vel[i+1].split(',')
                model_vel_c[i, :] = (float(vel_x_val), float(vel_y_val),
                                     float(vel_z_val))

        # =========================================================================
        #                         DATA MANIPULATION
        # =========================================================================

        # Interpolate between the grid edges, in order to give correct dust
        # cell position.

        r_c = np.zeros(len(r_wall) - 1)
        theta_c = np.zeros(len(theta_wall) - 1)
        phi_c = np.zeros(len(phi_wall) - 1)
        dist_to_stars = np.zeros(n_cells)

        r_c[:] = [(x_1 + x_2) / 2.0 for x_1, x_2 in zip(r_wall[0:-1],
                                                        r_wall[1:])]
        theta_c[:] = [(x_1 + x_2) / 2.0 for x_1, x_2 in zip(theta_wall[0:-1],
                                                            theta_wall[1:])]
        phi_c[:] = [(x_1 + x_2) / 2.0 for x_1,
                    x_2 in zip(phi_wall[0:-1], phi_wall[1:])]

    elif krome:

        # Jon's code;
        # load numpy arrays from file

        # Default is the Krome test files.
        grid_r_inp = np.load(model_dir + 'r.npy', mmap_mode='r')  # radial coordinate, 2-D; units = cm
        grid_z_inp = np.load(model_dir + 'z.npy', mmap_mode='r')  # vertical coordinate, 2-D; units = cm
        nH2_inp = np.load(model_dir + 'ngas.npy', mmap_mode='r')  # total gas number density; units = cm**-3
        tgas_inp = np.load(model_dir + 'Tdust.npy', mmap_mode='r')  # dust temperature; units = K

        # gas phase abundances; units = cm**-3
        nElec_inp = nH2_inp  # TODO replace with real electron array.
        nNH2D_inp = np.load(model_dir + 'nNH3.npy', mmap_mode='r') * 0.01  # from Lars
        nHCN_inp = np.load(model_dir + 'nHCN.npy', mmap_mode='r')
        nH13CN_inp = np.load(model_dir + 'nHCN.npy', mmap_mode='r') / 70.0  # from Lars
        nHC15N_inp = np.load(model_dir + 'nHC15N.npy', mmap_mode='r')  # included in the chemistry runs
        nHCOj_inp = np.load(model_dir + 'nHCOj.npy', mmap_mode='r')

        if TW_Hya or HL_Tau:
            nH2_inp = np.load(model_dir + 'nH2.npy', mmap_mode='r')  # total gas number density; units = cm**-3
            nElec_inp = np.load(model_dir + 'nE.npy', mmap_mode='r')  # electron number density, cm**-3
            nDCN_inp = np.load(model_dir + 'nDCN.npy', mmap_mode='r')
            n15NH2D_inp = np.load(model_dir + 'n15NH2D.npy', mmap_mode='r')  # included in the chemistry runs
            
        # Numpy arrays shapes are [r, z], but the kd_tree for lime requires
        # a flat list (so it can handle local refinement.)
        len_r, len_z = grid_r_inp.shape
        grid_r_twhya = np.load(model_dir + 'r_hya.npy', mmap_mode='r')  # radial coordinate, 2-D; units = cm
        
        inp_len = len_r*len_z
        print('len_r', len_r)
        print('len_z', len_z)
        print('inp_len is', inp_len)
        print('grid_r_twhya shape is', grid_r_twhya.shape)
        print('normal grid_r shape is', grid_r_inp.shape)

        print('nH2_inp', nH2_inp)
        print('nH2_inp[20,:]', nH2_inp[20, :])
        print('nH2_inp[150,:]', nH2_inp[150, :])

        # Check for NaNs

        nH2_inp = copy.deepcopy(nH2_inp)
        nElec_inp = copy.deepcopy(nElec_inp)
        tgas_inp = copy.deepcopy(tgas_inp)
        nNH2D_inp = copy.deepcopy(nNH2D_inp)
        nHCN_inp = copy.deepcopy(nHCN_inp)
        nH13CN_inp = copy.deepcopy(nH13CN_inp)
        nHC15N_inp = copy.deepcopy(nHC15N_inp)
        nHCOj_inp = copy.deepcopy(nHCOj_inp)

        NaN_acceptable_list = [nH2_inp, nElec_inp, tgas_inp, nNH2D_inp,
                               nHCN_inp, nH13CN_inp, nHC15N_inp, nHCOj_inp]

        NaN_unacceptable_list = [grid_r_inp, grid_z_inp]
        NaN_found = False

        for arr in NaN_acceptable_list:
            nx, ny = arr.shape
            for ix in xrange(nx):
                for iy in xrange(ny):
                    if np.isnan(arr[ix, iy]):
                        if ix != nx-1 and ix != 0:
                            arr[ix, iy] = (arr[ix+1, iy] + arr[ix-1, iy])/2.0
                        elif ix == nx-1:
                            arr[ix, iy] = arr[ix-1, iy]
                        elif ix == 0:
                            arr[ix, iy] = arr[ix+1, iy]
                        NaN_found = True

        for arr in NaN_unacceptable_list:
            nx, ny = arr.shape
            for ix in xrange(nx):
                for iy in xrange(ny):
                    assert not np.isnan(arr[ix, iy]), 'Stop program, found NaN in grid coords.'

        grid_r = np.zeros((inp_len))
        grid_z = np.zeros((inp_len))
        data_t_dum = np.zeros((inp_len))
        nH2 = np.zeros((inp_len))
        nElec = np.zeros((inp_len))
        nNH2D = np.zeros((inp_len))
        nHCN = np.zeros((inp_len))
        nDCN_inp = np.zeros((inp_len))
        n15NH2D_inp = np.zeros((inp_len))
        nH13CN = np.zeros((inp_len))
        nHC15N = np.zeros((inp_len))
        nHCOj = np.zeros((inp_len))
        ctr = 0

        # Convert cgs to SI (LIME requirement)
        for i in xrange(len_r):
            for j in xrange(len_z):
                grid_r[ctr] = grid_r_inp[i, j]/100.0  # cm to m
                grid_z[ctr] = grid_z_inp[i, j]/100.0
                nH2[ctr] = nH2_inp[i, j]*10**6  # cm^-3 to m^-3
                nElec[ctr] = nElec_inp[i, j]*10**6  # cm^-3 to m^-3

                data_t_dum[ctr] = tgas_inp[i, j]

                # Convert to abundance wrt. H2
                nNH2D[ctr] = nNH2D_inp[i, j]*10**6/nH2[ctr]
                nHCN[ctr] = nHCN_inp[i, j]*10**6/nH2[ctr]
                nH13CN[ctr] = nH13CN_inp[i, j]*10**6/nH2[ctr]
                nHC15N[ctr] = nHC15N_inp[i, j]*10**6/nH2[ctr]
                nHCOj[ctr] = nHCOj_inp[i, j]*10**6/nH2[ctr]
                ctr += 1

        print('nHCN', nHCN)
        print('global min r is', min(grid_r)/(AU/100.))
        print('global max r is', max(grid_r)/(AU/100.))

        use_electr = False  # Only use electrons in the case of HCN

        if mol == 'NH2D':
            abun = nNH2D
        elif mol == 'HCN':
            abun = nHCN
        elif mol == 'H13CN':
            abun = nH13CN
        elif mol == 'HC15N':
            abun = nHC15N
        elif mol == 'HCO':
            abun = nHCOj

        # First science case is an ALMA proposal for TW Hya, so we'll just
        # use a wee bit of hardcoding.
        n_specs = 1  # One dust species.
        n_cells = len(grid_r)
        vel_NNS = False  # We use a zero velocity model, no data to be loaded.

    if lime_dump:

        # =====================================================================
        #                      WRITE DATA TO HEADER FILE
        # =====================================================================

        r_out = []
        theta_out = []
        phi_out = []
        header_file_name = model_dir + header_file_name
        header_file = open(header_file_name, 'w')

        # Write out header file for LIME.
        size = n_cells

# ============================ DECLARE EXTERNS ================================
        header_file.write("extern const int size;\n")
        if radmc3d:

            header_file.write("extern const n_r;\n")
            header_file.write("extern const double model_temp_c[2][%i];\n" % size)
            header_file.write("extern const double model_dens_c[2][%i];\n" % size)
            header_file.write("extern const double model_vel_c[%i][3];\n" % size)
            header_file.write("extern const double model_abun_c[1][1];\n")

            header_file.write("const n_r = %i;\n" % n_r)

        if krome:
            header_file.write("extern const double model_temp_c[1][%i];\n" % size)
            header_file.write("extern const double model_dens_c[1][%i];\n" % size)
            header_file.write("extern const double model_abun_c[1][%i];\n" % size)
            if use_electr:
                header_file.write("extern const double model_electr_dens_c[1][%i];\n" % size)
                
        # r values, converted from cm to m.
        header_file.write("const int size = %i;\n" % size)

        header_file.write(
            "void disk_coords(double, double, double, double, double, double, double *, double *, double *,double *,  double *, double *);\n")
        header_file.write("void transf_vel(double, double, double, double, double, double, double, double, double *, double *, double *);\n")


# ============================ CREATE STATIC ARRAYS ===========================

        if rad_dump:
            # Star dist values - used in rad setup.
            if grd_sty == 1:
                for i in xrange(n_cells):

                    x_ca, y_ca, z_ca = transf_sph_cart(
                        r_out[i], theta_out[i], phi_out[i])

                    min_dist = 10.**(70.)

                    for j in xrange(source_nr):
                        dist_to_star = np.sqrt((p_stars[j][0] - x_ca)**2.
                                               + (p_stars[j][1] - y_ca)**2. + (p_stars[j][2] - z_ca)**2.)
                        if dist_to_star < min_dist:
                            dist_to_stars[i] = dist_to_star
                            min_dist = dist_to_star
            # Assert that the octree refinement was done correctly.
            assert np.count_nonzero(dist_to_stars) == n_cells, ("There is an issue"
                                                                " with the octree refinement."
                                                                "np.count_nonzero(dist_to_stars): %i while nr of cells = %i" % (np.count_nonzero(dist_to_stars),
                                                                                                                                n_cells))

#============================== PHYSICAL VALUES ===============================

        # T values. One for each dust species.
        header_file.write(
            "const double model_temp_c[%i][%i] = {\n" % (n_specs, n_cells))
        for j in xrange(n_specs):
            header_file.write("{")
            if radmc3d:
                for i in xrange(n_cells):
                    if i != n_cells - 1:
                        header_file.write("%.10e," % (data_t_dum[i, j]))
                    if i == n_cells - 1:
                        header_file.write("%.10e" % (data_t_dum[i, j]))
                if j != n_specs - 1:
                    header_file.write("},\n")
                if j == n_specs - 1:
                    header_file.write("}\n")
            elif krome:
                for i in xrange(n_cells):
                    if i != n_cells - 1:
                        header_file.write("%.10e," % data_t_dum[i])
                    if i == n_cells - 1:
                        header_file.write("%.10e" % data_t_dum[i])
                header_file.write("}\n")
        header_file.write("};\n")
        print('Done with assigning temperature')

        # Density values. One for each dust species.
        header_file.write(
            "const double model_dens_c[%i][%i] = {\n" % (n_specs, n_cells))
        for j in xrange(n_specs):
            header_file.write("{")
            for i in xrange(n_cells):
                # Write to file and include conversion from cgs to SI units.
                if i != n_cells - 1:
                    if radmc3d:
                        # g/cm^3 *0.001 kg/g * 10^6 cm^3/m^3 --> Multiply by a
                        # factor 1000 to get kg/m^3
                        header_file.write("%.10e," %
                                          (data_rho_dum[i, j] * 10.**3.))
                    elif krome:
                        header_file.write("%.10e," % nH2[i])
                if i == n_cells - 1:
                    if radmc3d:
                        # g/cm^3 *0.001 kg/g * 10^6 cm^3/m^3 --> Multiply by a
                        # factor 1000 to get kg/m^3
                        header_file.write("%.10e" %
                                          (data_rho_dum[i, j] * 10.**3.))
                    elif krome:
                        header_file.write("%.10e" % nH2[i])

            if j != n_specs - 1:
                header_file.write("},\n")
            if j == n_specs - 1:
                header_file.write("}\n")
        header_file.write("};\n")
        print('Done with assigning density')

        if krome:
            # Abundance values.

            header_file.write("const double model_abun_c[1][%i] = {\n" % n_cells)
            header_file.write("{")
            for i in xrange(n_cells):
                if i != n_cells - 1:
                    header_file.write("%.10e," % (abun[i]))
                if i == n_cells - 1:
                    header_file.write("%.10e" % (abun[i]))
            header_file.write("}\n")
            header_file.write("};\n")

            if use_electr:
                header_file.write("const double model_electr_dens_c[1][%i] = {\n" % n_cells)
                header_file.write("{")
                for i in xrange(n_cells):
                    if i != n_cells - 1:
                        header_file.write("%.10e," % (nElec[i]))
                    if i == n_cells - 1:
                        header_file.write("%.10e" % (nElec[i]))
                header_file.write("}\n")
                header_file.write("};\n")
        else:
            header_file.write("const double model_abun_c[1][1] = {\n")
            header_file.write("{-99}")
            header_file.write("};\n")

        # Rot. coll. vel. values
        if vel_NNS:
            header_file.write("const double model_vel_c[%i][3] = {\n" % (n_cells))

            for i in xrange(n_cells):
                # Write to file and include conversion from cgs to SI units.
                if i != n_cells - 1:
                    header_file.write("{%.20e, %.20e, %.20e}," % (
                                      model_vel_c[i, 0],
                                      model_vel_c[i, 1], model_vel_c[i, 2]))
                if i == n_cells - 1:
                    header_file.write("{%.20e, %.20e, %.20e} \n" % (
                                      model_vel_c[i, 0], model_vel_c[i, 1],
                                      model_vel_c[i, 2]))
            header_file.write("\n };\n")
            print('Done with assigning rotating collapse values')
        else:
            # Need dummy array, otherwise my LIME velocity() routine will
            # complain.
            header_file.write("const double model_vel_c[%i][3] = {\n" % (n_cells))

            for i in xrange(n_cells):
                # Write to file and include conversion from cgs to SI units.
                if i != n_cells - 1:
                    header_file.write("{%.20e, %.20e, %.20e}," % (
                                      -99, -99, -99))
                if i == n_cells - 1:
                    header_file.write("{%.20e, %.20e, %.20e} \n" % (
                                      -99, -99, -99))

            header_file.write("\n };\n")
            print('Done with assigning model velocity')

    # =========================================================================
    #                 DUMP cartesian coordinates file for k-d tree
    # =========================================================================
        ca_coords_file = open(ca_coords_file_name, 'w')
        ca_coords_file.write("#ifndef kd_coord_ca_H \n")
        ca_coords_file.write("#define kd_coord_ca_H \n")
        if krome:
            kd_tree_dim = 3  # 2 dimensions + count number
        else:
            kd_tree_dim = 4  # 3 dimensions + count number

        ca_coords_file.write("extern int kd_tree_dim;\n")
        ca_coords_file.write("int kd_tree_dim = %i ;\n" % kd_tree_dim)

        ca_coords_file.write("struct kd_node_t kd_coord_ca[]= {\n")

        if krome:
            for i in xrange(size):

                if i != size - 1:
                    ca_coords_file.write("{{%.10e, %.10e, 0.0, %i}}," %
                                         (grid_r[i], grid_z[i], i))

                elif i == size - 1:
                    ca_coords_file.write("{{%.10e, %.10e, 0.0, %i}}" %
                                         (grid_r[i], grid_z[i], i))
        elif radmc3d:
            for i in xrange(size):

                x_c, y_c, z_c = transf_sph_cart(octree_x_list[i] / 100.0,
                                                octree_y_list[i],
                                                octree_z_list[i])

                if i != size - 1: # the following two conds. were x != size - 1: before?
                    ca_coords_file.write("{{%.10e, %.10e, %.10e, %i}}," %
                                         (x_c, y_c, z_c, i))

                elif i == size - 1:
                    ca_coords_file.write("{{%.10e, %.10e, %.10e, %i}}" %
                                         (x_c, y_c, z_c, i))
        ca_coords_file.write("};\n")
        ca_coords_file.write("#endif")

        ca_coords_file.close

    # =========================================================================
    #                               rad DUMP
    # =========================================================================

    if rad_field:

        # In the case of a spherically symmetric model.
        r_samples = np.arange(160)
        r_samples[:] = [abs(x * p_stars[0][2] / 8.) for x in r_samples]
        r_samples = r_samples[8:]

        # Read the mean radiation intensity of the cells.
        with open(mcmono_file, 'r') as f:
            data_dum = f.readlines()
            for i in xrange(n_specs):
                n_freq = int(data_dum[2])
                freq = np.zeros(n_freq)
                loc_rad_field = np.zeros([n_cells, n_freq], dtype=np.float64)

                data_freq_dum = data_dum[3]
                freq[:] = data_freq_dum.split()
                freq[:] = [float(x) for x in freq]
                for j in xrange(n_freq):
                    loc_rad_field[:, j] = data_dum[
                        4 + n_cells * (j):n_cells * (j + 1) + 4]
                    loc_rad_field[:, j] = [float(x)
                                           for x in loc_rad_field[:, j]]

        rad_file = open(rad_file_name, 'w')
        # All meridional and azimuthal values are pi/2
        rad_file.write(
            "!All local radiation fields (split into frequency values) are erg s^-1 cm^-2 Hz^-1 ster^-1.\n")
        rad_file.write("!The frequency points are listed below in GHz.\n")
        for j in xrange(int(n_freq / 2.0)):
            if j != int(n_freq / 2.0) - 1:
                rad_file.write("%.1e," % (freq[j] / 10.**9.))
            if j == int(n_freq / 2.0) - 1:
                rad_file.write("%.1e \n" % (freq[j] / 10.**9.))
        rad_file.write(
            "**************************************************************************\n")

        rad_file.write(
            "!r [cm] (radial distance to star), rho_dust [g cm^-3], n_H2 [cm^-3],T [K], loc. rad. field \n")
        # I assume that the wavelength grid is split up evenly between the UV-Visual region interesting for
        # rad and the IR/MM interesting for radmc3d, thus I only print the UV_visual region for the
        # rad file.

        # print(r_samples[:])
        i_last = 0
        print('**************************************************************')
        H2_conv = 100.0 / M_H2
        idx1 = int(10 + (n_theta / 2. * 0.0) * n_r +
                   (n_phi / 4. * 0.0) * n_r * n_theta)

        print("nr: %i, ntheta: %i, nphi: %i" % (n_r, n_theta, n_phi))
        print("r_out: %.5f,theta_out: %.5f,phi_out: %.5f" % (r_out[idx1] / AU,
                                                             theta_out[idx1],
                                                             phi_out[idx1]))

        for x in xrange(len(r_samples)):
            # OBS: Remember to correct dust density closests to star.
            count = 0
            for i in xrange(n_r):
                idx = i

                if r_out[idx] >= r_samples[
                        x] and count == 0 and int(idx) > i_last:

                    print("index: %i,T: %.3f, r:%.3f, Theta: %.3f,Phi: %.3f,"
                          "st_dist: %.3f" %
                          (idx, data_t_dum[idx, 1], r_out[idx] / AU,
                           theta_out[idx], phi_out[idx],
                           dist_to_stars[idx] / AU))
                    i_last = idx
                    if dist_to_stars[idx] > 66. * AU:
                        rad_file.write("%.4e, %.4e, %.4e, %.4e, " % (dist_to_stars[idx],
                                                                     data_rho_dum[idx, 0], data_rho_dum[
                                                                     idx, 0] * H2_conv,
                                                                     data_t_dum[idx, 0]))
                        for j in xrange(int(n_freq / 2.0)):
                            if j != int(n_freq / 2.0) - 1:
                                rad_file.write("%.4e," %
                                                 loc_rad_field[idx, j])
                            if j == int(n_freq / 2.0) - 1:
                                rad_file.write("%.4e \n" %
                                                 loc_rad_field[idx, j])

                    elif dist_to_stars[idx] <= 66. * AU:
                        rad_file.write("%.4e, %.4e, %.4e, %.4e, " % (dist_to_stars[idx],
                                                                       data_rho_dum[idx, 1], data_rho_dum[
                                                                           idx, 1] * H2_conv,
                                                                       data_t_dum[idx, 1]))
                        for j in xrange(int(n_freq / 2.0)):
                            if j != int(n_freq / 2.0) - 1:
                                rad_file.write("%.4e," %
                                                 loc_rad_field[idx, j])
                            if j == int(n_freq / 2.0) - 1:
                                rad_file.write("%.4e \n" %
                                                 loc_rad_field[idx, j])
                    count += 1
        rad_file.close

    if krome:
        if NaN_found:
            print('ATTENTION! FOUND NANS IN KROME INPUT FILES!')


def arcsec_to_AU(arcsec, dist, cgs=False):
    """
    This function converts arcsec values to AU using parallax.
    Args:
        arcsec: Input distance in arcsec.
        dist: Distance in units of parsec
        cgs: Boolean, use cgs units or not. If false, SI units are used instead.
    Returns:
        Distance value in AU.
    """
    dist_AU = arcsec * dist

    return dist_AU


def AU_to_arcsec(len_AU, dist, cgs=False):
    """
    This function converts arcsec values to AU using parallax.
    Args:
        len_AU: Input length in AU.
        dist: Distance in units of parsec
        cgs: Boolean, use cgs units or not. If false, SI units are used instead.
    Returns:
        Distance value in arcsec.
    """
    len_arcsec = len_AU / dist

    return len_arcsec


def freq_to_vel(rest_freq, obs_freq_0, obs_freq_1=0.0, find_vel_range=False):
    """This function converts frequency into a radial velocity. If
    find_vel_range is set, then the frequency interval (obs_freq_1-obs_freq_0)
    is converted into an velocity interval. """

    if find_vel_range:
        v_range = abs(C_L * (1.0 - obs_freq_0 / rest_freq) -
                      C_L * (1.0 - obs_freq_1 / rest_freq))
        return v_range
    if not find_vel_range:
        print('obs_freq_0 is {}'.format(obs_freq_0))
        print('rest_freq is {}'.format(rest_freq))
        v_radio = C_L * (1.0 - obs_freq_0 / rest_freq)
        return v_radio


def vel_to_freq(v_radio, rest_freq):
    """
    This function converts radial velocity into observed frequency.
    """

    obs_freq = rest_freq * (1.0 - v_radio / C_L)

    return obs_freq


def mk_radmc3d_cont_im(freq, n_pix, rad, out_name, radmc3d_dir, out_dir,
                       dparc, incl=180, phi=270, threads_nr=8,
                       trash_dir='/Users/Steffen_KJ/work_trash'):
    """
    This function creates a continuum model to be used on a LIME model, whose
    parameters are supplied.
    Args:
        freq: Central frequency of the LIME model cube.
        n_pix: Pixel number in x and y direction, i.e. the image is assumed
            to be a square.
        rad: Model radius [AU]. Important! Supply in units of AU.
        out_name: output filename.
        radmc3d_dir: Name of the radmc3d directory [string].
        out_dir: output directory
        dparc: distance in parsec
    returns:
        None. Writes a continuum image fits file into the target directory.
    """
    os.chdir(radmc3d_dir)
    os.system('rm image.fits')
    im_wav = freq_wav_conv(data=freq, freq=True, micron=True)

    os.system('radmc3d image lambda %.4f npix %i sizeau %i incl %i phi %i '
              'setthreads %i' % (im_wav, n_pix, 2 * rad, incl, phi, threads_nr))

#    os.system('radmc3d image lambda %i npix %i sizeau %i incl %i' % (im_wav,n_pix,
#              rad*2,incl, phi))

    out_to_fits('image.out', dparc, 'image', trash_dir=trash_dir)
    os.system('mv image.fits %s/%s' % (out_dir, out_name))


def wrt_lime_model(cont, **all_args):
    """
    Function for writing an executable LIME script.
    Args:
        cont: Boolean, indicating if a continuum lime image should be made.
        mol: Dictionary containing the molecule name to be investigated.
        vel: Dictionary of velocity arguments.
        n_pxls: Number of pixels in each direction.
        server: To be executed on the server with all relevant files in the
        same folder.
    Returns:
        A LIME script file in the target model folder.

    """

    # =========================  LINE REST FREQUENCIES  =======================
    # IMPORTANT SPECIES (all values are in Hz)
    #
    # H2CO (formaldehyde):
    # 362.736048e9
    # 365.363428e9
    # 12C18O
    #219.5603541 (3-2)
    # Three CO isotopologues will be used.13CO 3-2, C18O 3-2 og C17O 3-2 from
    # the PILS survey.
    # Abundance [H2/13CO] abundance and threshold extinction of 3.8 × 10^5 (Note: from what source?)
    # Abundance of [12C18O/12C17O] 4.11 ± 0.14, from http://www.aanda.org.ep.fjernadgang.kb.dk/articles/aa/pdf/2005/05/aa0437-04.pdf
    # From this isotope ratio, an [H2/C17O] ratio of circa 2.5*10^{-8} is derived.
    # Abundance [C18O/H2/] of circa 10^{-7} from http://cab.inta-csic.es/users/jrpardo/paper38.pdf Double check this
    # Abundance [13C16O/H2] of circa 2*10^{-6} from
    # http://arxiv.org/pdf/1302.2679v1.pdf Double check this

    # From Wilson & Rood 1994 (Local ISM):
    # 12C/13C --> 77 +/- 7
    # 16O/18O --> 560 +/- 25
    # 18O/17O --> 3.2 +/- 0.2
    # From Wilson & Rood 1994 (Orion A cloud):
    # C/H --> 3.4 to 2.1 * 10^-4
    # O/H --> 4.0 to 3.8 * 10^-4

    # From these abundances, assuming that the formation mechanisms of C17O,
    # C18O and 13CO have the same efficiency in the ISM, I can derive expected
    # CO isotopologue abundances.
    #
    # CO/H2 --> (((2.75+3.9)/2)*10^-4)*2 = 3.325 *10^-4 )*2 = 6.65*10^-4
    # 13CO/H2 --> 6.65*10^-4 * 1/77 = 8.64*10^-6
    # C18O/H2 --> 6.65*10^-4 * 1/560 = 1.19*10^-6
    # C17O/H2 --> 1.19*10^-6 * 1/3.2 = 3.71*10^-7

    # if CO/H2 = 10^-4
    # 13CO/H2 --> 10^-4 * 1/77 = 1.30e-06
    # C18O/H2 --> 10^-4 * 1/560 = 1.79e-07
    # C17O/H2 --> 1.79e-07 * 1/3.2 = 5.58e-08

    # =========================================================================

    if not all_args['server']:
        lm_file = all_args['loc_dir'] + '%s_pp.c' % all_args['molecule']
        if cont:
            lm_file = all_args['loc_dir'] + '%s_cont.c' % all_args['molecule']
    elif all_args['server']:
        lm_file = all_args['loc_dir'] + '%s_pp.c' % all_args['molecule']
        if cont:
            lm_file = all_args['loc_dir'] + '%s_cont.c' % all_args['molecule']

    with open(lm_file, 'w') as f:
        f.write("""

    /*
    *  model.c
    *  LIME, The versatile 3D line modeling tool

    *  Created by Christian Brinch on 11/05/07.
    *  Copyright 2006-2013, Christian Brinch,
    *  <brinch@nbi.dk>
    *  Niels Bohr institutet
    *  University of Copenhagen
    *	All rights reserved.
    *
    */

    /*
    * Include headers.
    */

    #include "lime.h"
    #include <stdlib.h>
    #include <stdio.h>
    #include "model_header.h"

    /****************************************************************************************/

    void
    input(inputPars *par, image *img){
    /*
     * Basic parameters. See cheat sheet for details.
     */

    par->radius             = %f;""" % all_args['radius'] + """
    par->minScale	   		  = %f;""" % all_args['minScale'] + """
    par->pIntensity    	  = %i;""" % all_args['pIntensity'] + """
    par->sinkPoints   	  = %i;""" % all_args['sinkPoints'] + """
    par->dust               = "%s";""" % all_args['dust'] + """
    par->moldatfile[0] 	  = "%s";""" % all_args['moldatfile'] + """
    par->antialias          = %i;""" % all_args['antialias'] + """
    par->sampling			  = %i;""" % all_args['sampling'] + """
    par->outputfile 		  = "%s";""" % all_args['outputfile'] + """
    par->binoutputfile 	  = "%s";""" % all_args['binoutputfile'] + """
    par->gridfile			  = "%s";""" % all_args['gridfile'] + """
    par->lte_only           =  %i;""" % all_args['lte_only'] + """
    par->blend              =  %i;""" % all_args['blend'] + """

    /*
     * Custom parameters for the grid.c routine. Added to accommodate very
     * small density-gradient scales.
     */
    par->x_star_1         =  %f;""" % all_args['x_star_1'] + """
    par->y_star_1         =  %f;""" % all_args['y_star_1'] + """
    par->x_star_2         =  %f;""" % all_args['x_star_2'] + """
    par->y_star_2         =  %f;""" % all_args['y_star_2'] + """
    par->rad_disk_1         =  %f;""" % all_args['rad_disk_1'] + """
    par->rad_disk_2         =  %f;""" % all_args['rad_disk_2'] + """
    par-> nodes_dom_1       = %i;""" % all_args['nodes_dom_1'] + """
    par-> nodes_dom_2       = %i;""" % all_args['nodes_dom_2'] + """
    par-> tor_dens          = %.6f;""" % all_args['tor_dens_repres'] + """

    /*
     * Image setup. Add blocks for additional images.
     */

    /*
     * Formaldehyd setup.
     */

    """)

        if cont:
            f.write("""
    img[0].freq             = %f; //Rest frequency [Hz].""" % all_args['rest_freq'])
        if not cont:
            f.write("""
    img[0].freq             = %f; //Formaldehyd    //Rest frequency of ALMA observation [Hz].""" % all_args['rest_freq'] + """
    img[0].bandwidth        = %f; //Bandwidth in [Hz]. """ % all_args['band_width'] + """
    img[0].nchan            = %i; // Number of channels. """ % all_args['n_channel'] + """
            """)
        f.write("""
     /*
     * Definitions for image #0. Add blocks for additional images.
     */
    img[0].pxls        = %i; // Pixels per dimension. """ % all_args['n_pxls'] + """
    img[0].imgres	     = %f;  // Resolution in arc seconds.""" % all_args['imgres'] + """
    img[0].theta	    	= %f; // 0: face-on, pi/2: edge-on """ % all_args['theta'] + """
    img[0].phi	    	= %f; """ % all_args['phi'] + """
    img[0].distance         = %f; // source distance in m """ % all_args['distance'] + """
    img[0].source_vel       = %f; // systemic source velocity in m/s """ % all_args['source_vel'] + """
    img[0].unit             = %i; // 0:Kelvin 1:Jansky/pixel 2:SI 3:Lsun/pixel 4:tau  """ % all_args['unit'] + """ """)
        if not cont:
            f.write("""\n img[0].filename         = "%s"; // Output filename  """ %
                    all_args['file_name'] + """ \n""")
        elif cont:
            f.write("""\n img[0].filename         = "%s"; // Output filename  """ %
                    all_args['cont_file'] + """ \n""")

        f.write("""}

    /******************************************************************************/

    void
    density(double x, double y, double z, double *density, int kd_count){

    double x_m,y_m,z_m;
    double m_H2, dens_dum;
    int n_col_partn = %i; """ % all_args['n_col_partn'] + """
    int simple_disk = %i; """ % all_args['simple_disk'] + """
    double dist_to_star_1, dist_to_star_2;
    int krome_input;

    m_H2 = 2.0*1.660538921e-27; // Mass of the main collision partner, H_2 [kg]
    krome_input = %i; """ % all_args['krome_input'] + """

    /*
    This is the number (!) density of the collision partner, H_2. Note that for pure
    line emission results, you should multiply your dust density profile with the
    gas-to-dust ratio (xi) everywhere to get the actual H_2 density.
    If you include continuum emission, the dust is assumed to be sigma_g/xi.
    Now, compare the LIME node with the model input and find the closests cell.
    Find nearest grid point.
    */
    if (simple_disk == 1){
        disk_dens(x, y, z, &dens_dum);
        //dens_dum = 1e15;
        //printf("dens_dum is %%.1e \\n", dens_dum);
        }
    else{
        if (krome_input == 1){
            dens_dum = model_dens_c[0][kd_count];
        }
        else{
            //Find density value. If the density for the first dust species is
            //set to the default low value (e-50), use the second dust species.

            if (model_dens_c[0][kd_count]>= 1.0e-45){
                dens_dum = model_dens_c[0][kd_count]*100./m_H2;
                }
            else if (model_dens_c[0][kd_count] < 1.0e-45){
                dens_dum = model_dens_c[1][kd_count]*100./m_H2;
                };
            // Avoid extremely low H2 number density values.
            if (dens_dum < 1.0e-15){
                dens_dum == 1.0e-15;
                };
            };
        };
    // Take into account multiple collision partners.
    if (n_col_partn == 1){
             density[0] = dens_dum;
        }
        else if(n_col_partn == 2){
            density[0] = dens_dum/2.;
            density[1] = dens_dum/2.;
        }

        else{
             printf("# of collision partners is not acceptable.");
        }
    };
    /*
    /*****************************************************************************
    */

    void
    gasIIdust(double x, double y, double z, double *gtd){
    // Experiment to handle scenario when the molecular hydrogen is split into
    // two collision partners - this might half the inferred dust density, affecting
    // the continuum emission calculation.
    int n_col_partn = %i; """ % all_args['n_col_partn'] + """
    if (n_col_partn == 1){
        *gtd = 100;
    }
    else if (n_col_partn == 2){
        *gtd = 50;
    }
    };

    /*
    /**************************************************************************
    */

    void
    temperature(double x, double y, double z, double *temperature, int kd_count){

        double temp_dum;
        int simple_disk = %i; """ % all_args['simple_disk'] + """
        int krome_input = %i; """ % all_args['krome_input'] + """

        if (simple_disk == 1){
            disk_temp(x, y, z, &temp_dum);
            temperature[0] = temp_dum;
            //printf("temp_dum is %%.20f \\n", temp_dum);
        }
        else{
            if (krome_input == 1){
                temperature[0] = model_temp_c[0][kd_count];
            }
            else{
                if (model_dens_c[0][kd_count]>= 1.0e-45){
                    temperature[0] = model_temp_c[0][kd_count];
                }
                else if (model_dens_c[0][kd_count] < 1.0e-45){
                    temperature[0] = model_temp_c[1][kd_count];
                }
            };
        };
        if (temperature[0] < 5.0){
            temperature[0] = 5.0;
        };
    };

    /*************************************************************************/

    void
    abundance(double x, double y, double z, double *abundance, int kd_count){
        /*
         * Here we use a constant abundance. Could be a
         * function of (x,y,z).
         */

        //Variables.
        //If relevant use a CH2O ortho:para of 1.6:1
        int krome_input = %i; """ % all_args['krome_input'] + """
        int simple_disk = %i; """ % all_args['simple_disk'] + """
        double x_m, y_m, z_m;
        double min_dist, dist;
        double T_evap, T;

        if (simple_disk == 1){
            abundance[0] = %e; """ % all_args['abun_high'] + """
        }
        else if (krome_input == 1){
            abundance[0] = model_abun_c[0][kd_count];
        }

        else{
            T_evap = %f;""" % all_args['T_evap'] + """

            if (model_dens_c[0][kd_count]>= 1.0e-45){
                T = model_temp_c[0][kd_count];
            }
            else if (model_dens_c[0][kd_count] < 1.0e-45){
                T = model_temp_c[1][kd_count];
            }

            if (T > T_evap){
              	 abundance[0] = %e; """ % all_args['abun_high'] + """
            }
            else if(T <= T_evap){
                abundance[0] = %e; """ % all_args['abun_low'] + """
            }
        }
    };

    /*************************************************************************/

    void
    doppler(double x, double y, double z, double *doppler){
    /*
     * The doppler b-parameter. This
     * can be a function of (x,y,z) as well.
     * Note that *doppler is a pointer, not an array.
     * Remember the * in front of doppler.
     */
      *doppler = %.2f """ % all_args['doppler'] + """;
    }

    /*************************************************************************/

    void
    velocity(double x, double y, double z, double *vel, int kd_count){
      /*
       * Variables for spherical coordinates
       */

      double R, r, phit, theta, m_stars;
      int free_fall, rot_col, disk_rot_coll, quiescent;
      double r_st, theta_st, phi_st;
      double vel_x_fin, vel_y_fin, vel_z_fin;
      double m_star_1 = %.20f; """ % all_args['m_star_1'] + """
      double m_star_2 = %.20f; """ % all_args['m_star_2'] + """
      double x_1, y_1, z_1, x_2, y_2, z_2;
      double d_st_1, d_st_2;
      double st_1_coords[3] = {%.20f, %.20f, 0.0}; """ % (all_args['x_star_1'], all_args['y_star_1']) + """
      double st_2_coords[3] = {%.20f, %.20f, 0.0}; """ % (all_args['x_star_2'], all_args['y_star_2']) + """
      double azi_turn_1, azi_turn_2;
      double incl_1, incl_2;
      double vel_x_rot, vel_y_rot, vel_z_rot;
      double vel_x, vel_y, vel_z;
      int simple_disk = %i; """ % all_args['simple_disk'] + """
      int krome_input = %i; """ % all_args['krome_input'] + """
      free_fall = %i; """ % all_args['ins_out'] + """
      rot_col = %i; """ % all_args['rot_col'] + """
      disk_rot_coll = %i; """ % all_args['disk_rot_coll'] + """
      quiescent = %i; """ % all_args['quiescent'] + """

      /*
       * Transform Cartesian coordinates into spherical coordinates
       */

      if (simple_disk == 1){
          disk_vels(x, y, m_star_1, &vel_x, &vel_y, &vel_z);
          vel[0] = vel_x;
          vel[1] = vel_y;
          vel[2] = vel_z;
          //printf("vel_x is %%.20f \\n", vel_x);
      }
      else if (krome_input == 1 || quiescent == 1){
          vel[0] = 0.0;
          vel[1] = 0.0;
          vel[2] = 0.0;
      }
      else if (krome_input == 0 || quiescent == 0 || simple_disk == 0){
      //else {
          R=sqrt(x*x+y*y+z*z);
          theta=acos(z/R);
          phit=atan2(y,x);

          //Add two pi if negative value, to ensure [0,2*pi] range.
          if (phit<0){
            phit = phit+2.*M_PI;
          }

          m_stars = %f; """ % all_args['m_stars'] + """
          //Choose velocity profile.

          /*
           * Free-fall velocity in the radial direction onto a central
           * mass of 2.0 solar mass. Zeroth order approximation, I need to correct for the position
             of the two stars.
          */

          /*Find distance to the two stars */

          /*Get distance to the stars */

          x_1 = (x - st_1_coords[0]);
          y_1 = (y - st_1_coords[1]);
          z_1 = (z - st_1_coords[2]);
 
          x_2 = (x - st_2_coords[0]);
          y_2 = (y - st_2_coords[1]);
          z_2 = (z - st_2_coords[2]);
 
          d_st_1 = sqrt(pow(x_1, 2.) + pow(y_1, 2.) + pow(z_1, 2.));
          d_st_2 = sqrt(pow(x_2, 2.) + pow(y_2, 2.) + pow(z_2 ,2.));
 
          azi_turn_1 = %.20f; """ % all_args['disk_1_azi_t'] + """
          incl_1 = %.20f; """ % all_args['disk_1_incl'] + """
 
          azi_turn_2 = %.20f; """ % all_args['disk_2_azi_t'] + """
          incl_2 = %.20f; """ % all_args['disk_2_incl'] + """
 
          if (d_st_1 <= %.20f && rot_col==0){ """ % all_args['disk_1_r_out'] + """
 
              if(disk_rot_coll==0){            
                  disk_coords(x_1, y_1, z_1, azi_turn_1, incl_1, m_star_1, &r_st,
                              &theta_st, &phi_st, &vel_x_fin, &vel_y_fin,
                              &vel_z_fin);
 
                  vel[0] = vel_x_fin;
                  vel[1] = vel_y_fin;
                  vel[2] = vel_z_fin;
              } else if(disk_rot_coll==1){
                  vel[0] = model_vel_c[kd_count][0];
                  vel[1] = model_vel_c[kd_count][1];
                  vel[2] = model_vel_c[kd_count][2];

              };

          } else if (d_st_2 <= %.20f && rot_col==0){ """ % all_args['disk_2_r_out'] + """

              if(disk_rot_coll==0){            
                  disk_coords(x_2, y_2, z_2, azi_turn_2, incl_2, m_star_2, &r_st, 
                              &theta_st, &phi_st, &vel_x_fin, &vel_y_fin, 
                              &vel_z_fin);
 
                  /*Keplerian rotation in x,y,z dimensions. Check this further [24/09/14]*/
                  vel[0] = vel_x_fin;
                  vel[1] = vel_y_fin;
                  vel[2] = vel_z_fin;
               
              } else if(disk_rot_coll==1){
                  vel[0] = model_vel_c[kd_count][0];
                  vel[1] = model_vel_c[kd_count][1];
                  vel[2] = model_vel_c[kd_count][2];
              };
           } else if(free_fall==1){
            if (R>0.){
 
              r=-sqrt(2.0*6.67e-11*m_stars/R);
              vel[0]=x*r/R;
              vel[1]=y*r/R;
              vel[2]=z*r/R;
            } else {
              vel[0]=0.0;
              vel[1]=0.0;
              vel[2]=0.0;
            };          
          } else if(rot_col==1){
 
              vel[0]= model_vel_c[kd_count][0];
              vel[1]= model_vel_c[kd_count][1];
              vel[2]= model_vel_c[kd_count][2];
          };
    };

    };

/*****************************************************************************/

    void disk_coords(double x, double y, double z, double azith_turn,
    double incl, double m_star_1, double *st_r, double *st_theta, double *st_phi,
    double *vel_x_fin, double *vel_y_fin, double *vel_z_fin){
    /*
    This function creates new coordinates for the disk position

    The input x,y,z must be in the star rest coordinate system.
    */
    double phi_native, phi_1;
    double theta_transf;
    double r_disk;
    double x_c_zrot, y_c_zrot, y_c_xrot, z_c_xrot;
    double v_c_st, vel_x, vel_y,vel_z;
    double vel_y_corr, vel_x_fin_dum;
    double vel_y_xback, vel_z_xback;

    phi_native = atan2(y,x);

    if (phi_native < 0.0){
        phi_native = 2.*M_PI+phi_native;
    } else if (phi_native >= 0.0){
        phi_native = phi_native;
        phi_1 = phi_native;
    }

    //Transform coordinate system by rotating around axes.
    //First z-axis rotation (i.e. turn in azimuth).
    x_c_zrot = x*cos(azith_turn) + y*sin(azith_turn);
    y_c_zrot = -x*sin(azith_turn) + y*cos(azith_turn);

    phi_native = atan2(y_c_zrot,x_c_zrot);
    if (phi_native < 0.0){
        phi_native = 2.*M_PI+phi_native;
    } else if (phi_native >= 0.0){
        phi_native = phi_native;
    }

    y_c_xrot = y_c_zrot*cos(incl) + z*sin(incl);
    z_c_xrot = -y_c_zrot*sin(incl) + z*cos(incl);

    r_disk = sqrt(pow(x_c_zrot,2.)+pow(y_c_xrot,2.)+pow(z_c_xrot,2.));
    theta_transf = acos(z_c_xrot/r_disk);
    phi_native = atan2(y_c_xrot,x_c_zrot);

    if (phi_native < 0.0){
        phi_native = 2.*M_PI+phi_native;
    } else if (phi_native >= 0.0){
        phi_native = phi_native;
    }

    //Equations from http://www.chem.ox.ac.uk/teaching/Physics20for20CHemists/Rotation/Circular.html

    v_c_st = sqrt(6.67e-11*(m_star_1)/r_disk);
    vel_z = 0.0;

    //Correct velocity vector direction
    vel_x = -v_c_st*sin(phi_native);
    vel_y = v_c_st*cos(phi_native);

    //First correct y and z vel component back from the tilted plane (revers x-rotation).
    vel_y_xback = vel_y*cos(2.*M_PI-incl) + vel_z*sin(2.*M_PI-incl);
    vel_z_xback = -vel_y*sin(2.*M_PI-incl) + vel_z*cos(2.*M_PI-incl);

    //Then use the de-tilted y vel-component to find the vel x-component
    *vel_x_fin = vel_x*cos(2.*M_PI-azith_turn) + vel_y_xback*sin(2.*M_PI-azith_turn);
    *vel_y_fin = -vel_x*sin(2.*M_PI-azith_turn) + vel_y_xback*cos(2.*M_PI-azith_turn);
    *vel_z_fin = vel_z_xback;

    *st_r = r_disk;
    *st_theta = theta_transf;
    *st_phi = phi_native;

    };

    /*****************************************************************************/

    void
    ins_rot_coll(double x, double y, double z, int *inside){
    /*
     * This function calculates the distance to the two stars.
     */
     int krome_input = %i; """ % all_args['krome_input'] + """
     if (krome_input == 1){
         *inside = 0;
     }
     else{
      
         double st_1_coords[3] = {%.20f, %.20f, 0.0}; """ % (all_args['x_star_1'], all_args['y_star_1']) + """
         double st_2_coords[3] = {%.20f, %.20f, 0.0}; """ % (all_args['x_star_2'], all_args['y_star_2']) + """
         double d_st_1, d_st_2;
         /*Find distance to the two stars */
    
         d_st_1 = sqrt(pow(x - st_1_coords[0], 2.) + pow(y - st_1_coords[1], 2.) + pow(z - st_1_coords[2], 2.));
         d_st_2 = sqrt(pow(x - st_2_coords[0], 2.) + pow(y - st_2_coords[1], 2.) + pow(z - st_2_coords[2], 2.));
    
         if (d_st_1 < %.20f){ """ % all_args['disk_1_r_out'] + """
             *inside = 1;
         } else if (d_st_2 < %.20f){ """ % all_args['disk_2_r_out'] + """
             *inside = 1;
         } else {
             *inside = 0;
         }
    }
    };         

    /*****************************************************************************/

    void
    transf_vel(double x, double y, double z, double vel_x, double vel_y, 
               double vel_z, double incl, double azith_turn, 
               double *vel_x_rot, double *vel_y_rot, double *vel_z_rot){
    /*
     * This function first translates the coordinates of the local star
     * reference frame to spherical coords. Then it transforms the spherical coordinate system
     * velocities to cartesian velocities (required by LIME).
     */

    double phi_native, phi;
    double theta, r_disk;
    double x_c_zrot, y_c_zrot, y_c_xrot, z_c_xrot;
    double vel_y_xback, vel_z_xback;
    double vel_x_tr, vel_y_tr, vel_z_tr;
    double vel_x_rot_fin, vel_y_rot_fin, vel_z_rot_fin;

    phi_native = atan2(y, x);
    if (phi_native < 0.0){
        phi_native = 2.0*M_PI+phi_native;
    } else if (phi_native >= 0.0){
        phi_native = phi_native;
    }

    //Transform coordinate system by rotating around axes.
    //First z-axis rotation (i.e. turn in azimuth).
    x_c_zrot = x*cos(azith_turn) + y*sin(azith_turn);
    y_c_zrot = -x*sin(azith_turn) + y*cos(azith_turn);

    phi_native = atan2(y_c_zrot, x_c_zrot);
    if (phi_native < 0.0){
        phi_native = 2.0*M_PI+phi_native;
    } else if (phi_native >= 0.0){
        phi_native = phi_native;
    }

    //Transform coordinate system by tilting the x-y plane.
    //X-axis rotation (i.e. increase incl).
    y_c_xrot = y_c_zrot*cos(incl) + z*sin(incl);
    z_c_xrot = -y_c_zrot*sin(incl) + z*cos(incl);

    r_disk = sqrt(pow(x_c_zrot, 2.0) + pow(y_c_xrot, 2.0) + pow(z_c_xrot, 2.0));
    theta = acos(z_c_xrot/r_disk); //This is the transformed theta
    phi_native = atan2(y_c_xrot, x_c_zrot);

    if (phi_native < 0.0){
        phi = 2.*M_PI+phi_native;
    } else if (phi_native >= 0.0){
        phi = phi_native;
    }

    /*
    * Change rot vel values.
    */
    if (theta > M_PI/2.0){
        theta = M_PI/2.0 - (theta- M_PI/2.0);
    }

    vel_x_tr  = (vel_x*sin(theta)*cos(phi) + cos(theta)*cos(phi)*vel_y
        - sin(phi)*vel_z);
    vel_y_tr  = (vel_x*sin(theta)*sin(phi) + cos(theta)*sin(phi)*vel_y +
        cos(phi)*vel_z);
    vel_z_tr  = vel_x*cos(theta) - sin(theta)*vel_y;
    if (z<0.0){
        vel_z_tr = -vel_z_tr;
    }

    /*
    * Now, the velocity vectors in the star reference frame has been found.
    * But LIME requires the cartesian vel vectors in a single, model reference
    * frame.
    */
    
    /* First correct y and z vel component back from the tilted plane 
    * (reverse x-rotation).
    */
    
    vel_y_xback = vel_y_tr*cos(2.*M_PI-incl) + vel_z_tr*sin(2.*M_PI-incl);
    vel_z_xback = -vel_y_tr*sin(2.*M_PI-incl) + vel_z_tr*cos(2.*M_PI-incl);

    //Then use the de-tilted y vel-component to find the vel x-component
    vel_x_rot_fin = vel_x_tr*cos(2.*M_PI-azith_turn) + vel_y_xback*sin(2.*M_PI-azith_turn);
    vel_y_rot_fin = -vel_x_tr*sin(2.*M_PI-azith_turn) + vel_y_xback*cos(2.*M_PI-azith_turn);
    vel_z_rot_fin = vel_z_xback;

    *vel_x_rot = vel_x_rot_fin;
    *vel_y_rot = vel_y_rot_fin;
    *vel_z_rot = vel_z_rot_fin;

    };

/*****************************************************************************/

    void disk_dens(double x, double y, double z, double *dens){
    /*
       This function calculates the disk density, in kg m^{-3}.
       Requires x,y,z coords in the disk reference frame.
    */

    double mid_r, H_d, Sigma_d, mid_r_0, Sigma_0, H_0;
    double dens_dum;
    double disk_radius = %.20e""" % all_args['disk_radius'] + """;
    double m_H2 = 2.0*1.660538921e-27; // Mass of the main collision partner, H_2 [kg]

    mid_r_0 = 1.0*AU; // Reference radius [M]
    mid_r = pow(x*x + y*y, 0.5);
    Sigma_0 = 2.0; // Surface density at 1 AU [kg pr m^2]
    H_0 = 0.3*AU; // Scale-height at 1 AU [M]

    H_d = H_0*pow(mid_r/mid_r_0, 1.25);
    Sigma_d = Sigma_0*pow(mid_r/mid_r_0, -0.75);
    dens_dum = Sigma_d/(H_d*pow(2.0*M_PI, 0.5))*exp(-pow(z, 2.0)/(2.0*pow(H_d, 2.0)))*100./m_H2; // Correct to number density of H_2
    if (mid_r > disk_radius){
        dens_dum = 1e5;
    };
    if (dens_dum < 1e5){
        dens_dum = 1e5;
    };
    *dens = dens_dum;

    };

/*****************************************************************************/

    void disk_temp(double x, double y, double z, double *temp){
    /*
       This function calculates the disk temperature, in Kelvin.
       Requires x, y coords in the disk reference frame.
    */

    double mid_r;
    double T_0 = 500.0; // Reference temperature at 1.0 AU.
    double mid_r_0 = 1.0*AU; // Reference radius [M]
    mid_r = pow(x*x + y*y + z*z, 0.5);

    *temp = T_0*pow(mid_r/mid_r_0, -0.5);

    };

/*****************************************************************************/

    void disk_vels(double x, double y, double m_star, double *vel_x, double *vel_y, double *vel_z){
    /*
       This function calculates the disk velocity vectors, in m s^{-1}.
       Requires x, y coords in the disk reference frame.
    */

    double mid_r, phi, v_mag;
    double mid_r_0 = 1.0*AU; // Reference radius [m]
    mid_r = pow(x*x + y*y, 0.5);

    //Equations from http://www.chem.ox.ac.uk/teaching/Physics20for20CHemists/Rotation/Circular.html

    phi = atan2(y, x);
    if (phi < 0.0){
        phi = 2.0*M_PI + phi;
    };

    v_mag = sqrt(6.67e-11*m_star/mid_r);

    //Correct velocity vector direction
    *vel_x = -v_mag*sin(phi);
    *vel_y = v_mag*cos(phi);
    *vel_z = 0.0;

    };

    """)


class plot():
    """
    This is a remake of the class yt_plots(), a bottom-up construction of plot
    methods, using only matplotlib, so hopefully *everything* will be possible,
    instead of being restricted using other peoples matplotlib plot packages.

    The use_arcsec handle require that the fits CUNIT is set to 'AU', even
    though all the CVAL and relevant fits keywords are actually arcsec. This
    terrible hack is necessary since the Yt project does not recognize arcsec
    as a distance unit.
    """

    def __init__(self, loc, all_files='dummy',
                 n_cont=12, c_lim=(0.5, 3.0), cont=True, line_em=False,
                 use_arcsec=True, set_log=False, draw_stars=False, L483=False,
                 pos_angle=0.0, line_list=[], cont_file='', mom_1_list=[],
                 beam_list=[], **kwargs):

        self.loc = loc  # Save location for file
        self.all_files = all_files  # A list of files to be used in the

        # multiplot method
        self.cont = cont
        self.line_em = line_em
        self.n_cont = n_cont
        self.c_lim = c_lim
        self.use_arcsec = use_arcsec
        self.set_log = set_log
        self.draw_stars = draw_stars
        self.L483 = L483
        self.pos_angle = pos_angle
        self.line_list = line_list
        self.mom_1_list = mom_1_list
        self.cont_file = cont_file
        self.beam_list = beam_list

    def multi_plot(self, out_name, cont=False, rows=1, arrows=True,
                   cmap_color='bone', cont_color='yellow',
                   arrow_color='orange', annot_text_color='white',
                   titles=None, cont_line_width=0.1, plot_rad=5.0,
                   font_size=11, annot_fontsize=9, title_size_reduc=2,
                   color_map=cm.YlOrBr, sel_lines=False, main_lines=False,
                   label_size=10, mk_pv=False):
        """
        Create a multiplot.
        See http://matplotlib.org/mpl_toolkits/axes_grid/api/axes_grid_api.html

        """

# =============================================================================
# Set up the fontstyles, AxesGrid etc.
# =============================================================================

        fns = self.all_files
        frames = int(len(fns)/rows)  # Number of images
        print('row, frames', rows, frames)
        fig = plt.figure()

        pad = 0.25
        plt.rcParams['text.usetex'] = True  # Let TeX do the typsetting
        plt.rcParams['font.family'] = 'Times New Roman'
        plt.rcParams['font.weight'] = 'bold'
        plt.rcParams['font.size'] = font_size

        if self.cont:
            grid = AxesGrid(fig, (0.075, 0.075, 0.85, 0.85),
                            nrows_ncols=(rows, frames),
                            axes_pad=pad,
                            label_mode="L",
                            share_all=True)

        elif self.line_em:
            print('Setting up grid')
            grid = AxesGrid(fig, (0.075, 0.075, 0.85, 0.85),
                            nrows_ncols=(rows, frames),
                            axes_pad=pad,
                            label_mode="L",
                            share_all=True,
                            cbar_location="right",
                            cbar_mode="single",
                            cbar_size="3%",
                            cbar_pad="5%")

        elif self.L483:
            print('Setting up grid')
            grid = AxesGrid(fig, (0.075, 0.075, 0.85, 0.85),
                            nrows_ncols=(rows, frames),
                            axes_pad=0.0,
                            label_mode="L",
                            share_all=True,
                            cbar_location="right",
                            cbar_mode="single",
                            cbar_size="4%",
                            cbar_pad="0%")


# =============================================================================
# Start loop placing the images in the AxesGrid, incrementally
# =============================================================================

        print('fns', fns)
        for i, fn in enumerate(fns):
            #  Load the data and create a single plot
            print('fn is {}'.format(fn))
            ds, header = load_fits_im(fn)
            if self.line_list[i] in ['H$^{13}$CN J=4-3', 'CS J=7-6']: #['HCN J=4-3', 'HCO$^{+}$ J=4-3']:
                print('Inverting y-axis. self.line_list[i] is', self.line_list[i])
                ds = ds[:, ::-1]
            if self.line_list[i] == 'CS J=7-6':
                ds = np.delete(ds, range(0, 29), axis=0)
            if self.line_list[i] == 'H$^{13}$CN J=4-3':
                ds = np.delete(ds, range(0, 3), axis=0)
            if self.line_list[i] == 'HCN J=4-3':
                ds = np.delete(ds, range(0, 10), axis=0)
#            if self.line_list[i] == 'HCO$^{+}$ J=4-3':
#                ds.shape = np.delete(ds, range(0, 3), axis=0)
            ny, nx = ds.shape

            if self.L483 and not mk_pv:
                # ds is then the moment 0 file
                print(self.mom_1_list)
                data_mom_1, hdr_mom_1 = load_fits_im(self.mom_1_list[i])
                data_cont, hdr_cont = load_fits_im(self.cont_file)
                im_corr = 0
                print('data_cont max is ', max(map(max, data_cont)))
                disk_center = (482.5, 497.0)
                data_mom_1 = data_mom_1/1000.0

            if mk_pv:
                im_corr = 0
            if not self.L483:
                if cont:
                    val_lims = self.c_lim
                    # Obs pixlen divided by model pixlen
                    resize = 0.1/0.024414062500008

                else:
                    resize = 3.0  # Resize obs file to the same pixel dims as
                                  # the model dims.
                    val_lims = [-1.0, self.c_lim[1] - 3.0]

                # This part is tricky. The observation fits files have 196x196
                # pixels and have 0.1 arcsec/pixel. The model files have
                # 1000x1000 pixels and have 0.033 arcsec/pixel.

                if nx == 196:

                    vals = np.reshape(ds, (196*196))
                    pts = np.array([[j, k] for j in np.arange(0.5, 196, 1.0)
                                   for k in np.arange(0.5, 196, 1.0)])

                    grid_x, grid_y = np.mgrid[0:196:1./resize, 0:196:1./resize]

                    ds = inter.griddata(pts, vals, (grid_x, grid_y),
                                        method='cubic', fill_value=0.0)

                    ny, nx = ds.shape
                    header['CDELT1'] = header['CDELT1']/resize
                    header['CDELT2'] = header['CDELT2']/resize
                    print('NEW PIXLEN', header['CDELT1'], header['CDELT2'])
                    obs_pixlenx = header['CDELT2']

                    if cont:
                        im_corr = -1
                    else:
                        im_corr = 2
                else:
                    mod_pixlenx = header['CDELT2']
                    assert np.allclose(obs_pixlenx, mod_pixlenx, rtol=0.0001), (
                                      'obs_pixlenx is %.20e, mod_pixlen is %.20e' %
                                      (obs_pixlenx, mod_pixlenx))
                    im_corr = 0

            # Correct image sizes
            print('header[CDELT1] is {}'.format(header['CDELT1']))
            arcs_len = abs(header['CDELT1']*nx)  # Size of obs. file in arcsec
            print('arcs_len is {}'.format(arcs_len))
            print('im_corr is', im_corr)
            reduction = (2.0 * plot_rad) / arcs_len
            clips = (1.0 - reduction)/2.0
        #    ds = ds[int(nx * clips): int(nx * (1.0 - clips)) + im_corr,
        #            int(ny * clips): int(ny * (1.0 - clips)) + im_corr]

            nx, ny = ds.shape
            # Check that the image size is correct
#            assert np.allclose(nx*header['CDELT2'], 2.0*plot_rad, rtol=0.01), (
#                    'Image after clipping is not of desired size. We want %.2f'
#                    ' but have %.2f' % (2.*plot_rad, ny*header['CDELT2']))

            ax = grid[i]

            if self.L483:
                if main_lines:
                    want_tick_marks = [math.floor(x*10.0)/10.0 for x in
                                       np.arange(-3, 4, 1.0)]
                    midpoint_x = disk_center[0]
                    midpoint_y = disk_center[1]
                elif mk_pv:
                    want_xtick_marks = [math.floor(x*10.0)/10.0 for x in
                                        np.arange(-3.0, 3.5, 0.5)]

                    want_ytick_marks = [math.floor(x*10.0)/10.0 for x in
                                        np.arange(-6.0e3, 7.0e3, 1.0e3)]

                    if self.line_list[i] in ['CS J=7-6']:
                        print('Changing chan_pix_offset')
                        chan_pix_offset = -28.0
                    else:
                        chan_pix_offset = 0.0

                    midpoint_x = header['NAXIS1']/2.0
                    midpoint_y = header['NAXIS2']/2.0 + chan_pix_offset

                else:
                    want_tick_marks = [-0.4, -0.2, 0.0, 0.2, 0.4]
                    midpoint_x = disk_center[0]
                    midpoint_y = disk_center[1]

            else:
                want_tick_marks = [-4.0, -2.0, 0.0, 2.0, 4.0]
            if mk_pv:
                tick_locs_x = get_pixls_ticks_in_data_units(want_xtick_marks,
                                                            header['CDELT1'],
                                                            midpoint_x)
                tick_locs_y = get_pixls_ticks_in_data_units(want_ytick_marks,
                                                            header['CDELT2'],
                                                            midpoint_y)
            else:
                tick_locs_x = get_pixls_ticks_in_data_units(want_tick_marks,
                                                            header['CDELT1'],
                                                            midpoint_x)

                tick_locs_y = get_pixls_ticks_in_data_units(want_tick_marks,
                                                            header['CDELT2'],
                                                            midpoint_y)

            if self.L483:
                if main_lines:
                    xtick_lbls = [math.floor(x*10.0)/10.0 for x in
                                  np.arange(-3, 4, 1.0)][::-1]
                    ytick_lbls = [math.floor(x*10.0)/10.0 for x in
                                  np.arange(-3, 4, 1.0)]

                elif mk_pv:
                    xtick_lbls = [math.floor(x*10.0)/10.0 for x in
                                  np.arange(-3, 3.5, 0.5)][::-1]
                    ytick_lbls = [math.floor(x*10.0)/10.0 for x in
                                  np.arange(-6.0, 7.0, 1.0)]
                else:
                    xtick_lbls = [-0.4, -0.2, 0.0, 0.2, 0.4][::-1]
                    ytick_lbls = [-0.4, -0.2, 0.0, 0.2, 0.4]
            else:
                xtick_lbls = [-4, -2, 0, 2, 4][::-1]
                ytick_lbls = [-4, -2, 0, 2, 4]

            ax.set_xticks(tick_locs_x)
            ax.set_xticklabels(xtick_lbls)
            ax.set_yticks(tick_locs_y)
            ax.set_yticklabels(ytick_lbls)
            ax.tick_params(direction='in', axis='both', which='major',
                           labelsize=label_size)
            imy, imx = ds.shape
            print('SHAPE IS NOW,', ds.shape)

            if self.L483 and not mk_pv:
                # Find value limits
                data_mom_1_box = data_mom_1[460:540, 460:540]
                lim = max(abs(min(map(min, data_mom_1_box))),
                          abs(max(map(max, data_mom_1_box))))

                im = ax.imshow(data_mom_1, aspect='auto',
                               interpolation='nearest',
                               cmap=plt.cm.seismic, origin='lower',
                               vmin=-1.0*lim,
                               vmax=lim)
                if sel_lines:
                    map_alpha = 0.7
                    map_linewidths = 0.8
                elif main_lines:
                    map_alpha = 0.8
                    map_linewidths = 0.8
                if sel_lines and i == 15:
                    # Contour levels for cont
                    levels = np.logspace(np.log10(data_cont.max()*0.05),
                                         np.log10(data_cont.max()), 10)
                    ax.contour(data_cont, levels, colors='black',
                               extent=[0, imx, 0, imy], alpha=0.5,
                               linewidths=0.5)

                mom_0_levels = np.logspace(np.log10(ds.max()*0.05),
                                           np.log10(ds.max()), 10)
                ax.contour(ds, mom_0_levels, colors='green',
                           extent=[0, imx, 0, imy], alpha=map_alpha,
                           linewidths=map_linewidths)

                if sel_lines:
                    ax.set_xlim(disk_center[0] - 15, disk_center[0] + 15)
                    ax.set_ylim(disk_center[1] - 15, disk_center[1] + 15)
                elif main_lines:
                    ax.set_xlim(disk_center[0] - 75, disk_center[0] + 75)
                    ax.set_ylim(disk_center[1] - 75, disk_center[1] + 75)
            elif mk_pv:
                    im = ax.imshow(ds, origin='lower', interpolation='bicubic',
                                   cmap=color_map)
                    print('max ds is ', ds.max())

                    ax.set_ylim(header['NAXIS2']/2.0 + chan_pix_offset - 35,
                                header['NAXIS2']/2.0 + chan_pix_offset + 35)
                    print('ylims',header['NAXIS2']/2.0 + chan_pix_offset - 35,
                                header['NAXIS2']/2.0 + chan_pix_offset + 35)
                    levels = np.logspace(np.log10(ds.max()*0.05),
                                         np.log10(ds.max()), 12)
                    ax.contour(ds, levels, colors=cont_color,
                               linewidths=cont_line_width)

            else:
                if self.line_em:
                    im = ax.imshow(ds, origin='lower',
                                   interpolation='bicubic',
                                   cmap=color_map, vmin=val_lims[0],
                                   vmax=val_lims[1],
                                   extent=[0, imy, 0, imx])
                elif self.cont:
                    im = ax.imshow(ds, origin='lower',
                                   interpolation='bicubic',
                                   cmap=color_map, norm=LogNorm(vmin=0.019,
                                   vmax=val_lims[1] + 3.05),
                                   extent=[0, imy, 0, imx])
                else:
                    assert 2 + 2 == 5, ('You did not specify if cube is'
                                        'continuum or line emission. '
                                        'Stopping program.')
                if self.L483:
                    levels = np.logspace(np.log10(vmin),
                                         np.log10(vmax),
                                         self.n_cont)

                levels = np.logspace(np.log10(self.c_lim[0]),
                                     np.log10(self.c_lim[1]),
                                     self.n_cont)
                ax.contour(ds, levels, colors=cont_color,
                           linewidths=cont_line_width)

            if self.L483:
                # Read from L483_cont.phase12amp.robust.pbcor.cyc3_arcsec.fits
                beam_size_pxls = 0.13/header['CDELT2']
            else:
                beam_size_pxls = 0.5/header['CDELT2']

# ------------------------ Create arrows ---------------------

            if arrows and i == 0 or arrows and titles == 'CO_obs' or (
            titles in ['CO_obs_mods', 'class_ab_divs'] and i in [0, 1, 2]):
                print("Making arrows")
                arrow_posA1 = [0.075, 0.05]
                arrow_posA2 = [0.3, 0.275]
                arrow_posB1 = [0.9, 0.9]
                arrow_posB2 = [0.73, 0.73]
                arrow_dict = {'facecolor': arrow_color,
                              'shrink': 0.00,
                              'width': 0.5,
                              'headwidth': 3.0,
                              'headlength': 5.0,
                              'alpha': 0.8}
                ax.annotate('A', xy=arrow_posA2, xytext=arrow_posA1,
                            xycoords='axes fraction',
                            textcoords='axes fraction',
                            alpha=1.0, fontsize=annot_fontsize,
                            arrowprops=arrow_dict)

                ax.annotate('B', xy=arrow_posB2, xytext=arrow_posB1, alpha=1.0,
                            xycoords='axes fraction',
                            textcoords='axes fraction',
                            fontsize=annot_fontsize,
                            arrowprops=arrow_dict)

            disk_abbr_cont = "DM"
            disk_abbr_gas = "DM"
            RT_abbr = "RTM"

            if titles == 'cont_mods':
                if i == 0:
                    ax.set_title(r'ALMA obs.',
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls,
                                      ALMA_obs=True)

                elif i == 1:
                    ax.set_title(r'%s$_1$ (L$_\mathrm{A}$ = 18 '
                                 'L$_{\odot}$)' % RT_abbr,
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 2:
                    ax.set_title(r'%s$_2$ (L$_\mathrm{A}$ = 10.5 '
                                 'L$_{\odot}$)' % RT_abbr,
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)
                elif i == 3:
                    ax.set_title(r'%s$_3$ (L$_\mathrm{A}$ = 20 '
                                 'L$_{\odot}$)' % RT_abbr,
                                 fontsize=font_size - title_size_reduc)

                    self.make_ellipse(ax, beam_size=beam_size_pxls)
                elif i == 4:
                    ax.set_title(r'%s$_1$ (L$_\mathrm{A}$ = 18 '
                                 'L$_{\odot}$)' %disk_abbr_cont,
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 5:
                    ax.set_title(r'%s$_2$ (L$_\mathrm{A}$ = 14 '
                                 'L$_{\odot}$)' %disk_abbr_cont,
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 6:
                    ax.set_title(r'%s$_3$ (L$_\mathrm{A}$ = 10.5 '
                                 'L$_{\odot}$)' %disk_abbr_cont,
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 7:
                    ax.set_title(r'%s$_4$ (L$_\mathrm{A}$ = 7 '
                                 'L$_{\odot}$)' % RT_abbr,
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 8:
                    ax.set_title(r'%s$_5$ (L$_\mathrm{A}$ = 3 '
                                 'L$_{\odot}$)' % RT_abbr,
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

            elif titles == 'CO_obs':
                if i == 0:
                    ax.set_title(r'Obs. $^{13}$CO',
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)
                elif i == 1:
                    ax.set_title(r'Obs. C$^{18}$O',
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 2:
                    ax.set_title(r'Obs. C$^{17}$O', fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

            elif titles == 'CO_obs_mods':
                if i == 0:
                    ax.set_title(r'Obs. $^{13}$CO',
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 1:
                    ax.set_title(r'Obs. C$^{18}$O',
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 2:
                    ax.set_title(r'Obs. C$^{17}$O',
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 3:
                    ax.set_title(r'%s $^{13}$CO' % disk_abbr_gas,
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 4:
                    ax.set_title(r'%s C$^{18}$O' % disk_abbr_gas,
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 5:
                    ax.set_title(r'%s C$^{17}$O' % disk_abbr_gas,
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 6:
                    ax.set_title(r'%s $^{13}$CO' % RT_abbr,
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 7:
                    ax.set_title(r'%s C$^{18}$O' % RT_abbr,
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 8:
                    ax.set_title(r'%s C$^{17}$O' % RT_abbr,
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

            elif titles == 'CO_obs_mods_only_rot':
                if i == 0:
                    ax.set_title(r'Obs. $^{13}$CO', fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 1:
                    ax.set_title(r'Obs. C$^{18}$O',fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 2:
                    ax.set_title(r'Obs. C$^{17}$O', fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 3:
                    ax.set_title(r'RT $^{13}$CO',fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 4:
                    ax.set_title(r'RT C$^{18}$O',fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 5:
                    ax.set_title(r'RT C$^{17}$O',
                                 fontsize=font_size - title_size_reduc)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

            elif titles == 'CO_17_lum':
                if i == 0:
                    ax.set_title(r'Obs. C$^{17}$O',
                                 fontsize=font_size - title_size_reduc)
                elif i == 1:
                    ax.set_title(r'{} C$^{17}$O (L$_\mathrm{A}$ = 3 '
                                 'L$_{\odot}$)'.format(disk_abbr_gas),
                        fontsize=font_size - title_size_reduc)
                elif i == 2:
                    ax.set_title(r'{} C$^{17}$O (L$_\mathrm{A}$ = 7 '
                                 'L$_{\odot}$)'.format(disk_abbr_gas),
                        fontsize=font_size - title_size_reduc)
                elif i == 3:
                    ax.set_title(r'{} C$^{17}$O (L$_\mathrm{A}$ = 10.5 '
                                 'L$_{\odot}$)'.format(disk_abbr_gas),
                        fontsize=font_size - title_size_reduc)
                elif i == 4:
                    ax.set_title(r'{} C$^{17}$O (L$_\mathrm{A}$ = 14 '
                                 'L$_{\odot}$)'.format(disk_abbr_gas),
                        fontsize=font_size - title_size_reduc)
                elif i == 5:
                    ax.set_title(r'{} C$^{17}$O (L$_\mathrm{A}$ = 18 '
                                 'L$_{\odot}$)'.format(disk_abbr_gas),
                        fontsize=font_size - title_size_reduc)

                self.make_ellipse(ax, beam_size=beam_size_pxls)

            elif titles == 'CO_17_rotlum':
                if i == 0:
                    ax.set_title(r'Obs. C$^{17}$O',
                                 fontsize=font_size - title_size_reduc)
                elif i == 1:
                    ax.set_title(r'%s C$^{17}$O (L$_\mathrm{A}$ = 3 '
                                 'L$_{\odot}$)' % RT_abbr,
                                 fontsize=font_size - title_size_reduc)
                elif i == 2:
                    ax.set_title(r'%s C$^{17}$O (L$_\mathrm{A}$ = 7 '
                                 'L$_{\odot}$)' % RT_abbr,
                                 fontsize=font_size - title_size_reduc)
                elif i == 3:
                    ax.set_title(r'%s C$^{17}$O (L$_\mathrm{A}$ = 14 '
                                 'L$_{\odot}$)' % RT_abbr,
                                 fontsize=font_size - title_size_reduc)
                elif i == 4:
                    ax.set_title(r'%s C$^{17}$O (L$_\mathrm{A}$ = 18 '
                                 'L$_{\odot}$)' % RT_abbr,
                                 fontsize=font_size - title_size_reduc)
                elif i == 5:
                    ax.set_title(r'%s C$^{17}$O (L$_\mathrm{A}$ = 20 '
                                 'L$_{\odot}$)' % RT_abbr,
                                 fontsize=font_size - title_size_reduc)

                self.make_ellipse(ax, beam_size=beam_size_pxls)

            elif titles == 'class_ab_divs':
                print("Making titles for class_abs")
                if i == 0:
                    ax.set_title(r'Obs. $^{13}$CO',
                                 fontsize=font_size - title_size_reduc - 2)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 1:
                    ax.set_title(r'Obs. C$^{18}$O',
                                 fontsize=font_size - title_size_reduc - 2)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 2:
                    ax.set_title(r'Obs. C$^{17}$O',
                                 fontsize=font_size - title_size_reduc - 2)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 3:
                    ax.set_title(r'$X_{\text{CO,ISM}}$/10 $^{13}$CO',
                                 fontsize=font_size - title_size_reduc - 2)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 4:
                    ax.set_title(r'$X_{\text{CO,ISM}}$/10 C$^{18}$O',
                                 fontsize=font_size - title_size_reduc - 2)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 5:
                    ax.set_title(r'$X_{\text{CO,ISM}}$/10 C$^{17}$O',
                                 fontsize=font_size - title_size_reduc - 2)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 6:
                    ax.set_title(r'$X_{\text{CO,ISM}}$/15 $^{13}$CO',
                                 fontsize=font_size - title_size_reduc - 2)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 7:
                    ax.set_title(r'$X_{\text{CO,ISM}}$/15 C$^{18}$O',
                                 fontsize=font_size - title_size_reduc - 2)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

                elif i == 8:
                    ax.set_title(r'$X_{\text{CO,ISM}}$/15 C$^{17}$O',
                                 fontsize=font_size - title_size_reduc - 2)
                    self.make_ellipse(ax, beam_size=beam_size_pxls)

            elif titles == 'Sel_lines':
                self.make_ellipse(ax, i=i, sel_lines=True)
                ax.annotate(r'{}'.format(self.line_list[i]),
                            xy=(482.5 - 12.0, 497 + 12.0),
                            xytext=(482.5 - 12.0, 497 + 12.0),
                            fontsize=3.5)

            elif titles == 'Main_lines':
                self.make_ellipse(ax, i=i)
                ax.annotate(r'{}'.format(self.line_list[i]),
                            xy=(500, 560), xytext=(500, 560), fontsize=6)
            elif titles == 'PV_plots':
                print('Now making plot titles for pv files..')
                ax.annotate(r'{}'.format(self.line_list[i]),
                            xy=(5, 495), xytext=(5, 495), fontsize=6)
            if self.use_arcsec and not self.L483:
                ax.set_xlabel(r'RA offset [$^{\prime \prime}$]',
                              fontsize=font_size)
                ax.set_ylabel(r'DEC offset [$^{\prime \prime}$]',
                              fontsize=font_size)
            if (self.use_arcsec and self.L483 and (titles == 'Sel_lines'
                                                   and i == 12
                    or titles == 'Main_lines' and i == 2)):
                ax.set_xlabel(r'RA offset [$^{\prime \prime}$]',
                              fontsize=font_size)
                ax.set_ylabel(r'DEC offset [$^{\prime \prime}$]',
                              fontsize=font_size)
            if (self.use_arcsec and self.L483 and titles == 'PV_plots' and
                i == 2):
                ax.set_xlabel(r'Offset [$^{\prime \prime}$]',
                              fontsize=font_size)
                ax.set_ylabel(r'Velocity [km~s$^{-1}$]', fontsize=font_size)
            elif not self.use_arcsec:
                ax.set_xlabel('AU')
                ax.set_ylabel('AU')

        # This forces the plots to redraw themselves on the AxesGrid
        # Add colorbar

        if self.cont:
            fig.subplots_adjust(right=0.8)
            cbar_ax = fig.add_axes([0.85, 0.075, 0.025, 0.85])
            cbar = plt.colorbar(im, cax=cbar_ax)
            cbar.ax.tick_params(direction='in', which='both')
            cbar.set_label(r"Jy beam$^{-1}$")
        elif self.line_em:
            grid[0].cax.colorbar(im)
            cax = grid.cbar_axes[0]
            axis = cax.axis[cax.orientation]
            axis.label.set_text(r"Jy beam$^{-1}$ km s$^{-1}$")
            cax.tick_params(direction='in', which='both')
            cax.minorticks_on()
        elif self.L483 and not mk_pv:
            print('Setting L483 plot up')
            grid[0].cax.colorbar(im)
            cax = grid.cbar_axes[0]
            axis = cax.axis[cax.orientation]
            axis.label.set_text(r"km s$^{-1}$")
            cax.tick_params(direction='in', which='both')
            cax.minorticks_on()

        elif self.L483 and mk_pv:
            print('Setting L483 PV plot up')
            grid[0].cax.colorbar(im)
            cax = grid.cbar_axes[0]
            axis = cax.axis[cax.orientation]
            axis.label.set_text(r"Jy beam$^{-1}$")
            cax.tick_params(direction='in', which='both')
            cax.minorticks_on()

        plt.savefig(out_name, dpi=300, bbox_inches='tight')
        print('Multiplot saved as %s' % out_name)

    def single_plot(self, file_name, out_name, arrows=True,
                    cmap_color='bone',
                    cont_color='yellow', arrow_color='orange',
                    annot_text_color='white', line_width=0.5, ALMA_prop=False,
                    font_size=14, cont_line_width=0.5, color_map=cm.YlOrBr,
                    annot_fontsize=18, title_size_reduc=2, plot_rad=5.0):

        """
        Make a single plot.

        Args:
            axis = projection axis of the image or cube.
        """

        plt.rcParams['text.usetex'] = True  # Let TeX do the typsetting
        plt.rcParams['font.family'] = 'Times New Roman'
        plt.rcParams['font.weight'] = 'bold'
        plt.rcParams['font.size'] = font_size

        fig, ax = plt.subplots()
        ds, header = load_fits_im(file_name)
        ny, nx = ds.shape

        if not self.L483:
            # Correct size.
            arcs_len = abs(header['CDELT1'])*nx  # Size of obs. file in arcsec
            reduction = (2.0 * plot_rad) / arcs_len
            clips = (1.0 - reduction)/2.0
            ds = ds[int(ny * clips): int(ny * (1.0 - clips)),
                    int(nx * clips): int(nx * (1.0 - clips))]
            ny, nx = ds.shape

        # Check that the image size is correct
        if not self.L483:
            assert np.allclose(ny*header['CDELT2'], 2.*plot_rad, rtol=0.01), (
                    'Image after clipping is not of desired size. We want %.2f'
                    ' but have %.2f' % (2.*plot_rad, ny*header['CDELT2']))

        if self.set_log:
            if self.L483:
                vmin=0.05*ds.max()
                vmax=ds.max()
            else:
                vmin=0.019
                vmax=self.c_lim[1] + 3.05

            cax = ax.imshow(ds, origin='lower',
                            interpolation='bicubic',
                            cmap=color_map,
                            norm=LogNorm(vmin=vmin,
                            vmax=vmax))
            if self.L483:
                levels = np.logspace(np.log10(vmin),
                                     np.log10(vmax),
                                     self.n_cont)
                alpha = 0.8
            else:
                levels = np.logspace(np.log10(self.c_lim[0]),
                                     np.log10(self.c_lim[1]),
                                     self.n_cont)
                alpha = 1.0
            ax.contour(ds, levels, colors=cont_color,
                       linewidths=cont_line_width, alpha=alpha)

            if arrows:
                size_factor = 3
                arrow_posA1 = [0.075, 0.05]
                arrow_posA2 = [0.3, 0.275]
                arrow_posB1 = [0.9, 0.9]
                arrow_posB2 = [0.73, 0.73]
                arrow_dict = {'facecolor': arrow_color,
                              'shrink': 0.00,
                              'width': 0.5*size_factor,
                              'headwidth': 3.0*size_factor,
                              'headlength': 5.0*size_factor,
                              'alpha': 0.8}

                ax.annotate('A', xy=arrow_posA2, xytext=arrow_posA1,
                            xycoords='axes fraction',
                            textcoords='axes fraction',
                            alpha=1.0, fontsize=annot_fontsize,
                            arrowprops=arrow_dict)

                ax.annotate('B', xy=arrow_posB2, xytext=arrow_posB1,
                            alpha=1.0,
                            xycoords='axes fraction',
                            textcoords='axes fraction',
                            fontsize=annot_fontsize,
                            arrowprops=arrow_dict)

        if self.L483:
            disk_center = (482.5, 497.0)
            want_tick_marks = [math.floor(x*10.0)/10.0 for x in
                               np.arange(-1, 1.5, 0.5)]
            midpoint_x = disk_center[0]
            midpoint_y = disk_center[1]
            tick_locs_x = get_pixls_ticks_in_data_units(want_tick_marks,
                                                        header['CDELT1'],
                                                        midpoint_x)
            tick_locs_y = get_pixls_ticks_in_data_units(want_tick_marks,
                                                        header['CDELT2'],
                                                        midpoint_y)
        else:
            want_tick_marks = [-4.0, -2.0, 0.0, 2.0, 4.0]
            tick_locs_x = get_pixls_ticks_in_data_units(want_tick_marks,
                                                        header['CDELT1'],
                                                        int(nx/2.0))
            tick_locs_y = get_pixls_ticks_in_data_units(want_tick_marks,
                                                        header['CDELT2'],
                                                        int(ny/2.0))
        if not self.L483:
            want_tick_marks = [-4, -2, 0, 2, 4]

        ytick_lbls = want_tick_marks
        xtick_lbls = want_tick_marks[::-1]
        ax.set_xticks(tick_locs_x)
        ax.set_xticklabels(xtick_lbls)
        ax.set_yticks(tick_locs_y)
        ax.set_yticklabels(ytick_lbls)
        ax.tick_params(direction='in')
        ax.set_aspect('equal', 'box')

        beam_size_pxls = 0.5/header['CDELT2']
        self.nx = nx
        self.ny = ny
        if self.L483:
            self.make_ellipse(ax)
            ax.set_xlim(disk_center[0] - 45, disk_center[0] + 45)
            ax.set_ylim(disk_center[1] - 45, disk_center[1] + 45)
        else:
            self.make_ellipse(ax, beam_size=beam_size_pxls, single_plot=True)

        # Add colorbar
        cbar = plt.colorbar(cax, pad=0.0)
        cbar.ax.tick_params(direction='in', which='both')

        if self.cont:
            cbar.set_label(r"Jy beam$^{-1}$")
        elif self.line_em:
            cbar.set_label(r"Jy beam$^{-1}$ km s$^{-1}$")

        if self.use_arcsec:
            ax.set_xlabel(r'RA offset [$^{\prime \prime}$]')
            ax.set_ylabel(r'DEC offset [$^{\prime \prime}$]')
        print('Done. Creating single plot at %s' % out_name)
        plt.savefig(out_name, dpi=300, bbox_inches='tight')

    def make_ellipse(self, ax, beam_size=0.5, ALMA_obs=False,
                     single_plot=False, i=0, sel_lines=False):
        """
        Create ellipse, presenting the beamsize of the observations.
        beamsize is the full width and height of ellipse.
        90 degrees is added to the position angle, as matplotlib angle is
        azimuth from x-axis towards y-axis (counter-clockwise).
        """
        if self.cont and not self.L483:
            if not single_plot:
                el = Ellipse((390, 21), width=beam_size, height=beam_size,
                             angle=pos_angle + 90.0, color='black', fill=True,
                             linewidth=0.001)
            elif single_plot:
                pxl_len = 0.1
                want_pos = [4.5, -4.5]
                ctr_pix = int(self.nx/2.0)
                pos = get_pixls_ticks_in_data_units(want_pos, pxl_len, ctr_pix)
                print(pos, ctr_pix, self.nx, self.ny)
                el = Ellipse(pos, width=self.semiminor_axis,
                             height=self.semimajor_axis,
                             angle=self.pos_angle + 90.0, color='black',
                             fill=True, linewidth=0.001)

        else:
            if self.L483:
                if sel_lines:
                    ell_loc = (482.5 + 10.6, 497 - 10.6)
                elif self.cont:
                    ell_loc = (482.5 + 30.0, 497 - 30.0)
                else:
                    ell_loc = (482.5 + 60.0, 497 - 60.0)
                print('i is ', i)
                beam_pars = self.beam_list[i]  # major, minor, pos angle
                print('beam_pars are', beam_pars)
                el = Ellipse(ell_loc, width=beam_pars[0]/0.033,
                             height=beam_pars[1]/0.033,
                             angle=beam_pars[2] + 90.0, color='gray',
                             fill=True, linewidth=0.001, alpha=1.0)
            else:
                el = Ellipse((285, 16), width=beam_size, height=beam_size,
                             angle=self.pos_angle + 90.0, color='black',
                             fill=True, linewidth=0.001)

        ax.add_artist(el)


def plot_vel_emission(vel, spec, v_0, v_1, oplot=False, vel_2=-1, spec_2=-1,
                      label_1='Spectrum 1', label_2='Spectrum 2',
                      show_int_lim=False, int_0=-1, int_1=-1,
                      out_name='fig.png'):

    with sns.axes_style("darkgrid"):
        plt.figure()
        plt.plot(vel/1000.0, spec, label=label_1)
        plt.xlabel(r'v [km s$^{-1}]$', fontsize=20)
        plt.ylabel(r'Flux [Jy beam$^{-1} \times$ pixels]', fontsize=20)
        plt.xlim(v_0/1000.0, v_1/1000.0)
        if oplot:
            plt.plot(vel_2/1000.0, spec_2, label=label_2)
        if show_int_lim:
            plt.axvline(x=int_0/1000.0, color='black', linestyle='--')
            plt.axvline(x=int_1/1000.0, color='black', linestyle='--')
        plt.grid()
        plt.tick_params(axis='both', which='both', labelsize=18)

        plt.legend(fontsize=18)
        plt.savefig(out_name, dpi=300, bbox_inches='tight')


def load_fits_im(cube_name, is_PV=False):
    """
    This function loads a fits image.

    Args:
        cube: spectral data cube, assumed dims are [im_x, im_y, stokes_nr]
    Returns:
        data, header
    """
    print('load_fits_im opening {}'.format(cube_name))
    data_cube_temp, hdr = pyfits.getdata(cube_name, 0, header=True)
    if len(data_cube_temp.shape) == 4:
        assert data_cube_temp.shape[0] == 1
        assert data_cube_temp.shape[1] == 1

    elif len(data_cube_temp.shape) == 3:
        assert data_cube_temp.shape[0] == 1, ('First fits file dimension '
                                              'should be the stokes nr.')

    if is_PV:
        assert (hdr['CTYPE2'] == 'VELO-LSR' or
                hdr['CTYPE2'] == 'VRAD' or
                hdr['CTYPE2'] == 'VEL'), ('Second dimension of PV file '
                                          'should be velocity')

    # Remove NaNs
    NaNs = np.isnan(data_cube_temp)
    data_cube_temp[NaNs] = 0.0

    assert len(data_cube_temp.shape) == 4 or len(data_cube_temp.shape) == 3 or len(data_cube_temp.shape) == 2
    if len(data_cube_temp.shape) == 4:
        ni, nz, ny, nx = data_cube_temp.shape
        data_cube = np.zeros((ny, nx), dtype=np.float64)

        # Collapse redundant dimension containing stokes_nr.
        data_cube[:, :] = data_cube_temp[0, 0, :, :]
    if len(data_cube_temp.shape) == 3:
        nz, ny, nx = data_cube_temp.shape
        data_cube = np.zeros((ny, nx), dtype=np.float64)

        # Collapse redundant dimension containing stokes_nr.
        data_cube[:, :] = data_cube_temp[0, :, :]

    elif len(data_cube_temp.shape) == 2:
        ny, nx = data_cube_temp.shape
        data_cube = data_cube_temp

    return data_cube, hdr


def load_fits_cube(cube_name):
    """
    This function loads a fits cube.

    Args:
        cube: spectral data cube, assumed dims are
        (im_x, im_y, n_chan, stokes_nr) or
        (im_x, im_y, n_chan).
    Returns:
        data, header
    """

    data_cube, hdr = pyfits.getdata(cube_name, 0, header=True)

    # Remove NaNs
    NaNs = np.isnan(data_cube)
    data_cube[NaNs] = 0.0

    assert len(data_cube.shape) == 4 or len(data_cube.shape) == 3, (
               'Wrong input cube dimensions. Cube must have 3 or 4 dimensions')

    # Account for the cube file format
    if len(data_cube.shape) == 4:
        assert data_cube.shape[0] == 1, 'Stokes nr expected to be 1.'
        stokes_nr, n_chan, ny, nx = data_cube.shape
        data_cube = data_cube[0, :, :, :]

    assert data_cube.shape[0] > 1, ('Must have more than 1 channel in cube. '
                                    'Did you use an image and not a cube? Then'
                                    ' use load_fits_im instead.')
    print('Shape returned from load_fits_cube is {}'.format(data_cube.shape))

    return data_cube, hdr


def extract_first_chan_as_cont(cube, out_name='cont_file_1stchan.fits'):
    """
    This function extracts the first channel from a fits cube as saves it as
    a single fits file, to be used as a continuum file.
    Args:
        cube: spectral data cube, assumed dims are [channel, im_y, im_x]
        out_name: Output filename
    Returns:
        None. Creates a file named as the input variable out_name.
    """

    data_cube, hdr = load_fits_cube(cube)

    hdr_ban = ['', 'COMMENT', 'HISTORY', 'OBSRA', 'OBSDEC',
               'CDELT3', 'CRPIX3', 'CUNIT3', 'CRVAL3', 'CTYPE3',
               'CDELT4', 'CRPIX4', 'CUNIT4', 'CRVAL4', 'CTYPE4']
    # extract first channel
    wrt_to_fits(data_cube[0, :, :], hdr, out_name, hdr_ban)


class cube():
    """
    This class contains methods for analyzing fits cubes.
    """

    def __init__(self, file_name, dist_pc, line_rest_freq, rest_vel):
        self.file = file_name
        self.dist_pc = dist_pc
        self.line_rest_freq = line_rest_freq
        self.rest_vel = rest_vel

    def extract_data(self, box=0, pt_pix=0, is_PV=False):
        """
        This function extracts and assigns the cube and header data as
        attributes to an cube instance.
        The integrated emission of the input data cube is given as both
        the full integrated map, a box and a single pixel spectrum.
        Args:
            box: box of integration.
        """

        if box != 0:
            use_box = True
        else:
            use_box = False

        if pt_pix != 0:
            pt_spectrum = True
        else:
            pt_spectrum = False
        assert not use_box and pt_spectrum or use_box and not pt_spectrum or \
               not use_box and not pt_spectrum, ('Cannot extract box and ',
                                                 'point spectrum at once.')
        if is_PV:
            self.data, self.hdr = load_fits_im(self.file)
        else:
            self.data, self.hdr = load_fits_cube(self.file)

        if is_PV:
            assert (self.hdr['CTYPE2'] == 'VELO-LSR' or
            self.hdr['CTYPE2'] == 'VRAD' or self.hdr['CTYPE2'] == 'VEL'), (''
            'Expected PV file.. Please add functionality for fits image file.')
        else:
            try:
                self.n_chan = int(self.hdr['NAXIS3'])
                self.chan_list = range(self.hdr['NAXIS3'])
            except KeyError:
                print('Third axis of cube not found..')
                print('self.hdr[CTYPE2] is {}'.format(self.hdr['CTYPE2']))

        self.dim = len(self.data.shape)
        self.n_axis_1 = self.hdr['NAXIS1']
        self.n_axis_2 = self.hdr['NAXIS2']

        self.int_spectrum = np.zeros(self.n_chan)
        self.pt_spectrum = np.zeros(self.n_chan)
        self.box_int_spectrum = np.zeros(self.n_chan)

        # Pixel units
        self.arcs_pr_px = abs(float(self.hdr['CDELT1']))  # arcsec pr. pixel
        if self.hdr['CTYPE1'] == 'RA---SIN':
            self.arcs_pr_px = self.arcs_pr_px*3600.0  # Convert degrees to arcsec.
        self.AU_pr_px = self.dist_pc * self.arcs_pr_px
        if is_PV:
            hdr_vel_freq_dim = 2
        else:
            hdr_vel_freq_dim = 3

        try:
            assert self.hdr['CTYPE{}'.format(hdr_vel_freq_dim)] == 'VRAD' or self.hdr['CTYPE{}'.format(hdr_vel_freq_dim)] == 'VELO-LSR'
        except KeyError:
            assert self.hdr['CTYPE{}'.format(hdr_vel_freq_dim)] == 'FREQ'

        if (self.hdr['CTYPE{}'.format(hdr_vel_freq_dim)] == 'VRAD' or
        self.hdr['CTYPE{}'.format(hdr_vel_freq_dim)] == 'VELO-LSR'):
            self.chan_width_vel = self.hdr['CDELT{}'.format(hdr_vel_freq_dim)]
            self.ref_chan = self.hdr['CRPIX{}'.format(hdr_vel_freq_dim)] - 1 # Fits files uses one-indexing, subtract 1 to conform to Python zero-indexing. 
            self.ref_vel = self.hdr['CRVAL{}'.format(hdr_vel_freq_dim)]
#            self.ref_freq = float(self.hdr['RESTFREQ'])
            print('Inside VELO-LSR')

            if not is_PV:
                print('Now finding vels for {}'.format(self.file))
                self.vel = find_chan_vels(self.ref_chan,
                                          self.chan_width_vel,
                                          self.ref_vel, self.chan_list)
                # Find channel offset between cube center and line freq
                
                self.freq = [vel_to_freq(x, self.line_rest_freq)
                             for x in self.vel]
                self.find_ctrl_chan()  # create attributes; self.ctrl_chan,
                                       # self.ctrl_offset_chan,
                                       # self.ctrl_offset_vel
        
            self.chan_width_Hz = self.line_rest_freq - vel_to_freq(
                                                    self.chan_width_vel,
                                                    self.line_rest_freq)

            print('self.freq is {} -- {} GHz'.format(self.freq[0]/1e9,
                  self.freq[-1]/1e9))

        elif self.hdr['CTYPE{}'.format(hdr_vel_freq_dim)] == 'FREQ':
            self.chan_width_Hz = float(self.hdr['CDELT{}'.format(hdr_vel_freq_dim)])
            self.ref_freq = float(self.hdr['CRVAL{}'.format(hdr_vel_freq_dim)])

            if not is_PV:
                self.ref_chan = self.hdr['CRPIX{}'.format(hdr_vel_freq_dim)] - 1

                self.vel = np.zeros(self.n_chan)
                self.freq = np.zeros(self.n_chan)

                self.freq = find_chan_freqs(self.ref_chan,
                                            self.chan_width_Hz,
                                            self.ref_freq, self.chan_list)
                self.vel[:] = [freq_to_vel(self.line_rest_freq, x)
                               for x in self.freq]
            self.ref_vel = freq_to_vel(self.line_rest_freq, self.ref_freq)

            self.chan_width_vel = freq_to_vel(self.line_rest_freq,
                                              self.line_rest_freq +
                                              self.chan_width_Hz)

        if not is_PV:
            if use_box:
                print('Calculating box spectrum..')
                assert type(np.nansum(self.data[5, int(box[0][1]):int(box[1][1]), int(box[0][0]):int(box[1][0])])) == np.float32
            elif pt_spectrum:
                print('Calculating pointer spectrum..')
                assert type(self.data[5, pt_pix[1], pt_pix[0]]) == np.float32
            for i in self.chan_list:
                if use_box:
                    self.box_int_spectrum[i] = np.nansum(
                        self.data[i, int(box[0][1]):int(box[1][1]), int(box[0][0]):int(box[1][0])])
                elif pt_spectrum:
                    self.pt_spectrum[i] = self.data[i, pt_pix[1],
                                                    pt_pix[0]]
                else:
                    self.int_spectrum[i] = np.nansum(self.data[i, :, :])
            if use_box:
                if np.count_nonzero(self.data[:, int(box[0][1]):int(box[1][1]), int(box[0][0]):int(box[1][0])]) != self.n_chan*self.data[1, int(box[0][1]):int(box[1][1]), int(box[0][0]):int(box[1][0])].shape[0]*self.data[1, int(box[0][1]):int(box[1][1]), int(box[0][0]):int(box[1][0])].shape[1]:
                    print('WARNING: Zeros found in box spectrum input data cube')               
                    for i in self.chan_list:
                        if np.count_nonzero(self.data[i, int(box[0][1]):int(box[1][1]), int(box[0][0]):int(box[1][0])]) != self.data[i, int(box[0][1]):int(box[1][1]), int(box[0][0]):int(box[1][0])].shape[0]*self.data[i, int(box[0][1]):int(box[1][1]), int(box[0][0]):int(box[1][0])].shape[1]:
                            print('Chan {} contains zeros'.format(i))

    def find_ctrl_chan(self):
        """
        Find channel with the input velocity, the distance from
        the channel center, in fractions of the channelwidth and in velocity.
        """

        min_dist = 1e10

        for chan in self.chan_list:
            chan_vel = ((chan - self.ref_chan)*self.chan_width_vel +
                        self.ref_vel)
            vel_diff = abs(self.rest_vel - chan_vel)
            if vel_diff < min_dist:
                min_dist = vel_diff
                res_chan = chan
                chan_offset = abs(min_dist/self.chan_width_vel)

        self.ctrl_chan = res_chan
        self.ctrl_offset_chan = chan_offset
        self.ctrl_offset_vel = min_dist

    def find_chan_range(self, line_rest_freq, integration_halfwidth):
        """
        Find channels within a certain velocity range around a given line
        rest frequency.
        Args:
            line_rest_freq: Line rest frequency [Hz, float]
        Returns:
            List of channel int numbers.
        """

        print('Input into self.find_chan_range search is {} to {}'.format(
                                                        self.chan_list[0],
                                                        self.chan_list[-1]))
        print('line_rest_freq is {}'.format(line_rest_freq))

        # Find frequency limits.
        freq_0 = vel_to_freq(integration_halfwidth + self.rest_vel,
                             line_rest_freq)
        freq_1 = vel_to_freq(-integration_halfwidth + self.rest_vel,
                             line_rest_freq)

        print('Searching for chans with {} -- {} GHz'.format(freq_0/1e9,
              freq_1/1e9))

        chan_selection = []
        for chan, freq in enumerate(self.freq):
            if freq_0 <= freq <= freq_1:
                chan_selection.append(chan)

        return chan_selection

    def moment(self, mom=1, mom_0_name='mom_0_map.fits',
               mom_1_name='mom_1_map.fits', rms=0.01,
               chan_0=None, chan_1=None, single_line=False,
               line_rest_freq=None, line=None, integration_halfwidth=3500.0,
               use_box=False, rest_vel=5800):

        """
        This function calculates moment maps, either zeroth or first moment
        maps.
        """

        print('Making mom_{} image of {}'.format(mom, self.file))

        mom_0_map = np.zeros((self.n_axis_2, self.hdr['NAXIS1']))

        pxl_sum = 0.0
        coords_for_mom_1 = []
        chan_list = xrange(len(self.chan_list))

        box = [(474, 489), (492, 506)]  # 472.5 in the imfit box

        if single_line:
            print('Finding chans for moment maps..')
            print('line_rest_freq is {}'.format(line_rest_freq))
            shifted_line_rest_freq = line_rest_freq - abs(self.chan_width_Hz)*(rest_vel/abs(self.chan_width_vel))

            chan_list = self.find_chan_range(line_rest_freq=line_rest_freq,
                                   integration_halfwidth=integration_halfwidth)

            line_vel = np.zeros(len(self.vel))
            # Redefine velocities relative to the targeted line rest frequency.
            min_dist = 1e20
            print('shifted_line_rest_freq is {} and line_rest_freq is {}'.format(shifted_line_rest_freq,
                                                                                 line_rest_freq))
            print('offset is {} MHz'.format(abs(self.chan_width_Hz)*(rest_vel/abs(self.chan_width_vel)/1e6)))
            print('offset is {} channels'.format(rest_vel/abs(self.chan_width_vel)))

            for chan in self.chan_list:
                freq_diff = abs(self.freq[chan] - shifted_line_rest_freq)
                if freq_diff < min_dist:
                    min_dist = freq_diff
                    res_chan = chan
                    chan_offset = abs(min_dist/self.chan_width_Hz)

            # Redefine all velocities relative to the central channel
            print('self.n_chan is', self.n_chan)
            print('res_chan is {}'.format(res_chan))
            print('chan_offset is {}'.format(chan_offset))
            print('Chanlist for moment maps is {} to {}'.format(chan_list[0],
                                                                chan_list[-1]))
            for chan in xrange(self.n_chan):
                line_vel[chan] = (chan - res_chan + chan_offset)*self.chan_width_vel

        else:
            line_vel = self.vel
        # Moment 0 map
        #max_val = max(map(max, self.data))*0.05
       # print('max_val', max_val)
        #max_val = map(max, self.data)*0.05
        #print('max_val', max_val)
# =============================================================================
#         max_val_0 = max(map(max, np.max(self.data, axis=0)))
#         max_val_1 = max(map(max, np.max(self.data, axis=1)))
#         max_val_2 = max(map(max, np.max(self.data, axis=2)))
#         print('max_val_0', max_val_0)
#         print('max_val_1', max_val_1)
#         print('max_val_2', max_val_2)
#         max_val = max([max_val_0, max_val_1, max_val_2])
#         print('max_val', max_val)
# =============================================================================

        if mom == 0:
            abs_chan_width_vel = abs(self.chan_width_vel)
            for x in xrange(self.n_axis_1):
                for y in xrange(self.n_axis_2):
                    for i in chan_list:
                        mom_0_map[y, x] += self.data[i, y, x]
                    mom_0_map[y, x] = mom_0_map[y, x]*abs_chan_width_vel

        elif mom == 1:
            mom_0_map, mom_0_hdr = load_fits_im(mom_0_name)
            min_val = max(map(max, mom_0_map))*0.05

            if use_box:
                for x in xrange(box[0][0], box[1][0]):
                    for y in xrange(box[0][1], box[1][1]):
                        if abs(mom_0_map[y, x]) >= min_val:
                            coords_for_mom_1.append((y, x))
            else:
                for x in xrange(self.n_axis_1):
                    for y in xrange(self.n_axis_2):
                        if abs(mom_0_map[y, x]) >= min_val:
                            coords_for_mom_1.append((y, x))

            assert len(coords_for_mom_1) != 0, 'No pixels passed to mom-1 map, mom_0_map.max() is {}'.format(mom_0_map.max())
            print('len of coords_for_mom_1 is {}'.format(len(coords_for_mom_1)))
            mom_1_map = np.zeros((self.n_axis_2, self.n_axis_1))
            for coords in coords_for_mom_1:
                y, x = coords[:]
                for i in chan_list:
#                    print('self.data[i, y, x]', self.data[i, y, x])
                    #print('self.vel[i]', self.vel[i])
                    mom_1_map[y, x] += line_vel[i]*self.data[i, y, x]
                    pxl_sum += self.data[i, y, x]

                assert pxl_sum != 0.0, 'No value allocated in chan loop'
                mom_1_map[y, x] = mom_1_map[y, x]/pxl_sum
                pxl_sum = 0.0
            print('chan_list is {} to {}'.format(chan_list[0], chan_list[-1]))
            for i in chan_list:
                print('self.vel[i]', line_vel[i])

        # Change header to arcsec
#        f = pyfits.open(self.file, mode='update')  # +'.fits'  # open a FITS file

        hdr = self.hdr
        hdr['CUNIT1'] = 'arcsec'
        hdr['CUNIT2'] = 'arcsec'
        hdr['CDELT1'] = -hdr['CDELT1']*3600.  # 2.77777777778E-05*3600  # Convert deg to arcsec
        hdr['CDELT2'] = hdr['CDELT2']*3600.  # 2.77777777778E-05*3600  # Convert deg to arcsec

        # Write moment map to fits file
        if mom == 0:
            wrt_to_fits(mom_0_map, hdr, mom_0_name)
            print('Moment 0 image written to {}'.format(mom_0_name))

        elif mom == 1:
            wrt_to_fits(mom_1_map, hdr, mom_1_name)
            print('Moment 1 image written to {}'.format(mom_1_name))

    def wrt_spectrum_to_file(self, box_spectrum_file_name,
                             pix_spectrum_file_name,
                             box=[473, 487, 492, 509],
                             pt_pix=(481, 497)):
        """
        Write spectrum to file.
        """
        self.extract_data(box=box)

        pt_pix = [int(round(x)) for x in pt_pix]

        assert len(self.box_int_spectrum.shape) == 1, ('Box integration format'
                                                       ' wrong.', self.box_int_spectrum.shape,
                                                       self.box_int_spectrum[0])

        # First, use a box around the region of interest.
        with open(box_spectrum_file_name, 'w') as f:
            f.write('Freq [Hz], Intensity [Jy/beam * pixels] \n')
            for freq_point, spectrum_point in zip(self.freq,
                                                  self.box_int_spectrum):
                f.write('{}, {}\n'.format(freq_point, spectrum_point))

        # Second, use the peak emission pixel.
        self.extract_data(pt_pix=pt_pix)
        assert len(self.pt_spectrum.shape) == 1, ('Point spectrum integration '
                                                  'format wrong.')

        with open(pix_spectrum_file_name, 'w') as f:
            f.write('Freq [Hz], Intensity [Jy/beam] \n')
            for freq_point, spectrum_point in zip(self.freq,
                                                  self.pt_spectrum):
                f.write('{}, {}\n'.format(freq_point, spectrum_point))
        print('Pointer spectrum written to {}'.format(pix_spectrum_file_name))
        print('Box spectrum written to {}'.format(box_spectrum_file_name))


class fits_tools():
    """
    This class contains methods for slicing and trimming fits files.
    """
    def __init__(self, frame_start, frame_end, file_name, file_dir):
        self.file_name = file_name
        self.frame_start = frame_start
        self.frame_end = frame_end
        self.file_dir = file_dir

    def _trim_file(self, new_name):
        """
        This function reduces the fits file by channel and image dimension.
        """

        frm_len = self.frame_end - self.frame_start
        data, hdr = load_fits_cube(self.file_name)

        assert len(data.shape) == 4 or len(data.shape) == 3, ('Dimensions '
                  'should be 3 or 4. Please use a data cube.')

        # Print fits info.
        pyfits.info(self.file_name)
        n_chan, y_width, x_width = data.shape
        new_cube = np.zeros((frm_len, y_width, x_width))
        new_cube[:, :, :] = data[self.frame_start:self.frame_end, :, :]
        hdr['NAXIS3'] = frm_len
        wrt_to_fits(new_cube, hdr, new_name, frame_start=self.frame_start)


def wrt_to_fits(data, hdr, fits_name, hdr_ban=None, frame_start=None,
                use_arcsec=False):
    """
    This function writes a dataset to a fits file. Demands
    an existing header.

    Data must be [chan, y, x] format.
    use_arcsec is a redundant option, should clean it. Kept to ensure
    compatability with the mk_fits_co.py script.
    """

    if len(data.shape) == 3:
        assert (int(hdr['NAXIS3']), int(hdr['NAXIS2']),
                int(hdr['NAXIS1'])) == data.shape, (
                    'Data cube format of {} is wrong. Expected {}, {}, {}. Please use (chan, y, x) '
                    'format used by fits.'.format(data.shape, int(hdr['NAXIS3']), int(hdr['NAXIS2']),
                int(hdr['NAXIS1'])))
    elif len(data.shape) == 2:
        assert (int(hdr['NAXIS2']), int(hdr['NAXIS1'])) == data.shape, (
                    'Data cube format of {} is wrong. Please use (chan, y, x) '
                    'format used by fits.'.format(data.shape))

    hdu = pyfits.PrimaryHDU(data)

    # We then create a HDUList to contain the newly created primary HDU,
    # and write to a new file:

    hdulist = pyfits.HDUList([hdu])
    prihdr = hdulist[0].header

    # Copy original header files onto the new fits file - this is very
    # important in order for miriad functions to work properly, i.e. CDELT3
    # (channel width) is needed in order to calculate the zeroth moment map
    # properly as a channel width of 1 km/s is otherwise assumed.
    if hdr_ban is None:
        hdr_ban = ['', 'COMMENT', 'HISTORY', 'OBSRA', 'OBSDEC']

    for x in hdr:
        if x not in hdr_ban:
            prihdr[x] = hdr[x]

    if frame_start is not None:
        prihdr['CRPIX3'] = hdr['CRPIX3'] - frame_start
    if use_arcsec:
        prihdr['CUNIT1'] = 'deg'
        prihdr['CUNIT2'] = 'deg'
    hdulist.writeto(fits_name, clobber=True)
    hdulist.close()


class PV():
    """
    Class containing methods for reading and plotting PV files.
    Args:
        cube_instance = Instance of the cube class, containing all the PV info
            as instance attributes.
    """

    def __init__(self, cube_instance, PV_file_name, m_star_kep, disk_rad_kep,
                 m_star_infall, disk_rad_infall,
                 rest_vel, molecule=None, beta=6.5e4):
        self.cube = cube_instance
        self.m_star_kep = m_star_kep
        self.m_star_infall = m_star_infall
        self.disk_rad_kep = disk_rad_kep
        self.disk_rad_infall = disk_rad_infall
        self.beta = beta
        self.PV_file = PV_file_name
        self.rest_vel = rest_vel
        self.molecule = molecule

        # Extract data from PV file.
        self.data, self.hdr = load_fits_im(PV_file_name)

    def plot(self, PV_ctrl_x_px, rotation_axis_len, rad_ran_kep,
             rad_ran_infall, imfit_chan_vel, peak_offset_pix,
             peak_offset_err_pix, sec_PV=None, cube_mol='H13CN'):

        # PV-PLOT NOTES:
        # - pos_vel regime is plotted as 0-high chan nr, offset by n_chan/2
        #   while neg_vel is also 0 to high chan nr, but reversed using
        #   a reverse y-axis.

        # -------------------------- PV diagram -------------------------------

        # Create velocity profile data
        # Extract velocity and offset info for imfit plots

        # Channel offset from zero velocity. Observation cubes of L483 are not
        # symmetrical around the zero-velocity. If this channel offset is not
        # accounted for, the PV diagram will be offset instead

        # H13CN offset, read from cube
        if cube_mol == 'H13CN':
            chan_offset_from_zero_obs_vel = -4
        elif cube_mol == 'CS':
            chan_offset_from_zero_obs_vel = 0#-25
        self.n_x = int(self.hdr['NAXIS1'])
        self.n_chan = int(self.hdr['NAXIS2'])

        PV_kep_vel_profile = vel_profile(self.m_star_kep, self.disk_rad_kep)
        PV_infall_vel_profile = vel_profile(self.m_star_infall,
                                            self.disk_rad_infall)

        self.pos_to_vel_offset_pix(PV_kep_vel_profile, PV_infall_vel_profile,
                                   rad_ran_kep, rad_ran_infall, PV_ctrl_x_px)

        font_size = 12
# =============================================================================
#         # Use every 2nd pixel as tickmark in the offset dimension
#         offset_range_pix = range(self.n_x)[0::2]
# 
#         # Find x-axis units (tickmark labels)
#         offset_tick_labels = [round(self.cube.arcs_pr_px*(x - PV_ctrl_x_px), 2)
#                               for x in offset_range_pix]
#         # Velocity range tickmarks
#         vel_ticks = range(self.n_chan)[0::5]
#         assert self.n_chan == len(self.cube.vel), ('Unexpected nr of channels,'
#                                                    ' n_chan is {},'
#                                                    'self.cube.vel length is '
#                                                    '{}. Is your PV file '
#                                                    'up-to-date?'.format(
#                                                            self.n_chan,
#                                                     len(self.cube.vel)))
# 
#         vel_tick_labels = [round((self.cube.vel[i] - self.rest_vel)/1000., 2)
#                            for i in vel_ticks]
# 
#         fig = plt.figure()
#         ax = fig.add_subplot(1, 1, 1)
#         ax.set_xticks(offset_range_pix)
#         ax.set_xticklabels(offset_tick_labels)
#         ax.set_yticks(vel_ticks)
#         ax.set_yticklabels(vel_tick_labels)
# =============================================================================

        want_xtick_marks = [math.floor(x*10.0)/10.0 for x in
                            np.arange(-3.0, 3.5, 0.5)]

        want_ytick_marks = [math.floor(x*10.0)/10.0 for x in
                            np.arange(-12.0e3, 13.0e3, 1.0e3)]

        midpoint_x = self.hdr['NAXIS1']/2.0
        midpoint_y = self.hdr['NAXIS2']/2.0 + chan_offset_from_zero_obs_vel

        tick_locs_x = get_pixls_ticks_in_data_units(want_xtick_marks,
                                                    self.hdr['CDELT1'],
                                                    midpoint_x)
        # tick_locs_y = get_pixls_ticks_in_data_units(want_ytick_marks,
        #                                            self.hdr['CDELT2'],
        #                                            midpoint_y)

        tick_locs_y = get_pixl_tick_pos_in_PV_file(want_ytick_marks,
                                                   self.cube.vel,
                                                   self.rest_vel,
                                                   self.cube.chan_width_vel)
        print('tick_locs_y are now', tick_locs_y)
        xtick_lbls = [math.floor(x*10.0)/10.0 for x in
                      np.arange(-3, 3.5, 0.5)][::-1]
        ytick_lbls = [math.floor(x*10.0)/10.0 for x in
                      np.arange(-12.0, 13.0, 1.0)]

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xticks(tick_locs_x)
        ax.set_xticklabels(xtick_lbls)
        ax.set_yticks(tick_locs_y)
        ax.set_yticklabels(ytick_lbls)
        ax.tick_params(direction='in', axis='both', which='major')

# =============================================================================
#         data_for_plot = copy.deepcopy(self.data)
#         data_for_plot[data_for_plot < 0.0455*data_for_plot.max()] = np.nan
# =============================================================================

        im = ax.imshow(self.data, aspect='auto', interpolation='nearest',
                       cmap=plt.cm.YlOrBr, origin='lower')

        offset_plot_halfwidth = 50
        chan_plot_halfwidth = 35
        for_pap_plot = True
        if sec_PV is None:

            opacity = 0.7
            # Contour levels
            # Levels within 2 sigma of the max value
            levels = np.linspace(self.data.max()*0.05, self.data.max(), 6)
            plt.contour(self.data, levels, colors='white')

            if not for_pap_plot:
                # Define duplicated axes, to allow the model velocities to reflect
                # both negative and positive values. By reverting the axes we allow
                # the neg vals to expand in the correct direction.
                par_1 = ax.twinx()
                ax.plot(self.offset_pix_kep, self.kepler_vel_pxls, color='red',
                        linewidth=4, alpha=opacity)
                ax.plot(self.offset_pix_infall, self.infall_vel_pxls,
                        color='red', linestyle='--', linewidth=4,
                        alpha=opacity)
                ax.plot(self.offset_pix_kep, self.edge_vel_kep_pxls,
                        color='orange', linewidth=4, alpha=opacity)
                ax.plot(self.offset_pix_infall, self.edge_vel_infall_pxls,
                        color='orange', linewidth=4, linestyle='--',
                        alpha=opacity)

                # Negative velocities
                par_1.plot(self.offset_neg_pix_kep, self.kepler_vel_pxls_neg,
                           color='green', linewidth=4, alpha=opacity)
                par_1.plot(self.offset_neg_pix_infall,
                           self.infall_vel_pxls_neg,
                           color='green', linestyle='--', linewidth=4,
                           alpha=opacity)
                par_1.plot(self.offset_neg_pix_kep, self.edge_vel_kep_pxls_neg,
                           color='orange', linewidth=4, alpha=opacity)
                par_1.plot(self.offset_neg_pix_infall,
                           self.edge_vel_infall_pxls_neg, color='orange',
                           linewidth=4, linestyle='--', alpha=opacity)

                par_1.set_xlim(self.n_x/2.0 - offset_plot_halfwidth,
                               self.n_x/2.0 + offset_plot_halfwidth)

                if self.cube.chan_width_vel >= 0.0:
                    par_1.set_ylim(self.n_chan/2.0 + chan_offset_from_zero_obs_vel + chan_plot_halfwidth, 
                                   self.n_chan/2.0 + chan_offset_from_zero_obs_vel - chan_plot_halfwidth)
                else:
                    par_1.set_ylim(self.n_chan/2.0 - chan_offset_from_zero_obs_vel - chan_plot_halfwidth, 
                                   self.n_chan/2.0 - chan_offset_from_zero_obs_vel + chan_plot_halfwidth)
    
                # Use reverse xlim direction compared to main axis
                #par_1.set_xlim(self.n_x/4, 3*self.n_x/4)
                par_1.axes.get_xaxis().set_visible(False)
                par_1.axes.get_yaxis().set_visible(False)
    
                # Overplot imfit results
                imfit_ypix_pos = self.find_PV_ypix(imfit_chan_vel)
                peak_offset_pix_for_plot = [x + PV_ctrl_x_px for x in
                                            peak_offset_pix]
                ax.errorbar(peak_offset_pix_for_plot, imfit_ypix_pos,
                            xerr=peak_offset_err_pix, yerr=0.0, fmt='o',
                            ecolor='black', color='black', markersize=5,
                            alpha=opacity)
            file_name = 'PV_obs_{}.png'.format(self.molecule)

        else:

            # Due to offset in ypix direction from rest_vel,
            # Fix x,y axes to a certain vel range and offset in arcsec range.
            # Find [-10 km/s, 10 km/s] range.
            # Get velocity range tickmarks

            # Create placeholder
            print('Lime model PV has chan width of {}'.format(sec_PV.cube.chan_width_vel))
            print('Obs PV has chan width of {}'.format(self.cube.chan_width_vel))

            print('Lime model PV has rest_vel of {}'.format(sec_PV.cube.rest_vel))
            print('Assumed observation rest_vel is {}'.format(self.rest_vel))
            print('Shapes are', sec_PV.data.shape, self.data.shape)

            assert np.allclose(sec_PV.cube.chan_width_vel, self.cube.chan_width_vel, rtol=0.1), 'Channel velocity widths of obs and model PV file not equal.'
            if abs((self.rest_vel - sec_PV.cube.rest_vel)/sec_PV.cube.chan_width_vel) > 1.0:
                vel_offset_chans = abs(int((self.rest_vel - sec_PV.cube.rest_vel)/sec_PV.cube.chan_width_vel))
            else:
                vel_offset_chans = 0
            print('Lime model PV will be shifted by {} channels due to '
                  'different rest velocities between model and assumed '
                  'observation rest velocity.'.format(vel_offset_chans))
            if vel_offset_chans > 0:
                if self.cube.chan_width_vel >= 0.0:
#                if self.cube.chan_width_vel < 0.0:
                    print('OBS: observation cube has positive channel '
                          'velocity direction.')
                    sec_PV_data_reduced = sec_PV.data[vel_offset_chans: -1, :]
                else:
                    print('OBS: observation cube has negative channel '
                          'velocity direction.')

                    # Todo this might be an error, if the PV data is presorted in extract_data()
                    # Note: array slicing confirmed (from-and-including, to_and_excluding)
                    sec_PV_data_reduced = sec_PV.data[0:self.n_chan - vel_offset_chans, :]
                    sec_PV_data_reduced = sec_PV_data_reduced[::-1, :]
            else:
                sec_PV_data_reduced = sec_PV.data

            levels = np.linspace(np.nanmax(sec_PV_data_reduced)/10,
                                 np.nanmax(sec_PV_data_reduced), 10)
            print('Levels of lime model PV file are ', levels)
            ax.contour(sec_PV_data_reduced, levels, colors='black',
                       linewidths=0.5)
            file_name = 'H13CN_PV_obs_mod.png'

        #ax.set_xlim(3*self.n_x/4, self.n_x/4)

     #   if self.cube.chan_width_vel >= 0.0:  # Cause?
     #       ax.set_ylim(0, self.n_chan - 1)
     #   else:
     #       ax.set_ylim(self.n_chan - 1, 0)
        ax.set_xlim(self.n_x/2.0 + offset_plot_halfwidth,
                    self.n_x/2.0 - offset_plot_halfwidth)

        if self.cube.chan_width_vel >= 0.0:
            ax.set_ylim(self.n_chan/2.0 - chan_offset_from_zero_obs_vel - chan_plot_halfwidth, 
                        self.n_chan/2.0 - chan_offset_from_zero_obs_vel + chan_plot_halfwidth)
        else:
            ax.set_ylim(self.n_chan/2.0 + chan_offset_from_zero_obs_vel + chan_plot_halfwidth, 
                        self.n_chan/2.0 + chan_offset_from_zero_obs_vel - chan_plot_halfwidth)

        ax.set_xlabel('Offset ["]', fontsize=font_size)
        ax.set_ylabel(r'Velocity [km s$^{-1}$]', fontsize=font_size)

        # Colorbar
        colorbar_ax = fig.add_axes([0.9, 0.11, 0.05, 0.77])
        fig.colorbar(im, cax=colorbar_ax, label=r'Jy Beam$^{-1}$')
        colorbar_ax.tick_params(labelsize=10)

        #ax.set_ylim(475 - 75, 475 + 75)
#        plt.grid(False)
        plt.grid()

        plt.savefig(file_name, dpi=300, bbox_inches='tight')
        print('PV saved as {}'.format(file_name))

    def pos_to_vel_offset_pix(self, PV_kep_vel_profile, PV_infall_vel_profile,
                              rad_ran_kep, rad_ran_infall,
                              PV_ctrl_x_px):
        """
        Calculate the peak emission position, in terms of pixels, in a given
        channel, based on the input velocity profile.

        Args:
            vel_func = velocity function [function object]
            rad_ran = radius values in AU [array]

            PV_ctrl_x_px = central x coord of PV diagram, i.e. the offset
                           value [integer]
        """

        # ------------------------- Kepler ------------------------------------
        vels_kep = np.array([PV_kep_vel_profile._kepler(x) for x in
                             rad_ran_kep])
        vels_neg_kep = np.array([-x for x in vels_kep])

        # Make Kepler vels for imageplot.
        # Find according channel number for the kepler vel, i.e. y_pix in the
        # PV diagram. Some chan_width_vel are negativear, due to negative
        # frequency direction.

        self.kepler_vel_pxls = np.array(self.find_PV_ypix(vels_kep))

        # Reverse velocity plot orientation for negative velocities
        self.kepler_vel_pxls_neg = self.find_PV_ypix_neg_vels(vels_neg_kep)

        # ------------------------- INFALL ------------------------------------
        vels_infall = np.array([PV_infall_vel_profile._infall_ang_mom_cons(x, self.beta)
                                for x in rad_ran_infall])
        vels_neg_infall = np.array([-x for x in vels_infall])

        # Make Kepler vels for imageplot.
        # Find according channel number for the kepler vel, i.e. y_pix in the
        # PV diagram. Some chan_width_vel are negative, due to negative
        # frequency direction.

        self.infall_vel_pxls = np.array(self.find_PV_ypix(vels_infall))

        # Reverse velocity plot orientation for negative velocities
        self.infall_vel_pxls_neg = self.find_PV_ypix_neg_vels(vels_neg_infall)

        #  ------------------------ DISK EDGE VELS ----------------------------

        vels_edge_kep = np.array([PV_kep_vel_profile._proj_kepler(x) for x in
                                  rad_ran_kep])
        vels_neg_edge_kep = np.array([-x for x in vels_edge_kep])

        # Make Kepler vels for imageplot.
        # Find according channel number for the kepler vel, i.e. y_pix in the
        # PV diagram. Some chan_width_vel are negative, due to negative
        # frequency direction.

        self.edge_vel_kep_pxls = np.array(self.find_PV_ypix(vels_edge_kep))

        # Reverse velocity plot orientation for negative velocities
        self.edge_vel_kep_pxls_neg = self.find_PV_ypix_neg_vels(vels_neg_edge_kep)

        vels_edge_infall = np.array([PV_infall_vel_profile._proj_kepler(x) for
                                     x in rad_ran_infall])
        vels_neg_edge_infall = np.array([-x for x in vels_edge_infall])

        # Make Kepler vels for imageplot.
        # Find according channel number for the kepler vel, i.e. y_pix in the
        # PV diagram. Some chan_width_vel are negative, due to negative
        # frequency direction.

        self.edge_vel_infall_pxls = np.array(self.find_PV_ypix(vels_edge_infall))

        # Reverse velocity plot orientation for negative velocities
        self.edge_vel_infall_pxls_neg = self.find_PV_ypix_neg_vels(vels_neg_edge_infall)

        # --------------------- image offset in pixels (x-axis) ---------------

        self.offset_pix_kep = self.find_PV_xpix(PV_ctrl_x_px, rad_ran_kep)

        self.offset_neg_pix_kep = self.find_PV_xpix(PV_ctrl_x_px, rad_ran_kep,
                                                    pos_dir=False)

        self.offset_pix_infall = self.find_PV_xpix(PV_ctrl_x_px,
                                                   rad_ran_infall)

        self.offset_neg_pix_infall = self.find_PV_xpix(PV_ctrl_x_px,
                                                       rad_ran_infall,
                                                       pos_dir=False)

    def find_PV_ypix(self, vels):
        """
        Convert an input velocity array to y_pixel values, to be used in a
        PV diagram.
        """

        PV_ypix = [x/self.cube.chan_width_vel + self.cube.ctrl_chan for x in vels]

        return PV_ypix

    def find_PV_xpix(self, PV_ctrl_x_px, rad_ran, pos_dir=True):
        """
        Convert an input offset pixel array to x_pixel values, to be used in a
        PV diagram.
        """
        if pos_dir:
            PV_x_px = [PV_ctrl_x_px + x/(self.cube.AU_pr_px*AU_SI) for
                       x in rad_ran]
        else:
            PV_x_px = [PV_ctrl_x_px - x/(self.cube.AU_pr_px*AU_SI) for
                       x in rad_ran]

        return PV_x_px

    def find_PV_ypix_neg_vels(self, vels):

        chan_arr = np.array([-x/self.cube.chan_width_vel + self.n_chan -
                             self.cube.ctrl_chan for x in
                             vels])

        return chan_arr


def find_chan_vels(ref_chan, chan_width_vel, ref_vel, chan_list):
    """
    Find channel velocities, return array.
    """
    print('In find_chan_vels..')
    print('ref_chan is {}'.format(ref_chan))
    print('chan_width_vel is {}'.format(chan_width_vel))
    print('ref_vel is {}'.format(ref_vel))

    vels = [(chan - ref_chan)*chan_width_vel + ref_vel for chan in chan_list]
    print('First chan nr is {}'.format(chan_list[0]))
    print('First chan vel is {}'.format(vels[0]))

    print('Last chan nr is {}'.format(chan_list[-1]))
    print('Last chan vel is {}'.format(vels[-1]))

    return vels


def find_chan_freqs(ref_chan, chan_width_Hz, ref_freq, chan_list):
    """
    Find channel frequencies, return array.
    """
    freq = [(chan - ref_chan)*chan_width_Hz + ref_freq for chan in chan_list]

    return freq


def get_pixls_ticks_in_data_units(want_tick_marks, pxl_len, ctr_pix):
    """
    Find pixel positions corresponding to the input data unit positions.
    Returns:
        ticks_data_unit, the desired tick marks for the data units.
    """

    # data_arr = pixels * pxl_len  # Data values at each position.
    # Pix positions of desired plot ticks values

    pix_pos_of_want = [ctr_pix + x/abs(pxl_len) for x in
                       want_tick_marks]

    return pix_pos_of_want


def get_pixl_tick_pos_in_PV_file(want_tick_marks, vels, rest_vel,
                                 chan_width_vel):
    """
    Find pixel positions corresponding to the input data unit positions.
    Requires the velocity array of the PV file.
    Returns:
        chan_pos, the desired tick marks for the data units.
    """

    # Pix positions of desired plot ticks values
    chan_pos = []
    if chan_width_vel > 0.0:
        for x in want_tick_marks:
            min_dist = 1e10
            for i, vel in enumerate(vels):
                dist = (vel - rest_vel) - x
                if dist < 0 and abs(dist) < min_dist:
                    chan = i
                    diff = dist/chan_width_vel
                    min_dist = dist
            chan_pos.append(chan + abs(diff))
    else:
        for x in want_tick_marks:
            min_dist = 1e10
            for i, vel in enumerate(vels):
                dist = (vel - rest_vel) - x
                if dist > 0 and dist < min_dist:
                    chan = i
                    diff = dist/chan_width_vel
                    min_dist = dist
            chan_pos.append(chan + abs(diff))
        
    return chan_pos
