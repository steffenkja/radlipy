# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 14:32:57 2015

@author: Steffen_KJ
"""
from __future__ import division
from __future__ import print_function
import numpy as np

PI = 3.14159265359
AU  = 1.49597871*10.0**13.0 #[cm]
M_SUN = 1.9891*10.0**33.0 #[g]
R_SUN = 6.955*10.0**10.0 #Solar radius [cm]
G = 6.67259*10.0**(-8.0) #	[cm^3 g-1 s-2]

m_star = 2.*M_SUN
r_cell = 100.*AU
theta_center_pos = 1./4.*PI
theta_cell = 1./3.*PI

if theta_cell > PI/2.: 
    theta_cell = PI/2. - (theta_cell- PI/2.)

vel_r = (-np.sqrt(G*m_star/r_cell)*np.sqrt(1+np.cos(theta_cell)/
        np.cos(theta_center_pos)))
  
vel_theta = (np.sqrt(G*m_star/r_cell)*(np.cos(theta_center_pos)-
        np.cos(theta_cell))*np.sqrt((np.cos(theta_center_pos)+np.cos(theta_cell))/
        (np.cos(theta_center_pos)*(np.sin(theta_cell))**2.)))
        
vel_phi = (np.sqrt(G*m_star/r_cell)*np.sqrt(1- np.cos(theta_cell)/
        np.cos(theta_center_pos))*np.sin(theta_center_pos)/np.sin(theta_cell))

#The new coordinate system has other directions
#theta_s = np.pi/2. - theta_cell 
theta_s = theta_cell 

#vel_theta = -vel_theta


phi_s = 1.0
cot_t = np.cos(theta_s)/np.sin(theta_s) #TODO where was this expression from?? 



#TODO: Why is this correct? Are the coordinate systems not different?
#vel_x = (np.cos(theta_s)*np.cos(phi_s)*vel_r + cot_t*np.sin(phi_s)*vel_phi
#    + np.sin(theta_s)*np.cos(phi_s)*vel_theta)
#vel_y = (np.cos(theta_s)*np.sin(phi_s)*vel_r - cot_t*np.cos(phi_s)*vel_phi +
#    np.sin(theta_s)*np.sin(phi_s)*vel_theta)
#vel_z = np.sin(theta_s)*vel_r -np.cos(theta_s)*vel_theta

vel_x = (vel_r*np.sin(theta_s)*np.cos(phi_s) + np.cos(theta_s)*np.cos(phi_s)*vel_theta
    - np.sin(phi_s)*vel_phi)
vel_y = (vel_r*np.sin(theta_s)*np.sin(phi_s) + np.cos(theta_s)*np.sin(phi_s)*vel_theta +
    np.cos(phi_s)*vel_phi)
vel_z = vel_r*np.cos(theta_s) - np.sin(theta_s)*vel_theta

#Check v^2 = v_r**2 + v_theta**2 + v_phi**2 = GM/r

vel_sqrd = vel_r**2.+vel_theta**2.+vel_phi**2.

vel_cart_sqrd = vel_x**2. +vel_y**2.+vel_z**2.

GM_R = np.sqrt(2.*G*m_star/r_cell)

print("vel_r is %.2f" % vel_r)
print("vel_theta/r is %.20f" % vel_theta)
print("vel_phi is %.2f" % vel_phi)

print("vel_x is %.2f" % vel_x)
print("vel_y is %.2f" % vel_y)
print("vel_z is %.2f" % vel_z)

print("vel_sqrd is %.2f" % vel_sqrd)
print("GMR speed is %.2f" % GM_R)
print("Tot sph. speed is %.2f cm/s" % np.sqrt(vel_sqrd))
print("Tot cart. speed is %.2f cm/s" % np.sqrt(vel_cart_sqrd))

#cartesian positions.
x = r_cell*np.cos(theta_s)*np.cos(phi_s)
y = r_cell*np.cos(theta_s)*np.sin(phi_s)
z = r_cell*np.sin(theta_s)

r_cart = np.sqrt(x**2.+y**2.+z**2)
print("r_cart is %.2f vs %.2f" % (r_cart,r_cell))

#Alternative definition of cart. velocities. Formulae 3 in http://www.astrosurf.com/jephem/library/li110spherCart_en.htm
#vel_x_2 = (x/r_cell)*vel_r+y*(vel_phi/r_cell)+z*np.cos(phi_s)*(vel_theta/r_cell)
#vel_y_2 = (y/r_cell)*vel_r-x*vel_phi/r_cell+z*np.sin(phi_s)*vel_theta/r_cell
#vel_z_2 = (z/r_cell)*vel_r-r_cell*np.cos(theta_s)*vel_theta/r_cell

#vel_cart_2 = np.sqrt(vel_x_2**2. +vel_y_2**2.+vel_z_2**2.)
#print("Tot cart. speed w. form. 3 is %.2f cm/s" % vel_cart_2)



