# -*- coding: utf-8 -*-
"""
Created on Tue Feb  3 15:05:15 2015

@author: steffenkj
"""

import warnings

cdef extern from "Python.h":
    char* __FILE__

cdef extern from "Python.h":
    int __LINE__

def dowarn():
    warnings.warn_explicit("a warning", category=UserWarning, filename=__FILE__, lineno=__LINE__)