#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 08:47:08 2017

@author: Steffen_KJ
"""
from __future__ import division
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from cython_mod import z_x_axis_rot
# test z-x axis rotation
x_ran = np.array(range(10))
y_ran = np.array(range(10))*0.0
z_ran = np.zeros(10) + 10

x_turn = np.zeros(10)
y_turn = np.zeros(10)
z_turn = np.zeros(10)

for i in xrange(len(x_ran)):
    x_turn[i], y_turn[i], z_turn[i] = z_x_axis_rot(x_ran[i], y_ran[i],
                                                   z_ran[i], incl=0,
                                                   azith_turn=3.0*np.pi/2.0,
                                                   return_cart=True)

plt.figure()
plt.plot(x_ran, y_ran, color='blue')
plt.plot(x_turn, y_turn, color='red')
plt.show()
